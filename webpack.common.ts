const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const dotenv = require('dotenv');
dotenv.config();


module.exports = {
  target: "web",
  externals: [],
  node: {
    __dirname: false
  },
  module: {
    rules: [
      {
        test: /\.(png|jpeg|jpg|gif|svg)$/,
        use: {
          loader: "file-loader"
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env', {targets: {node: 'current'}}],
            ['@babel/preset-react', {targets: {node: 'current'}}] // add this
          ]
        }
     },
      {
        test: [/.tsx?$/, /\.ts?$/],
        use: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: [/\.js?$/, /\.ts?$/, /\.jsx?$/, /\.tsx?$/],
        enforce: 'pre',
        exclude: /node_modules/,
        use: ['source-map-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      }
    ]
  },
  mode: "development",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".css"],
    alias: {
      "config": path.resolve(__dirname, 'src/config/'),
      "constant": path.resolve(__dirname, 'src/constant/'),
      "entities": path.resolve(__dirname, 'src/entities/'),
      "helpers": path.resolve(__dirname, 'src/helpers/'),
      "layout": path.resolve(__dirname, 'src/layout/'),
      "media": path.resolve(__dirname, 'src/media/'),
      "interface": path.resolve(__dirname, 'src/interface/'),
      "store": path.resolve(__dirname, 'src/store/'),
      "components": path.resolve(__dirname, 'src/components/'),
    },
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, './src/'),
    ],
    symlinks: false,
    fallback: { "path": require.resolve("path-browserify") }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Fuda",
      inject: true,
      filename: "./index.html",
      template: "index.html",
      base: '/',
      meta: {},
      favicon: '',
      publicPath: '',
      templateParameters: {},
      hash: true
    }),
    new webpack.DefinePlugin({
       'process.env': JSON.stringify({
        REACT_APP_NODE_ENV: process.env.REACT_APP_NODE_ENV,
        REACT_APP_VERSION: '1.1.7',
        REACT_APP_PUBLIC_URL: process.env.REACT_APP_PUBLIC_URL,
        REACT_APP_AJAX_URL: process.env.REACT_APP_AJAX_URL,
        REACT_APP_AJAX_CHAT: process.env.REACT_APP_AJAX_CHAT,
        REACT_APP_API_CHATDEV: process.env.REACT_APP_API_CHATDEV,
        REACT_APP_API_REQUEST_TIMEOUT: process.env.REACT_APP_API_REQUEST_TIMEOUT
       }),
    }),
  ],
  output: {
    path: path.join(__dirname, "dist/"),
    filename: "bundled.js",
    clean: true
  }
};
