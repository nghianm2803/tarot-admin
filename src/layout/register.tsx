import { useLocation, useNavigate } from "react-router-dom";
import {
  EmptyState,
  FormLayout,
  TextField,
  Form,
  Button,
  Page,
  Layout,
  Card,
  Link,
  Stack,
  Checkbox,
  Heading,
  Subheading,
  FooterHelp,
  Toast,
  Frame,
  Banner,
} from "@shopify/polaris";
import React, { useState, useCallback, useEffect } from "react";
import LoginLogo from "../media/LoginLogo.svg";
import {
  asChoiceField,
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  notEmptyString,
  numericString,
  useChoiceField,
  useField,
  useForm,
} from "@shopify/react-form";

import helpers from "../helpers";
import { useAppDispatch, useAppSelector } from "../config/store";
import { register, logout, clearError } from "../store/user.store.reducer";

function Register() {
  /**
   * Kéo kho tổng ra...
   */
  const [toastActive, setToastActive] = useState("");
  const account = useAppSelector((state) => state.user.account);
  const isAuthenticated = useAppSelector((state) => state.user.isAuthenticated);
  const error = useAppSelector((state) => state.user.errorMessage);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  let useParam = {} as any;
  useParam = useLocation();
  let user_referrer: any = helpers.ExtractUrl(useParam.search) || null;

  /**
   * Start at first time...
   */
  useEffect(() => {
    dispatch(logout());
    dispatch(clearError());
  }, []);

  useEffect(() => {
    if (! helpers.isEmpty(account)) {
      resetForm();
      history("/login");
    }
  }, [account]);

  /**
   * Khai báo field cho form!
   */
  const useFields = {
    user_email: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthLessThan(50, "Quá dài!"),
        lengthMoreThan(6, "Quá ngắn!"),
        (inputValue) => {
          if (! helpers.isEmail(inputValue)) {
            return "Định dạng Email không hợp lệ! Vui lòng kiểm tra lại email của bạn!";
          }
          if ( helpers.isUTF8(inputValue)) {
            return "Email không nên có mã Unicode, bạn vui lòng kiểm tra!";
          }
        },
      ],
    }),

    display_name: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthLessThan(100, "Tên của bạn quá dài!"),
        lengthMoreThan(4, "Tên của bạn quá ngắn!"),
      ],
    }),

    password: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthMoreThan(6, "Mật khẩu quá ngắn!"),
        (inputValue) => {
          if ( helpers.isUTF8(inputValue)) {
            return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
          }
          if ( helpers.getPasswordStrength(inputValue) < 2) {
            return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
          }
        },
      ],
    }),

    repeatpassword: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthMoreThan(6, "Mật khẩu quá ngắn!"),
        (inputValue) => {
          if ( helpers.isUTF8(inputValue)) {
            return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
          }
        },
      ],
    }),

    user_referrer: useField<string>({
      value: user_referrer.ref || '',
      validates: [
        // notEmptyString('Trường này không được để trống.'),
      ],
    }),

    agreement: useField<boolean>({
      value: false,
      validates: [
        (inputValue) => {
          if (!inputValue) {
            return "You must agree with Term of Use";
          }
        },
      ],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: resetForm,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(form) {
      if (form.password !== form.repeatpassword) {
        setToastActive("Hai trường mật khẩu không giống nhau!");
        return { status: "success" };
      }
      dispatch(
        register({
          user_email: form.user_email,
          display_name: form.display_name,
          password: form.password,
          user_referrer: form.user_referrer,
        })
      );
      return { status: "success" };
    },
  });

  useEffect(() => {
    if (error !== "") setToastActive(error);
  }, [error]);

  const toggleToastActive = useCallback(() => {
    setToastActive("");
    dispatch(clearError());
  }, []);

  const errorBanner = toastActive ? (
    <>
      <Banner title="Opps...." status="critical" onDismiss={toggleToastActive}>
        <p>{toastActive}</p>
      </Banner>
      <br />
    </>
  ) : null;

  return (
    <Frame>
      <div id="login_page">
        <Page narrowWidth>
          <Layout>
            <Layout.AnnotatedSection>
              <Card sectioned>
                <Form onSubmit={submit}>
                  <div
                    className="Login_logo"
                    style={{ textAlign: "center", marginBottom: "50px" }}
                  >
                    <img src={LoginLogo} alt="Logo" />
                    <Heading element="h1">Welcome, </Heading>
                    <Subheading element="h3">
                      Register to continue...
                    </Subheading>
                  </div>
                  {errorBanner}
                  <FormLayout>
                    <TextField
                      type="email"
                      placeholder="YourEmail@mail.com"
                      label="Your email"
                      {...fields.user_email}
                      requiredIndicator
                      autoComplete="off"
                    />

                    <TextField
                      type="text"
                      placeholder="Your display name"
                      label="Your display name"
                      {...fields.display_name}
                      requiredIndicator
                      autoComplete="off"
                    />

                    <TextField
                      type="password"
                      label="Your password"
                      {...fields.password}
                      requiredIndicator
                      autoComplete="off"
                    />

                    <TextField
                      type="password"
                      label="Retype your password"
                      {...fields.repeatpassword}
                      requiredIndicator
                      autoComplete="off"
                    />

                    <TextField
                      type="text"
                      label="Referrer to register"
                      {...fields.user_referrer}
                      // requiredIndicator
                      autoComplete="off"
                    />

                    <Stack vertical>
                      <Checkbox
                        label="I have read and agree to the Terms of Use."
                        {...asChoiceField(fields.agreement)}
                        helpText={
                          <p>
                            {" "}
                            Please read{" "}
                            <Link external url="page/TOS">
                              Term of Service
                            </Link>{" "}
                            carefully!{" "}
                          </p>
                        }
                      />
                    </Stack>

                    <Button
                      submit
                      primary
                      fullWidth
                      disabled={!dirty}
                      onClick={submit}
                    >
                      Register
                    </Button>
                  </FormLayout>
                </Form>
              </Card>
            </Layout.AnnotatedSection>
          </Layout>
          <FooterHelp>
            Go back to <Link url="/">HOME PAGE</Link> or{" "}
            <Link url="/login">LOGIN PAGE</Link>
          </FooterHelp>
        </Page>
      </div>
    </Frame>
  );
}

export default Register;
