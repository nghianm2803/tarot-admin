import { useNavigate } from 'react-router-dom';

import { 
EmptyState,
FormLayout,
TextField,
Form,
Button,
Page,
Layout,
Card,
Link,
Stack,
Checkbox,
Heading,
Subheading,
Toast,
Frame,
Banner,
FooterHelp
} from '@shopify/polaris';
import React, { useState, useCallback, useEffect } from "react";
import LoginLogo from '../media/LoginLogo.svg';
import {
asChoiceField,
lengthLessThan,
lengthMoreThan,
notEmpty,
notEmptyString,
numericString,
useChoiceField,
useField,
useForm
} from '@shopify/react-form';
  
import helpers from '../helpers';
import { useAppDispatch, useAppSelector } from '../config/store';
import { clearError, recoverPassword } from '../store/user.store.reducer';
import { isNullishCoalesce } from 'typescript';


  function _RecoverPassword() {
    /**
     * Kéo kho tổng ra...
     */
     const dispatch = useAppDispatch();
     const history = useNavigate();
     const error = useAppSelector(state => state.user.errorMessage );
     const account = useAppSelector(state => state.user.account );
    const [notification, setNotification] = useState('');

    

     useEffect( () => {
      dispatch( clearError() );
     }, []);


     useEffect( () => {
       if ( ! helpers.isEmpty(account ) ) {
        dispatch( clearError() );
        setNotification("Successful! Please check your email!");        
       }
     }, [account]);


  /**
   * Khai báo field cho form!
   */
    const useFields = {
        user_email: useField<string>({
          value: '',
          validates: [
            notEmptyString('Trường này không được để trống.'),
            lengthLessThan(50, 'Quá dài!'),
            lengthMoreThan(6, 'Quá ngắn!'),
            inputValue => {
              if (  ! helpers.isEmail(inputValue) ) {
                return "Định dạng Email không hợp lệ! Vui lòng kiểm tra lại email của bạn!";
              }
              if (  helpers.isUTF8(inputValue) ) {
                return "Email không nên có mã Unicode, bạn vui lòng kiểm tra!";
              }
            }
          ],
        }),
    };
  
  
    const {
      fields,
      submit,
      submitting,
      dirty,
      reset: resetForm,
      submitErrors,
      makeClean,
    } = useForm({
      fields: useFields,
      async onSubmit(form) {
        dispatch( recoverPassword( {user_email: form.user_email}) );
        resetForm();
        return {status: 'success'};
      },
    });

    const toggleToastActive = useCallback(() => { dispatch(clearError()) }, []);
    const toastMarkup = error ? <Toast onDismiss={toggleToastActive} content={error} error /> : null;

    const toggleToastActive2 = useCallback(() => { 
      setNotification('');
      history('/login');
    }, []);
    const Notification =  notification ? <Toast onDismiss={toggleToastActive2} content={notification} /> : null;
  
      return(
        <Frame>
        <div id="login_page">
            {toastMarkup}
            {Notification}
          <Page narrowWidth>
            <Layout>
              <Layout.AnnotatedSection>
                <Card sectioned>
  
                    <Form onSubmit={submit}>
                      <div className="Login_logo" style={{textAlign: "center", marginBottom: '50px' }}>
                        <img src={LoginLogo} alt="Logo" />
                        <Heading element="h1">Welcome, </Heading>
                        <Subheading element="h3">Reset your password...</Subheading>
                      </div>
                    <FormLayout>
                        
                      <TextField type="email" placeholder="YourEmail@mail.com" label="Your email"
                        {...fields.user_email}
                        requiredIndicator
                        autoComplete="off"
                        helpText="Please use your email, also check junk folder to make sure you can receive our email. If you do NOT receive any email from us, check it back after 5 minutes."
                        />
                      <Button submit primary fullWidth disabled={!dirty} onClick={submit}>Send me recover password link</Button>
                      

                    </FormLayout>
                    </Form>
                   
  
                </Card>
              </Layout.AnnotatedSection>
            </Layout>
            <FooterHelp>
                Go back to <Link url="/">HOME PAGE</Link> or <Link url="/login">LOGIN PAGE</Link>
            </FooterHelp>
        </Page>
        </div>
        </Frame>
      );
    }
  
  export default _RecoverPassword;