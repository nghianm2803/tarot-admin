import {
  Frame,
  FormLayout,
  TextField,
  Form,
  Button,
  Page,
  Layout,
  Card,
  Link,
  Stack,
  Checkbox,
  Heading,
  Subheading,
  FooterHelp,
  Banner,
  Loading,
} from "@shopify/polaris";
import { useCallback, useEffect } from "react";
import LoginLogo from "../media/LoginLogo.svg";
import { useAppDispatch, useAppSelector } from "../config/store";
import { useNavigate } from "react-router-dom";
import { asChoiceField, lengthLessThan, lengthMoreThan, notEmptyString, useField, useForm } from "@shopify/react-form";
import helpers from "../helpers";
import {
  login,
  clearError,
  // createSession,
} from "../store/user.store.reducer";

export default function Login(props: any) {
  const dispatch = useAppDispatch();
  /**
   * Log out truoc !
   */
  const history = useNavigate();
  const account = useAppSelector((state) => state.user.account);
  const isAuthenticated = useAppSelector((state) => state.user.isAuthenticated);
  const error = useAppSelector((state) => state.user.errorMessage);
  const loading = useAppSelector((state) => state.user.loading);
  const session = useAppSelector((state) => state.user.session);

  useEffect(() => {
    localStorage.removeItem("session");
    localStorage.removeItem("user");
  }, []);

  /**
   * Khai báo field cho form!
   */
  const useFields = {
    user_email: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthLessThan(50, "Email của bạn quá dài!"),
        lengthMoreThan(6, "Email của bạn quá ngắn!"),
        (inputValue) => {
          if (!helpers.isEmail(inputValue)) {
            return "Định dạng Email không hợp lệ! Vui lòng kiểm tra lại email của bạn!";
          }
          if (helpers.isUTF8(inputValue)) {
            return "Email không nên có mã Unicode, bạn vui lòng kiểm tra!";
          }
        },
      ],
    }),
    password: useField<string>({
      value: "",
      validates: [
        notEmptyString("Trường này không được để trống."),
        lengthMoreThan(6, "Mật khẩu quá ngắn!"),
        (inputValue) => {
          if (helpers.isUTF8(inputValue)) {
            return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
          }
        },
      ],
    }),
    remember: useField<boolean>({
      value: false,
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    reset: resetForm,
  } = useForm({
    fields: useFields,
    async onSubmit(form) {
      let dataLogin: any = await dispatch(
        login({
          user_email: form.user_email,
          password: form.password,
          remember: form.remember,
          device_type: "website",
          device_uuid: 1,
          device_signature: "",
        })
      );
      if (dataLogin && dataLogin?.payload?.data) {
        let dataUser = dataLogin?.payload?.data;
        let sessionCode = localStorage.getItem("session");
        if (sessionCode) {
          let userData = {};
          userData = {
            ...dataUser,
            ...{
              auth_code: sessionCode,
            },
          };
          localStorage.setItem("user", JSON.stringify(userData));
          //dispatch(logout());
          dispatch(clearError());
        }
      }
      console.log(dataLogin);

      // const remoteErrors: string | any[] = []; // your API call goes here
      //if (remoteErrors.length > 0) {
      //return {status: 'fail', errors: remoteErrors};
      //}
      return { status: "success" };
    },
  });

  /**
   * Nghia modify
   */
  useEffect(() => {
    resetForm();
    /**
     * Get session and write down to browser
     */

    //  localStorage.removeItem("session");

    // let sessionCode = localStorage.getItem("session");
    // if (sessionCode) {
    //   let userData = useFields;
    //   console.log(userData)
    //   userData = {
    //     ...useFields,
    //     ...{
    //       authCode: sessionCode,
    //     },
    //   };
    //   localStorage.setItem("user", JSON.stringify(userData));
    //   //dispatch(logout());
    //   dispatch(clearError());
    // }

    // dispatch(createSession());
  }, []);

  /****************************
   * Fixed ...
   * If browser has no sesssion key, f5
   */

  /**
   * Nếu đăng nhập thành công!
   */
  useEffect(() => {
    if (isAuthenticated) {
      history("/");
      //window.location.reload();
    }
  }, [isAuthenticated]);

  const toggleBannerActive = useCallback(() => {
    dispatch(clearError());
  }, []);
  const errorBanner = error ? (
    <>
      <Banner title="Opps...." status="critical" onDismiss={toggleBannerActive}>
        <p>{error}</p>
      </Banner>
      <br />
    </>
  ) : null;

  return (
    <Frame>
      <div style={{ height: "100px" }}>{loading ? <Loading /> : null}</div>
      <div id="login_page">
        <Page narrowWidth>
          <Layout>
            <Layout.AnnotatedSection>
              <Card sectioned>
                <Form onSubmit={submit}>
                  <div className="Login_logo" style={{ textAlign: "center", marginBottom: "50px" }}>
                    <img src={LoginLogo} alt="Logo" />
                    <Heading element="h1">Welcome, </Heading>
                    <Subheading element="h3">Login to continue...</Subheading>
                  </div>
                  {errorBanner}
                  <FormLayout>
                    <TextField
                      type="text"
                      disabled={loading}
                      autoFocus
                      placeholder="youremail@example.com"
                      label="Your email"
                      {...fields.user_email}
                      requiredIndicator
                      autoComplete="on"
                    />
                    <TextField
                      type="password"
                      disabled={loading}
                      label="Your password"
                      placeholder="Your password"
                      {...fields.password}
                      requiredIndicator
                      autoComplete="off"
                    />
                    <Stack vertical>
                      <Checkbox
                        disabled={loading}
                        label="Remember me"
                        {...asChoiceField(fields.remember)}
                        helpText="Don't use this feature while using public computer!"
                      />
                    </Stack>
                    <Button submit primary fullWidth disabled={loading} onClick={submit}>
                      Login
                    </Button>
                    <p>
                      Forgot your password ? <Link url="/recover_password">Click here</Link> to recover! If you don't
                      have an account, you can <Link url="/register">register</Link> here!
                    </p>
                  </FormLayout>
                </Form>
              </Card>
            </Layout.AnnotatedSection>
          </Layout>
        </Page>

        <FooterHelp>
          Go back to <Link url="/">HOME PAGE</Link>
        </FooterHelp>
      </div>
    </Frame>
  );
}

