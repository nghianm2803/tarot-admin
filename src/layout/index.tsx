import { useCallback, useRef, useState } from "react";
import { AccessibilityMajor } from "@shopify/polaris-icons";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../config/store";
import { logout } from "../store/user.store.reducer";
//  import { setMessage,  clearMessage } from "store/toast.store.reducer";
import Menumain from "../config/menu";
import Logo_Tarot from "media/logo-tarot.png";
import { Frame, VisuallyHidden, Icon, TopBar } from "@shopify/polaris";
import { LogOutMinor, ProfileMinor } from "@shopify/polaris-icons";

export default function AppFrame({ children }) {
  /**
   * Check login ...
   */
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const skipToContentRef = useRef(null);

  // useEffect(() => {
  //   dispatch(getCurrentUser());
  // }, []);

  const account = useAppSelector((state) => state.user.account);
  const isAuthenticated = useAppSelector((state) => state.user.isAuthenticated);
  const isAuthenticating = useAppSelector((state) => state.user.isAuthenticating);
  const message = useAppSelector((state) => state.toast.message);

  /**
   * Check if login or not, not login? Redirect
   *
  useEffect( () => {
    if ( ! isAuthenticated ) {
      history.push('/login');
    }
  }, [isAuthenticated]);
*/

  const [searchActive, setSearchActive] = useState(false);
  // const [searchValue, setSearchValue] = useState("");
  const [userMenuActive, setUserMenuActive] = useState(false);
  const [mobileNavigationActive, setMobileNavigationActive] = useState(false);

  /**
   * Thay doi giao dien sang toi
   * Lưu vào Cookie để dùng sau!
   */
  const [cookies, setCookie] = useCookies(["isDarkTheme"]);
  const isDarkTheme = String(cookies.isDarkTheme) === "dark" ? true : false;
  const handleThemeChange = () => {
    setCookie("isDarkTheme", !isDarkTheme ? "dark" : "light", {
      path: "/",
      secure: true,
      sameSite: "strict",
    });
  };

  // const handleSearchResultsDismiss = useCallback(() => {
  //   setSearchActive(false);
  //   setSearchValue("");
  // }, []);
  // const handleSearchFieldChange = useCallback((value) => {
  //   setSearchValue(value);
  //   setSearchActive(value.length > 0);
  // }, []);

  const toggleUserMenuActive = useCallback(() => setUserMenuActive((_userMenuActive) => !_userMenuActive), []);
  const toggleMobileNavigationActive = useCallback(
    () => setMobileNavigationActive((_mobileNavigationActive) => !_mobileNavigationActive),
    []
  );

  const userMenuMarkup = isAuthenticated ? (
    <TopBar.UserMenu
      actions={[
        {
          items: [
            {
              content: "Thông tin của tôi",
              url: "/profile",
              icon: ProfileMinor,
            },
          ],
        },
        {
          items: [
            {
              content: "Đăng xuất",
              icon: LogOutMinor,
              onAction: () => {
                dispatch(logout());
              },
            },
          ],
        },
      ]}
      name={account?.display_name}
      detail={account?.user_email}
      initials={account?.display_name[0]}
      open={userMenuActive}
      onToggle={toggleUserMenuActive}
    />
  ) : (
    <TopBar.UserMenu
      actions={[
        {
          items: [
            { content: "Đăng nhập", url: "/login", icon: LogOutMinor },
            {
              content: "Đăng ký tài khoản",
              url: "/register",
              icon: ProfileMinor,
            },
          ],
        },
      ]}
      name="Chào mừng"
      detail="Quý khách"
      initials="U"
      open={userMenuActive}
      onToggle={toggleUserMenuActive}
    />
  );

  // const searchResultsMarkup = (
  //   <ActionList
  //     items={[
  //       { content: "Shopify help center" },
  //       { content: "Community forums" },
  //     ]}
  //   />
  // );

  // const searchFieldMarkup = (
  //   <TopBar.SearchField
  //     onChange={handleSearchFieldChange}
  //     value={searchValue}
  //     placeholder="Search"
  //   />
  // );

  /**
   * Nút bật tắt chế độ ban đêm!
   */
  const secondaryMenuMarkup = (
    <TopBar.Menu
      activatorContent={
        <span>
          <Icon source={AccessibilityMajor} />
          <VisuallyHidden>Theme mode</VisuallyHidden>
        </span>
      }
      open={false}
      actions={[]}
      onOpen={handleThemeChange}
      onClose={() => {}}
    />
  );
  const topBarMarkup = (
    <TopBar
      showNavigationToggle
      userMenu={userMenuMarkup}
      // searchResultsVisible={searchActive}
      // searchField={searchFieldMarkup}
      // searchResults={searchResultsMarkup}
      // onSearchResultsDismiss={handleSearchResultsDismiss}
      onNavigationToggle={toggleMobileNavigationActive}
      secondaryMenu={secondaryMenuMarkup}
    />
  );

  /* 
  const loadingPageMarkup = (
    <SkeletonPage>
      <Layout>
        <Layout.Section>
          <Card>
            <TextContainer>
              <SkeletonDisplayText size="medium" />
              <SkeletonBodyText lines={9} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  ); */

  const logo = {
    width: 50,
    topBarSource: isDarkTheme === true ? Logo_Tarot : Logo_Tarot,
    url: "/",
    accessibilityLabel: "Fuda",
  };

  // const Toasty = message ? <Toast content={message} onDismiss={() => dispatch(clearMessage()) } duration={4000} /> : null;

  return (
    <div style={{ display: isAuthenticating ? "none" : "block" }}>
      <Frame
        topBar={topBarMarkup}
        logo={logo}
        navigation={<Menumain />}
        showMobileNavigation={mobileNavigationActive}
        onNavigationDismiss={toggleMobileNavigationActive}
        skipToContentTarget={skipToContentRef.current}
      >
        {children}
      </Frame>
    </div>
  );
}

