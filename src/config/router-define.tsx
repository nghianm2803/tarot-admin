/**
 * Config bắt đầu từ đây!
 * Quy định route với: Private, public, separate route ...
 */
import Login from "../layout/login";
import Register from "../layout/register";
import RecoverPassword from "../layout/recover-password";
import Profile from "layout/my-profile";
import Home from "../entities/home";
import Page from "../entities/page";
import Pages from "../entities/pages";
import Users from "../entities/users";
import Media from "../entities/media";
import Notification from "../entities/notification";
import Contact from "../entities/contactform";
import BecomeAdvisor from "../entities/become_advisor";
import Dashboard from "../entities/dashboard";
import Settings from "../entities/settings";
import BackUp from "../entities/back_up";
import Restore from "../entities/restore";
import EmailSetup from "../entities/email_setup";
import EmailTemplate from "../entities/email_template";
import EmailQueue from "../entities/email_queue";
import Affiliate from "entities/affiliate";
import Livestream from "entities/livestream";
import Comment from "entities/comment";
import News from "../entities/news";
import QANDA from "../entities/qanda";
import Advisors from "../entities/advisors";
import TopAdvisors from "../entities/top_advisors";
import Transaction from "../entities/transaction";
import Invoice from "../entities/invoice";
import GeneralService from "../entities/general_service";
import Orders from "../entities/orders";
import IncomingWebhook from "entities/incoming_webhook";
import OutgoingWebhook from "entities/outgoing_webhook";
import UserRole from "entities/user_role";
import AppPackage from "entities/app_package";
import Taxonomy from "entities/taxonomy";
import Banner from "entities/banner";
import Review from "entities/review";
import Horoscope from "entities/horoscope";
import NeedHelp from "entities/need_help";
import Analytics from "entities/analytics";
import Healing from "entities/healing";
import SmsLog from "entities/sms_log";
import ChatSystem from "../entities/chat_system";

/**
 * Private, w/ <AppFrame />
 * Only authorization can access this route
 */
export const private_route = [
  {
    path: "/",
    main: Home,
  },
  {
    path: "/page/:pageslug",
    main: Page,
  },
  {
    path: "/dashboard",
    main: Dashboard,
  },
  {
    path: "/dashboard/:any",
    main: Dashboard,
  },
  {
    path: "/profile",
    main: Profile,
  },
  {
    path: "/profile/:user_id",
    main: Profile,
  },

  {
    path: "/settings",
    main: Settings,
  },
  {
    path: "/backup",
    main: BackUp,
  },
  {
    path: "/restore",
    main: Restore,
  },

  {
    path: "/email_setup",
    main: EmailSetup,
  },
  {
    path: "/email_template/:slug/:sub_slug",
    main: EmailTemplate,
  },
  {
    path: "/email_template/:slug",
    main: EmailTemplate,
  },
  {
    path: "/email_template",
    main: EmailTemplate,
  },

  {
    path: "/email_queue/:slug/:sub_slug",
    main: EmailQueue,
  },
  {
    path: "/email_queue/:slug",
    main: EmailQueue,
  },
  {
    path: "/email_queue",
    main: EmailQueue,
  },

  {
    path: "/pages",
    main: Pages,
  },
  {
    path: "/pages/:slug/:pages_slug",
    main: Pages,
  },
  {
    path: "/pages/:slug",
    main: Pages,
  },

  {
    path: "/news",
    main: News,
  },
  {
    path: "/news/:slug/:news_slug",
    main: News,
  },
  {
    path: "/news/:slug",
    main: News,
  },

  {
    path: "/qanda",
    main: QANDA,
  },
  {
    path: "/qanda/:slug/:qanda_slug",
    main: QANDA,
  },
  {
    path: "/qanda/:slug",
    main: QANDA,
  },

  {
    path: "/need_help",
    main: NeedHelp,
  },
  {
    path: "/need_help/:slug/:need_help_slug",
    main: NeedHelp,
  },
  {
    path: "/need_help/:slug",
    main: NeedHelp,
  },

  {
    path: "/media",
    main: Media,
  },
  {
    path: "/media/:slug/:media_slug",
    main: Media,
  },
  {
    path: "/media/:slug",
    main: Media,
  },

  {
    path: "/notification",
    main: Notification,
  },
  {
    path: "/notification/:slug/:notification_slug",
    main: Notification,
  },
  {
    path: "/notification/:slug",
    main: Notification,
  },

  {
    path: "/contact",
    main: Contact,
  },
  {
    path: "/contact/:slug/:contact_slug",
    main: Contact,
  },
  {
    path: "/contact/:slug",
    main: Contact,
  },
  
  {
    path: "/become_advisor",
    main: BecomeAdvisor,
  },
  {
    path: "/become_advisor/:slug/:become_advisor_slug",
    main: BecomeAdvisor,
  },
  {
    path: "/become_advisor/:slug",
    main: BecomeAdvisor,
  },

  {
    path: "/livestream",
    main: Livestream,
  },
  {
    path: "/livestream/:slug/:livestream_slug",
    main: Livestream,
  },
  {
    path: "/livestream/:slug",
    main: Livestream,
  },

  {
    path: "/comment",
    main: Comment,
  },
  {
    path: "/comment/:slug/:comment_slug",
    main: Comment,
  },
  {
    path: "/comment/:slug",
    main: Comment,
  },

  {
    path: "/users",
    main: Users,
  },
  {
    path: "/users/:slug/:users_slug",
    main: Users,
  },
  {
    path: "/users/:slug",
    main: Users,
  },

  {
    path: "/advisors",
    main: Advisors,
  },
  {
    path: "/advisors/:slug/:advisors_slug",
    main: Advisors,
  },
  {
    path: "/advisors/:slug",
    main: Advisors,
  },

  {
    path: "/top_advisors",
    main: TopAdvisors,
  },
  {
    path: "/top_advisors/:slug/:top_advisors_slug",
    main: TopAdvisors,
  },
  {
    path: "/top_advisors/:slug",
    main: TopAdvisors,
  },

  {
    path: "/user_role",
    main: UserRole,
  },
  {
    path: "/user_role/:slug/:user_role_slug",
    main: UserRole,
  },
  {
    path: "/user_role/:slug",
    main: UserRole,
  },

  {
    path: "/affiliate",
    main: Affiliate,
  },
  {
    path: "/affiliate/:slug/:affiliate_slug",
    main: Affiliate,
  },
  {
    path: "/affiliate/:slug",
    main: Affiliate,
  },

  {
    path: "/orders",
    main: Orders,
  },
  {
    path: "/orders/:slug/:orders_slug",
    main: Orders,
  },
  {
    path: "/orders/:slug",
    main: Orders,
  },

  {
    path: "/general_service",
    main: GeneralService,
  },
  {
    path: "/general_service/:slug/:general_service_slug",
    main: GeneralService,
  },
  {
    path: "/general_service/:slug",
    main: GeneralService,
  },

  {
    path: "/transaction",
    main: Transaction,
  },
  {
    path: "/transaction/:slug/:transaction_slug",
    main: Transaction,
  },
  {
    path: "/transaction/:slug",
    main: Transaction,
  },

  {
    path: "/analytics",
    main: Analytics,
  },
  {
    path: "/analytics/:slug/:analytics_slug",
    main: Analytics,
  },
  {
    path: "/analytics/:slug",
    main: Analytics,
  },

  {
    path: "/healing",
    main: Healing,
  },
  {
    path: "/healing/:slug/:healing_slug",
    main: Healing,
  },
  {
    path: "/healing/:slug",
    main: Healing,
  },

  {
    path: "/sms_log",
    main: SmsLog,
  },
  {
    path: "/sms_log/:slug/:sms_log_slug",
    main: SmsLog,
  },
  {
    path: "/sms_log/:slug",
    main: SmsLog,
  },

  {
    path: "/invoice",
    main: Invoice,
  },
  {
    path: "/invoice/:slug/:invoice_slug",
    main: Invoice,
  },
  {
    path: "/invoice/:slug",
    main: Invoice,
  },

  {
    path: "/incoming_webhook",
    main: IncomingWebhook,
  },
  {
    path: "/incoming_webhook/:slug/:incoming_webhook_slug",
    main: IncomingWebhook,
  },
  {
    path: "/incoming_webhook/:slug",
    main: IncomingWebhook,
  },

  {
    path: "/outgoing_webhook",
    main: OutgoingWebhook,
  },
  {
    path: "/outgoing_webhook/:slug/:outgoing_webhook_slug",
    main: OutgoingWebhook,
  },
  {
    path: "/outgoing_webhook/:slug",
    main: OutgoingWebhook,
  },

  {
    path: "/app_package",
    main: AppPackage,
  },
  {
    path: "/app_package/:slug/:app_package_slug",
    main: AppPackage,
  },
  {
    path: "/app_package/:slug",
    main: AppPackage,
  },

  {
    path: "/taxonomy",
    main: Taxonomy,
  },
  {
    path: "/taxonomy/:slug/:taxonomy_slug",
    main: Taxonomy,
  },
  {
    path: "/taxonomy/:slug",
    main: Taxonomy,
  },

  {
    path: "/banner",
    main: Banner,
  },
  {
    path: "/banner/:slug/:banner_slug",
    main: Banner,
  },
  {
    path: "/banner/:slug",
    main: Banner,
  },

  {
    path: "/review",
    main: Review,
  },
  {
    path: "/review/:slug/:review_slug",
    main: Review,
  },
  {
    path: "/review/:slug",
    main: Review,
  },

  {
    path: "/horoscope",
    main: Horoscope,
  },
  {
    path: "/horoscope/:slug/:horoscope_slug",
    main: Horoscope,
  },
  {
    path: "/horoscope/:slug",
    main: Horoscope,
  },
  {
    path: "/chat_system",
    main: ChatSystem,
  },
];

/**
 * Any one can access this URL
 */
export const public_route = [];

/**
 * Run first and wihout <AppFrame>,
 * It is like login page, register page
 */

export const separate_route = [
  {
    path: "/login",
    main: Login,
  },
  {
    path: "/register",
    main: Register,
  },
  {
    path: "/recover_password",
    main: RecoverPassword,
  },
];
