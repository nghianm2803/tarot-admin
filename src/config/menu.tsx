import {
  HomeMajor,
  SettingsMinor,
  BlogMajor,
  FolderMajor,
  CardReaderTapMajor,
  CustomersMajor,
  HorizontalDotsMinor,
  EmailMajor,
  OrdersMajor,
  NotificationMajor,
  SmileyJoyMajor,
  ChatMajor,
  DomainNewMajor,
  ThemesMajor,
  IncomingMajor,
  OutgoingMajor,
  PackageMajor,
  HashtagMajor,
  SlideshowMajor,
  ViewMajor,
  AnalyticsMajor,
  IqMajor,
  QuestionMarkMajor,
  HeartMajor,
} from "@shopify/polaris-icons";
import { Navigation, Page, Link } from "@shopify/polaris";
import { useAppSelector } from "../config/store";
import { useLocation } from "react-router-dom";
import { ItemProps } from "@shopify/polaris/build/ts/latest/src/components/Navigation/components";

/**
 * Public, no login
 */
const navigationItems: ItemProps[] = [
  {
    url: "/",
    label: "Trang chủ",
    icon: HomeMajor,
  },
  {
    url: "/pages",
    label: "Pages",
    icon: BlogMajor,
  },
  {
    url: "/news",
    label: "News",
    icon: DomainNewMajor,
  },

  {
    url: "/qanda",
    label: "Q&A",
    icon: QuestionMarkMajor,
  },

  {
    url: "/need_help",
    label: "Need Help",
    icon: QuestionMarkMajor,
  },

  {
    url: "/media",
    label: "Media",
    icon: FolderMajor,
  },

  {
    url: "/orders",
    label: "Orders",
    icon: OrdersMajor,
  },

  {
    url: "/review",
    label: "Review",
    icon: ViewMajor,
  },

  {
    url: "/become_advisor",
    label: "Contact Form",
    icon: EmailMajor,
    subNavigationItems: [
      {
        url: "/contact",
        label: "All form",
      },
      {
        url: "/become_advisor",
        exactMatch: true,
        label: "Become advisor",
      },
    ],
  },

  {
    url: "/comment",
    label: "Comments & Reviews",
    icon: SmileyJoyMajor,
  },

  {
    url: "/chat_system",
    label: "Chat System",
    icon: ChatMajor,
  },

  // {
  //   url: "/analytics",
  //   label: "Analytics",
  //   icon: AnalyticsMajor,
  // },

  {
    url: "/healing",
    label: "Healing",
    icon: HeartMajor,
  },

  {
    url: "/notification",
    label: "Thông báo",
    icon: NotificationMajor,
  },

  // {
  //   url: "/livestream",
  //   label: "Livestream",
  //   icon: MarketingMajor,
  // },

  // {
  //   url: "/affiliate",
  //   label: "Affiliate Program",
  //   icon: ReferralMajor,
  // },

  {
    url: "/general_service",
    label: "General Service",
    icon: ThemesMajor,
  },

  {
    url: "/transaction",
    label: "All Transaction",
    icon: CardReaderTapMajor,
  },

  {
    url: "/sms_log",
    label: "SMS Log",
    icon: EmailMajor,
  },

  // {
  //   url: "/transaction",
  //   label: "Transaction",
  //   icon: CardReaderTapMajor,
  //   subNavigationItems: [
  //     {
  //       url: "/transaction",
  //       exactMatch: true,
  //       label: "All transaction",
  //     },
  //     {
  //       url: "/invoice",
  //       label: "Invoice",
  //     },
  //   ],
  // },

  {
    url: "/incoming_webhook",
    label: "Webhook",
    icon: IncomingMajor,
    subNavigationItems: [
      {
        url: "/incoming_webhook",
        label: "Incoming Webhook",
      },
    
      {
        url: "/outgoing_webhook",
        label: "Outgoing Webhook",
      },
    
    ],
  },

  // {
  //   url: "/incoming_webhook",
  //   label: "Incoming Webhook",
  //   icon: IncomingMajor,
  // },

  // {
  //   url: "/outgoing_webhook",
  //   label: "Outgoing Webhook",
  //   icon: OutgoingMajor,
  // },

  {
    url: "/app_package",
    label: "App Package",
    icon: PackageMajor,
  },

  {
    url: "/taxonomy",
    label: "Taxonomy",
    icon: HashtagMajor,
  },

  {
    url: "/banner",
    label: "Banner",
    icon: SlideshowMajor,
  },

  {
    url: "/horoscope",
    label: "Horoscope",
    icon: IqMajor,
  },
];

/**
 * For admin
 */
const adminMenuItems = [
  {
    url: "/settings/",
    label: "Cài đặt chung",
    icon: SettingsMinor,
    subNavigationItems: [
      {
        url: "/settings",
        icon: HorizontalDotsMinor,
        label: "System",
      },
      {
        url: "/backup",
        icon: HorizontalDotsMinor,
        label: "Back up",
      },
      {
        url: "/restore",
        icon: HorizontalDotsMinor,
        label: "Restore",
      },
    ],
  },

  {
    url: "/email_setup",
    label: "Email setup",
    icon: EmailMajor,
    subNavigationItems: [
      {
        url: "/email_setup",
        icon: HorizontalDotsMinor,
        exactMatch: true,
        label: "Email setup",
      },
      {
        url: "/email_template",
        icon: HorizontalDotsMinor,
        label: "Email template",
      },
      {
        url: "/email_queue",
        icon: HorizontalDotsMinor,
        label: "Email queue",
      },
    ],
  },
  {
    url: "/users",
    label: "Users",
    icon: CustomersMajor,
    subNavigationItems: [
      {
        url: "/users",
        icon: HorizontalDotsMinor,
        exactMatch: true,
        label: "All users",
      },
      {
        url: "/advisors",
        icon: HorizontalDotsMinor,
        label: "Advisor",
      },
      {
        url: "/top_advisors",
        icon: HorizontalDotsMinor,
        label: "Top Advisor",
      },
      {
        url: "/user_role",
        icon: HorizontalDotsMinor,
        label: "User role",
      },
    ],
  },
];

/**
 * START ...
 * @returns
 */
function NavMakeUp() {
  const account = useAppSelector((state) => state.user.account);
  const isAuthenticated = useAppSelector((state) => state.user.isAuthenticated);

  const isAdmin = account.user_role === "admin";
  const location = useLocation();
  const version = process.env.REACT_APP_VERSION;
  /**
   *
   */
  /*
    useEffect( () => {
      if (isAuthenticated)
        navigations = navigationItems.concat(navigationItemsForLoginUser);
    }, [isAuthenticated]);
  */
  return (
    <Navigation location={location.pathname}>
      <Navigation.Section items={navigationItems} />

      {isAdmin ? <Navigation.Section title="Quản trị CRM" separator items={adminMenuItems} /> : null}

      <Navigation.Section fill items={[]} />

      <Page fullWidth>
        <div style={{ fontSize: "11px" }}>
          &copy; 2021 Taki.vn, ver: {version} <br />
          <div style={{ lineHeight: 1.2 }}>
            <Link url="/page/contact">Contact</Link>, <Link url="/page/tos">Term of Services</Link>,{" "}
            <Link url="/page/about-us">About us</Link>
          </div>
        </div>
      </Page>
    </Navigation>
  );
}
export default NavMakeUp;

