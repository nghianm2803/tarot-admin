/**
 * Jamviet.com
 * https://medium.com/swlh/ckeditor5-with-custom-image-uploader-on-react-67b4496cb07d
 */
import React, { Component, Fragment } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import Heading from "@ckeditor/ckeditor5-heading/src/heading";

const REACT_APP_AJAX_UPLOAD_URL = process.env.REACT_APP_AJAX_UPLOAD_URL;
const REACT_APP_AJAX_UPLOAD_IMAGE = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;

interface CKConfig {
  value: string;
  onChange: any;
}

class Custom_ckEditor extends Component<CKConfig> {
  value: any;
  onChange: any;

  constructor(props: CKConfig) {
    super(props);
    this.value = props.value;
    var _this = this;
    this.onChange = props.onChange;
  }

  render() {
    const value = this.value;
    const onChange = this.onChange;

    const custom_config = {
      extraPlugins: [MyCustomUploadAdapterPlugin],
      toolbar: {
        items: [
          "heading",
          "|",
          "bold",
          "italic",
          "link",
          "bulletedList",
          "numberedList",
          "|",
          "blockQuote",
          "insertTable",
          "|",
          "imageUpload",
          "undo",
          "redo",
        ],
      },
      heading: {
        options: [
          {
            model: "paragraph",
            title: "Paragraph",
            class: "ck-heading_paragraph",
          },
          {
            model: "heading1",
            view: "h1",
            title: "Heading 1",
            class: "ck-heading_heading1",
          },
          {
            model: "heading2",
            view: "h2",
            title: "Heading 2",
            class: "ck-heading_heading2",
          },
          {
            model: "heading3",
            view: "h3",
            title: "Heading 3",
            class: "ck-heading_heading3",
          },
        ],
      },
      table: {
        contentToolbar: ["tableColumn", "tableRow", "mergeTableCells"],
      },
    };

    return (
      <CKEditor
        editor={ClassicEditor}
        config={custom_config}
        data={value}
        onReady={(editor: any) => {
          // You can store the "editor" and use when it is needed.
        }}
        onChange={(event, editor) => {
          const data = editor.getData();
          onChange(data);
        }}
      />
    );
  }
}

function MyCustomUploadAdapterPlugin(editor) {
  editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
    return new MyUploadAdapter(loader);
  };
}

class MyUploadAdapter {
  loader: any;
  url: string;
  xhr: any;
  timeout: number;
  Authentication: string;

  constructor(props: any) {
    // CKEditor 5's FileLoader instance.
    this.loader = props;
    // URL where to send files.
    this.url = `${REACT_APP_AJAX_UPLOAD_URL}`;
    this.xhr = new XMLHttpRequest();
    this.timeout = process.env.REACT_APP_API_REQUEST_TIMEOUT
      ? Number(process.env.REACT_APP_API_REQUEST_TIMEOUT)
      : 5;
    this.Authentication = localStorage.getItem("session");
  }

  // Starts the upload process.
  upload() {
    return new Promise((resolve, reject) => {
      this._initRequest();
      this._initListeners(resolve, reject);
      this._sendRequest();
    });
  }

  // Aborts the upload process.
  abort() {
    if (this.xhr) {
      this.xhr.abort();
    }
  }

  // Example implementation using XMLHttpRequest.
  _initRequest() {
    const xhr = this.xhr;

    // xhr.timeout = this.timeout;

    xhr.open("POST", this.url, true);
    xhr.responseType = "json";
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("X-Authorization", this.Authentication);
  }

  // Initializes XMLHttpRequest listeners.
  _initListeners(resolve, reject) {
    const xhr = this.xhr;
    const loader = this.loader;
    const genericErrorText = "Couldn't upload file:" + ` ${loader.file.name}.`;

    xhr.addEventListener("error", () => reject(genericErrorText));
    xhr.addEventListener("abort", () => reject());
    xhr.addEventListener("load", () => {
      const response = xhr.response;

      if (!response || response.error) {
        return reject(
          response && response.error ? response.error.message : genericErrorText
        );
      }

      // If the upload is successful, resolve the upload promise with an object containing
      // at least the "default" URL, pointing to the image on the server.
      resolve({
        default: `${REACT_APP_AJAX_UPLOAD_IMAGE}` + response.media_filename,
      });
    });

    if (xhr.upload) {
      xhr.upload.addEventListener("progress", (evt: any) => {
        if (evt.lengthComputable) {
          loader.uploadTotal = evt.total;
          loader.uploaded = evt.loaded;
        }
      });
    }
  }

  // Prepares the data and sends the request.
  _sendRequest() {
    const data = new FormData();
    this.loader.file.then((result: any) => {
      data.append("file", result);
      this.xhr.send(data);
    });
  }
}

export default Custom_ckEditor;
