import {
  Card,
  Layout,
  SkeletonBodyText,
  SkeletonDisplayText,
  SkeletonPage,
  TextContainer,
} from "@shopify/polaris";

export default function page_markup_loading() {
  return (
    <SkeletonPage>
      <Layout>
        <Layout.Section>
          <Card>
            <TextContainer>
              <SkeletonDisplayText size="medium" />
              <SkeletonBodyText lines={9} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  );
} 
