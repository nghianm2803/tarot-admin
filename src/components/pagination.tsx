import { useEffect, useState } from "react";

interface PAGING {
  TotalRecord: number;
  onChangePage: any;
  initialPage?: number;
  pageSize?: number;
  activeCurrentPage?: number;
}

/**
 * Caculating page number ...
 */

export default function Pagination(props: PAGING) {
  const [pager, setPager] = useState({
    totalItems: 1,
    currentPage: 1,
    pageSize: 20,
    totalPages: 1,
    startPage: 1,
    endPage: 1,
    startIndex: 1,
    endIndex: 1,
    pages: [],
  });

  useEffect(() => {
    if (props.TotalRecord > 0) {
      setPage(props.activeCurrentPage || 1);
    }
  }, [props]);

  if (!pager.totalItems || props.TotalRecord <= pager.pageSize) {
    // don't display pager if there is only 1 page
    return null;
  }

  function setPage(page: any) {
    var { TotalRecord, pageSize } = props;
    if (page < 1 || page > pager.totalPages) {
      return;
    }

    // get new pager object for specified page
    let pagerx = getPager(TotalRecord, page, pageSize);

    // update state
    setPager(pagerx);
  }

  /**
   *
   * @param {*} page Page number that was clicked!
   * @returns
   */
  function _setPage(page: number) {
    var { TotalRecord, pageSize } = props;
    if (page < 1 || page > pager.totalPages) {
      return;
    }

    // get new pager object for specified page
    let pagerx = getPager(TotalRecord, page, pageSize);

    // update state
    setPager(pagerx);
    // call change page function in parent component
    if (Number(props.activeCurrentPage) === Number(page)) return;
    props.onChangePage(page);
  }

  function getPager(totalItems, currentPage, pageSize) {
    // default to first page
    currentPage = Number(currentPage) || 1;
    // default page size is 10
    pageSize = pageSize || 5;

    // calculate total pages
    var totalPages = Math.ceil(totalItems / pageSize);

    var startPage, endPage;
    if (totalPages <= 5) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 4) {
        startPage = 1;
        endPage = 6;
      } else if (currentPage + 2 >= totalPages) {
        startPage = totalPages - 5;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
      }
    }

    // calculate start and end item indexes
    var startIndex = (currentPage - 1) * pageSize;
    var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    // @ts-ignore
    var pages = [...Array(endPage + 1 - startPage).keys()].map((i) => startPage + i);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages,
    };
  }

  return (
    <ul className="pagination">
      <li className={pager.currentPage === 1 ? "disabled" : ""}>
        <a onClick={() => _setPage(1)}>First</a>
      </li>
      <li className={pager.currentPage === 1 ? "disabled" : ""}>
        <a onClick={() => _setPage(pager.currentPage - 1)}>Previous</a>
      </li>
      {pager.pages.map((page, index) => (
        <li key={index} className={pager.currentPage === page ? "active" : ""}>
          <a onClick={() => _setPage(page)}>{page}</a>
        </li>
      ))}
      <li className={pager.currentPage === pager.totalPages ? "disabled" : ""}>
        <a onClick={() => _setPage(pager.currentPage + 1)}>Next</a>
      </li>
      <li className={pager.currentPage === pager.totalPages ? "disabled" : ""}>
        <a onClick={() => _setPage(pager.totalPages)}>Last</a>
      </li>
    </ul>
  );
}