/*************
 * User permission
 */

export const USER_CAPACITY_LIST = {
  "Visitor": [
    "visitor_view_post"
  ],
  "User": [
    "user_view_post",
    "user_view_news"
  ],
  "Advisor": [
    "advisor_general_service",
    "advisor_premium_service",
    "advisor_livestream",
  ],
  "Staff": [
    "staff_write_post",
    "staff_delete_post",
    "staff_edit_post",
    "staff_view_post",
  ],
  "Admin": [
    "admin_write_post",
    "admin_delete_post",
    "admin_edit_post",
    "admin_view_post",
  ],
}