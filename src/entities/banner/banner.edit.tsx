import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/banner.store.reducer";
import {
  getEntities as getNews,
  getEntity as getNew,
} from "../../store/posts.store.reducer";
import SkeletonLoading from "components/skeleton_loading";
import {
  lengthLessThan,
  lengthMoreThan,
  useField,
  useForm,
  notEmpty,
} from "@shopify/react-form";
import QuickUploadImage from "components/oneclick-upload";
import uploadimage from "../../media/choose-file.png";
import helpers from "helpers";

export default function Edit_banner() {
  const entity = useAppSelector((state) => state.banner.entity);
  const updating = useAppSelector((state) => state.banner.updating);
  const entities = useAppSelector((state) => state.banner.entities);
  const loading = useAppSelector((state) => state.banner.loading);
  const errorMessage = useAppSelector((state) => state.banner.errorMessage);
  const updateSuccess = useAppSelector((state) => state.banner.updateSuccess);

  const entityNew = useAppSelector((state) => state.posts.entity);
  const entitiesNews = useAppSelector((state) => state.posts.entities);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  // jamviet.com
  const [message, setMessage] = useState(null);

  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState(null);
  const [internalErrorNotice, setInternalErrorNotice] = useState("");

  /**
   * Upload thumbnail
   */
  const [srcThumb, setSrcThumb] = useState(uploadimage);

  useEffect(() => {
    if (entity && entity?.banner_media) {
      if (Param) {
        setSrcThumb(entity?.banner_media);
      } else {
        setSrcThumb(uploadimage);
      }
    }
  }, [entity]);

  useEffect(() => {
    if (Param) {
      setSrcThumb(entity?.banner_media);
    }
  }, [history]);

  function handQuickUpdateSuccess(res: any) {
    let media_root = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;
    setSrcThumb(media_root + "" + res.media_filename);
  }

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  /**
   * Button value
   */

  const [bannerActive, setBannerActive] = useState(1);

  const [optionButtonValue, setOptionButtonValue] = useState(true);

  const handleRadioButtonChange = useCallback((_checked, newValue) => {
    setOptionButtonValue(newValue === "enabled");
  }, []);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.banner_slug || false;

  useEffect(() => {
    dispatch(getNews({ post_type: "news", post_status: "publish" }));
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (entity) {
      if (entity?.post_id !== null) {
        dispatch(getNew(entity?.posts?.post_id));
      }
      setBannerActive(entity?.banner_active);
      if (entity?.banner_active === 0) {
        setOptionButtonValue(false);
      } else {
        setOptionButtonValue(true);
      }
    }
  }, [entity]);

  useEffect(() => {
    if (optionButtonValue === false) {
      setBannerActive(0);
    } else {
      setBannerActive(1);
    }
  }, [optionButtonValue]);

  useEffect(() => {
    if (updateSuccess) {
      history("/banner/edit/" + entity?.banner_id, { replace: true });
      setNotification("Banner đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  /**
   * New list
   */

  const [newList, setNewList] = useState([]);

  useEffect(() => {
    const optionType = [
      {
        label: "",
        value: "0",
      },
    ];
    for (let { post_id, post_title } of entitiesNews) {
      optionType.push({
        label: String(post_title),
        value: String(post_id),
      });
    }
    setNewList(optionType);
  }, [entitiesNews]);

  /**
   * Select new
   */

  const [post_new_selected, setPost_new_selected] = useState(0);
  const handNewChange = useCallback((value) => {
    setPost_new_selected(value);
  }, []);

  useEffect(() => {
    if (entityNew) {
      if (Param) {
        setPost_new_selected(entityNew?.post_id);
      } else {
        setPost_new_selected(0);
      }
    }
  }, [entityNew]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    banner_content: useField<string>({
      value: entity?.banner_content ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Nội dung quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    banner_title: useField<string>({
      value: entity?.banner_title ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    banner_URL: useField<string>({
      value: entity?.banner_URL ? decodeURIComponent(entity?.banner_URL) : "",
      validates: [
        notEmpty("URL trống!"),
        (inputValue) => {
          if (!helpers.isUrl(inputValue)) {
            return "URL không hợp lệ!";
          }
        },
      ],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: Userreset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              banner_content: values.banner_content,
              banner_title: values.banner_title,
              banner_media: srcThumb,
              banner_active: bannerActive,
              banner_URL: values.banner_URL,
              post_id: post_new_selected,
            })
          );
        } else {
          dispatch(
            updateEntity({
              banner_id: entity.banner_id,
              banner_content: values.banner_content,
              banner_title: values.banner_title,
              banner_media: srcThumb,
              banner_active: bannerActive,
              banner_URL: values.banner_URL,
              post_id: post_new_selected,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page
      title="Banner"
      breadcrumbs={[{ content: "Banner list", url: "/banner" }]}
    >
      <EmptyState heading="Không có banner!" image={emptyIMG}>
        <p>Banner không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? (
    <Toast content={notification} onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Banner"
          breadcrumbs={[{ content: "Banner list", url: "/banner" }]}
          primaryAction={{
            content: "Lưu",
            // disabled: !dirty,
            loading: updating,
            onAction: submit,
          }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <div id="profile_heading">
                        <div className="banner_thumbnail_inner">
                          <div className="banner_thumbnail_avatar">
                            <QuickUploadImage
                              onSuccess={handQuickUpdateSuccess}
                              onError={handUploadError}
                            />
                            <img
                              src={srcThumb}
                              crossOrigin="anonymous"
                              // alt="Upload banner here"
                              style={{ width: "115px" }}
                            />
                          </div>
                        </div>
                      </div>

                      <TextField
                        autoComplete="off"
                        label="Tiêu đề Banner"
                        requiredIndicator
                        {...fields.banner_title}
                      />

                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        label="Nội dung banner"
                        requiredIndicator
                        {...fields.banner_content}
                        multiline={2}
                      />

                      <TextField
                        autoFocus
                        autoComplete="off"
                        label="Banner URL"
                        requiredIndicator
                        {...fields.banner_URL}
                      />

                      <Select
                        requiredIndicator
                        label="News"
                        options={newList}
                        onChange={handNewChange}
                        value={post_new_selected?.toString()}
                      />

                      <Stack vertical>
                        <RadioButton
                          label="Banner active off"
                          checked={optionButtonValue === false}
                          id="disabled"
                          name="accounts"
                          value="0"
                          onChange={handleRadioButtonChange}
                        />
                        <RadioButton
                          label="Banner active on"
                          id="enabled"
                          name="accounts"
                          value="1"
                          checked={optionButtonValue === true}
                          onChange={handleRadioButtonChange}
                        />
                      </Stack>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              // disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Delete entity?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
