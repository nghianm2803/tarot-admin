import {
  Card,
  DatePicker,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  Select,
  TextContainer,
  TextField,
  TextStyle,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/horoscope.store.reduce";
import {
  lengthLessThan,
  lengthMoreThan,
  useField,
  useForm,
} from "@shopify/react-form";
import QuickUploadImage from "components/oneclick-upload";
import uploadimage from "../../media/choose-file.png";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_horoscope() {
  const entity = useAppSelector((state) => state.horoscope.entity);
  const updating = useAppSelector((state) => state.horoscope.updating);
  const entities = useAppSelector((state) => state.horoscope.entities);
  const loading = useAppSelector((state) => state.horoscope.loading);
  const errorMessage = useAppSelector((state) => state.horoscope.errorMessage);
  const updateSuccess = useAppSelector(
    (state) => state.horoscope.updateSuccess
  );

  const dispatch = useAppDispatch();
  const history = useNavigate();

  // jamviet.com
  const [message, setMessage] = useState(null);

  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState(null);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.horoscope_slug || false;

  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  /**
   * Option language
   */

  const [language, setLanguage] = useState("vi");
  const handleSelectChangeLanguage = useCallback(
    (value) => setLanguage(value),
    []
  );

  const methodLanguage = [
    { label: "Vietnamese", value: "vi" },
    { label: "English", value: "en" },
    { label: "China", value: "cn" },
  ];

  /**
   * Option zodiac
   */

  const [zodiac, setZodiac] = useState("aries");
  const handleSelectChangeZodiac = useCallback((value) => setZodiac(value), []);

  const methodZodiac = [
    { label: "Aries", value: "aries" },
    { label: "Taurus", value: "taurus" },
    { label: "Gemini", value: "gemini" },
    { label: "Cancer", value: "cancer" },
    { label: "Leo", value: "leo" },
    { label: "Virgo", value: "virgo" },
    { label: "Libra", value: "libra" },
    { label: "Scorpio", value: "scorpio" },
    { label: "Sagittarius", value: "sagittarius" },
    { label: "Capricorn", value: "capricorn" },
    { label: "Aquarius", value: "aquarius" },
    { label: "Pisces", value: "pisces" },
  ];

  /**
   * Option type
   */

  const [type, setType] = useState("day");
  const handleSelectChangeType = useCallback((value) => setType(value), []);

  const methodType = [
    { label: "Day", value: "day" },
    { label: "Week", value: "week" },
    { label: "Month", value: "month" },
  ];

  useEffect(() => {
    if (entity) {
      if (Param) {
        setLanguage(entity?.language);
        setZodiac(entity?.horoscope_zodiac);
        setType(entity?.horoscope_type);
      } else {
        setLanguage("vi");
        setZodiac("aries");
        setType("day");
      }
    }
  }, [entity]);

  /* Date picker */
  function pad(num: number, size = 2) {
    let numbr = num.toString();
    while (numbr.length < size) numbr = "0" + numbr;
    return numbr;
  }

  /**
   * Format date and time
   */
  function extractTimeAndDateFromSource(source: string) {
    let DateObject = new Date();
    try {
      if (source) {
        DateObject = new Date(String(source));
      }
      const _date =
        pad(DateObject.getDate(), 2) +
        "/" +
        pad(DateObject.getMonth() + 1, 2) +
        "/" +
        DateObject.getFullYear();
      const _timestamp = DateObject.getTime();
      return {
        date: _date,
        timestamp: _timestamp,
        day: pad(DateObject.getDate(), 2),
        month: DateObject.getMonth(),
        year: DateObject.getFullYear(),
      };
    } catch (_) {
      return {
        date: "",
        timestamp: 0,
        day: 0,
        month: 0,
        year: 0,
      };
    }
  }

  const today_active = extractTimeAndDateFromSource("");
  const [{ month, year }, setDate] = useState({
    month: today_active.month,
    year: today_active.year,
  });
  const [selectedDates, setSelectedDates] = useState({
    start: new Date(),
    end: new Date(),
  });

  const handleMonthChange = useCallback(
    (_month, _year) => setDate({ month: _month, year: _year }),
    []
  );

  /**
   * upload image
   */

  const [internalErrorNotice, setInternalErrorNotice] = useState("");
  const [srcImage, setSrcImage] = useState(uploadimage);
  const [image, setImage] = useState("");

  useEffect(() => {
    if (Param) {
      if (entity && entity?.meta_image) {
        setSrcImage(entity?.meta_image);
      } else {
        setSrcImage(uploadimage);
      }
    } else {
      setSrcImage(uploadimage);
    }
  }, [entity]);

  function handQuickUpdateSuccess(res: any) {
    let media_root = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;
    setSrcImage(media_root + "" + res.media_filename);
    setImage(`${process.env.REACT_APP_AJAX_UPLOAD_IMAGE}` + res.media_filename);
  }

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    horoscope_value: useField<string>({
      value: entity?.horoscope_value ?? "",
      validates: [
        lengthLessThan(10000, "No more than 1000 characters."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Nội dung quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    seo_title: useField<string>({
      value: entity?.seo_title ?? "",
      validates: [],
    }),

    seo_description: useField<string>({
      value: entity?.seo_description ?? "",
      validates: [],
    }),

    seo_keyword: useField<string>({
      value: entity?.seo_keyword ?? "",
      validates: [],
    }),

    meta_index: useField<string>({
      value: entity?.meta_index ?? "",
      validates: [],
    }),

    meta_json: useField<string>({
      value: entity?.meta_json ?? "",
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: Userreset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        const datety = new Date(selectedDates.start).getDate();

        if (!Param) {
          // create new
          dispatch(
            createEntity({
              horoscope_value: values.horoscope_value,
              language: language,
              horoscope_zodiac: zodiac,
              horoscope_type: type,
              horoscope_date: `${year}/${pad(month + 1, 2)}/${pad(datety)}`,
              seo_title: values.seo_title,
              seo_description: values.seo_description,
              seo_keyword: values.seo_keyword,
              meta_index: values.meta_index,
              meta_json: values.meta_json,
              meta_image: image,
            })
          );
        } else {
          dispatch(
            updateEntity({
              horoscope_id: entity.horoscope_id,
              horoscope_value: values.horoscope_value,
              language: language,
              seo_title: values.seo_title,
              seo_description: values.seo_description,
              seo_keyword: values.seo_keyword,
              meta_index: values.meta_index,
              meta_json: values.meta_json,
              meta_image: image,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  useEffect(() => {
    if (updateSuccess) {
      history("/horoscope/edit/" + entity?.horoscope_id, { replace: true });
      setNotification("This page has been updated!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (Param) {
      setLanguage(entity?.language);
      setZodiac(entity?.horoscope_zodiac);
      setType(entity?.horoscope_type);
    }
  }, [history]);

  const emptyData = (
    <Page
      title="Horoscope"
      breadcrumbs={[{ content: "Horoscope list", url: "/horoscope" }]}
    >
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? (
    <Toast content={notification} onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Horoscope"
          breadcrumbs={[{ content: "Horoscope list", url: "/horoscope" }]}
          primaryAction={{
            content: "Lưu",
            // disabled: !dirty,
            loading: updating,
            onAction: submit,
          }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        label="Horoscope Content"
                        requiredIndicator
                        {...fields.horoscope_value}
                        multiline={4}
                      />
                      <Select
                        label="Horoscope Language"
                        options={methodLanguage}
                        requiredIndicator
                        onChange={handleSelectChangeLanguage}
                        value={language}
                      />
                      <Select
                        label="Horoscope Zodiac"
                        disabled={Param ? true : false}
                        options={methodZodiac}
                        requiredIndicator
                        onChange={handleSelectChangeZodiac}
                        value={zodiac}
                      />
                      <Select
                        label="Horoscope Type"
                        disabled={Param ? true : false}
                        options={methodType}
                        requiredIndicator
                        onChange={handleSelectChangeType}
                        value={type}
                      />
                      <TextStyle>Publish date</TextStyle>
                      <DatePicker
                        month={month}
                        year={year}
                        onChange={setSelectedDates}
                        onMonthChange={handleMonthChange}
                        selected={selectedDates}
                      />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
            <Layout.Section secondary>
              <Card title="SEO" sectioned>
                <FormLayout>
                  <TextField
                    autoFocus
                    autoComplete="off"
                    maxLength={250}
                    label="Seo Title"
                    {...fields.seo_title}
                  />
                  <p>Image</p>
                  <div id="profile_heading">
                    <div className="news_thumbnail_inner">
                      <div className="news_thumbnail_avatar">
                        <QuickUploadImage
                          onSuccess={handQuickUpdateSuccess}
                          onError={handUploadError}
                        />
                        <img
                          src={srcImage}
                          crossOrigin="anonymous"
                          style={{ width: "115px" }}
                        />
                      </div>
                    </div>
                  </div>
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Seo Description"
                    {...fields.seo_description}
                    multiline={2}
                  />
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Seo Keyword"
                    {...fields.seo_keyword}
                  />
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Meta Index"
                    {...fields.meta_index}
                  />
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Meta Json"
                    {...fields.meta_json}
                  />
                </FormLayout>
              </Card>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              // disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Delete entity?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading/> : Actual_page}
    </>
  );
}
