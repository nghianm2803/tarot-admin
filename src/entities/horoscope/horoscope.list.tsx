import {
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Select,
  Stack,
  Tabs,
  TextStyle,
  Toast,
  Tooltip,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/horoscope.store.reduce";
import HoroscopeFilter from "./filter";
import helpers from "helpers";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";

export default function General_Horoscope() {
  const [initial_loading, setInitial_loading] = useState<boolean>(true);
  const entity = useAppSelector((state) => state.horoscope.entity);
  const entities = useAppSelector((state) => state.horoscope.entities);
  const loading = useAppSelector((state) => state.horoscope.loading);
  const errorMessage = useAppSelector((state) => state.horoscope.errorMessage);
  const totalItems = useAppSelector((state) => state.horoscope.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    page: 1,
    limit: 20,
    horoscope_zodiac: "aries",
    horoscope_type: "day",
  });
  const [queryValue, setQueryValue] = useState("");
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery(mainQuery);
  }, []);

  /**
   * Change page number
   */

  const [numberPage, setNumberPage] = useState(1);

  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  /**
   * Filter Zodiac
   */

  const [selectedZodiac, setSelectedZodiac] = useState(0);
  const [zodiac, setZodiac] = useState("aries");

  const handleTabChangeZodiac = useCallback((selectedTabIndex) => {
    let _value = "";
    switch (selectedTabIndex) {
      case 0:
        _value = "aries";
        break;
      case 1:
        _value = "taurus";
        break;
      case 2:
        _value = "gemini";
        break;
      case 3:
        _value = "cancer";
        break;
      case 4:
        _value = "leo";
        break;
      case 5:
        _value = "virgo";
        break;
      case 6:
        _value = "libra";
        break;
      case 7:
        _value = "scorpio";
        break;
      case 8:
        _value = "sagittarius";
        break;
      case 9:
        _value = "capricorn";
        break;
      case 10:
        _value = "aquarius";
        break;
      case 11:
        _value = "pisces";
        break;
    }
    setSelectedZodiac(selectedTabIndex);
    setZodiac(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ horoscope_zodiac: `${zodiac}`, page: 1 },
    });
  }, [zodiac]);

  const tabZodiacs = [
    {
      id: "aries",
      content: (
        <Tooltip content="Mar 21 – Apr 19" dismissOnMouseOut>
          <TextStyle>Aries</TextStyle>
        </Tooltip>
      ),
      panelID: "tab1-content",
    },
    {
      id: "taurus",
      content: (
        <Tooltip content="Apr 20 – May 20" dismissOnMouseOut>
          <TextStyle>Taurus</TextStyle>
        </Tooltip>
      ),
      panelID: "tab2-content",
    },
    {
      id: "gemini",
      content: (
        <Tooltip content="May 21 – Jun 21" dismissOnMouseOut>
          <TextStyle>Gemini</TextStyle>
        </Tooltip>
      ),
      panelID: "tab3-content",
    },
    {
      id: "cancer",
      content: (
        <Tooltip content="Jun 22 – Jul 22" dismissOnMouseOut>
          <TextStyle>Cancer</TextStyle>
        </Tooltip>
      ),
      panelID: "tab4-content",
    },
    {
      id: "leo",
      content: (
        <Tooltip content="Jul 23 – Aug 22" dismissOnMouseOut>
          <TextStyle>Leo</TextStyle>
        </Tooltip>
      ),
      panelID: "tab5-content",
    },
    {
      id: "virgo",
      content: (
        <Tooltip content="Aug 23 – Sep 22" dismissOnMouseOut>
          <TextStyle>Virgo</TextStyle>
        </Tooltip>
      ),
      panelID: "tab6-content",
    },
    {
      id: "libra",
      content: (
        <Tooltip content="Sep 23 – Oct 22" dismissOnMouseOut>
          <TextStyle>Libra</TextStyle>
        </Tooltip>
      ),
      panelID: "tab7-content",
    },
    {
      id: "scorpio",
      content: (
        <Tooltip content="Oct 23 – Nov 21" dismissOnMouseOut>
          <TextStyle>Scorpio</TextStyle>
        </Tooltip>
      ),
      panelID: "tab8-content",
    },
    {
      id: "sagittarius",
      content: (
        <Tooltip content="Nov 22 – Dec 21" dismissOnMouseOut>
          <TextStyle>Sagittarius</TextStyle>
        </Tooltip>
      ),
      panelID: "tab9-content",
    },
    {
      id: "capricorn",
      content: (
        <Tooltip content="Dec 22 – Jan 19" dismissOnMouseOut>
          <TextStyle>Capricorn</TextStyle>
        </Tooltip>
      ),
      panelID: "tab10-content",
    },
    {
      id: "aquarius",
      content: (
        <Tooltip content="Jan 20 – Feb 18" dismissOnMouseOut>
          <TextStyle>Aquarius</TextStyle>
        </Tooltip>
      ),
      panelID: "tab11-content",
    },
    {
      id: "pisces",
      content: (
        <Tooltip content="Feb 19 – Mar 20" dismissOnMouseOut>
          <TextStyle>Pisces</TextStyle>
        </Tooltip>
      ),
      panelID: "tab12-content",
    },
  ];

  /**
   * Filter type
   */

  const [selectedType, setSelectedType] = useState(0);
  const [type, setType] = useState("day");

  const handleTabChangeType = useCallback((selectedTabIndex) => {
    let _value = "";
    switch (selectedTabIndex) {
      case 0:
        _value = "day";
        break;
      case 1:
        _value = "week";
        break;
      case 2:
        _value = "month";
        break;
    }
    setSelectedType(selectedTabIndex);
    setType(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ horoscope_type: `${type}`, page: 1 },
    });
  }, [type]);

  const tabTypes = [
    {
      id: "day",
      content: "Day",
      panelID: "tab1-content",
    },
    {
      id: "week",
      content: "Week",
      panelID: "tab2-content",
    },
    {
      id: "month",
      content: "Month",
      panelID: "tab3-content",
    },
  ];

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/horoscope" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery(mainQuery);
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param horoscope_id
   */
  const shortcutActions = (horoscope_id: number) => {
    history("edit/" + horoscope_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (horoscope: any) => {
    const {
      horoscope_id,
      horoscope_zodiac,
      horoscope_type,
      horoscope_date,
      horoscope_week,
      horoscope_month,
      horoscope_year,
      horoscope_value,
      slug,
      language,
      createAt,
      updateAt,
      createBy,
    } = horoscope;
    return [
      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        {horoscope_id}
      </div>,

      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        {horoscope_zodiac}
      </div>,
      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        {helpers.trimContentString(horoscope_value)}
      </div>,

      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        <time>
          {horoscope_date
            ? dateandtime.format(
                new Date(horoscope_date),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,

      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(new Date(createAt), "DD-MM-YYYY HH:mm:ss")
            : "-"}
        </time>
      </div>,
      <div
        className="clickable"
        key={horoscope_id}
        onClick={() => shortcutActions(horoscope_id)}
      >
        <time>
          {updateAt
            ? dateandtime.format(new Date(updateAt), "DD-MM-YYYY HH:mm:ss")
            : "-"}
        </time>
      </div>,
    ];
  };

  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Horoscope Zodiac",
            "Horoscope Content",
            "Horoscope Date",
            "Thời gian tạo",
            "Thời gian cập nhập",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="Horoscope"
      primaryAction={{
        content: "Tạo mới",
        disabled: false,
        onAction: () => {
          history("new");
        },
      }}
    >
      <Layout>
        <Layout.Section>
          <Tabs
            selected={selectedZodiac}
            onSelect={handleTabChangeZodiac}
            tabs={tabZodiacs}
          >
            <Card>
              <Tabs
                selected={selectedType}
                onSelect={handleTabChangeType}
                tabs={tabTypes}
                fitted
              >
                <div style={{ padding: "16px" }}>
                  <Stack alignment="center">
                    <Stack.Item fill>
                      <HoroscopeFilter
                        queryValue={StringQuery?.query}
                        onChange={handleFiltersQueryChange}
                      />
                    </Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "Horoscope all", value: "" },
                        { label: "Active", value: "active" },
                        { label: "Inactive", value: "inactive" },
                      ]}
                    />
                  </Stack>
                </div>
                {PagesList}
              </Tabs>
            </Card>
          </Tabs>

          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ?  <SkeletonLoading /> : Actual_page}
    </>
  );
}
