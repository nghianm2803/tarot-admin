import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import horoscope_list from "./horoscope.list";
import horoscope_edit from "./horoscope.edit";

/**
 *   Create index file for Horoscope
 */

export default function List_horoscope() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = horoscope_list;
      break;

    case "edit":
      ActualPage = horoscope_edit;
      break;

    case "new":
      ActualPage = horoscope_edit;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
