import { Layout, Card, Page } from "@shopify/polaris";

const Restore = (props: any) => {
  return (
    <>
      <Page title="Restore" fullWidth>
        <p>
          I have a huge passion for tech, design, and marketers so I wanted to share everything I find and uncover in an
          easy to share format! 😊
        </p>
      </Page>
    </>
  );
};

export default Restore;
