import { Card, DataTable, EmptyState, Layout, Page, Select, Stack, Toast, Badge, Tabs, Button } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { CategoriesMajor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/orders.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import OrdersFilter from "./filter";
import SkeletonLoading from "components/skeleton_loading";

export default function General() {
  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const entities = useAppSelector((state) => state.orders.entities);
  const loading = useAppSelector((state) => state.orders.loading);
  const errorMessage = useAppSelector((state) => state.orders.errorMessage);
  const totalItems = useAppSelector((state) => state.orders.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const [queryValue, setQueryValue] = useState("");

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  /**
   * Filter query
   */

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "order_id,desc",
      order_status: "",
    },
  });

  const [input, setInput] = useState("");
  const handleFiltersQueryChange = useCallback(
    (_value) => {
      setInput(_value);
    },
    [mainQuery]
  );

  useEffect(() => {
    setMainQuery({ ...mainQuery, ...{ query: input } });
  }, [input]);

  /**
   * Selected query
   */

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `${selectedParentId}`, page: 1 },
    });
  }, [selectedParentId]);

  /**
   * Tab query
   */
  const [selected, setSelected] = useState(0);
  const [status, setStatus] = useState("");

  const handleTabChange = useCallback((selectedTabIndex) => {
    let _value = "";
    switch (selectedTabIndex) {
      case 1:
        _value = "draft";
        break;
      case 2:
        _value = "cancelled";
        break;
      case 3:
        _value = "refunded";
        break;
      case 4:
        _value = "in_process";
        break;
      case 5:
        _value = "processed";
        break;
      case 6:
        _value = "paid";
        break;
      case 7:
        _value = "deleted";
        break;
      default:
        _value = "";
        break;
    }
    setSelected(selectedTabIndex);
    setStatus(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ order_status: `${status}`, page: 1 },
    });
  }, [status]);

  const tabs = [
    {
      id: "all",
      content: "All",
      panelID: "tab1-content",
    },
    {
      id: "draft",
      content: "Draft",
      panelID: "tab2-content",
    },
    {
      id: "cancelled",
      content: "Cancelled",
      panelID: "tab3-content",
    },
    {
      id: "refunded",
      content: "Refunded",
      panelID: "tab4-content",
    },
    {
      id: "in_process",
      content: "In Process",
      panelID: "tab5-content",
    },
    {
      id: "processed",
      content: "Processed",
      panelID: "tab6-content",
    },
    {
      id: "paid",
      content: "Paid",
      panelID: "tab7-content",
    },
    {
      id: "deleted",
      content: "Deleted",
      panelID: "tab8-content",
    },
  ];

  const showAll = () => {
    setMainQuery({
      query: "",
      page: 1,
      limit: 20,
      sort: "order_id,desc",
    });
    setSelectedParentId("");
    setSelected(0);
  };

  /**
   * Change page number
   */

  const [numberPage, setNumberPage] = useState(1);

  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/orders" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param order_id
   */
  const shortcutActions = (order_id: number) => {
    history("/orders/edit/" + order_id);
  };

  const emptyData = (
    <EmptyState heading="No order here!" image={emptyIMG}>
      <p>Oh! There is no order here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  const handleSort = useCallback(
    (index, direction) => {
      let _direction = direction === "descending" ? "desc" : "asc";
      let sort = "";
      sort = "createAt," + _direction;
      setMainQuery({ ...mainQuery, sort: sort });
    },
    [entities]
  );

  const renderItem = (order: any) => {
    const {
      order_id,
      order_customer_id,
      order_seller_id,
      order_service_id,
      order_price,
      order_quantity,
      order_status,
      order_note,
      createAt,
      createBy,
      updateAt,
      updateBy,
      user_orders_createByTouser,
      user_orders_order_customer_idTouser,
      user_orders_order_seller_idTouser,
      orders_meta,
      service,
    } = order;
    return [
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {order_id}
      </div>,
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {user_orders_order_seller_idTouser.display_name}
      </div>,
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {user_orders_order_customer_idTouser.display_name}
      </div>,
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {helpers.trimContentString(service.service_name)}
      </div>,
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {order_price * order_quantity}
        <svg
          data-v-2d000d8f=""
          viewBox="0 0 24 24 "
          style={{
            width: "18px",
            height: "18px",
            margin: "0px 0px -3px 2px",
            fill: "#ee9b00",
          }}
        >
          <path
            data-v-2d000d8f=""
            d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 3c-4.971 0-9 4.029-9 9s4.029 9 9 9 9-4.029 9-9-4.029-9-9-9zm1 13.947v1.053h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.385-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85v-1.067h1v1.018c.725.019 1.535.145 2.442.42l-.362 1.648c-.768-.27-1.616-.515-2.442-.465-1.489.087-1.62 1.376-.581 1.916 1.711.804 3.943 1.401 3.943 3.546.002 1.718-1.344 2.632-3 2.864z"
          ></path>
        </svg>
      </div>,

      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={order_id} onClick={() => shortcutActions(order_id)}>
        {order_status === "draft" ? (
          <Badge status="info">Nháp</Badge>
        ) : order_status === "cancelled" ? (
          <Badge status="warning">Đã huỷ</Badge>
        ) : order_status === "refunded" ? (
          <Badge status="critical">Đã hoàn tiền</Badge>
        ) : order_status === "processed" ? (
          <Badge status="attention">Đã xử lý</Badge>
        ) : order_status === "in_process" ? (
          <Badge status="success">Đang xử lý</Badge>
        ) : order_status === "paid" ? (
          <Badge status="success" progress="complete">
            Đã thanh toán
          </Badge>
        ) : order_status === "deleted" ? (
          <Badge status="critical">Đã xoá</Badge>
        ) : null}
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          sortable={[false, false, false, false, false, true, false]}
          defaultSortDirection="descending"
          initialSortColumnIndex={7}
          onSort={handleSort}
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Chuyên gia", "Khách hàng", "Dịch vụ", "Giá", "Thời gian tạo", "Trạng thái"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page title="Orders">
      <Layout>
        <Layout.Section>
          <Card>
            <Tabs selected={selected} onSelect={handleTabChange} tabs={tabs}>
              <Card.Section>
                <div style={{ padding: "16px" }}>
                  <Stack alignment="center">
                    <Stack.Item fill>
                      <OrdersFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                    </Stack.Item>
                    <Stack.Item>
                      <Select
                        label=""
                        value={selectedParentId}
                        onChange={handleSelectedChange}
                        options={[
                          { label: "All", value: "" },
                          { label: "Order customer id", value: "order_customer_id" },
                          { label: "Order seller id", value: "order_seller_id" },
                          { label: "Order service id", value: "order_service_id" },
                          { label: "Order pnr", value: "order_pnr" },
                          { label: "Create by", value: "createBy" },
                        ]}
                      />
                    </Stack.Item>
                  </Stack>
                </div>
                {PagesList}
              </Card.Section>
            </Tabs>
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

