import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  TextField,
  Toast,
  Button,
  Autocomplete,
  Icon,
  DisplayText,
  TextStyle,
  Popover,
  ButtonGroup,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import helpers from "../../helpers";
import SkeletonLoading from "components/skeleton_loading";
import {
  clearError,
  getEntity,
  updateEntity,
  createEntity,
  reset,
} from "../../store/orders.store.reducer";
import {
  createEntity as createentityTaxonomy,
  getEntities as getTaxonomys,
} from "../../store/taxonomy.order.store.reducer";
import {
  createEntity as createentityTaxonomyRelationship,
  deleteEntity as deleteTaxonomyRelationshiptags,
} from "../../store/taxonomy_relationship.order.store.reducer";
import {
  getEntities as getServices,
  getEntity as getService,
} from "store/services.store.reducer";
import {
  getEntities as getUsers,
  getEntity as getUser,
  searchEntities as searchAdvisor,
} from "store/advisor.store.reduce";
import { useForm } from "@shopify/react-form";
import { SearchMinor } from "@shopify/polaris-icons";
import OrderInformation from "./orders.information";
import OrderTimeline from "./orders.timeline";
import default_thumb from "../../media/392fce1a48cc636380d9fcc19a42f6c4.jpg";
import CreateDiscount from "./discount";
import dateandtime from "date-and-time";

/**
 * Convert Tag object to String and join with comma
 * @param tags Object
 */

function convertTagObjectToString(tags: any) {
  let TagArray = [];
  for (var p in tags) {
    if (tags.hasOwnProperty(p)) {
      TagArray.push(tags[p]);
    }
  }
  const TagArrayTaxonomy = TagArray.map((option) => {
    if (option.taxonomy) {
      return option.taxonomy.taxonomy_name;
    } else {
      return option;
    }
  });
  // return null;
  return TagArrayTaxonomy.join(",");
}

/**
 * Main function
 * @returns
 */

export default function NewEdit() {
  const entity = useAppSelector((state) => state.orders.entity);
  const updating = useAppSelector((state) => state.orders.updating);
  const loading = useAppSelector((state) => state.orders.loading);
  const errorMessage = useAppSelector((state) => state.orders.errorMessage);
  const updateSuccess = useAppSelector((state) => state.orders.updateSuccess);
  const entityTaxonomy = useAppSelector((state) => state.taxonomy_order.entity);
  const updateSuccessTaxonomy = useAppSelector(
    (state) => state.taxonomy_order.updateSuccess
  );
  const entitiesServices = useAppSelector((state) => state.services.entities);
  const entityService = useAppSelector((state) => state.services.entity);
  const entityUser = useAppSelector((state) => state.advisor.entity);
  const entitiesUsers = useAppSelector((state) => state.advisor.entities);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [notification, setNotification] = useState(null);

  /**
   * Create user
   */

  const [inputValueUsers, setInputValueUsers] = useState("");
  const [optionUsers, setOptionUsers] = useState([]);
  const [selectedOptionUsers, setSelectedOptionUsers] = useState([]);
  const [advisorID, setAdvisorID] = useState(0);

  /**
   * Create service
   */
  const [inputValueServices, setInputValueServices] = useState("");
  const [optionServices, setOptionServices] = useState([]);
  const [selectedOptionServices, setSelectedOptionServices] = useState([]);
  const [serviceId, setServiceId] = useState(0);
  const [srcImage, setSrcImage] = useState(default_thumb);
  const [minutePrice, setMinutePrice] = useState(0);

  /**
   * Create props
   */
  const [information, setInformation] = useState({
    customerID: 0,
    order_status: "",
    order_note: "",
    selectedOptionTags: [],
    deleteTag: [],
  });

  /**
   * Discount
   */
  const [uploadModelactive, setUploadModelactive] = useState(false);
  const toggleUpdateActive = useCallback(
    () => setUploadModelactive((active) => !active),
    []
  );

  /**
   * Input minute value
   */
  const [minuteValue, setMinuteValue] = useState("1");
  const [applyMinute, setApplyMinute] = useState("1");
  const handlePriceChange = useCallback((value) => {
    setMinuteValue(value);
  }, []);

  /**
   * Popover
   */
  const [closeModelPopoveractive, setCloseModelPopoveractive] = useState(false);
  const toggleCancelActivePopover = useCallback(() => {
    setCloseModelPopoveractive((active) => !active);
  }, []);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.orders_slug || false;

  useEffect(() => {
    dispatch(getUsers({ user_role: "advisor" }));
    // dispatch(getUser(advisorID));
    dispatch(getTaxonomys({ taxonomy_type: "order_tag" }));
    if (Param) {
      dispatch(getEntity(Param));
    } else {
      dispatch(reset());
    }
  }, []);

  useEffect(() => {
    dispatch(getServices({ service_active: 1, createBy: entityUser?.user_id }));
  }, [entityUser]);

  useEffect(() => {
    if (entity) {
      setServiceId(entity?.order_service_id);
      setApplyMinute(entity?.order_quantity);
      setTimeout(() => {
        setMinutePrice(entity?.order_quantity * entity?.order_price);
      }, 100);

      if (Param) {
        setAdvisorID(entity?.service?.createBy);
      } else {
        setAdvisorID(0);
      }
    } else {
      setServiceId(0);
      setTimeout(() => {
        setMinutePrice(0);
      }, 100);
    }
  }, [entity]);

  useEffect(() => {
    if (advisorID !== 0) {
      dispatch(getUser(advisorID));
    }
  }, [advisorID]);

  /**
   * Set image
   */

  useEffect(() => {
    dispatch(getService(serviceId));
    if (entityService && entityService.service_thumbnail) {
      setSrcImage(
        entityService.service_thumbnail +
          "?session_id=" +
          helpers.get_session_id()
      );
    }
  }, [serviceId]);

  useEffect(() => {
    // dispatch(getUser(entityService?.createBy));
    if (!Param) {
      if (entityService) {
        setMinutePrice(entityService?.service_price);
      } else {
        setMinutePrice(0);
      }
    }
  }, [entityService]);

  useEffect(() => {
    setMinutePrice(Number(applyMinute) * entityService?.service_price);
  }, [applyMinute]);

  /**
   * Apply minute
   */

  const [uploadModelPopoveractive, setUploadModelPopoveractive] =
    useState(false);

  const toggleUpdateActivePopover = useCallback(() => {
    setCloseModelPopoveractive((active) => !active);
    setUploadModelPopoveractive((active) => !active);
  }, []);

  useEffect(() => {
    setApplyMinute(minuteValue);
  }, [uploadModelPopoveractive]);

  /**
   * Get data user
   */

  const deselectedUsers = entitiesUsers?.map(({ user_id, display_name }) => {
    return {
      label: String(display_name),
      value: String(user_id),
    };
  });

  const updateUser = useCallback(
    (value) => {
      dispatch(searchAdvisor(value));

      setInputValueUsers(value);

      if (value === "") {
        setOptionUsers(deselectedUsers);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptionUsers = deselectedUsers.filter((option) =>
        option.label.match(filterRegex)
      );
      setOptionUsers(resultOptionUsers);
    },
    [deselectedUsers]
  );

  const updateSelectionUsers = useCallback(
    (selected) => {
      const selectedValue = selected.map((selectedItem) => {
        const matchedOption = optionUsers.find((option) => {
          return option.value.match(selectedItem);
        });
        return matchedOption && matchedOption.label;
      });
      const selectedNumber = Number(selected.pop());

      setSelectedOptionUsers(selected);
      setInputValueUsers(selectedValue[0]);
      setAdvisorID(selectedNumber);
    },
    [optionUsers]
  );

  const userField = (
    <Autocomplete.TextField
      autoComplete={null}
      onChange={updateUser}
      label=""
      value={inputValueUsers}
      prefix={<Icon source={SearchMinor} color="base" />}
      placeholder="Search customer"
    />
  );

  /**
   * User information
   */

  const UserList = (props: any) => {
    return (
      <>
        <Stack>
          <Stack.Item>
            <DisplayText>
              <div style={{ fontSize: "14px" }} key={props.id}>
                Name: {props.name}
              </div>
            </DisplayText>
            <DisplayText size="small">
              <div style={{ fontSize: "14px" }} key={props.id}>
                Email: {props.email}
              </div>
            </DisplayText>
            <DisplayText size="small">
              <div style={{ fontSize: "14px" }} key={props.id}>
                Phone number: {props.numberphone}
              </div>
            </DisplayText>
          </Stack.Item>
        </Stack>
      </>
    );
  };

  /**
   * Delete user
   */

  const handleClickAdvisor = () => {
    setAdvisorID(0);
    setInputValueUsers("");
    setServiceId(0);
    setInputValueServices("");
    setMinuteValue("1");
    setApplyMinute("1");
  };

  /**
   * Get data service
   */

  const deselectedServices = entitiesServices?.map(
    ({ service_id, service_name }) => {
      return {
        label: String(service_name),
        value: String(service_id),
      };
    }
  );

  /**
   * Create tag
   */

  useEffect(() => {
    if (updateSuccess === true) {
      setNotification("This order has been updated!");
      for (let tagItem of information?.selectedOptionTags) {
        dispatch(
          createentityTaxonomy({
            order_tag: tagItem,
          })
        );
      }
      if (!Param) {
        navigate(-1);
      }
    }
  }, [updateSuccess]);

  /**
   * Create taxonomy_relationship
   */

  useEffect(() => {
    if (entityTaxonomy) {
      dispatch(
        createentityTaxonomyRelationship({
          taxonomy_id: entityTaxonomy[0]?.taxonomy_id,
          object_id: entity?.order_id,
        })
      );
    }
  }, [entityTaxonomy]);

  useEffect(() => {
    if (updateSuccessTaxonomy === true) {
      for (let tagItem of information?.deleteTag) {
        dispatch(deleteTaxonomyRelationshiptags(tagItem));
      }
    }
  }, [updateSuccessTaxonomy]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const updateService = useCallback(
    (value) => {
      setInputValueServices(value);

      if (value === "") {
        setOptionServices(deselectedServices);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptionServices = deselectedServices.filter((option) =>
        option.label.match(filterRegex)
      );
      setOptionServices(resultOptionServices);
    },
    [deselectedServices]
  );

  const updateSelectionServices = useCallback(
    (selected) => {
      const selectedValue = selected.map((selectedItem) => {
        const matchedOption = optionServices.find((option) => {
          return option.value.match(selectedItem);
        });
        return matchedOption && matchedOption.label;
      });
      const selectedNumber = Number(selected.pop());
      setSelectedOptionServices(selected);
      setInputValueServices(selectedValue[0]);
      setServiceId(selectedNumber);
    },
    [optionServices]
  );

  const serviceField = (
    <Autocomplete.TextField
      autoComplete={null}
      onChange={updateService}
      label=""
      value={inputValueServices}
      prefix={<Icon source={SearchMinor} color="base" />}
      placeholder="Find services"
      // disabled={entityService ? true : false}
    />
  );

  /**
   * Delete service
   */

  const handleClickService = () => {
    setServiceId(0);
    setInputValueServices("");
    setMinuteValue("1");
    setApplyMinute("1");
  };

  /**
   * Format money
   */

  // const dollarUS = Intl.NumberFormat("en-US", {
  //   style: "currency",
  //   currency: "USD",
  // });

  /**
   * Coin
   */

  const coin = (
    <>
      <svg
        data-v-2d000d8f=""
        // viewBox="-5 -10 35 35"
        viewBox="0 0 24 24 "
        style={{
          width: "18px",
          height: "18px",
          margin: "0px 0px -3px 2px",
          fill: "#ee9b00",
        }}
      >
        <path
          data-v-2d000d8f=""
          d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 3c-4.971 0-9 4.029-9 9s4.029 9 9 9 9-4.029 9-9-4.029-9-9-9zm1 13.947v1.053h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.385-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85v-1.067h1v1.018c.725.019 1.535.145 2.442.42l-.362 1.648c-.768-.27-1.616-.515-2.442-.465-1.489.087-1.62 1.376-.581 1.916 1.711.804 3.943 1.401 3.943 3.546.002 1.718-1.344 2.632-3 2.864z"
        ></path>
      </svg>
    </>
  );

  /**
   * Information service
   */

  const OrderList = (props: any) => {
    return (
      <>
        <FormLayout>
          <Stack distribution="fill">
            <div style={{ marginRight: "45px" }}>
              <Stack.Item>
                <DisplayText size="small"></DisplayText>
              </Stack.Item>
              <Stack.Item>
                <Stack distribution="fill">
                  <Stack.Item>
                    <DisplayText size="small">
                      <TextStyle variation="strong">
                        <div style={{ fontSize: "14px" }}>Sản phẩm </div>
                      </TextStyle>
                    </DisplayText>
                  </Stack.Item>
                  <Stack.Item>
                    <DisplayText size="small">
                      <TextStyle variation="strong">
                        <div style={{ fontSize: "14px", paddingLeft: "30px" }}>
                          Mô tả{" "}
                        </div>
                      </TextStyle>
                    </DisplayText>
                  </Stack.Item>
                </Stack>
                <Stack distribution="fill">
                  <Stack.Item>
                    <div style={{ fontSize: "14px" }}>{props.service}</div>
                  </Stack.Item>
                  <Stack.Item>
                    <div style={{ fontSize: "14px", paddingLeft: "30px" }}>
                      {props.description}
                    </div>
                  </Stack.Item>
                </Stack>
              </Stack.Item>
            </div>
            <Stack.Item>
              <DisplayText size="small">
                <TextStyle variation="strong">
                  <div
                    style={{
                      fontSize: "14px",
                      textAlign: "right",
                      paddingLeft: "10px",
                    }}
                  >
                    Tổng
                  </div>
                </TextStyle>
              </DisplayText>
              <DisplayText size="small">
                <TextStyle variation="subdued">
                  <div style={{ fontSize: "14px", textAlign: "right" }}>
                    {!Param ? props.price || "0" : entity?.order_price}
                    {coin}
                  </div>
                </TextStyle>
              </DisplayText>
            </Stack.Item>
            {!Param ? (
              <Stack.Item>
                <Button plain onClick={handleClickService}>
                  X
                </Button>
              </Stack.Item>
            ) : null}
          </Stack>
        </FormLayout>
      </>
    );
  };

  const emptyData = (
    <EmptyState heading="Không có đơn hàng ở đây!" image={emptyIMG}>
      <p>Đơn hàng này không tồn tại!</p>
    </EmptyState>
  );
  const toastMarkup = notification ? (
    <Toast content={notification} onDismiss={toggleActive} />
  ) : null;

  /**
   * Input minute
   */

  function PopoverContentExample() {
    const activator = (
      <Button
        onClick={toggleCancelActivePopover}
        plain
        disabled={entityService && !Param ? false : true}
      >
        Phút
      </Button>
    );

    return (
      <div>
        <Popover
          active={closeModelPopoveractive}
          activator={activator}
          autofocusTarget="first-node"
          onClose={toggleCancelActivePopover}
        >
          <Popover.Pane fixed>
            <Popover.Section>
              <p>Minutes are automatically calculated.</p>
              <br />
              <TextField
                label=""
                type="number"
                maxLength={4}
                min={1}
                max="9999"
                value={minuteValue}
                onChange={handlePriceChange}
                autoComplete="off"
                // {...fields.order_quantity}
              />
            </Popover.Section>
          </Popover.Pane>
          <Popover.Pane>
            <Popover.Section>
              <ButtonGroup>
                <Button size="slim" onClick={toggleCancelActivePopover} outline>
                  Huỷ
                </Button>
                <Button size="slim" onClick={toggleUpdateActivePopover} primary>
                  Áp dụng
                </Button>
              </ButtonGroup>
            </Popover.Section>
          </Popover.Pane>
        </Popover>
      </div>
    );
  }

  const useFields = {};

  const { submit } = useForm({
    fields: useFields,
    async onSubmit() {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              order_service_id: serviceId,
              order_customer_id: information?.customerID,
              order_seller_id: entityService?.createBy,
              order_status: information?.order_status,
              order_note: information?.order_note,
              order_quantity: Number(applyMinute),
              // order_quantity: Number(minuteValue),
            })
          );
        } else {
          dispatch(
            updateEntity({
              order_id: entity?.order_id,
              // order_status: information?.order_status,
              order_note: information?.order_note,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Orders"
          titleMetadata={
            Param ? (
              <p>
                Tạo bởi {entityUser?.display_name} at{" "}
                {dateandtime.format(
                  new Date(Number(entity?.createAt)),
                  "DD-MM-YYYY HH:mm:ss"
                )}{" "}
              </p>
            ) : (
              " "
            )
          }
          breadcrumbs={[
            {
              content: "Page list",
              url: "/orders",
            },
          ]}
          // primaryAction={{
          //   content: "Lưu",
          //   // disabled: !dirty,
          //   loading: updating,
          //   onAction: submit,
          // }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card
                  title="Advisor"
                  sectioned
                  actions={[
                    {
                      content: `${entityUser && !Param ? "X" : ""}`,
                      onAction: handleClickAdvisor,
                    },
                  ]}
                >
                  <FormLayout>
                    {entityUser ? (
                      <>
                        <Stack distribution="equalSpacing">
                          <Stack.Item>
                            <UserList
                              id={entityUser?.user_id}
                              name={entityUser?.display_name || ""}
                              email={entityUser?.user_email || ""}
                              numberphone={entityUser?.user_numberphone || ""}
                            />
                          </Stack.Item>
                        </Stack>
                        <hr />
                        {entityService ? (
                          <Card.Section>
                            <OrderList
                              image={srcImage}
                              service={entityService?.service_name}
                              description={entityService?.service_description}
                              price={entityService?.service_price}
                            />
                          </Card.Section>
                        ) : (
                          <Autocomplete
                            options={optionServices}
                            selected={selectedOptionServices}
                            onSelect={updateSelectionServices}
                            textField={serviceField}
                          />
                        )}
                      </>
                    ) : (
                      <Autocomplete
                        options={optionUsers}
                        selected={selectedOptionUsers}
                        onSelect={updateSelectionUsers}
                        textField={userField}
                      />
                    )}
                  </FormLayout>
                </Card>

                <Card title="Payment">
                  <Card.Section>
                    <FormLayout>
                      <Stack distribution="fill">
                        <div style={{ marginLeft: "20px" }}>
                          <Stack.Item>
                            <DisplayText size="small"></DisplayText>
                          </Stack.Item>
                          <Stack.Item>
                            <DisplayText size="small">
                              <TextStyle>
                                <div style={{ fontSize: "14px" }}>Subtotal</div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <PopoverContentExample />
                            </DisplayText>
                            <DisplayText size="small">
                              <Button
                                plain
                                onClick={toggleUpdateActive}
                                disabled={
                                  entityService && !Param ? false : true
                                }
                              >
                                Add discount
                              </Button>
                            </DisplayText>
                            <CreateDiscount
                              showModal={uploadModelactive}
                              closeModal={toggleUpdateActive}
                            />
                            <DisplayText size="small">
                              <TextStyle variation="strong">
                                <div style={{ fontSize: "14px" }}>Total</div>
                              </TextStyle>
                            </DisplayText>
                          </Stack.Item>
                        </div>
                        <div style={{ paddingLeft: "50px" }}>
                          <Stack.Item>
                            <DisplayText size="small">
                              <TextStyle>
                                <div style={{ fontSize: "14px" }}>_</div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="subdued">
                                <div style={{ fontSize: "14px" }}>
                                  {applyMinute ? applyMinute : "_"}
                                </div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="subdued">
                                <div style={{ fontSize: "14px" }}>_</div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="strong">
                                <div style={{ fontSize: "14px" }}>_</div>
                              </TextStyle>
                            </DisplayText>
                          </Stack.Item>
                        </div>
                        <div style={{ paddingRight: "20px" }}>
                          <Stack.Item>
                            <DisplayText size="small">
                              <TextStyle>
                                <div
                                  style={{
                                    fontSize: "14px",
                                    textAlign: "right",
                                  }}
                                >
                                  {!Param
                                    ? entityService?.service_price
                                      ? entityService?.service_price
                                      : "0"
                                    : entity?.order_price}
                                  {coin}
                                </div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="subdued">
                                <div
                                  style={{
                                    fontSize: "14px",
                                    textAlign: "right",
                                  }}
                                >
                                  {minutePrice ? minutePrice : "0"}
                                  {coin}
                                </div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="subdued">
                                <div
                                  style={{
                                    fontSize: "14px",
                                    textAlign: "right",
                                  }}
                                >
                                  0 {coin}
                                </div>
                              </TextStyle>
                            </DisplayText>
                            <DisplayText size="small">
                              <TextStyle variation="strong">
                                <div
                                  style={{
                                    fontSize: "14px",
                                    textAlign: "right",
                                  }}
                                >
                                  {minutePrice ? minutePrice : "0"}
                                  {coin}
                                </div>
                              </TextStyle>
                            </DisplayText>
                          </Stack.Item>
                        </div>
                      </Stack>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
              <br />
              <OrderTimeline />
            </Layout.Section>
            <Layout.Section secondary>
              <OrderInformation changeInformation={setInformation} />
            </Layout.Section>
          </Layout>
        </Page>
      </>
    ) : (
      emptyData
    );

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
