import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import orders_list from "./orders.list";
import orders_edit from "./orders.edit";
// import users_view from './users.view';

export default function OrdersIndex() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = orders_list;
      break;

    case "edit":
      ActualPage = orders_edit;
      break;

    case "new":
      ActualPage = orders_edit;
      break;

    // case 'view':
    //     ActualPage = users_view;
    // break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
};
