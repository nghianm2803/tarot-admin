import {
  Card,
  Select,
  Stack,
  Modal,
  TextField,
  FormLayout,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";

export default function CreateDiscount({ showModal, closeModal }) {
  const updating = useAppSelector((state) => state.services.updating);

  const [uploading, setUploading] = useState(false);
  const dispatch = useAppDispatch();
  const [uploadModelactive, setUploadModelactive] = useState(false);
  const toggleUpdateActive = useCallback(() => {
    setUploadModelactive((active) => !active);
  }, [uploadModelactive]);

  /**
   * Price form
   */
  const [priceValue, setPriceValue] = useState("0.00");
  const handlePriceChange = useCallback((value) => {
    setPriceValue(
      value.toLocaleString("en-US", {
        style: "currency",
        currency: "USD",
      })
    );
  }, []);

  const [typeDurationValue, setTypeDurationValue] = useState("Amount");

  const handleTypeDurationChange = useCallback((value) => {
    setTypeDurationValue(value);
  }, []);

  useEffect(() => {
    setUploadModelactive(showModal);
    showUploadModal();
  }, [showModal]);

  function showUploadModal() {
    setUploading(false);
  }

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.advisors_slug || false;

  const [value, setValue] = useState("");

  const handleChange = useCallback((newValue) => setValue(newValue), []);

  return (
    <Modal
      open={uploadModelactive}
      onClose={() => closeModal()}
      title={
        uploading
          ? "Do NOT close this modal or refresh your browser!"
          : "Add discount"
      }
      primaryAction={{
        content: "Lưu",
        // disabled: !dirty,
        loading: updating,
        // onAction: () => {
        //   submit();
        // },
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          disabled: uploading,
          onAction: toggleUpdateActive,
        },
      ]}
    >
      <Modal.Section>
        {/* <Form onSubmit={submit}> */}
        <Card>
          <Card.Section>
            <Stack vertical spacing="extraTight">
              <FormLayout>
                <FormLayout.Group condensed>
                  <Select
                    label="Discount type"
                    value={typeDurationValue}
                    onChange={handleTypeDurationChange}
                    options={["Amount", "Percentage"]}
                  />
                  <TextField
                    label="Giá"
                    type="number"
                    min="0"
                    value={priceValue}
                    onChange={handlePriceChange}
                    prefix="$"
                    autoComplete="off"
                    // {...fields.service_price}
                  />
                </FormLayout.Group>
              </FormLayout>
            </Stack>
            <br />
            <TextField
              label="Reason"
              autoComplete="off"
              value={value}
              onChange={handleChange}
              helpText={<p>Your customers can see this reason</p>}
            />
          </Card.Section>
        </Card>
        {/* </Form> */}
      </Modal.Section>
    </Modal>
  );
}
