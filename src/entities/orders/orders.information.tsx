import {
  Card,
  FormLayout,
  Stack,
  Tag,
  Autocomplete,
  Icon,
  DisplayText,
  Select,
  TextStyle,
  Button,
  Scrollable,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import {
  getEntities as getUsers,
  getEntity as getUser,
  searchEntities as searchUsers,
} from "store/customer.store.reduce";
import { getEntities as getTags } from "store/taxonomy.order.store.reducer";
import { getEntities as getTaxonomys } from "../../store/taxonomy_relationship.order.store.reducer";
import { SearchMinor } from "@shopify/polaris-icons";
import { useField, useForm, lengthLessThan, lengthMoreThan } from "@shopify/react-form";
import date from "date-and-time";
import VideoPlayer from "react-video-js-player";
import { getEntities as getEntitiesOrderMeta } from "store/orders_meta.store.reducer";
import "./order.css";

export default function OrderInformation(props: any) {
  const entity = useAppSelector((state) => state.orders.entity);
  const entitiesUsers = useAppSelector((state) => state.customer.entities);
  const entityUser = useAppSelector((state) => state.customer.entity);
  const searchEntities = useAppSelector((state) => state.customer.searchEntities);
  const entitiesTags = useAppSelector((state) => state.taxonomy_order.entities);
  const entitiesTaxonomy = useAppSelector((state) => state.taxonomy_relationship_order.entities);
  const entitiesMeta = useAppSelector((state) => state.orders_meta.entities);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  /**
   * Create tag
   */
  const [inputValueTag, setInputValueTag] = useState("");
  const [selectedOptionTags, setSelectedOptionTags] = useState([]);
  const [optionTags, setOptionTags] = useState([]);

  /**
   * Create user
   */
  const [inputValueUsers, setInputValueUsers] = useState("");
  const [optionUsers, setOptionUsers] = useState([]);
  const [selectedOptionUsers, setSelectedOptionUsers] = useState([]);
  const [customerID, setCustomerID] = useState(0);
  const [deleteTag, setDeleteTag] = useState([]);

  /**
   * Create status
   */
  const [orderStatusSelected, setOrderStatusSelected] = useState("draft");
  const handleSelectChangeStatus = useCallback((value) => setOrderStatusSelected(value), []);
  const Status = [
    { label: "DRAFT", value: "draft" },
    { label: "CANCELLED", value: "cancelled" },
    { label: "REFUNDED", value: "refunded" },
    { label: "PROCESSED", value: "processed" },
    { label: "IN PROCESS", value: "in_process" },
    { label: "PAID", value: "paid" },
  ];

  const useFields = {
    order_note: useField<string>({
      value: entity?.order_note ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),
  };

  const { fields, reset: Userreset } = useForm({
    fields: useFields,
  });

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.orders_slug || false;

  /**
   * Create Props
   */

  const [newData, setNewData] = useState({
    customerID: customerID,
    order_status: orderStatusSelected,
    order_note: fields.order_note.value,
    selectedOptionTags: selectedOptionTags,
    deleteTag: deleteTag,
  });

  useEffect(() => {
    props.changeInformation(newData);
  }, [newData]);

  useEffect(() => {
    dispatch(getTags({ taxonomy_type: "order_tag" }));
    dispatch(getUsers({ user_role: "user" }));
    // console.log(customerID);
    // if (customerID !== 0) {
    //   dispatch(getUser(customerID));
    // }
  }, []);

  useEffect(() => {
    if (entity && entity.order_id) {
      dispatch(getEntitiesOrderMeta(entity?.order_id));
      dispatch(
        getTaxonomys({
          object_id: entity?.order_id,
          object_type: "order_tag",
        })
      );
      if (Param) {
        setCustomerID(entity?.order_customer_id);
        setOrderStatusSelected(entity?.order_status);
      } else {
        setCustomerID(0);
        setOrderStatusSelected("draft");
      }
    }
  }, [entity]);

  const [orderMeta, setOrderMeta] = useState([]);

  useEffect(() => {
    if (entitiesMeta.length > 0) {
      const result = entitiesMeta.filter((e) => e.meta_key === "order_result");
      setOrderMeta(result);
    }
  }, [entitiesMeta]);

  let orderContent = null;
  try {
    orderContent = JSON.parse(orderMeta[0]?.meta_value);
  } catch (e) {
    orderContent = String(orderContent);
  }

  useEffect(() => {
    const all_tag = [];
    for (let e of entitiesTaxonomy) {
      if (e.taxonomy.taxonomy_type == "order_tag") {
        all_tag.push(e);
      }
    }

    if (Param) {
      if (all_tag.length > 0) {
        setSelectedOptionTags(all_tag);
      } else {
        setSelectedOptionTags([]);
      }
    } else {
      setSelectedOptionTags([]);
    }
  }, [entitiesTaxonomy]);

  useEffect(() => {
    if (customerID !== 0) {
      dispatch(getUser(customerID));
    }
    setNewData({ ...newData, customerID: customerID });
  }, [customerID]);

  useEffect(() => {
    setNewData({ ...newData, order_note: fields.order_note.value });
  }, [fields.order_note]);

  useEffect(() => {
    setNewData({ ...newData, order_status: orderStatusSelected });
  }, [orderStatusSelected]);

  /**
   * Get data user
   */
  const deselectedUsers = entitiesUsers?.map(({ user_id, display_name }) => {
    return {
      label: String(display_name),
      value: String(user_id),
    };
  });

  const updateUser = useCallback(
    (value) => {
      dispatch(searchUsers(value));

      setInputValueUsers(value);

      if (value === "") {
        setOptionUsers(deselectedUsers);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptionUsers = deselectedUsers.filter((option) => option.label.match(filterRegex));
      setOptionUsers(resultOptionUsers);
    },
    [deselectedUsers]
  );

  const updateSelectionUsers = useCallback(
    (selected) => {
      const selectedValue = selected.map((selectedItem) => {
        const matchedOption = optionUsers.find((option) => {
          return option.value.match(selectedItem);
        });
        return matchedOption && matchedOption.label;
      });
      const selectedNumber = Number(selected.pop());

      setSelectedOptionUsers(selected);
      setInputValueUsers(selectedValue[0]);
      setCustomerID(selectedNumber);
    },
    [optionUsers]
  );

  const userField = (
    <Autocomplete.TextField
      autoComplete={null}
      onChange={updateUser}
      label=""
      value={inputValueUsers}
      prefix={<Icon source={SearchMinor} color="base" />}
      placeholder="Search customer"
    />
  );

  /**
   * User information
   */

  const UserList = (props: any) => {
    return (
      <>
        <Stack>
          <Stack.Item>
            <DisplayText size="small">
              <Stack>
                <Stack.Item>
                  <div style={{ fontSize: "14px" }} key={props.id}>
                    <TextStyle variation="strong">Name:</TextStyle>
                  </div>
                </Stack.Item>
                <div style={{ fontSize: "14px" }} key={props.id}>
                  <Stack.Item>{props.name}</Stack.Item>
                </div>
              </Stack>
            </DisplayText>
            <DisplayText size="small">
              <Stack>
                <Stack.Item>
                  <div style={{ fontSize: "14px" }} key={props.id}>
                    <TextStyle variation="strong">Email:</TextStyle>
                  </div>
                </Stack.Item>
                <div style={{ fontSize: "14px" }} key={props.id}>
                  <Stack.Item>{props.email}</Stack.Item>
                </div>
              </Stack>
            </DisplayText>
            <DisplayText size="small">
              <Stack>
                <Stack.Item>
                  <div style={{ fontSize: "14px" }} key={props.id}>
                    <TextStyle variation="strong">Phone number:</TextStyle>
                  </div>
                </Stack.Item>
                <div style={{ fontSize: "14px" }} key={props.id}>
                  <Stack.Item>{props.phonenumber}</Stack.Item>
                </div>
              </Stack>
            </DisplayText>
          </Stack.Item>
        </Stack>
      </>
    );
  };

  /**
   * Delete user
   */

  const handleClick = () => {
    setCustomerID(0);
    setInputValueUsers("");
  };

  /**
   * Get data tag
   */

  const deselectedTags = entitiesTags?.map(({ taxonomy_slug, taxonomy_name }) => {
    let map1 = taxonomy_slug.split("_");
    let a = [];
    for (let i of map1) {
      if (isNaN(i)) {
        a.push(i);
      } else {
        break;
      }
    }
    let label = a.join(" ");
    return {
      label: String(taxonomy_name),
      value: String(label),
    };
  });

  const updateTag = useCallback(
    (value) => {
      setInputValueTag(value);
      if (value === "") {
        setOptionTags(deselectedTags);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptions = deselectedTags.filter((option) => option.label.match(filterRegex));
      let endIndex = resultOptions.length - 1;
      if (resultOptions.length === 0) {
        endIndex = 0;
      }
      setOptionTags(resultOptions);
    },
    [deselectedTags]
  );

  /**
   * Remove tag
   */
  const removeTagTaxonomy = useCallback(
    (tag) => () => {
      let optionsTagPrepare = selectedOptionTags.filter((value, index) => {
        if (value.taxonomy_id == tag.taxonomy_id) {
          return false;
        } else {
          return true;
        }
      });
      setSelectedOptionTags(optionsTagPrepare);

      let prepareData = deleteTag;
      prepareData = [...prepareData, ...[tag.taxonomy_relationship_id]];
      setDeleteTag(prepareData);
      setNewData({
        ...newData,
        deleteTag: prepareData,
        selectedOptionTags: optionsTagPrepare,
      });
    },
    [selectedOptionTags]
  );

  // const removeTagTaxonomy = useCallback(
  //   (tag) => () => {
  //     setSelectedOptionTags((previousTags) =>
  //       previousTags.filter((previousTag) => previousTag !== tag)
  //     );

  //     let prepareData = deleteTag;
  //     prepareData = [...prepareData, ...[tag.taxonomy_relationship_id]];
  //     setDeleteTag(prepareData);
  //   },
  //   [selectedOptionTags]
  // );

  // useEffect(() => {
  //   setNewData({ ...newData, deleteTag: deleteTag });
  // }, [deleteTag]);

  const tagsMarkup = selectedOptionTags.map((option) => {
    if (option) {
      // let tagLabel = String(option || " ").replace("_", " ");

      let tagLabel = String(option || " ").split("_");
      return (
        <Tag key={`option${tagLabel[0]}`} onRemove={removeTagTaxonomy(option)} accessibilityLabel="2">
          {option.taxonomy?.taxonomy_name ? option.taxonomy?.taxonomy_name : tagLabel[0]}
        </Tag>
      );
    }
  });

  const createTag = () => {
    if (inputValueTag.length > 2) {
      const newTag = inputValueTag.split(",");

      // setSelectedOptionTags([...selectedOptionTags, ...newTag]);

      let a = [...selectedOptionTags, ...newTag];
      setSelectedOptionTags(a);
      setNewData({ ...newData, selectedOptionTags: a });

      setInputValueTag("");
    } else {
      alert("Your tag is too short");
    }
  };

  const handleChat = () => {
    history("https://fuda.appuni.io/chat/chat/14skzin8ixbxzb5x44dc");
  };

  const tagField = (
    <Autocomplete.TextField
      disabled
      autoComplete={null}
      onChange={updateTag}
      label=""
      value={inputValueTag}
      placeholder="Find or create tags"
      // connectedRight={<Button onClick={createTag}>Add Tag</Button>}
    />
  );

  // const isSamePerson(msg, prev) {
  //   if (prev === null) {
  //     return false;
  //   } else if (
  //     prev[0]?.createBy?.display_name == msg?.createBy?.display_name
  //   ) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // },

  useEffect(() => {
    setNewData({ ...newData, selectedOptionTags: selectedOptionTags });
  }, [selectedOptionTags]);

  let userContent = null;
  try {
    userContent = JSON.parse(entity?.order_note);
  } catch (e) {
    userContent = String(userContent);
  }

  return (
    <>
      <Card
        sectioned
        title="Khách hàng"
        actions={[
          {
            content: `${entityUser && !Param ? "X" : ""}`,
            onAction: handleClick,
          },
        ]}
      >
        <FormLayout>
          {entityUser ? (
            <>
              <Stack distribution="equalSpacing">
                <Stack.Item>
                  <UserList
                    id={entityUser?.user_id}
                    name={entityUser?.display_name || ""}
                    email={entityUser?.user_email || ""}
                    numberphone={entityUser?.user_numberphone || ""}
                  />
                </Stack.Item>
              </Stack>
            </>
          ) : (
            <Autocomplete
              options={optionUsers}
              selected={selectedOptionUsers}
              onSelect={updateSelectionUsers}
              textField={userField}
            />
          )}
        </FormLayout>
      </Card>
      <Card title="Trạng thái" sectioned>
        <Select disabled label={""} options={Status} onChange={handleSelectChangeStatus} value={orderStatusSelected} />
      </Card>
      <Card title="Chi tiết khách hàng" sectioned>
        <FormLayout>
          {userContent ? (
            <>
              <div>
                <p>
                  {userContent?.display_name ? (
                    <DisplayText size="small">
                      <div style={{ fontSize: "14px" }}>
                        <TextStyle variation="strong">Tên:&nbsp;</TextStyle>
                        {userContent?.display_name}
                      </div>
                    </DisplayText>
                  ) : (
                    ""
                  )}
                </p>
              </div>

              <div>
                <p>
                  {userContent?.birth_day ? (
                    <DisplayText size="small">
                      <div style={{ fontSize: "14px" }}>
                        <TextStyle variation="strong">Ngày sinh:&nbsp;</TextStyle>
                        <time>{date.format(new Date(userContent?.birth_day), "DD-MM-YYYY")}</time>
                      </div>
                    </DisplayText>
                  ) : (
                    ""
                  )}
                </p>
              </div>
              <div>
                <p>
                  {userContent?.gender ? (
                    <DisplayText size="small">
                      <div style={{ fontSize: "14px" }}>
                        <TextStyle variation="strong">Giới tính:&nbsp;</TextStyle>
                        {userContent?.gender}
                      </div>
                    </DisplayText>
                  ) : (
                    ""
                  )}
                </p>
              </div>
              <div>
                <p>
                  {userContent?.situation ? (
                    <DisplayText size="small">
                      <div style={{ fontSize: "14px" }}>
                        <TextStyle variation="strong">Situation:&nbsp;</TextStyle>
                        {userContent?.situation}
                      </div>
                    </DisplayText>
                  ) : (
                    ""
                  )}
                </p>
              </div>
              <div>
                {userContent?.video_link ? (
                  <VideoPlayer controls={true} src={userContent?.video_link} width="250px" height="115px" />
                ) : (
                  ""
                )}
              </div>
            </>
          ) : (
            <p>
              <TextStyle variation="strong">Available for video order</TextStyle>
            </p>
          )}
        </FormLayout>
      </Card>

      {/* <Card title="Tags" sectioned>
        <FormLayout>
          <div>
            <Autocomplete
              allowMultiple
              options={optionTags}
              selected={selectedOptionTags}
              textField={tagField}
              onSelect={setSelectedOptionTags}
              listTitle="Suggested Tags"
            />
            <br />
            <TextContainer>
              <Stack>{tagsMarkup}</Stack>
              <Stack>{createTag}</Stack>
            </TextContainer>
          </div>
        </FormLayout>
      </Card> */}

      <Card title="Phản hồi của chuyên gia" sectioned>
        <FormLayout>
          <div>
            {orderContent?.videoPath ? (
              <VideoPlayer controls={true} src={orderContent?.videoPath} width="250px" height="115px" />
            ) : (
              ""
            )}
          </div>

          <div>
            {entity?.service?.service_type === "chat" ? (
              <a href={"https://fuda.appuni.io/chat/chat/" + `${entity?.order_pnr}`}>Go to Room Chat</a>
            ) : (
              ""
            )}
          </div>
        </FormLayout>
      </Card>
      {/* <Card title="Chat" sectioned>
        <Scrollable shadow style={{ height: "280px" }} focusable>
          <div>
            <div
              className="user_chat_paragraph"
              style={{
                textAlign: "left",
                backgroundColor: "#f3f3f3",
                color: "#414141",
                margin: "4px 40px 8px 0px",
                maxWidth: "400px",
                borderRadius: "0px 20px 20px 20px",
                display: "inline-block",
                padding: "0.8rem",
                fontSize: "14px",
                borderBottom: "1px #efefef solid",
                fontWeight: "400",
                marginBottom: "12px",
                wordBreak: "break-word",
              }}
            >
              Text user
            </div>

            <div
              className="chat__mymessage__paragraph"
              style={{
                textAlign: "right",
                backgroundColor: "#5E0D8D",
                color: "#ffffff",
                margin: "8px 0 0 40px",
                maxWidth: "400px",
                display: "flex",
                justifyContent: "right",
                alignItems: "flex-end",
                borderRadius: "20px 20px 0px 20px",
                padding: "0.8rem",
                fontSize: "14px",
                wordBreak: "break-word",
              }}
            >
              Text advisor
            </div>
          </div>
        </Scrollable>
      </Card> */}
    </>
  );
}
