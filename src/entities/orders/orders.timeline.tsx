import { Stack, DisplayText, TextStyle, Card, Icon } from "@shopify/polaris";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../config/store";
import { useParams } from "react-router-dom";
import { getEntities as getEntitiesOrderMeta } from "store/orders_meta.store.reducer";
import { getEntity as getentityUser } from "store/customer.store.reduce";
import { getEntitybyOrderId as getentityReview } from "store/review.store.reducer";
import dateandtime from "date-and-time";
import default_avatar from "../../media/user-default.svg";
import helpers from "../../helpers";
import { StarFilledMinor } from "@shopify/polaris-icons";
import "./order.css";

export default function OrderTimeline(props: any) {
  const entity = useAppSelector((state) => state.orders.entity);
  const entities = useAppSelector((state) => state.orders_meta.entities);
  const entityUser = useAppSelector((state) => state.customer.entity);
  const entityReview = useAppSelector((state) => state.review.entity);
  const dispatch = useAppDispatch();
  const [srcImage, setSrcImage] = useState(default_avatar);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.orders_slug || false;

  useEffect(() => {
    if (entity && entity.order_id) {
      dispatch(getEntitiesOrderMeta(entity?.order_id));
      dispatch(getentityUser(entity?.order_customer_id));
      dispatch(getentityReview(entity?.order_id));
    }
  }, [entity]);

  useEffect(() => {
    // console.log("tgdfgfd");
    if (entityUser && entityUser?.user_avatar) {
      setSrcImage(
        entityUser.user_avatar + "?session_id=" + helpers.get_session_id()
      );
    }
  }, [entityUser]);

  const Timeline = () =>
    entities.length > 0 && (
      <div className="timeline-container">
        {entities.map((data, idx) => {
          return (
            <>
              {data?.meta_key !== "order_result" ? (
                <div className="timeline-item">
                  <div className="timeline-item-content">
                    <time>
                      {data.createAt
                        ? dateandtime.format(
                            new Date(Number(data.createAt)),
                            "DD-MM-YYYY HH:mm:ss"
                          )
                        : "-"}
                    </time>

                    {/* <p>{data.meta_value.split(`(#${data?.createBy})`)[1]}</p> */}
                    <p>{data?.meta_value}</p>
                    <span className="circle" />
                  </div>
                </div>
              ) : null}
            </>
          );
        })}
      </div>
    );

  const Review = (props: any) => {
    return (
      <>
        <Card title="Review" sectioned>
          <DisplayText size="small">
            <Stack>
              <Stack.Item>
                <div className="order-thumbnail">
                  <img src={srcImage} crossOrigin="anonymous" />
                </div>
              </Stack.Item>
              <Stack.Item>
                <DisplayText>
                  <TextStyle variation="strong">
                    <div style={{ fontSize: "14px" }} key={props.id}>
                      {entityUser?.display_name}
                    </div>
                  </TextStyle>
                </DisplayText>
                <div style={{ width: "15px" }}>
                  {entityReview[0]?.review_point === 1 ? (
                    <>
                      <Stack wrap={false} spacing="extraTight">
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                      </Stack>
                    </>
                  ) : entityReview[0]?.review_point === 2 ? (
                    <>
                      <Stack wrap={false} spacing="extraTight">
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                      </Stack>
                    </>
                  ) : entityReview[0]?.review_point === 3 ? (
                    <>
                      <Stack wrap={false} spacing="extraTight">
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="subdued" />
                        <Icon source={StarFilledMinor} color="subdued" />
                      </Stack>
                    </>
                  ) : entityReview[0]?.review_point === 4 ? (
                    <>
                      <Stack wrap={false} spacing="extraTight">
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="subdued" />
                      </Stack>
                    </>
                  ) : (
                    <>
                      <Stack wrap={false} spacing="extraTight">
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                        <Icon source={StarFilledMinor} color="warning" />
                      </Stack>
                    </>
                  )}
                </div>
              </Stack.Item>
            </Stack>
            <Stack.Item>
              <div style={{ fontSize: "14px" }}>
                {entityReview[0]?.review_content}
              </div>
            </Stack.Item>
          </DisplayText>
        </Card>
      </>
    );
  };

  return (
    <>
      {" "}
      <Card>{entityReview && Param ? <Review /> : " "}</Card>
      {Param ? <Timeline /> : null}
    </>
  );
}
