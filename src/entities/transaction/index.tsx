import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import transactions_list from "./transaction.list";
import transactions_edit from "./transaction.edit";
import transactions_view from "./transaction.view";

/**
 *   Create index file for Transactions
 */
export default function List_transactions() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = transactions_list;
      break;

    case "edit":
      ActualPage = transactions_edit;
      break;

    case "new":
      ActualPage = transactions_edit;
      break;

    case "view":
      ActualPage = transactions_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
