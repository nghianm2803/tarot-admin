import {
  FormLayout,
  Modal,
  TextField,
  Form,
  Card,
  Toast,
  Select,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/transactions.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";

/**
 *   Create upload Modal for Transaction
 */

// eslint-disable-next-line import/no-anonymous-default-export

export default function TransactionUpload({ onClose, show }) {
  const dispatch = useAppDispatch();

  const entity = useAppSelector((state) => state.transactions.entity);
  const updating = useAppSelector((state) => state.transactions.updating);
  const errorMessage = useAppSelector(
    (state) => state.transactions.errorMessage
  );

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  useEffect(() => {
    formReset();
  }, []);

  /**
   * Service condition hook
   */
  const [conditionSelected, setConditionSelected] = useState<string>("pending");
  const handleConditionChange = useCallback(
    (value) => setConditionSelected(value),
    []
  );

  const conditionOptions = [
    { label: "Đang chờ", value: "pending" },
    { label: "Đã xong", value: "done" },
    { label: "Đã huỷ", value: "cancel" },
  ];

  /**
   * Service method  hook
   */
  const [methodSelected, setMethodSelected] = useState<string>("plus");
  const handleMethodChange = useCallback(
    (value) => setMethodSelected(value),
    []
  );

  const methodOptions = [
    { label: "Cộng", value: "plus" },
    { label: "Trừ", value: "minus" },
  ];

  useEffect(() => {
    if (entity && entity.transaction_condition) {
      setConditionSelected(entity.transaction_condition);
      setMethodSelected(entity.transaction_method);
    }
  }, [entity]);

  const {
    fields,
    submit,
    dirty,
    reset: formReset,
    submitErrors,
  } = useForm({
    fields: {
      transaction_value: useField<string>({
        value: "",
        validates: [
          notEmpty("Giá trị giao dịch trống!"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
      transaction_ref: useField<string>({
        value: "Order: #Admin --------------  paid",
        validates: [
          lengthMoreThan(5, "Mã giao dịch có ít nhất 5 ký tự"),
        ],
      }),
      transaction_note: useField<string>({
        value: "User ID Admin paid for order #-------",
        validates: [notEmpty("Ghi chú trống!")],
      }),
      transaction_condition: useField<string>({
        value: "",
        validates: [],
      }),
      transaction_method: useField<string>({
        value: "",
        validates: [],
      }),
      object_id: useField<string>({
        value: "",
        validates: [
          notEmpty("Object ID trống!"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
    },

    async onSubmit(values) {
      try {
        // create new
        dispatch(
          createEntity({
            transaction_value: Number(values.transaction_value),
            transaction_ref: values.transaction_ref,
            transaction_note: values.transaction_note,
            transaction_condition: conditionSelected,
            transaction_method: methodSelected,
            object_id: Number(values.object_id),
          })
        );
        formReset();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ?? "Undefined error. Try again!";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo giao dịch mới"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                requiredIndicator
                type="number"
                min={1}
                label="Giá trị giao dịch"
                {...fields.transaction_value}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Số tham chiếu"
                {...fields.transaction_ref}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Ghi chú"
                {...fields.transaction_note}
              />
              <Select
                label="Condition"
                options={conditionOptions}
                onChange={handleConditionChange}
                value={conditionSelected}
              />
              <Select
                label="Phương thức gia dịch"
                options={methodOptions}
                onChange={handleMethodChange}
                value={methodSelected}
              />
              <TextField
                autoComplete="off"
                type="number"
                min={1}
                requiredIndicator
                label="Object ID"
                {...fields.object_id}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
