import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Page,
  Select,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
} from "../../store/transactions.store.reducer";
import { notEmpty, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_transaction() {
  const entity = useAppSelector((state) => state.transactions.entity);
  const updating = useAppSelector((state) => state.transactions.updating);
  const loading = useAppSelector((state) => state.transactions.loading);
  const errorMessage = useAppSelector(
    (state) => state.transactions.errorMessage
  );
  const updateSuccess = useAppSelector(
    (state) => state.transactions.updateSuccess
  );

  const dispatch = useAppDispatch();

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [notification, setNotification] = useState<string | null>(null);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  /**
   * Service condition hook
   */
  const [conditionSelected, setConditionSelected] = useState<string>("done");
  const [hideCondition, setHideCondition] = useState<boolean>(false);
  const handleConditionChange = useCallback((value) => {
    if (value === "pending") {
      setHideCondition(false);
      setConditionSelected(value);
    } else {
      setHideCondition(true);
      setConditionSelected(value);
    }
  }, []);

  const conditionOptions = [
    { label: "Đang chờ", value: "pending" },
    { label: "Đã hoàn thành", value: "done" },
    { label: "Đã huỷ", value: "cancel" },
  ];

  useEffect(() => {
    if (entity && entity.transaction_condition) {
      setConditionSelected(entity.transaction_condition);
    }
  }, [entity]);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.transaction_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else formReset();
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Giao dịch đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const {
    fields,
    submit,
    reset: formReset,
    dirty,
    submitErrors,
  } = useForm({
    fields: {
      transaction_value: useField<string>({
        value: entity?.transaction_value + "" ?? "",
        validates: [],
      }),
      transaction_ref: useField<string>({
        value: entity?.transaction_ref ?? "",
        validates: [],
      }),
      transaction_note: useField<string>({
        value: entity?.transaction_note ?? "",
        validates: [notEmpty("Note is required!")],
      }),
      transaction_condition: useField<string>({
        value: entity?.transaction_condition ?? "",
        validates: [notEmpty("Condition is required!")],
      }),
      object_id: useField<string>({
        value: entity?.object_id + "" ?? "",
        validates: [],
      }),
      transaction_current_balance: useField<string>({
        value: entity?.transaction_current_balance + "" ?? "",
        validates: [],
      }),
      transaction_new_balance: useField<string>({
        value: entity?.transaction_new_balance + "" ?? "",
        validates: [],
      }),
    },
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              transaction_id: entity.transaction_id,
              transaction_note: values.transaction_note,
              transaction_condition: conditionSelected,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  const emptyData = (
    <Page
      title="Sửa giao dịch"
      breadcrumbs={[{ content: "Transaction list", url: "/transaction" }]}
    >
      <EmptyState heading="Không có giao dịch!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Sửa giao dịch"
          breadcrumbs={[{ content: "Transaction list", url: "/transaction" }]}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        disabled
                        requiredIndicator
                        label="Transaction value"
                        {...fields.transaction_value}
                      />
                      <TextField
                        autoComplete="off"
                        label="Transaction reference"
                        disabled
                        {...fields.transaction_ref}
                      />
                      <TextField
                        autoComplete="off"
                        requiredIndicator
                        label="Transaction note"
                        {...fields.transaction_note}
                      />
                      <Select
                        label="Condition"
                        disabled={hideCondition}
                        options={conditionOptions}
                        onChange={handleConditionChange}
                        value={conditionSelected}
                      />
                      <TextField
                        autoComplete="off"
                        disabled
                        label="Object ID"
                        {...fields.object_id}
                      />
                      <TextField
                        autoComplete="off"
                        disabled
                        label="Current balance"
                        {...fields.transaction_current_balance}
                      />
                      <TextField
                        autoComplete="off"
                        disabled
                        label="New balance"
                        {...fields.transaction_new_balance}
                      />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <Page
            primaryAction={{
              content: "Lưu",
              disabled: !dirty,
              onAction: submit,
              loading: updating,
            }}
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  return (
    <>
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
