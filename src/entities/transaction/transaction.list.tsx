import {
  Badge,
  Button,
  Card,
  DataTable,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  Select,
  Stack,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities, searchDate } from "store/transactions.store.reducer";
import helpers from "../../helpers";
import TransactionNew from "./transaction.new";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";
import SearchFilter from "./filter";
import debounce from "lodash.debounce";
import { CalendarMinor } from "@shopify/polaris-icons";
import moment from "moment";

export default function Transaction_List() {
  const entities = useAppSelector((state) => state.transactions.entities);
  const loading = useAppSelector((state) => state.transactions.loading);
  const errorMessage = useAppSelector((state) => state.transactions.errorMessage);
  const totalItems = useAppSelector((state) => state.transactions.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    sort: "transaction_id,desc",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  /**
   * Show modal
   */

  const [openModal, setOpenModal] = useState(false);
  function showModal() {
    setOpenModal(true);
  }

  /**
   * Search by date time
   */
  const [mainQueryDate, setMainQueryDate] = useState({
    ...{
      // select: "_id",
      query: "",
      page: 1,
      limit: 20,
      sort: "transaction_id,desc",
    },
  });

  /**
   * Search object id
   */
  const [searchObjectID, setSearchObjecrtID] = useState("");
  const handleSearchObjectID = useCallback((newValue) => setSearchObjecrtID(newValue), []);

  /**
   * Start date
   */
  const [startDayValue, setStartDayValue] = useState("1");
  const handleStartDayChange = useCallback((value) => {
    setStartDayValue(value);
  }, []);

  /**
   * Start month
   */
  const [startMonthValue, setStartMonthValue] = useState("10");
  const handleStartMonthChange = useCallback((value) => {
    setStartMonthValue(value);
  }, []);

  /**
   * Start year
   */

  const [startYearValue, setStartYearValue] = useState("2022");
  const handleStarYearChange = useCallback((value) => {
    setStartYearValue(value);
  }, []);

  /**
   * Start time
   */
  const [startTimeValue, setStartTimeValue] = useState("00:00:00");
  const handleStartTimeChange = useCallback((value) => {
    setStartTimeValue(value);
  }, []);

  /**
   * End date
   */
  const [endDayValue, setEndDayValue] = useState("1");
  const handleEndDayChange = useCallback((value) => {
    setEndDayValue(value);
  }, []);

  /**
   * End month
   */
  const [endMonthValue, setEndMonthValue] = useState("10");
  const handleEndMonthChange = useCallback((value) => {
    setEndMonthValue(value);
  }, []);

  /**
   * End year
   */
  const [endYearValue, setEndYearValue] = useState("2022");
  const handleEndYearChange = useCallback((value) => {
    setEndYearValue(value);
  }, []);

  /**
   * End time
   */
  const [endTimeValue, setEndTimeValue] = useState("00:00:00");
  const handleEndTimeChange = useCallback((value) => {
    setEndTimeValue(value);
  }, []);

  /**
   * Status
   */

  // const [status, setStatus] = useState("");
  // const handleStatus = useCallback((_value) => {
  //   setStatus(_value);
  // }, []);

  /**
   * gte: greater or equal
   * lte: less than or equal
   */
  const dateStart = moment(
    `${startYearValue}-${startMonthValue}-${
      Number(startDayValue) <= 9 ? "0" + startDayValue : startDayValue
    } ${startTimeValue}`
  ).format("x");

  const dateEnd = moment(
    `${endYearValue}-${endMonthValue}-${Number(endDayValue) <= 9 ? "0" + endDayValue : endDayValue} ${endTimeValue}`
  ).format("x");

  const filterDate = () => {
    setMainQueryDate({
      ...mainQueryDate,
      ...{
        object_id: searchObjectID,
        createAt: `gte:${dateStart},lte:${dateEnd}`,
        // status: status,
      },
    });
  };

  /**
   * Change page number
   * Must be mainQuery or it will reset mainQuery. BUG!
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback((value) => setQueryValue(value), []);

  const [selectedParentId, setSelectedParentId] = useState<string>("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `&transaction_condition=${_value}` },
    });
  }, []);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(() => setNewModelactive((active) => !active), []);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  useEffect(() => {
    dispatch(searchDate(mainQueryDate));
  }, [mainQueryDate]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/transaction" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param transaction_id
   */
  const shortcutActions = (transaction_id: number) => {
    history("edit/" + transaction_id);
  };

  const emptyData = (
    <EmptyState heading="No transaction here!" image={emptyIMG}>
      <p>Oh! There is no transaction here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  /**
   * Coin
   */
  const coin = (
    <>
      <svg
        data-v-2d000d8f=""
        // viewBox="-5 -10 35 35"
        viewBox="0 0 24 24 "
        style={{
          width: "18px",
          height: "18px",
          margin: "0px 0px -3px 2px",
          fill: "#ee9b00",
        }}
      >
        <path
          data-v-2d000d8f=""
          d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 3c-4.971 0-9 4.029-9 9s4.029 9 9 9 9-4.029 9-9-4.029-9-9-9zm1 13.947v1.053h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.385-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85v-1.067h1v1.018c.725.019 1.535.145 2.442.42l-.362 1.648c-.768-.27-1.616-.515-2.442-.465-1.489.087-1.62 1.376-.581 1.916 1.711.804 3.943 1.401 3.943 3.546.002 1.718-1.344 2.632-3 2.864z"
        ></path>
      </svg>
    </>
  );

  const handleSort = (index: any, direction: string) => {
    let _direction = direction === "descending" ? "desc" : "asc";
    let sort = "";
    sort = "createAt," + _direction;
    setMainQuery({ ...mainQuery, sort: sort });
  };

  const renderItem = (transaction: any) => {
    const {
      transaction_id,
      transaction_value,
      transaction_ref,
      transaction_note,
      transaction_condition,
      transaction_current_balance,
      transaction_new_balance,
      transaction_method,
      user,
      object_id,
      createAt,
    } = transaction;
    return [
      <div
        className="clickable"
        key={transaction_id + "_transaction_id"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {transaction_id}
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_ref"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {helpers.trimContentString(transaction_ref)}
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_note"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {transaction_note}
      </div>,
      <div
        style={{ textAlign: "right", marginRight: "7px" }}
        className="clickable"
        key={transaction_id + "_transaction_curBalance"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {transaction_current_balance} {coin}
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_method"}
        onClick={() => shortcutActions(transaction_id)}
      >
        <div style={{ textAlign: "center" }}>{transaction_method === "minus" ? "-" : "+"}</div>
      </div>,
      <div
        style={{ textAlign: "right", paddingRight: "42px" }}
        className="clickable"
        key={transaction_id + "_transaction_value"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {transaction_value} {coin}
      </div>,
      <div
        style={{ textAlign: "right", marginRight: "7px" }}
        className="clickable"
        key={transaction_id + "_transaction_newBalance"}
        onClick={() => shortcutActions(transaction_id)}
      >
        {transaction_new_balance} {coin}
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_condition"}
        onClick={() => shortcutActions(transaction_id)}
      >
        <div style={{ textAlign: "center" }}>
          {transaction_condition === "pending" ? (
            <Badge progress="incomplete" status="attention">
              Pending
            </Badge>
          ) : transaction_condition === "done" ? (
            <Badge progress="complete" status="success">
              Done
            </Badge>
          ) : (
            <Badge progress="complete" status="critical">
              Cancel
            </Badge>
          )}
        </div>
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_object_id"}
        onClick={() => shortcutActions(transaction_id)}
      >
        <div style={{ textAlign: "left" }}>{object_id}</div>
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_display_username"}
        onClick={() => shortcutActions(transaction_id)}
      >
        <div style={{ textAlign: "left" }}>{user.display_name}</div>
      </div>,
      <div
        className="clickable"
        key={transaction_id + "_transaction_createAt"}
        onClick={() => shortcutActions(transaction_id)}
      >
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };

  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          sortable={[false, false, false, false, false, false, false, false, false, false, true, false]}
          defaultSortDirection="descending"
          initialSortColumnIndex={9}
          onSort={handleSort}
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Số tham chiếu",
            "Ghi chú",
            "Số dư hiện tại",
            "Phương thức",
            "Giá trị giao dịch",
            "Số dư mới",
            "Tình trạng",
            "Object ID",
            "Tên người dùng",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const clearDate = useCallback(() => {
    dispatch(getEntities(mainQuery));
  }, []);

  /**
   * Modal search
   */
  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Search"
      primaryAction={{
        content: "Search",
        onAction: filterDate,
      }}
      secondaryActions={[
        {
          content: "Clear",
          onAction: clearDate,
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={filterDate}>
          <FormLayout>
            <Stack>
              <Stack.Item>
                Total <Badge status="info">{totalItems.toString()}</Badge>
              </Stack.Item>
            </Stack>
            <Badge status="success">Start At</Badge>
            <FormLayout.Group condensed>
              <TextField
                label="Day"
                type="number"
                max="31"
                min="1"
                value={startDayValue}
                onChange={handleStartDayChange}
                autoComplete="off"
              />
              <TextField
                label="Month"
                type="number"
                max="12"
                min="1"
                value={startMonthValue}
                onChange={handleStartMonthChange}
                autoComplete="off"
              />
              <TextField
                label="Year"
                type="number"
                maxLength={4}
                value={startYearValue}
                onChange={handleStarYearChange}
                autoComplete="off"
              />
              <TextField
                label="Time"
                value={startTimeValue}
                onChange={handleStartTimeChange}
                helpText="hh:mm:ss"
                autoComplete="off"
              />
            </FormLayout.Group>

            <Badge status="warning">End At</Badge>
            <FormLayout.Group condensed>
              <TextField
                label="Day"
                type="number"
                max="31"
                min="1"
                value={endDayValue}
                onChange={handleEndDayChange}
                autoComplete="off"
              />
              <TextField
                label="Month"
                type="number"
                max="12"
                min="1"
                value={endMonthValue}
                onChange={handleEndMonthChange}
                autoComplete="off"
              />
              <TextField
                label="Year"
                type="number"
                maxLength={4}
                value={endYearValue}
                onChange={handleEndYearChange}
                autoComplete="off"
              />
              <TextField
                label="Time"
                value={endTimeValue}
                onChange={handleEndTimeChange}
                helpText="hh:mm:ss"
                autoComplete="off"
              />
            </FormLayout.Group>
            <FormLayout.Group condensed>
              <TextField
                label="User ID"
                type="number"
                value={searchObjectID}
                onChange={handleSearchObjectID}
                autoComplete="off"
              />
            </FormLayout.Group>
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  ) : null;

  const Actual_page = (
    <>
      <Page
        // fullWidth
        title="Giao dịch"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack alignment="center">
                  <Stack.Item fill>
                    <SearchFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                  </Stack.Item>
                  <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "Tất cả", value: "" },
                        { label: "Đang chờ", value: "pending" },
                        { label: "Đã huỷ", value: "cancel" },
                        { label: "Đã xong", value: "done" },
                      ]}
                    />
                  </Stack.Item>
                  <Stack.Item>
                    <Button icon={CalendarMinor} onClick={showModal}>
                      Date
                    </Button>
                  </Stack.Item>
                </Stack>
              </div>
              {PagesList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <TransactionNew show={newModelactive} onClose={() => setNewModelactive(false)} />
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {/* {toastMarkup} */}
      {_Modal}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

