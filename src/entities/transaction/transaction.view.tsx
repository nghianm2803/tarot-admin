import { Page, Layout, Card } from "@shopify/polaris";
import { useEffect } from "react";
import Theme404 from "../../layout/404";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import Parser from "html-react-parser";
import dateandtime from "date-and-time";
import { getEntity } from "../../store/transactions.store.reducer";
import PageMarkupLoading from "components/page_markup_loading";

/*
react-router-dom:
let useParam =  {} as any;
    useParam = useParams();
    let Param = useParam.any || false;
    or
console.log(props.match.params)
*/

export default function View_transactions() {
  const data = useAppSelector((state) => state.transactions.entity);
  const loading = useAppSelector((state) => state.transactions.loading);
  const dispatch = useAppDispatch();
  /**
   * Lấy URL ra... để query. chú ý phải load nội dung mới
   * trong useEffect
   */
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.transactions_slug || false;

  useEffect(() => {
    dispatch(getEntity(Param));
  }, [Param, dispatch]);

  const actual_page = data?.transaction_id ? (
    <Page
      title={data?.post_title}
      subtitle={`${data?.createBy}, last update at ${dateandtime.format(
        new Date(data?.createAt),
        "DD/MM/YYYY HH:mm:ss"
      )} `}
    >
      <Layout>
        <Layout.Section>
          <Card title={null} sectioned>
            {Parser(data ? data?.post_content : " ")}
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  ) : (
    <Theme404 />
  );

  const pageMarkup = loading ? <PageMarkupLoading /> : actual_page;
  return <div id="general_page">{pageMarkup}</div>;
}
