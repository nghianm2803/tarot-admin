import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import review_list from "./review.list";
import review_view from "./review.view";

/**
 *   Create index file for Review
 */

export default function List_review() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = review_list;
      break;

    case "view":
      ActualPage = review_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
