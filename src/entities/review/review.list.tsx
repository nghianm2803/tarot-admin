import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Icon,
  IndexTable,
  Layout,
  Modal,
  Page,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
  useIndexResourceState,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import {
  clearError,
  getEntities,
  deleteEntity,
  updateEntity,
} from "../../store/review.store.reducer";
import ReviewFilter from "./filter";
import helpers from "helpers";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import { StarFilledMinor } from "@shopify/polaris-icons";
import SkeletonLoading from "components/skeleton_loading";
import dateandtime from "date-and-time";

import { getEntitiesAll as getUsers } from "store/users.store.reducer";

import { getEntity as getOrder } from "store/orders.store.reducer";

export default function General_review() {
  const entity = useAppSelector((state) => state.review.entity);
  const entities = useAppSelector((state) => state.review.entities);
  const loading = useAppSelector((state) => state.review.loading);
  const errorMessage = useAppSelector((state) => state.review.errorMessage);
  const totalItems = useAppSelector((state) => state.review.totalItems);
  const updating = useAppSelector((state) => state.review.updating);
  const updateSuccess = useAppSelector((state) => state.review.updateSuccess);

  const entitieusers = useAppSelector((state) => state.users.entities);

  const entityorder = useAppSelector((state) => state.orders.entity);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "order_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState<string>("");

  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  // const handleFiltersQueryChange = useCallback(
  //   (_value) => {
  //     setMainQuery({ ...mainQuery, ...{ query: _value, page: 1 } });
  //   },
  //   [mainQuery]
  // );

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/review" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    dispatch(getUsers({}));
    // dispatch(getOrders({}));
  }, []);

  const [orderId, setOrderId] = useState([]);

  useEffect(() => {
    const optionType = [];
    if (entities) {
      entities.filter((e, i) => {
        optionType.push(e);
      });
      setOrderId(optionType);
    }
  }, [entities]);

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  const [showEditModel, setShowEditModel] = useState<any>(false);

  const closeModal = useCallback(() => {
    setShowEditModel((active) => !active);
  }, [showEditModel]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   * Delete review
   */

  const [openModal, setOpenModal] = useState<any>(false);

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(entities, { resourceIDResolver });
  function resourceIDResolver(review_data: any) {
    return review_data.review_id ?? null;
  }

  function _deleteEntityAction() {
    setOpenModal(true);
  }

  /**
   * Delete modal ...
   */
  const onModalAgree = useCallback(() => {
    dispatch(deleteEntity(selectedResources.join(",")));
    setOpenModal(false);
    // @ts-ignore
    handleSelectionChange("all", false); // <~~ This will trigger the recalculation
  }, [selectedResources]);

  useEffect(() => {
    dispatch(getEntities(mainQuery));
  }, [updateSuccess]);

  /**
   * must use callback function ...
   */
  const [commentDetail, setCommentDetail] = useState<any>(null);
  const getCommentDetail = useCallback((comment_data: any) => {
    setCommentDetail(comment_data);
    setShowEditModel(true);
  }, []);

  const { fields, dirty, submit, submitting } = useForm({
    fields: {
      review_content: useField<string>({
        value: commentDetail?.review_content ?? "",
        validates: [
          notEmpty("Nội dung trống!"),
          lengthMoreThan(5, "Nội dung không được ít hơn 5 ký tự"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
    },
    async onSubmit(values) {
      try {
        dispatch(
          updateEntity({
            review_id: commentDetail.review_id,
            review_content: values.review_content,
          })
        );
        setShowEditModel(false);
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * read out reviewDetail
   */

  const detailModal = (
    <Modal
      activator={null}
      open={showEditModel}
      onClose={closeModal}
      title="Edit Review"
      primaryAction={{
        content: "Lưu",
        disabled: !dirty,
        loading: submitting,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: closeModal,
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card.Section>
            <FormLayout>
              <TextField
                autoFocus
                disabled
                autoComplete="off"
                requiredIndicator
                label="Content"
                {...fields.review_content}
              />
            </FormLayout>
          </Card.Section>
        </Form>
      </Modal.Section>
    </Modal>
  );

  /**
   *  Index table
   */
  function IndexTableComment() {
    const resourceName = {
      singular: "comment",
      plural: "comments",
    };

    const promotedBulkActions = [
      {
        content: "Delete review",
        onAction: _deleteEntityAction,
      },
    ];

    const rowMarkup = entities?.map((props, index) => {
      const {
        review_id,
        order_id,
        seller_id,
        review_content,
        review_point,
        createAt,
        user_review_createByTouser,
      } = props;
      return (
        <>
          <IndexTable.Row
            id={review_id}
            key={review_id}
            selected={selectedResources.includes(review_id)}
            position={index}
          >
            <IndexTable.Cell>
              <div key={review_id} onClick={() => getCommentDetail(props)}>
                {order_id}
              </div>
            </IndexTable.Cell>
            <IndexTable.Cell>
              <div key={review_id} onClick={() => getCommentDetail(props)}>
                {user_review_createByTouser.display_name}
              </div>
            </IndexTable.Cell>
            <IndexTable.Cell>
              <div key={review_id} onClick={() => getCommentDetail(props)}>
                {helpers.trimContentString(review_content)}
              </div>
            </IndexTable.Cell>
            <IndexTable.Cell>
              <div
                style={{ width: "15px" }}
                key={review_id}
                onClick={() => getCommentDetail(props)}
              >
                {review_point === 1 ? (
                  <>
                    <Stack wrap={false} spacing="extraTight">
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                    </Stack>
                  </>
                ) : review_point === 2 ? (
                  <>
                    <Stack wrap={false} spacing="extraTight">
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                    </Stack>
                  </>
                ) : review_point === 3 ? (
                  <>
                    <Stack wrap={false} spacing="extraTight">
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="subdued" />
                      <Icon source={StarFilledMinor} color="subdued" />
                    </Stack>
                  </>
                ) : review_point === 4 ? (
                  <>
                    <Stack wrap={false} spacing="extraTight">
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="subdued" />
                    </Stack>
                  </>
                ) : (
                  <>
                    <Stack wrap={false} spacing="extraTight">
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                      <Icon source={StarFilledMinor} color="warning" />
                    </Stack>
                  </>
                )}
              </div>
            </IndexTable.Cell>
            <IndexTable.Cell>
              <time>
                {createAt
                  ? dateandtime.format(
                      new Date(Number(createAt)),
                      "DD-MM-YYYY HH:mm:ss"
                    )
                  : "-"}
              </time>
            </IndexTable.Cell>
          </IndexTable.Row>
        </>
      );
    });

    return (
      <Card>
        <IndexTable
          loading={loading}
          resourceName={resourceName}
          itemCount={entities.length}
          selectedItemsCount={
            allResourcesSelected ? "All" : selectedResources.length
          }
          onSelectionChange={handleSelectionChange}
          // bulkActions={bulkActions}
          promotedBulkActions={promotedBulkActions}
          headings={[
            { title: "Order ID" },
            { title: "Customer" },
            { title: "Review" },
            { title: "Review Point" },
            // { title: "Seller" },
            { title: "Thời gian tạo" },
          ]}
        >
          {rowMarkup}
        </IndexTable>
      </Card>
    );
  }

  const emptyData = (
    <EmptyState heading="Không có đánh giá!" image={emptyIMG}>
      <p>Oh! Không có đánh giá! Thử bỏ bộc lọc hoặc thêm đánh giá!</p>
    </EmptyState>
  );

  const PagesList =
    totalItems > 0 ? (
      <>
        <IndexTableComment />
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page title="Đánh giá">
      {detailModal}
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack alignment="center">
                <Stack.Item fill>
                  <ReviewFilter
                    queryValue={StringQuery?.query}
                    onChange={handleFiltersQueryChange}
                  />
                </Stack.Item>
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  /**
   * Delete modal
   * selectedResources <------ Toan bo ID checked o day = array
   */
  const commentArray = [selectedResources];
  commentArray.join(",");

  const _Modal = (
    <Modal
      activator={null}
      open={openModal}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá đánh giá?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
        destructive: true,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Bạn có chắc chắn muốn xoá {selectedResources.length} đánh giá?</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
