import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import { clearError, deleteEntity, getEntity, updateEntity } from "../../store/need_help.store.reducer";
import { lengthLessThan, lengthMoreThan, notEmpty, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_need_help() {
  const entity = useAppSelector((state) => state.need_help.entity);
  const updating = useAppSelector((state) => state.need_help.updating);
  const loading = useAppSelector((state) => state.need_help.loading);
  const errorMessage = useAppSelector((state) => state.need_help.errorMessage);
  const updateSuccess = useAppSelector((state) => state.need_help.updateSuccess);

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("This need help has been updated!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  /**
   * Language hook
   */
  const [languageSetting, setLanguageSetting] = useState<string>("en");
  const handleLanguageChange = useCallback((value) => setLanguageSetting(value), []);

  const language = [
    { label: "English", value: "en" },
    { label: "Vietnamese", value: "vi" },
  ];

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.need_help_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else reset();
  }, []);

  useEffect(() => {
    if (updateSuccess) setNotification("Cập nhật thành công!");
  }, [updateSuccess]);

  useEffect(() => {
    if (entity && entity.languge) {
      setLanguageSetting(entity.languge);
    }
  }, [entity]);

  const useFields = {
    help_title: useField<string>({
      value: entity?.help_title ?? "",
      validates: [
        notEmpty("Tiêu đề trống!"),
        lengthLessThan(25, "Không được dài hơn 25 ký tự"),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    help_description: useField<string>({
      value: entity?.help_description ?? "",
      validates: [
        notEmpty("Mô tả trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(5, "No shorter than 5 characters."),
      ],
    }),
  };

  const { fields, submit, dirty, reset } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              help_id: entity.help_id,
              help_title: values.help_title,
              help_description: values.help_description,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Need help" breadcrumbs={[{ content: "Need_help list", url: "/need_help" }]}>
      <EmptyState heading="No need help post here!" image={emptyIMG}>
        <p>Need help không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page title="Need help" breadcrumbs={[{ content: "Need_help list", url: "/need_help" }]}>
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField autoFocus autoComplete="off" label="Help title" {...fields.help_title} />
                      <TextField autoComplete="off" label="Help description" {...fields.help_description} />
                      <Select label="Language" options={language} onChange={handleLanguageChange} value={languageSetting} />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            // secondaryActions={
            //   Param
            //     ? [
            //         {
            //           content: "Xoá",
            //           destructive: true,
            //           onAction: _deleteEntityAction,
            //         },
            //       ]
            //     : null
            // }
          />
        </Page>
      </>
    ) : (
      emptyData
    );
  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Delete this need help?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      // secondaryActions={[
      //   {
      //     content: "Huỷ",
      //     onAction: () => {
      //       setOpenModal(false);
      //     },
      //   },
      // ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
