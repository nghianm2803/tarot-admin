import { FormLayout, Modal, TextField, Form, Card, Toast, Select } from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/need_help.store.reducer";
import { lengthLessThan, lengthMoreThan, notEmpty, useField, useForm } from "@shopify/react-form";
import { useParams } from "react-router-dom";

/**
 *   Create upload Modal for Need Help
 */
export default function NeedHelpCreate({ onClose, show }) {
  const dispatch = useAppDispatch();

  const entity = useAppSelector((state) => state.need_help.entity);
  const updating = useAppSelector((state) => state.need_help.updating);
  const errorMessage = useAppSelector((state) => state.need_help.errorMessage);

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  /**
   * Language hook
   */
  const [languageSetting, setLanguageSetting] = useState<string>("en");
  const handleLanguageChange = useCallback((value) => setLanguageSetting(value), []);

  const language = [
    { label: "English", value: "en" },
    { label: "Vietnamese", value: "vi" },
  ];

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.need_help_slug || false;

  useEffect(() => {
    reset();
  }, []);

  useEffect(() => {
    if (entity && entity.languge) {
      setLanguageSetting(entity.languge);
    }
  }, [entity]);

  const useFields = {
    help_title: useField<string>({
      value: "",
      validates: [
        notEmpty("Tiêu đề giúp đỡ trống!"),
        lengthLessThan(25, "Không được dài hơn 25 ký tự"),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    help_description: useField<string>({
      value: "",
      validates: [
        notEmpty("Mô tả trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(5, "No shorter than 5 characters."),
      ],
    }),

    language: useField<string>({
      value: "",
      validates: [],
    }),
  };

  const { fields, submit, dirty, reset, submitErrors } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              help_title: values.help_title,
              help_description: values.help_description,
              language: languageSetting,
            })
          );
        }
        reset();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Create new need help"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField autoFocus autoComplete="off" label="Need help title" {...fields.help_title} />
              <TextField autoComplete="off" label="Need help description" {...fields.help_description} />
              <Select label="Language" options={language} onChange={handleLanguageChange} value={languageSetting} />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
