import { Badge, Card, DataTable, EmptyState, Layout, Page, Select, Stack, Toast } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/need_help.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import NeedHelpCreate from "./need_help.create";
import SkeletonLoading from "components/skeleton_loading";

export default function Need_Help_List() {
  const entities = useAppSelector((state) => state.need_help.entities);
  const loading = useAppSelector((state) => state.need_help.loading);
  const errorMessage = useAppSelector((state) => state.need_help.errorMessage);
  const totalItems = useAppSelector((state) => state.need_help.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    language: "vi",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback((value) => setQueryValue(value), []);

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(() => setNewModelactive((active) => !active), []);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/need_help" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param help_id
   */
  const shortcutActions = (help_id: number) => {
    history("/need_help/edit/" + help_id);
  };

  const emptyData = (
    <EmptyState heading="No need help post here!" image={emptyIMG}>
      <p>Oh! Không có bài đăng ở đây! Thử bỏ bộ lọc hoặc tạo bài đăng mới!</p>
    </EmptyState>
  );

  const renderItem = (need_help: any) => {
    const { help_id, help_title, help_description, language, createAt, updateAt } = need_help;
    return [
      <div className="clickable" key={help_id + "_help_id"} onClick={() => shortcutActions(help_id)}>
        {help_id}
      </div>,
      <div className="clickable" key={help_id + "_help_title"} onClick={() => shortcutActions(help_id)}>
        <p>{helpers.trimContentString(help_title)}</p>
      </div>,
      <div className="clickable" key={help_id + "_help_des"} onClick={() => shortcutActions(help_id)}>
        <p>{helpers.trimContentString(help_description)}</p>
      </div>,
      <div className="clickable" key={help_id + "_need_help_createAt"} onClick={() => shortcutActions(help_id)}>
        {/* <time>
            {createAt
              ? dateandtime.format(
                  new Date(Number(createAt)),
                  "DD-MM-YYYY HH:mm:ss"
                )
              : "-"}
              2022-09-06T08:59:28.000Z
          </time> */}
        {createAt}
      </div>,
      <div className="clickable" key={help_id + "_need_help_updateAt"} onClick={() => shortcutActions(help_id)}>
        <time>{updateAt ? dateandtime.format(new Date(Number(updateAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text"]}
          headings={["ID", "Tiêu đề", "Mô tả", "Thời gian tạo", "Thời gian cập nhập"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
          .clickable {
            margin: -1.6rem;
            padding: 1.6rem;
            cursor: pointer;
          }
          .small-icon {
            font-size: 12px;
            padding: 0;
            width: 15px;
            height: auto;
          }
        `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        title="Need help"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>{PagesList}</Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <NeedHelpCreate show={newModelactive} onClose={() => setNewModelactive(false)} />
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
