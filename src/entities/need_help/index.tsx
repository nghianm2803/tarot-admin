import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from '../../layout/404';
import needhelp_list from './need_help.list';
import needhelp_edit from './need_help.edit';


/**
*   Create index file for NeedHelp
*/

export default function List_needhelp() {
    let useParam =  {} as any;
        useParam = useParams();
    let Param = useParam.slug || 'list';

    let ActualPage: any;
    switch (Param) {
        
        case 'list':
            ActualPage = needhelp_list;
        break;

        case 'edit':
            ActualPage = needhelp_edit;
        break;

        default:
            ActualPage =  Theme404;
        break;
    }

    return (
        <>
            {<ActualPage />}
        </>
    );
}
