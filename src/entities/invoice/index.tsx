import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
// import invoice_list from "./invoice.list";
import invoice_detail from "./invoice.detail";
// import invoice_view from "./invoice.view";
// import invoice_create from "./invoice.create";

/**
 *   Create index file for Invoice
 */

export default function List_invoice() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "detail";

  let ActualPage: any;
  switch (Param) {
    // case "list":
    //   ActualPage = invoice_list;
    //   break;

    case "detail":
      ActualPage = invoice_detail;
      break;

    // case "new":
    //   ActualPage = invoice_create;
    //   break;

    // case "view":
    //   ActualPage = invoice_view;
    //   break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
