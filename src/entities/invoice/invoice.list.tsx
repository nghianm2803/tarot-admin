import {
  Badge,
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Select,
  Stack,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
// import {
//   clearError,
//   getEntities,
// } from "../../store/general_service.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";
import SearchFilter from "./filter";

export default function Invoice_List() {
  // const loading = useAppSelector((state) => state.general_service.loading);
  // const entities = useAppSelector((state) => state.general_service.entities);

  const entities: any[] = [
    {
      invoice_id: 1,
      invoice_name: "Invoice to DooCharSiu",
      invoice_number: "100",
      invoice_status: 1,
      createAt: new Date(),
      updateAt: new Date(),
    },
  ];

  const errorMessage = useAppSelector(
    (state) => state.general_service.errorMessage
  );
  // const totalItems = useAppSelector(
  //   (state) => state.general_service.totalItems
  // );

  const dispatch = useAppDispatch();
  const history = useNavigate();

  // const toggleActive = useCallback(() => {
  //   dispatch(clearError());
  // }, []);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    sort: "invoice_id,desc",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(
    () => setNewModelactive((active) => !active),
    []
  );

  const [selectedParentId, setSelectedParentId] = useState<string>("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `&invoice_number=${_value}` },
    });
  }, []);

  // useEffect(() => {
  //   if (loading === false) setInitial_loading(false);
  // }, [loading]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/invoice" + buildURLSearch);
    //   dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param invoice_id
   */
  const shortcutActions = (invoice_id: number) => {
    history("/invoice/edit/" + invoice_id);
  };

  const emptyData = (
    <EmptyState heading="No invoice here!" image={emptyIMG}>
      <p>Oh! Không có hoá đơn! Thử bỏ bộ lọc hoặc thêm hoá đơn!</p>
    </EmptyState>
  );

  const renderItem = (invoice: any) => {
    const {
      invoice_id,
      invoice_name,
      invoice_number,
      invoice_status,
      createAt,
      updateAt,
    } = invoice;
    return [
      <div
        className="clickable"
        key={invoice_id + "_invoice_id"}
        onClick={() => shortcutActions(invoice_id)}
      >
        {invoice_id}
      </div>,
      <div
        className="clickable"
        key={invoice_id + "_invoice_name"}
        onClick={() => shortcutActions(invoice_id)}
      >
        {invoice_name}
      </div>,
      <div
        className="clickable"
        key={invoice_id + "_invoice_number"}
        onClick={() => shortcutActions(invoice_id)}
      >
        {invoice_number}
      </div>,
      <div
        className="clickable"
        key={invoice_id + "_invoice_status"}
        onClick={() => shortcutActions(invoice_id)}
      >
        {invoice_status === 1 ? (
          <Badge status="success">Sent</Badge>
        ) : (
          <Badge status="critical">Send to advisor</Badge>
        )}
      </div>,
      <div
        className="clickable"
        key={invoice_id + "_invoice_createAt"}
        onClick={() => shortcutActions(invoice_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
      <div
        className="clickable"
        key={invoice_id + "_invoice_updateAt"}
        onClick={() => shortcutActions(invoice_id)}
      >
        <time>
          {updateAt
            ? dateandtime.format(
                new Date(Number(updateAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };
  const PagesList = (
    // totalItems > 0 ? (
    <>
      <DataTable
        columnContentTypes={["text", "text", "text", "text", "text", "text"]}
        headings={[
          "ID",
          "Tên hoá đơn",
          "Số hoá đơn",
          "Tình trạng hoá đơn",
          "Thời gian tạo",
          "Thời gian cập nhập",
        ]}
        rows={entities?.map(renderItem)}
        hideScrollIndicator
        //footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
      />
      <style>{`
          .clickable {
            margin: -1.6rem;
            padding: 1.6rem;
            cursor: pointer;
          }
        `}</style>
    </>
  );
  // ) : (
  //   emptyData
  // );

  const Actual_page = (
    <>
      <Page title="Invoice">
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <SearchFilter
                  queryValue={StringQuery?.query}
                  onChange={handleFiltersQueryChange}
                />
              </div>
              {PagesList}
            </Card>
            <br />
            {/* {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null} */}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  // const toastMarkup = errorMessage ? (
  //   <Toast content={errorMessage} error onDismiss={toggleActive} />
  // ) : null;

  return (
    <>
      {/* {toastMarkup} */}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
