import {
  Button,
  Card,
  DataTable,
  Layout,
  Page,
  Stack,
  TextStyle,
} from "@shopify/polaris";

export default function Invoice() {
  const entities: any[] = [
    {
      invoice_id: 1,
      service_name: "Video Reading",
      service_price: "100",
      service_unit: "hour",
      service_duration: "24",
      service_type: "qclip",
      invoice_status: 1,
      createAt: new Date(),
    },
  ];

  const renderItem = (invoice: any) => {
    const {
      invoice_id,
      service_name,
      service_price,
      service_unit,
      service_type,
      service_duration,
      invoice_status,
    } = invoice;
    return [
      <div className="clickable" key={invoice_id}>
        {invoice_id}
      </div>,
      <div className="clickable" key={invoice_id}>
        {service_name}
      </div>,
      <div className="clickable" key={invoice_id}>
        {service_price}
      </div>,
      <div className="clickable" key={invoice_id}>
        {service_unit}
      </div>,
      <div className="clickable" key={invoice_id}>
        {service_duration}
      </div>,
      <div className="clickable" key={invoice_id}>
        {service_type}
      </div>,
      <div className="clickable" key={invoice_id}>
        {invoice_status}
      </div>,
      <div className="button" key={invoice_id}>
        <Button primary size="medium">
          Send
        </Button>
      </div>,
    ];
  };

  return (
    <>
      <Page title="Invoice">
        <Layout>
          <Layout.Section>
            <Card>
              <Card.Section>
                <Stack>
                  <Stack.Item fill>
                    <b>Taki Academy</b>
                    <br />
                    <p>
                      47 Nguyen Tuan street, Thanh Xuan Trung, <br /> Thanh
                      Xuan, Ha Noi city.
                    </p>
                  </Stack.Item>
                  <Stack.Item>
                    <b>Invoice to</b>
                    <br />
                    <p>Customer name:</p>
                    <p>Phone number:</p>
                    <p>Address:</p>
                  </Stack.Item>
                  <Stack.Item>
                    <p>&nbsp;</p>
                    <p>Invoice #:</p>
                    <p>Invoice Date:</p>
                    <p>Due Date:</p>
                  </Stack.Item>
                </Stack>
              </Card.Section>
              <Card.Section>
                <DataTable
                  columnContentTypes={[
                    "text",
                    "text",
                    "text",
                    "text",
                    "text",
                    "text",
                    "text",
                    "text",
                  ]}
                  headings={[
                    "ID",
                    "Service Name",
                    "Price",
                    "Unit",
                    "Duration",
                    "Type",
                    "Status",
                    "Action",
                  ]}
                  rows={entities?.map(renderItem)}
                  hideScrollIndicator
                />
              </Card.Section>
              <Card.Section>
                <Stack vertical={true}>
                  <div style={{ float: "right" }}>
                    <Stack.Item>
                      <TextStyle>Subtotal $100 </TextStyle>
                    </Stack.Item>
                    <Stack.Item>
                      <TextStyle>Tax 0.00% </TextStyle>
                    </Stack.Item>
                    <Stack.Item>
                      <TextStyle>Discount $0 </TextStyle>
                    </Stack.Item>
                    <Card.Section>
                      <Stack.Item>
                        <TextStyle variation="strong">Total $100</TextStyle>
                      </Stack.Item>
                    </Card.Section>
                  </div>
                </Stack>
              </Card.Section>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );
}
