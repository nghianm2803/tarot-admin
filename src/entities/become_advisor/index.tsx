import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import becomeadvisor_list from "./becomeadvisor.list";
import becomeadvisor_detail from "./becomeadvisor.detail";
import becomeadvisor_new from "./becomeadvisor.new";

/**
 *   Create index file for Contactform
 */

export default function List_contactform() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = becomeadvisor_list;
      break;
    case "edit":
      ActualPage = becomeadvisor_detail;
      break;
    case "new":
      ActualPage = becomeadvisor_new;
      break;
    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
