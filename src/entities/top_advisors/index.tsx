import { useParams } from "react-router-dom";
import Theme404 from '../../layout/404';
import top_advisors_list from './top_advisors.list';

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || 'list';

  let ActualPage: any;
  switch (Param) {
    case 'list':
      ActualPage = top_advisors_list;
      break;
    default:
      ActualPage = Theme404;
      break;
  }
  return (
    <>
      {<ActualPage />}
    </>
  );
}
