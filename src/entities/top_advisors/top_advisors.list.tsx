import {
  Autocomplete,
  Button,
  Card,
  DataTable,
  EmptyState,
  Icon,
  Layout,
  Page,
  Stack,
  Tag,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "config/store";
import emptyIMG from "../../media/empty.png";
import Pagination from "components/pagination";
import { clearError, getTopAdvisor, getCloneUsers, getEntities } from "store/users.store.reducer";
import { updateEntity as _updateTopAdvisor, getEntity as getTopAdvisorFromSetting } from "store/settings.store.reducer";
import helpers from "../../helpers";
import CountryName from "components/countries";
import SkeletonLoading from "components/skeleton_loading";
import { StarFilledMinor } from "@shopify/polaris-icons";

/**
 * @returns JSX
 */
export default function Top_Advisor_List() {
  const _entitySettings = useAppSelector((state) => state.settings.entity);
  const entities = useAppSelector((state) => state.users.entities);
  const cloneAdvisors = useAppSelector((state) => state.users.entities);
  const loading = useAppSelector((state) => state.users.loading);
  const updating = useAppSelector((state) => state.users.updating);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const totalItems = useAppSelector((state) => state.users.totalItems);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "user_id,desc",
      user_role: "advisor",
    },
    ...StringQuery,
  });

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  /**
   * Search advisors to add them into top advisors
   */
  const [inputValueAdvisors, setInputValueAdvisors] = useState("");
  const [optionAdvisors, setOptionAdvisors] = useState([]);
  const [selectedOptionAdvisors, setSelectedOptionAdvisors] = useState([]);
  const [removeAdvisor, setRemoveAdvisor] = useState([]);

  /**
   * Remove advisor
   */
  const removePreAdvisor = useCallback(
    (advisor) => () => {
      setSelectedOptionAdvisors((preAdvisors) => preAdvisors.filter((preAdvisor) => preAdvisor !== advisor));
      let prepareData = removeAdvisor;
      prepareData = [...prepareData, ...[advisor.user]];
      setRemoveAdvisor(prepareData);
    },
    [selectedOptionAdvisors]
  );

  const advisorMarkup = selectedOptionAdvisors.map((option) => {
    let advisorLabel = String(option || " ").replace("_", " ");
    return (
      <Tag key={`option${advisorLabel}`} onRemove={removePreAdvisor(option)} accessibilityLabel="2">
        {option.user_email ? option.user_email : advisorLabel}
      </Tag>
    );
  });

  /**
   * Get data advisors
   */
  const deselectedAdvisors = cloneAdvisors?.map(({ user_id, user_email }) => {
    return {
      label: String(user_email),
      value: String(user_id),
    };
  });

  const updateAdvisor = (value: any) => {
    setInputValueAdvisors(value);
    if (value === "") {
      setOptionAdvisors(deselectedAdvisors);
      return;
    }
    const filterRegex = new RegExp(value, "i");
    const resultOptions = deselectedAdvisors.filter((option) => option.label.match(filterRegex));
    let endIndex = resultOptions.length - 1;
    if (resultOptions.length === 0) {
      endIndex = 0;
    }
    setOptionAdvisors(resultOptions);
  };

  /**
   * Add advisor into top advisor
   */
  const addTopAdvisor = useCallback(() => {
    console.log(selectedOptionAdvisors.join(","), "selectedOptionAdvisors");
    dispatch(
      _updateTopAdvisor({
        top_advisor: String(selectedOptionAdvisors.join(",")),
      })
    );
    setSelectedOptionAdvisors([]);
  }, [_entitySettings, selectedOptionAdvisors]);

  const advisorField = (
    <Autocomplete.TextField
      autoComplete={null}
      onChange={updateAdvisor}
      label=""
      value={inputValueAdvisors}
      placeholder="Search advisor"
      connectedRight={<Button onClick={addTopAdvisor}>Add advisor into Top</Button>}
    />
  );

  /**
   * dispatched then get value from store ...
   */
  const [allAdvisorFromSetting, setAllAdvisorFromSetting] = useState<string>("");
  useEffect(() => {
    dispatch(getTopAdvisorFromSetting("top_advisor"));
  }, []);

  /**
   * take it from setting table
   */
  useEffect(() => {
    if (_entitySettings) {
      setAllAdvisorFromSetting(_entitySettings);
    }
  }, [_entitySettings]);

  /**
   * pick it up from users
   */
  useEffect(() => {
    if (allAdvisorFromSetting !== "") dispatch(getTopAdvisor(allAdvisorFromSetting));
  }, [allAdvisorFromSetting]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  // Get all advisors from users
  useEffect(() => {
    /** Top advisor */
    // dispatch(getEntities(mainQuery));
    dispatch(getTopAdvisor(mainQuery));
  }, [mainQuery]);

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  /**
   * Get nation name
   * @param code string
   * @returns string
   */
  function handleGetNationNameByCode(code: string): string {
    if (code) {
      let data = CountryName().filter((item, index) => {
        if (item.alpha2 === code) {
          return true;
        } else {
          return false;
        }
      });
      if (data && data[0]) {
        return data[0].name;
      }
    }
    return code;
  }

  const renderItem = (users: any) => {
    const {
      user_id,
      user_avatar,
      display_name,
      user_email,
      user_nation,
      user_address,
      user_numberphone,
      average_rating,
    } = users;
    return [
      <div className="clickable" key={user_id + "_user_id"}>
        {user_id}
      </div>,
      <div className="clickable" style={{ textAlign: "center" }} key={user_id + "_avatar"}>
        <div className="user_avatar">
          <img src={user_avatar ? user_avatar : user_id?.user_avatar} crossOrigin="anonymous" />
        </div>
      </div>,
      <div className="clickable" key={user_id + "_display_name"}>
        {display_name}
        <div style={{ fontWeight: "bold", color: "#999" }}>{user_email}</div>
      </div>,
      <div className="clickable" key={user_id + "_user_phone"}>
        {user_numberphone}
      </div>,
      <div className="clickable" key={user_id + "_user_rating"}>
        <div style={{ textAlign: "center", display: "inline-flex" }}>
          {Math.round(average_rating * 10) / 10}
          <Icon source={StarFilledMinor} color="warning" />
        </div>
      </div>,
      <div className="clickable" key={user_id + "_user_address"}>
        {user_address}
        <p>{handleGetNationNameByCode(user_nation) || ""} </p>
      </div>,
    ];
  };
  const AdvisorsList =
    entities.length > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Avatar", "Tên người dùng", "Số điện thoại", "Đánh giá", "Địa chỉ"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
          .clickable {
            margin: -1.6rem;
            padding: 1.6rem;
            cursor: pointer;
          }
        `}</style>
      </>
    ) : (
      emptyData
    );
  const Actual_page = (
    <>
      <Page title="Top Advisors">
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack distribution="equalSpacing">
                  <Autocomplete
                    allowMultiple
                    options={optionAdvisors}
                    selected={selectedOptionAdvisors}
                    textField={advisorField}
                    onSelect={setSelectedOptionAdvisors}
                    listTitle="Suggested Advisor"
                  />
                  <br />
                  <Stack>{advisorMarkup}</Stack>
                  <Stack>{addTopAdvisor}</Stack>
                </Stack>
              </div>
              {AdvisorsList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

