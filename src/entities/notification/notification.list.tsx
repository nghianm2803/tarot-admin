import {
  Button,
  Card,
  DataTable,
  DatePicker,
  EmptyState,
  Layout,
  Loading,
  Modal,
  Page,
  Select,
  Stack,
  Subheading,
  TextField,
  TextStyle,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import { TickSmallMinor, LockMinor, EditMinor } from "@shopify/polaris-icons";
import Pagination from "components/pagination";
import emptyIMG from "../../media/empty.png";
import { useAppDispatch, useAppSelector } from "config/store";
import {
  clearError,
  getEntity,
  getEntities,
  partialUpdateEntity,
  deleteEntity,
} from "store/notification.store.reducer";

export default function Notification_List() {
  const entity = useAppSelector((state) => state.notification.entity);
  const entities = useAppSelector((state) => state.notification.entities);
  const loading = useAppSelector((state) => state.notification.loading);
  const errorMessage = useAppSelector(
    (state) => state.notification.errorMessage
  );
  const totalItems = useAppSelector((state) => state.notification.totalItems);
  const updating = useAppSelector((state) => state.notification.updating);

  // const history = useNavigate();
  const dispatch = useAppDispatch();
  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    sort: "notification_id,desc",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  /**
   * Change page number
   * Must be mainQuery or it will reset mainQuery. BUG!
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      window.history.replaceState(
        null,
        "Notification",
        "/notification" + buildURLSearch
      );
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  /**
   * Query NOTIFICATION DETAIL
   * @param notification_id
   */

  const [showEditModel, setShowEditModel] = useState<boolean>(false);
  const [showFormEdits, setShowFormEdits] = useState<boolean>(false);
  const [deleteConfirm, setDeleteConfirm] = useState<boolean>(false);

  const getNotificationDetail = (notification_id: number) => {
    dispatch(getEntity(notification_id));
    setShowEditModel(true);
    setDeleteConfirm(false);
  };
  const closeModal = useCallback(() => {
    if (showEditModel) {
      // close model ? Reload data
      dispatch(getEntities(mainQuery));
    }
    setShowEditModel((active) => !active);
  }, [showEditModel]);

  // Discard schedule notification
  const discardContent = useCallback(() => {
    dispatch(deleteEntity(entity?.notification_id));
    closeModal();
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  // Confirm discard schedule notification
  const discardContentConfirm = useCallback(() => {
    setDeleteConfirm((deleteConfirm) => !deleteConfirm);
  }, []);

  useEffect(() => {
    if (entity) {
      setTitle(entity?.notification_title);
      setContent(entity?.notification_content);
    }
  }, [entity]);

  const [title, setTitle] = useState<string>("");
  const handChangeTitle = useCallback((newValue) => setTitle(newValue), []);

  const [content, setContent] = useState<string>("");
  const handChangeContent = useCallback((newValue) => setContent(newValue), []);

  /* Date picker */
  function pad(num: number, size = 2) {
    let numbr = num.toString();
    while (numbr.length < size) numbr = "0" + numbr;
    return numbr;
  }

  /**
   * Format date and time
   */
  function extractTimeAndDateFromSource(source: string) {
    let DateObject = new Date();
    try {
      if (source) {
        DateObject = new Date(String(source));
      }
      const _date =
        pad(DateObject.getDate(), 2) +
        "/" +
        pad(DateObject.getMonth() + 1, 2) +
        "/" +
        DateObject.getFullYear();
      const _timestamp = DateObject.getTime();
      return {
        date: _date,
        timestamp: _timestamp,
        day: pad(DateObject.getDate(), 2),
        month: DateObject.getMonth(),
        year: DateObject.getFullYear(),
      };
    } catch (_) {
      return {
        date: "",
        timestamp: 0,
        day: 0,
        month: 0,
        year: 0,
      };
    }
  }

  const today_active = extractTimeAndDateFromSource("");
  const [{ month, year }, setDate] = useState({
    month: today_active.month,
    year: today_active.year,
  });
  const [selectedDates, setSelectedDates] = useState({
    start: new Date(),
    end: new Date(),
  });

  const handleMonthChange = useCallback(
    (_month, _year) => setDate({ month: _month, year: _year }),
    []
  );

  /* Hours */
  const [notification_hour, setNotification_hour] = useState("00");
  const handleHourChange = useCallback(
    (value) => setNotification_hour(value),
    []
  );
  const hours: any = [];
  for (let i = 0; i < 24; i++) {
    hours.push({ label: pad(i), value: pad(i) });
  }

  /* Minutes */
  const [notification_minute, setNotification_minute] = useState("00");
  const handleMinuteChange = useCallback(
    (value) => setNotification_minute(value),
    []
  );
  const minutes = [
    { label: "00", value: "00" },
    { label: "15", value: "15" },
    { label: "30", value: "30" },
    { label: "45", value: "45" },
  ];

  const datety = new Date(selectedDates.start).getDate();

  const saveForm = () => {
    dispatch(
      partialUpdateEntity({
        notification_id: entity?.notification_id,
        notification_title: title,
        notification_content: content,
        notification_schedule:
          `${year}/${pad(month + 1, 2)}/${pad(datety)}` +
          ` ${notification_hour}:${notification_minute}`,
        createBy: entity?.createBy,
        //notification_data: entity?.notification_data,
      })
    );
    setShowFormEdits(false);
  };

  const cancelForm = () => {
    setShowFormEdits(false);
  };

  const showFormEdit = () => {
    setShowFormEdits(true);
  };

  let notiData = null;
  try {
    notiData = JSON.stringify(entity.notification_data);
  } catch (e) {
    notiData = String(notiData);
  }

  const editForm = (
    <>
      <Subheading>Edit notification</Subheading>
      <br />
      <TextField
        label="Tiêu đề"
        value={title}
        onChange={handChangeTitle}
        autoComplete="off"
        autoFocus
      />
      <TextField
        label="Nội dung"
        value={content}
        onChange={handChangeContent}
        maxLength={200}
        multiline={2}
        autoComplete="off"
        showCharacterCount
      />
      <TextStyle>Publish date</TextStyle>
      <DatePicker
        month={month}
        year={year}
        onChange={setSelectedDates}
        onMonthChange={handleMonthChange}
        selected={selectedDates}
      />
      <Stack>
        <Stack.Item>
          <Select
            label="Giờ"
            options={hours}
            onChange={handleHourChange}
            value={notification_hour}
          />
        </Stack.Item>
        <Stack.Item>
          <Select
            label="Phút"
            options={minutes}
            onChange={handleMinuteChange}
            value={notification_minute}
          />
        </Stack.Item>
      </Stack>
      <br />
      <Button primary onClick={saveForm} loading={updating}>
        Lưu
      </Button>
      <TextStyle variation="strong">&nbsp; or &nbsp;</TextStyle>
      <Button destructive outline onClick={cancelForm} loading={updating}>
        Huỷ
      </Button>
    </>
  );

  const detailModal = (
    <Modal
      open={showEditModel}
      onClose={closeModal}
      title="Xem thông báo"
      titleHidden
      primaryAction={{
        content: "Huỷ bỏ",
        destructive: true,
        disabled: entity?.notification_status,
        onAction: discardContentConfirm,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: closeModal,
        },
      ]}
    >
      <Modal.Section>
        {!entity && !loading ? (
          <Subheading>Oh! Không tìm thấy dữ liệu!</Subheading>
        ) : (
          <>
            <strong>{entity?.notification_title}</strong>
            <br />
            <br />
            <p style={{ fontWeight: "bold" }}>{entity?.notification_content}</p>
            <div style={{ display: "flex" }}>
              <p style={{ fontWeight: "bold" }}>Publish date:&nbsp;</p>
              <p>
                <time>
                  {entity?.notification_schedule
                    ? dateandtime.format(
                        new Date(Number(entity.notification_schedule)),
                        "DD-MM-YYYY HH:mm:ss"
                      )
                    : "-"}
                </time>
              </p>
            </div>
            <div
              style={{ display: "flex", float: "right", marginRight: "1.6rem" }}
            >
              <p style={{ fontWeight: "bold" }}>Tạo bởi:&nbsp;</p>
              <p>{entity?.createBy}</p>
            </div>
            <br />
            {showFormEdits ? editForm : null}
            {!showFormEdits ? <></> : null}
            <br />
            <br />
            <Button icon={EditMinor} onClick={showFormEdit}>
              Edit
            </Button>
            <Stack>
              {deleteConfirm ? (
                <Stack.Item>
                  <TextStyle>
                    Are you sure want to delete this notification?
                  </TextStyle>
                  <br />
                  <br />
                  <Button
                    destructive
                    onClick={discardContent}
                    loading={updating}
                  >
                    Yes
                  </Button>
                  <Button onClick={closeModal}>No</Button>
                </Stack.Item>
              ) : null}
            </Stack>
          </>
        )}
      </Modal.Section>
    </Modal>
  );

  /**
   * END QUERY NOTIFICATION DETAIL
   */
  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (notification: any) => {
    const {
      notification_id,
      notification_title,
      notification_content,
      notification_status,
      notification_channel,
      notification_user,
      notification_data,
      createAt,
    } = notification;
    return [
      <div
        className="clickable"
        key={notification_id + "_noti_id"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        {notification_id}
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_title"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        {notification_title}
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_content"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        <p>{helpers.trimContentString(notification_content)}</p>
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_data"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        <p>
          {notification_data && notification_data.type_action ? (
            <p>
              <TextStyle variation="strong">Type action:&nbsp;</TextStyle>
              {notification_data.type_action}
            </p>
          ) : (
            ""
          )}
        </p>
        <p>
          {notification_data && notification_data.param ? (
            <p>
              <TextStyle variation="strong">Param:&nbsp;</TextStyle>
              {helpers.trimContentString(notification_data.param)}
            </p>
          ) : (
            ""
          )}
        </p>
      </div>,
      <div
        className="small-icon"
        key={notification_id + "_noti_status"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        {notification_status === "publish" ? <LockMinor /> : <TickSmallMinor />}
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_channel"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        <p>
          {notification_channel === "/topics/all"
            ? "All"
            : notification_channel === "/topics/advisor"
            ? "Advisor only"
            : notification_channel === "/topics/advisor"
            ? "User only"
            : ""}
        </p>
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_userID"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        <div style={{ textAlign: "center" }}>
          <p>{notification_user}</p>
        </div>
      </div>,
      <div
        className="clickable"
        key={notification_id + "_noti_createAt"}
        onClick={() => getNotificationDetail(notification_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };

  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "ID",
            "Tiêu đề",
            "Nội dung",
            "Dữ liệu",
            "Trạng thái",
            "Kênh",
            "User ID",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          margin-left: 0.6rem;
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page>
        {detailModal}
        <Layout>
          <Layout.Section>
            <Card>{PagesList}</Card>
            <br />
            {totalItems > 0 ? (
              <Pagination
                TotalRecord={totalItems}
                onChangePage={onChangePageNumber}
                pageSize={Number(mainQuery.limit)}
                activeCurrentPage={Number(mainQuery.page)}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {loading ? <Loading /> : null}
      {Actual_page}
    </>
  );
}
