import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import user_role_list from "./user_role.list";
import user_role_edit from "./user_role.edit";
import user_role_view from "./user_role.view";

/**
 *   Create index file for User_role
 */

export default function List_user_role() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = user_role_list;
      break;

    case "edit":
      ActualPage = user_role_edit;
      break;

    case "new":
      ActualPage = user_role_edit;
      break;

    case "view":
      ActualPage = user_role_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
