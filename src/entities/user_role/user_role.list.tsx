import {
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/user_role.store.reducer";
import helpers from "../../helpers";
import SkeletonLoading from "components/skeleton_loading";
import GeneralUserRoleCreate from "./user_role.create";
import dateandtime from "date-and-time";

export default function User_Role_List() {
  const entities = useAppSelector((state) => state.user_role.entities);
  const loading = useAppSelector((state) => state.user_role.loading);
  const errorMessage = useAppSelector((state) => state.user_role.errorMessage);
  const totalItems = useAppSelector((state) => state.user_role.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(
    () => setNewModelactive((active) => !active),
    []
  );

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "role_id,desc",
    },
    ...StringQuery,
  });

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/user_role" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  /**
   *
   * @param role_id
   */
  const shortcutActions = (role_id: number) => {
    history("edit/" + role_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (user_role: any) => {
    const {
      role_id,
      role_name,
      role_content,
      createAt,
      updateAt,
      createBy,
    } = user_role;
    return [
      <div
        className="clickable"
        key={role_id + "_role_id"}
        onClick={() => shortcutActions(role_id)}
      >
        {role_id}
      </div>,
      <div
        className="clickable"
        key={role_id + "_role_name"}
        onClick={() => shortcutActions(role_id)}
      >
        {role_name}
      </div>,
      <div
        className="clickable"
        key={role_id + "_role_content"}
        onClick={() => shortcutActions(role_id)}
      >
        {role_content.replace(/,/g, " |")}
      </div>,
      <div
        className="clickable"
        key={role_id + "_role_createAt"}
        onClick={() => shortcutActions(role_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
      <div
        className="clickable"
        key={role_id + "_role_updateAt"}
        onClick={() => shortcutActions(role_id)}
      >
        <div style={{ textAlign: "center" }}>
          <time>
            {updateAt
              ? dateandtime.format(
                  new Date(Number(updateAt)),
                  "DD-MM-YYYY HH:mm:ss"
                )
              : "-"}
          </time>
        </div>
      </div>,
      <div
        className="clickable"
        key={role_id + "_role_createBy"}
        onClick={() => shortcutActions(role_id)}
      >
        <div style={{ textAlign: "center" }}>{createBy}</div>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "ID",
            "Tên vai trò",
            "Nội dung vai trò",
            "Thời gian tạo",
            "Thời gian cập nhập",
            "Tạo bởi",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        title="User role"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>{PagesList}</Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <GeneralUserRoleCreate
        show={newModelactive}
        onClose={() => setNewModelactive(false)}
      />
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
