import {
  Card,
  DisplayText,
  EmptyState,
  Form,
  FormLayout,
  Heading,
  Layout,
  Modal,
  Page,
  PageActions,
  TextContainer,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
} from "../../store/user_role.store.reducer";
import { lengthLessThan, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";
import { USER_CAPACITY_LIST } from "constant/permission";

export default function Edit_user_role() {
  const entity = useAppSelector((state) => state.user_role.entity);
  const updating = useAppSelector((state) => state.user_role.updating);
  const loading = useAppSelector((state) => state.user_role.loading);
  const errorMessage = useAppSelector((state) => state.user_role.errorMessage);
  const updateSuccess = useAppSelector((state) => state.user_role.errorMessage);

  const dispatch = useAppDispatch();

  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Vai trò người dùng đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  //Get list role
  const [choiceListSelected, setChoiceListSelected] = useState<any>([]);

  useEffect(() => {}, [choiceListSelected]);

  const handleChoiseListChange = (value) => {
    if (value.target.checked) {
      let choiceListPrepare = [...choiceListSelected, ...[value.target.value]];
      setChoiceListSelected(choiceListPrepare);
    } else {
      let uncheckItem = choiceListSelected.filter((item, index) => {
        if (item !== value.target.value) {
          return true;
        } else {
          return false;
        }
      });
      setChoiceListSelected(uncheckItem);
    }
  };

  useEffect(() => {
    if (entity) {
      let roleList = entity.role_content;
      let roleArray = roleList.split(", ");
      setChoiceListSelected(roleArray);
    }
  }, [entity]);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.user_role_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset_user_role);
  }, []);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    role_name: useField<string>({
      value: entity?.role_name ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    role_content: useField<string>({
      value: entity?.role_content ?? "",
      validates: [],
    }),
  };

  const { submit, reset: reset_user_role } = useForm({
    fields: useFields,
    async onSubmit() {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              role_id: entity.role_id,
              role_content: choiceListSelected.join(", "),
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page
      title="User role"
      breadcrumbs={[{ content: "User_role list", url: "/user_role" }]}
    >
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  /**
   * List permission
   * @returns JSX
   */
  function PermissionList() {
    let HTML_OUTPUT: any = [];

    if (typeof USER_CAPACITY_LIST === "object")
      for (let capacity_heading in USER_CAPACITY_LIST) {
        // heading: capacity_heading
        HTML_OUTPUT.push(
          <>
            <div className="heading-role" style={{ fontStyle: "italic" }}>
              <Heading>{capacity_heading}</Heading>
              <br />
            </div>
          </>
        );
        let all_capacity = USER_CAPACITY_LIST[capacity_heading];
        if (Array.isArray(all_capacity)) {
          for (let capacity_element of all_capacity) {
            if (capacity_element) {
              // element: capacity_element
              HTML_OUTPUT.push(
                <>
                  <input
                    type="checkbox"
                    id="element"
                    name="element"
                    value={capacity_element}
                    onClick={handleChoiseListChange}
                    checked={
                      choiceListSelected.indexOf(capacity_element) !== -1
                    }
                  />
                  <label htmlFor={capacity_element}>{capacity_element}</label>
                  <br />
                  <br />
                </>
              );
            }
          }
        }
      }
    return HTML_OUTPUT;
  }

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="User role"
          breadcrumbs={[{ content: "User_role list", url: "/user_role" }]}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <DisplayText size="medium">
                        {entity.role_name}
                      </DisplayText>
                      <PermissionList />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá vai trò người dùng?"
      primaryAction={{
        content: "Xoá",
        destructive: true,
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
