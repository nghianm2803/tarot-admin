import {
  Form,
  FormLayout,
  Heading,
  Modal,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import { clearError, createEntity } from "../../store/user_role.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import { USER_CAPACITY_LIST } from "constant/permission";

/**
 *   Create upload Modal for User role
 */
export default function GeneralUserRoleCreate({ onClose, show }) {
  const updating = useAppSelector((state) => state.user_role.updating);
  const errorMessage = useAppSelector((state) => state.user_role.errorMessage);

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.user_role_slug || false;

  const [choiceListSelected, setChoiceListSelected] = useState<any>([]);

  const handleChoiseListChange = (value) => {
    if (value.target.checked) {
      let choiceListPrepare = [...choiceListSelected, ...[value.target.value]];
      setChoiceListSelected(choiceListPrepare);
    } else {
      let uncheckItem = choiceListSelected.filter((item, index) => {
        if (item !== value.target.value) {
          return true;
        } else {
          return false;
        }
      });
      setChoiceListSelected(uncheckItem);
    }
  };

  useEffect(() => {
    reset_user_role();
  }, []);

  const useFields = {
    role_name: useField<string>({
      value: "",
      validates: [
        lengthLessThan(50, "Không được dài hơn 200 ký tự."),
        notEmpty("Tên vai trò trống!"),
      ],
    }),
    role_content: useField<string>({
      value: "",
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    dirty,
    reset: reset_user_role,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          dispatch(
            createEntity({
              role_name: values.role_name,
              role_content: choiceListSelected.join(", "),
            })
          );
        }
        reset_user_role();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  /**
   * List permission
   * @returns JSX
   */
  function PermissionList() {
    let HTML_OUTPUT: any = [];
    if (typeof USER_CAPACITY_LIST === "object")
      for (let capacity_heading in USER_CAPACITY_LIST) {
        // heading: capacity_heading
        HTML_OUTPUT.push(
          <>
            <div className="heading-role">
              <Heading>{capacity_heading}</Heading>
              <br />
            </div>
          </>
        );
        let all_capacity = USER_CAPACITY_LIST[capacity_heading];
        if (Array.isArray(all_capacity)) {
          for (let capacity_element of all_capacity) {
            if (capacity_element) {
              // element: capacity_element
              HTML_OUTPUT.push(
                <>
                  <input
                    type="checkbox"
                    id="element"
                    name="element"
                    value={capacity_element}
                    onClick={handleChoiseListChange}
                    checked={
                      choiceListSelected.indexOf(capacity_element) !== -1
                    }
                  />
                  <label htmlFor={capacity_element}>{capacity_element}</label>
                  <br />
                  <br />
                </>
              );
            }
          }
        }
      }
    return HTML_OUTPUT;
  }

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo vai trò mới"}
      primaryAction={{
        content: "Lưu",
        onAction: submit,
        disabled: !dirty,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <FormLayout>
            <TextField
              autoFocus
              autoComplete="off"
              label="Role name"
              {...fields.role_name}
            />
            <PermissionList />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
