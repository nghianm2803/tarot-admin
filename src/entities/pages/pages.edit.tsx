import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/posts.store.reducer";
import {
  getEntity as getUserEntity,
  reset as resetUser,
} from "../../store/users.store.reducer";
import { lengthLessThan, lengthMoreThan, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function PageEdit() {
  const entityUser = useAppSelector((state) => state.users.entity);
  const entity = useAppSelector((state) => state.posts.entity);
  const entities = useAppSelector((state) => state.posts.entities);
  const updating = useAppSelector((state) => state.posts.updating);
  const loading = useAppSelector((state) => state.posts.loading);
  const errorMessage = useAppSelector((state) => state.posts.errorMessage);
  const updateSuccess = useAppSelector((state) => state.posts.updateSuccess);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState(null);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.pages_slug || false;

  let useDataParam = {} as any;
  useDataParam = useParams();
  let UserParam = useDataParam.users_slug || false;

  useEffect(() => {
    if (UserParam) {
      dispatch(getUserEntity(UserParam));
    } else dispatch(resetUser());
  }, [entityUser]);

  useEffect(() => {
    if (Param) {
      dispatch(getEntity(Param));
    } else dispatch(reset());
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Thông tin đã được cập nhật!");
    }
  }, [updateSuccess]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    navigate(-1);
  }

  const useFields = {
    post_title: useField<string>({
      value: entity?.post_title ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    post_excerpt: useField<string>({
      value: entity?.post_excerpt ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    post_content: useField<string>({
      value: entity?.post_content ?? "",
      validates: [lengthLessThan(10000, "Nội dung quá dài"), lengthMoreThan(2, "Nội dung quá ngắn")],
    }),

    createBy: useField<any>({
      value: entity?.createBy ?? "",
      validates: [],
    }),
  };

  const { fields, submit, dirty } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              post_title: values.post_title,
              post_content: values.post_content,
              post_excerpt: values.post_excerpt,
              createBy: Number(values.createBy),
            })
          );
        } else {
          dispatch(
            updateEntity({
              post_id: entity.post_id,
              post_title: values.post_title,
              post_excerpt: values.post_excerpt,
              post_content: values.post_content,
              createBy: Number(values.createBy),
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Pages" breadcrumbs={[{ content: "Page list", url: "/pages" }]}>
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  let advisorContent = null;
  try {
    advisorContent = JSON.parse(entity.post_content);
  } catch (e) {
    advisorContent = String(advisorContent);
  }

  const toastMarkup = notification ? <Toast content={notification} onDismiss={toggleActive} /> : null;

  const getDisplayName = (posts: any) => {
    const { post_id, user } = posts;
    return (
      <>
        <div className="clickable" key={post_id + "post_display_name"}>
          {user.display_name}
        </div>
      </>
    );
  };

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Pages"
          breadcrumbs={[{ content: "Page list", url: "/pages" }]}
          primaryAction={{
            content: "Lưu",
            disabled: !dirty,
            loading: updating,
            onAction: submit,
          }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      {entity.post_type === "advisor_post" ? (
                        <>
                          <div>
                            {advisorContent.content ? (
                              <>
                                <TextField autoComplete="off" label="Created by advisor ID" {...fields.createBy} />
                                <TextField
                                  autoComplete="off"
                                  multiline={2}
                                  label="Content"
                                  value={advisorContent.content}
                                />
                              </>
                            ) : (
                              ""
                            )}
                          </div>
                          <br />
                          {advisorContent.images &&
                            advisorContent.images.map((img: any, index: number) => (
                              <img
                                src={img}
                                style={{
                                  display: "block",
                                  marginLeft: "auto",
                                  marginRight: "auto",
                                  marginTop: "5px",
                                  width: "40%",
                                }}
                                crossOrigin="anonymous"
                                alt="Advisor post image"
                              />
                            ))}
                        </>
                      ) : (
                        <>
                          <TextField autoComplete="off" autoFocus label="Tiêu đề" {...fields.post_title} />
                          <TextField
                            autoComplete="off"
                            maxLength={250}
                            label="Excerpt"
                            {...fields.post_excerpt}
                            multiline={2}
                          />
                          <TextField
                            autoComplete="off"
                            maxLength={2000}
                            label="Content"
                            {...fields.post_content}
                            multiline={4}
                          />
                        </>
                      )}
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá dữ liệu?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

