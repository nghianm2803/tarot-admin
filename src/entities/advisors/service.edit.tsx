import {
  Card,
  DisplayText,
  DropZone,
  Form,
  FormLayout,
  Modal,
  Select,
  TextField,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../config/store";
import { getEntity, updateEntity } from "store/services.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import helpers from "helpers";
import QuickUploadImage from "components/oneclick-upload";

export default function EditService({ toggle_show_edit_modal, service_id }) {
  const entity: any = useAppSelector((state) => state.services.entity);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(service_id));
  }, [service_id]);

  const [showEditModel, setShowEditModel] = useState<boolean>(true);

  /**
   * Upload thumbnail premium service
   */
  const [thumb, setThumb] = useState<string>("");
  const [srcThumb, setSrcThumb] = useState<any>(entity?.service_thumbnail);
  const [internalErrorNotice, setInternalErrorNotice] = useState<string>("");

  function handQuickUpdateSuccess(res: any) {
    let media_root = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;
    setSrcThumb(media_root + "" + res.media_filename);
    setThumb(`${process.env.REACT_APP_AJAX_UPLOAD_IMAGE}` + res.media_filename);
  }

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  /**
   * Duration form
   */
  const [typeDurationValue, setTypeDurationValue] = useState<string>("hour");
  const handleTypeDurationChange = useCallback(
    (value) => setTypeDurationValue(value),
    []
  );

  const closeModal = useCallback(() => {
    setShowEditModel((active) => !active);
  }, [showEditModel]);

  useEffect(() => {
    setShowEditModel(!showEditModel);
  }, [toggle_show_edit_modal]);

  const {
    fields,
    dirty,
    submit,
    submitting,
    reset: resetForm,
  } = useForm({
    fields: {
      service_name: useField<string>({
        value: entity?.service_name ?? "",
        validates: [
          notEmpty("Tên dịch vụ trống!"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự"),
          lengthMoreThan(6, "Tên dịch vụ quá ngắn"),
        ],
      }),

      service_description: useField<string>({
        value: entity?.service_description ?? "",
        validates: [
          notEmpty("Mô tả dịch vụ trống!"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
          lengthMoreThan(6, "Mô tả quá ngắn."),
        ],
      }),

      service_category: useField<string>({
        value: entity?.service_category + "" ?? "",
        validates: [
          notEmpty("Category dịch vụ trống!"),
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Service category must be number!";
            }
          },
        ],
      }),

      service_duration: useField<string>({
        value: entity?.service_duration + "" ?? "",
        validates: [
          notEmpty("Thời gian dịch vụ trống!"),
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Thời gian dịch vụ phải là số!";
            }
          },
        ],
      }),

      service_price: useField<string>({
        value: entity?.service_price + "" ?? "",
        validates: [
          notEmpty("Giá dịch vụ trống!"),
          lengthLessThan(
            6,
            "Giá dịch vụ không được lớn hơn 5 chữ số."
          ),
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Giá dịch vụ phải là số!";
            }
          },
        ],
      }),
    },
    async onSubmit(values) {
      try {
        dispatch(
          updateEntity({
            service_id: entity.service_id,
            service_name: values.service_name,
            service_duration: Number(values.service_duration),
            service_category: Number(values.service_category),
            service_price: Number(values.service_price),
            service_description: values.service_description,
            service_thumbnail: thumb,
          })
        );
        setShowEditModel(false);
        resetForm();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * read out service detail
   */
  const detailModal = (
    <Modal
      activator={null}
      open={showEditModel}
      onClose={closeModal}
      title="Edit service"
      primaryAction={{
        content: "Lưu",
        // disabled: !dirty,
        loading: submitting,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: closeModal,
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card.Section>
            <FormLayout>
              <DisplayText size="small">Thumbnail</DisplayText>
              <div id="profile_heading">
                <div className="service_thumbnail_inner">
                  <div className="service_thumbnail-avatar">
                    <QuickUploadImage
                      onSuccess={handQuickUpdateSuccess}
                      onError={handUploadError}
                    />
                    <DropZone>
                      <img
                        src={srcThumb}
                        crossOrigin="anonymous"
                        alt="Upload service thumbnail here"
                        style={{ width: "115px" }}
                      />
                    </DropZone>
                  </div>
                </div>
              </div>
              <br />
              <TextField
                label="Tên dịch vụ"
                autoComplete="off"
                {...fields.service_name}
              />
              <TextField
                label="Category"
                autoComplete="off"
                {...fields.service_category}
              />
              <TextField
                label="Thời gian"
                type="number"
                min="1"
                autoComplete="off"
                {...fields.service_duration}
                connectedRight={
                  <Select
                    value={typeDurationValue}
                    label="Đơn vị thời gian"
                    onChange={handleTypeDurationChange}
                    labelHidden
                    options={["second", "minute"]}
                  />
                }
              />
              <TextField
                label="Giá"
                type="number"
                min="0"
                autoComplete="off"
                {...fields.service_price}
              />
              <TextField
                label="Mô tả"
                maxLength={200}
                multiline={2}
                autoComplete="off"
                showCharacterCount
                {...fields.service_description}
              />
            </FormLayout>
          </Card.Section>
        </Form>
      </Modal.Section>
    </Modal>
  );
  return <>{detailModal}</>;
}
