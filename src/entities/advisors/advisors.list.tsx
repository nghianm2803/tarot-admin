import { Card, DataTable, EmptyState, Layout, Page, Stack, Toast, Icon } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { StarFilledMinor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/users.store.reducer";
import SearchFilter from "./filter";
import helpers from "../../helpers";
import date from "date-and-time";
import CountryName from "components/countries";
import SkeletonLoading from "components/skeleton_loading";

export default function Advisor_List() {
  const entities = useAppSelector((state) => state.users.entities);
  const loading = useAppSelector((state) => state.users.loading);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const totalItems = useAppSelector((state) => state.users.totalItems);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "user_id,desc",
      user_role: "advisor",
    },
    ...StringQuery,
  });

  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback((value) => setQueryValue(value), []);

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/advisors" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   * @param user_id
   */
  const shortcutActions = (user_id: number) => {
    history("/advisors/edit/" + user_id);
  };

  const emptyData = (
    <EmptyState heading="Không có chuyên gia nào ở đây!" image={emptyIMG}>
      <p>Oh! Không có chuyên gia nào ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const handleSort = (index: any, direction: string) => {
    let _direction = direction === "descending" ? "desc" : "asc";
    let sort = "";
    sort = "createAt," + _direction;
    setMainQuery({ ...mainQuery, sort: sort });
  };

  /**
   * Get nation name
   * @param code string
   * @returns string
   */
  // function handleGetNationNameByCode(code: string): string {
  //   if (code) {
  //     let data = CountryName().filter((item, index) => {
  //       if (item.alpha2 === code) {
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     });
  //     if (data && data[0]) {
  //       return data[0].name;
  //     }
  //   }
  //   return code;
  // }

  const renderItem = (users: any) => {
    const {
      user_id,
      user_avatar,
      user_email,
      display_name,
      average_rating,
      createAt,
      lastActive,
    } = users;
    return [
      <div className="clickable" key={user_id + "_user_id"} onClick={() => shortcutActions(user_id)}>
        {user_id}
      </div>,
      <div
        className="clickable"
        style={{ textAlign: "center" }}
        key={user_id + "_user_avatar"}
        onClick={() => shortcutActions(user_id)}
      >
        <div className="user_avatar">
          <img src={user_avatar ? user_avatar : user_id?.user_avatar} crossOrigin="anonymous" />
        </div>
      </div>,
      <div className="clickable" key={user_id + "_user_name"} onClick={() => shortcutActions(user_id)}>
        {display_name}
        <div style={{ fontWeight: "bold", color: "#999" }}>{user_email}</div>
      </div>,
      <div className="clickable" key={user_id + "_user_rating"} onClick={() => shortcutActions(user_id)}>
        <div style={{ textAlign: "center", display: "inline-flex" }}>
          {Math.round(average_rating * 10) / 10}
          <Icon source={StarFilledMinor} color="warning" />
        </div>
      </div>,
      // <div className="clickable" key={user_id + "_user_address"} onClick={() => shortcutActions(user_id)}>
      //   {user_address}
      //   <p>{handleGetNationNameByCode(user_nation) || ""} </p>
      // </div>,
      <div className="clickable" key={user_id + "user_lastActive"} onClick={() => shortcutActions(user_id)}>
        <time>{lastActive ? date.format(new Date(Number(lastActive)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={user_id + "_user_createAt"} onClick={() => shortcutActions(user_id)}>
        <time>{createAt ? date.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const AdvisorsList =
    entities.length > 0 ? (
      <>
        <DataTable
          sortable={[false, false, false, false, false, true]}
          defaultSortDirection="descending"
          initialSortColumnIndex={6}
          onSort={handleSort}
          columnContentTypes={["text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Avatar", "Tên người dùng", "Đánh giá", "Hoạt động lần cuối", "Thời gian tạo"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
          .clickable {
            margin: -1.6rem;
            padding: 1.6rem;
            cursor: pointer;
          }
          .small-icon {
            margin-left: 0.6rem;
            font-size: 12px;
            padding: 0;
            width: 15px;
            height: auto;
          }
        `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page title="Chuyên gia">
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack distribution="equalSpacing">
                  <SearchFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                </Stack>
              </div>
              {AdvisorsList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

