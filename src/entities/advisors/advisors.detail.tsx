import {
  Button,
  ButtonGroup,
  Card,
  DropZone,
  EmptyState,
  Form,
  FormLayout,
  Icon,
  Layout,
  Loading,
  Modal,
  Page,
  RadioButton,
  Select,
  Stack,
  Tabs,
  TextContainer,
  TextField,
  TextStyle,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import { clearError, getEntity, updateEntity, reset } from "store/users.store.reducer";
import { getEntities as getServices, updateEntity as updateGeneralService } from "store/services.store.reducer";
import { CashDollarMajor } from "@shopify/polaris-icons";
import {
  getEntity as getUser_meta,
  createEntity as createUser_meta,
} from "store/user_meta.store.reducer";
import { autoGenerateService, getEntities as getGeneralService } from "store/general_service.store.reducer";
import { lengthLessThan, notEmpty, useField, useForm } from "@shopify/react-form";
import helpers from "helpers";
import countriesData from "components/countryData";
import CreatePremiumService from "./service.create";
import { getEntity as getImages } from "store/media.store.reducer";
import EditService from "./service.edit";
import SkeletonLoading from "components/skeleton_loading";
import dateandtime from "date-and-time";
import VideoPlayer from "react-video-js-player";
import { presignService } from "store/presign.store.reducer";
import { s3Service } from "store/s3.services";
import { getEntities as getUserCategories } from "../../store/taxonomy_relationship.category.store.reducer";
import { getEntities as getUserTopic } from "../../store/taxonomy_relationship.topic.store.reducer";
import QuickUploadImage from "components/oneclick-upload";

export default function EditAdvisor() {
  const entity: any = useAppSelector((state) => state.users.entity);
  const updating = useAppSelector((state) => state.users.updating);
  const loading = useAppSelector((state) => state.users.loading);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const _updateService = useAppSelector((state) => state.services.updateSuccess);
  const servicesList = useAppSelector((state) => state.services.entities);
  const _premiumService = useAppSelector((state) => state.services.entity); // ignore
  const entityUser_meta = useAppSelector((state) => state.user_meta.entity);
  const entitiesTopic = useAppSelector((state) => state.taxonomy_relationship_topic.entities);
  const entitiesCategories = useAppSelector((state) => state.taxonomy_relationship_category.entities);

  const dispatch = useAppDispatch();

  let useParam = {} as any;
  useParam = useParams();
  let _advisor_id = useParam.advisors_slug || false;

  const [message, setMessage] = useState<string | null>(null);
  const [notification, setNotification] = useState<string | null>(null);

  const [uploadModelactive, setUploadModelactive] = useState<boolean>(false);
  const toggleUpdateActive = useCallback(() => setUploadModelactive((active) => !active), []);

  // Upload sample video
  const [videoSample, setVideoSample] = useState<any>([]);
  const [categoryAdvisor, setCategoryAdvisor] = useState<string>("");
  const [topicAdvisor, setTopicAdvisor] = useState<string>("");
  const [videoSampleList, setVideoSampleList] = useState<any>([]);
  const [openModal, setOpenModal] = useState<boolean>(false);

  // Delete Sample Video
  // function onModalAgree(meta_id: string | number) {
  //   dispatch(deleteUser_meta(meta_id));
  //   setOpenModal(false);
  // }

  const [srcImageAvatar, setSrcImageAvatar] = useState<string>("");
  useEffect(() => {
    if (entity && entity.user_avatar) {
      setSrcImageAvatar(entity.user_avatar);
    }
  }, [entity]);

  function handQuickUpdateSuccess(res: any) {
    setSrcImageAvatar(process.env.REACT_APP_AJAX_UPLOAD_IMAGE + res.media_filename);
  }

  const [internalErrorNotice, setInternalErrorNotice] = useState("");

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  useEffect(() => {
    if (entityUser_meta && entityUser_meta.length > 0) {
      let allEntitiesVideoSample: any = [];
      allEntitiesVideoSample = entityUser_meta.filter((el) => {
        return el.meta_key === "video_sample";
      });
      setVideoSampleList(allEntitiesVideoSample);
    }
  }, [entityUser_meta]);

  /**
   *  Get Category and Topic
   *  Category and Topic in contactform_content
   *  -> Get contactform_content.category
   */
  useEffect(() => {
    dispatch(getUserCategories({ object_type: "advisor_category", object_id: _advisor_id }));
    dispatch(getUserTopic({ object_type: "advisor_topic", object_id: _advisor_id }));
  }, []);

  useEffect(() => {
    if (entitiesTopic.length > 0) {
      for (let e of entitiesTopic) {
        setTopicAdvisor(e.taxonomy.taxonomy_name);
      }
    }
  }, [entitiesTopic]);

  useEffect(() => {
    if (entitiesCategories.length > 0) {
      for (let e of entitiesCategories) {
        setCategoryAdvisor(e.taxonomy.taxonomy_name);
      }
    }
  }, [entitiesCategories]);

  /**
   * Xác định xem file nào đang upload --------- From media upload
   */
  const [uploading, setUploading] = useState(false);

  // Auto Generate general service hook ----------- Sync data User -> Advisor
  const generateService = useCallback(() => {
    dispatch(autoGenerateService(_advisor_id));
    // dispatch(getBecomeAdvisorData(_advisor_id));
    dispatch(getGeneralService(_advisor_id));
    setTimeout(() => {
      window.location.reload();
      dispatch(
        getServices({
          createBy: _advisor_id,
        })
      );
    }, 1000);
  }, []);

  const [hideAutoGenerate, setHideAutoGenerate] = useState<boolean>(false);
  const [hidePremiumService, setHidePremium] = useState<boolean>(true);

  useEffect(() => {
    if (entity) {
      if (entity.is_premium === 1) {
        setHidePremium(false);
      } else {
        setHidePremium(true);
      }
    }
  }, [entity]);

  /**
   * Filter service group here <---------------
   */
  const _service = servicesList.length > 0 ? servicesList : [];
  const general_service = _service.filter((svr) => svr.service_group === "general_service");
  const premium_service = _service.filter((svr) => svr.service_group === "premium_service");

  /**
   * Auto turn of Auto generate service button. Do not do twice ...
   */
  useEffect(() => {
    if (general_service.length > 0) setHideAutoGenerate(true);
  }, [servicesList]);

  const [showModalEditor, setShowModalEditor] = useState<boolean>(false);

  /**
   * General service active hook
   * Jamviet.com refactor ...
   */

  // _normalService?.service_active
  const [isServiceActive, setIsServiceActive] = useState<any>([]);
  // On Button
  const handleServiceOn = useCallback(
    (service_id: number) => {
      if (isServiceActive.indexOf(service_id) > -1) return;
      setIsServiceActive([...isServiceActive, ...[service_id]]);
      handleCallbackUpdateAllService(service_id, 1);
    },
    [isServiceActive]
  );

  // Off button
  const handleServiceOff = useCallback(
    (service_id: number) => {
      if (isServiceActive.indexOf(service_id) < 0) return;
      let NewArray = isServiceActive.filter((el: number) => {
        return el !== service_id;
      });
      setIsServiceActive(NewArray);
      handleCallbackUpdateAllService(service_id, 0);
    },
    [isServiceActive]
  );

  /**
   * Default active button or not
   * @param service_id
   */
  useEffect(() => {
    let allowList: number[] = [];
    if (servicesList)
      for (let e of servicesList) {
        if (e.service_active === 1) {
          allowList.push(e.service_id);
        }
      }
    setIsServiceActive(allowList);
  }, [servicesList]);

  /**
   * Jamviet.com uploadMedia
   * Thực hiện upload ở đây,m trả về một promise
   * File nằm trong videoSample, upload lên và trả về URL rồi mới add vào META
   */
  const [uploadResult, setUploadResult] = useState<string[]>([]);

  useEffect(() => {
    videoSample.push(uploadResult);
  }, [uploadResult]);

  async function handleCreatePresignUrl(file: { name: string; type: string }) {
    console.info("Created a presignUrl");
    let userToken = "";
    try {
      userToken = localStorage.getItem("session");
    } catch (error) {}
    let dataReturn = await presignService
      .createNewPresign(file.name, file.type, userToken)
      .then((response) => {
        if (response.data) {
          return response.data;
        } else {
          return null;
        }
      })
      .catch((error) => {
        return null;
      });
    return dataReturn;
  }

  /**
   * @param {*} dataFile
   */
  async function handleSubmitFile(dataFile) {
    let fileArray = dataFile;

    //this.isLoadingMedia = true;
    for (var i = 0; i < fileArray.length; i++) {
      let file = fileArray[i];
      let fileTypeAllowed = ["image/jpeg", "image/jpg", "image/gif", "image/png", "video/mp4", "video/quicktime"];
      if (fileTypeAllowed.indexOf(file.type) === -1) {
        alert("Not support this file");
        return false;
      }
      let mediaPresign = await handleCreatePresignUrl(file);

      if (mediaPresign && mediaPresign.url) {
        let urlPut = mediaPresign.url;

        let newDataPromise = new Promise((resolve, reject) => {
          const reader = new FileReader();
          let fileType = file.type;
          reader.onload = () => {
            try {
              setUploading(true);
              s3Service
                .createUserMeta(urlPut, fileType, reader.result)
                .then(function (response) {
                  if (response) {
                    resolve(true);
                  } else {
                    reject(false);
                  }
                })
                .catch(function () {
                  reject(false);
                });
            } catch (err) {
              reject(false);
            }
          };
          reader.onerror = () => {
            reject(false);
          };
          reader.readAsArrayBuffer(file);
        });

        //Data Promise
        let dataReturn = await newDataPromise.then(
          (response) => {
            return response;
          },
          () => {
            return null;
          }
        );
        if (dataReturn) {
          let urlOriginal = [];
          let dataArrayUrl = mediaPresign.url?.split("?");
          if (dataArrayUrl[0]) {
            urlOriginal = dataArrayUrl[0];
          }
          setUploading(false);
          setUploadResult(urlOriginal);
        } else {
          alert("Not support this file");
        }
      } else {
        alert("Not support this file");
      }
    }
  }

  /**
   * Hand change all button callback, then dispatch
   * It can handle ALL., ALL Button, if change, then dispatch
   * Jamviet.com refactor
   * This function is an example, use useEffect to listen to isServiceActive change
   * All buttons are the same, you dont need to devide into 2 states
   */

  function handleCallbackUpdateAllService(service_id: number, mode: number) {
    // Wrong, dispatch update ALL service, because general service and normal service is the same endpoint...
    dispatch(
      updateGeneralService({
        service_id: service_id,
        service_active: mode,
      })
    );
  }

  /**
   * Nation check
   */
  const [user_nation_selected, setUser_nation_selected] = useState<string>("");

  /**
   * Get all country name
   */
  const option_country = countriesData?.countriesData;
  const changeHandler = (value: any) => {
    setUser_nation_selected(value);
  };

  /**
   * Option advisor/normal user account
   */
  const [optionRoleValue, setOptionRoleValue] = useState<boolean>(false);
  const handleRadioButtonChange = useCallback((_checked, newValue) => {
    setOptionRoleValue(newValue === "disabled");
  }, []);

  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback((selectedTabIndex) => setSelected(selectedTabIndex), []);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  useEffect(() => {
    if (_advisor_id) {
      dispatch(getEntity(_advisor_id));
    } else {
      dispatch(reset());
    }
    dispatch(
      getServices({
        createBy: _advisor_id,
      })
    );
  }, []);

  useEffect(() => {
    if (_premiumService && _premiumService.service_thumbnail) {
      dispatch(getImages(_premiumService?.service_thumbnail));
      // dispatch(getImages(_premiumService?.service_id));
    }
  }, [_premiumService]);

  useEffect(() => {
    if (entity && entity.user_role) {
      dispatch(getUser_meta(entity?.user_id));
      setOptionRoleValue(Boolean(entity.user_status));
    }
  }, [entity]);

  useEffect(() => {
    if (entity && entity.user_nation) {
      setUser_nation_selected(entity.user_nation);
      setOptionRoleValue(Boolean(entity.user_status));
    }
  }, [entity]);

  useEffect(() => {
    if (_updateService) {
      setNotification("Thông tin đã được cập nhật!");
      dispatch(getGeneralService(_advisor_id));
      dispatch(
        getServices({
          createBy: _advisor_id,
        })
      );
    }
  }, [_updateService]);

  const [currentServiceId, setCurrentServiceId] = useState<string>("");

  const useFields = {
    user_email: useField<string>({
      value: entity?.user_email ?? "",
      validates: [
        notEmpty("Email đang trống!"),
        (inputValue) => {
          if (!helpers.isEmail(inputValue)) {
            return "Email không hợp lệ!";
          }
        },
      ],
    }),

    display_name: useField<string>({
      value: entity?.display_name ?? "",
      validates: [
        lengthLessThan(50, "Tên hiển thị không được dài quá 50 ký tự"),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Tên hiển thị quá ngắn!";
          }
        },
      ],
    }),

    is_premium: useField<string>({
      value: entity?.is_premium ?? "",
      validates: [],
    }),

    user_numberphone: useField<string>({
      value: entity?.user_numberphone ?? "",
      validates: [],
    }),

    user_avatar: useField<string>({
      value: srcImageAvatar,
      validates: [],
    }),

    average_rating: useField<string>({
      value: entity?.average_rating + "" ?? "",
      validates: [],
    }),
    user_login: useField<string>({
      value: entity?.user_login + "" ?? "",
      validates: [],
    }),
    bio: useField<string>({
      value: entity?.bio + "" ?? "",
      validates: [],
    }),
  };

  const useFieldsSecurity = {
    user_pass: useField<string>({
      value: "",
      validates: [
        (inputValue) => {
          if (helpers.isUTF8(inputValue)) {
            return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
          }
          if (inputValue.length > 0 && helpers.getPasswordStrength(inputValue) < 2) {
            return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
          }
        },
      ],
    }),
  };

  const {
    fields,
    submit: submitGeneral,
    // dirty: dirtyGeneral,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        // check if videoSample is Array, then filter it
        let videoSampleAfterFilter: any = "";
        if (Array.isArray(videoSample) && videoSample.length > 0) {
          videoSampleAfterFilter = videoSample.filter((el) => {
            return typeof el === "string";
          });
        }

        // await uploadMedia();
        dispatch(
          updateEntity({
            user_id: entity.user_id,
            user_email: values.user_email,
            display_name: values.display_name,
            user_numberphone: values.user_numberphone,
            is_premium: Number(values.is_premium),
            user_nation: user_nation_selected,
            average_rating: values.average_rating,
            user_avatar: srcImageAvatar,
            bio: values.bio,
          })
        );

        // deleteUser_meta
        // entityUser_meta // video_sample
        if (videoSampleAfterFilter.length > 0) {
          // buoc 1: tim toan bo video_sample va delete
          // ignore .///

          // buoc 2: tao meta!
          let userMETA: any = [];
          videoSampleAfterFilter.map((el) => {
            userMETA.push({
              user_id: entity.user_id,
              meta_key: "video_sample",
              meta_value: el,
              allow_duplicated: 1,
            });
          });
          dispatch(createUser_meta(userMETA));
        }

        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const {
    fields: fieldSecurity,
    submit: submitSecurity,
    // dirty: dirtySecurity,
  } = useForm({
    fields: useFieldsSecurity,
    async onSubmit(values) {
      try {
        if (!_advisor_id) {
          dispatch(
            updateEntity({
              user_id: entity.user_id,
              user_pass: values.user_pass,
              user_status: Number(optionRoleValue),
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <EmptyState heading="No users here!" image={emptyIMG}>
      <p>Thông tin này không tồn tại!</p>
    </EmptyState>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const tabs = [
    {
      id: "general-tab",
      content: "Thông tin chung",
      panelID: "tab1-content",
    },
    {
      id: "security-tab",
      content: "Thông tin bảo mật",
      panelID: "tab2-content",
    },
  ];

  const processAvailableTime = useCallback((item: string) => {
    let availableTime = null;
    try {
      availableTime = JSON.parse(item);
    } catch (e) {
      availableTime = String(availableTime);
    }
    return availableTime;
  }, []);

  const generalService = general_service?.map((props: any) => {
    const {
      service_id,
      service_name,
      service_duration,
      service_unit,
      service_price,
      service_description,
      service_available_time,
    } = props;
    return (
      <>
        <Card>
          <Card.Section>
            {/**
             * Check if element in array, use indexOf
             */}
            <br />
            <Stack>
              <Stack.Item>
                <div key={service_id + "_nor_name"}>
                  <TextStyle variation="strong">Tên dịch vụ: </TextStyle>
                </div>
                <div key={service_id + "_nor_unit"}>
                  <TextStyle variation="strong">Đơn vị: </TextStyle>
                </div>
                <div key={service_id + "_nor_duration"}>
                  <TextStyle variation="strong">Thời gian: </TextStyle>
                </div>
                <div key={service_id + "_nor_price"}>
                  <TextStyle variation="strong">Giá: </TextStyle>
                </div>
                <div key={service_id + "_nor_description"}>
                  <TextStyle variation="strong">Mô tả: </TextStyle>
                </div>
                <br />
                <div key={service_id + "_nor_available_time"}>
                  <TextStyle variation="strong">Available time: </TextStyle>
                </div>
              </Stack.Item>
              <Stack.Item>
                <div>{service_name}</div>
                <div>{service_unit}</div>
                <div>{service_duration}</div>
                <div style={{ display: "flex" }}>
                  {service_price}
                  <div style={{ marginLeft: "5px" }}>
                    <Icon source={CashDollarMajor} color="warning" />
                  </div>
                </div>
                <div style={{ maxWidth: 170 }}>{service_description}</div>
                <div style={{ maxWidth: 170 }}>
                  {processAvailableTime(service_available_time).start_time
                    ? dateandtime.format(
                        new Date(Number(processAvailableTime(service_available_time).start_time)),
                        "HH:mm"
                      )
                    : ""}{" "}
                  -{" "}
                  {processAvailableTime(service_available_time).end_time
                    ? dateandtime.format(
                        new Date(Number(processAvailableTime(service_available_time).end_time)),
                        "HH:mm"
                      )
                    : ""}
                </div>
              </Stack.Item>
            </Stack>
            <br />
            <ButtonGroup segmented>
              <Button primary={isServiceActive.indexOf(service_id) > -1} onClick={() => handleServiceOn(service_id)}>
                Bật
              </Button>
              <Button pressed={isServiceActive.indexOf(service_id) < 0} onClick={() => handleServiceOff(service_id)}>
                Tắt
              </Button>
            </ButtonGroup>
          </Card.Section>
        </Card>
      </>
    );
  });

  const premiumService = premium_service?.map((props: any) => {
    const {
      service_id,
      service_name,
      service_thumbnail,
      service_category,
      service_unit,
      service_duration,
      service_price,
      service_description,
    } = props;
    return (
      <>
        <Card>
          <Card.Section>
            <Stack>
              <Stack.Item fill>
                <div id="profile_heading">
                  <div className="service_thumbnail_inner">
                    <div className="service_thumbnail-avatar">
                      <img
                        src={service_thumbnail}
                        crossOrigin="anonymous"
                        alt="Service thumbnail here"
                        style={{ width: "115px" }}
                      />
                    </div>
                  </div>
                </div>
              </Stack.Item>
            </Stack>
            <br />
            <Stack>
              <Stack.Item>
                <div key={service_id + "_pre_name"}>
                  <TextStyle variation="strong">Tên dịch vụ: </TextStyle>
                </div>
                <div key={service_id + "_pre_cate"}>
                  <TextStyle variation="strong">Category: </TextStyle>
                </div>
                <div key={service_id + "_pre_unit"}>
                  <TextStyle variation="strong">Đơn vị: </TextStyle>
                </div>
                <div key={service_id + "_pre_duration"}>
                  <TextStyle variation="strong">Thời gian: </TextStyle>
                </div>
                <div key={service_id + "_pre_price"}>
                  <TextStyle variation="strong">Giá: </TextStyle>
                </div>
                <div key={service_id + "_pre_description"}>
                  <TextStyle variation="strong">Mô tả: </TextStyle>
                </div>
              </Stack.Item>
              <Stack.Item>
                <div>{service_name}</div>
                <div>{service_category}</div>
                <div>{service_unit}</div>
                <div>{service_duration}</div>
                <div style={{ display: "flex" }}>
                  {service_price}
                  <div style={{ marginLeft: "5px" }}>
                    <Icon source={CashDollarMajor} color="warning" />
                  </div>
                </div>
                <div style={{ maxWidth: 150 }}>{service_description}</div>
              </Stack.Item>
            </Stack>
            <Stack>
              <Stack.Item fill>
                <br />
                <ButtonGroup segmented>
                  <Button
                    primary={isServiceActive.indexOf(service_id) > -1}
                    onClick={() => handleServiceOn(service_id)}
                  >
                    Bật
                  </Button>
                  <Button
                    pressed={isServiceActive.indexOf(service_id) < 0}
                    onClick={() => handleServiceOff(service_id)}
                  >
                    Tắt
                  </Button>
                </ButtonGroup>
              </Stack.Item>
              <Stack.Item>
                <div className="edit-premium-service">
                  <Page
                    primaryAction={{
                      content: "Sửa",
                      onAction: () => {
                        setCurrentServiceId(service_id);
                        setShowModalEditor(!showModalEditor);
                      },
                      loading: updating,
                    }}
                  />
                </div>
              </Stack.Item>
            </Stack>
          </Card.Section>
        </Card>
      </>
    );
  });

  const generalTabContent = [
    <>
      <Card title="Sửa thông tin">
        <Card.Section>
          <Layout>
            <Layout.Section>
              <Form onSubmit={submitGeneral}>
                {internalErrorNotice ? (
                  <Toast content={internalErrorNotice} error onDismiss={() => setInternalErrorNotice("")} />
                ) : null}
                <FormLayout>
                  <div className="profile-edit-avatar">
                    <QuickUploadImage onSuccess={handQuickUpdateSuccess} onError={handUploadError} />
                    <img src={srcImageAvatar} crossOrigin="anonymous" alt="User Avatar" />
                  </div>
                  <TextField autoFocus autoComplete="off" requiredIndicator label="Email" {...fields.user_email} />
                  <TextField label="Tên hiển thị" requiredIndicator autoComplete="off" {...fields.display_name} />
                  <TextField label="User login" requiredIndicator autoComplete="off" {...fields.user_login} />
                  <TextField
                    label="Premium advisor"
                    helpText="1 là chuyên gia premium. 0 là chuyên gia thường"
                    requiredIndicator
                    autoComplete="off"
                    {...fields.is_premium}
                  />
                  <TextField
                    label="Số điện thoại"
                    autoComplete="off"
                    min={1}
                    type="number"
                    {...fields.user_numberphone}
                  />
                  <TextField
                    label="Đánh giá"
                    type="number"
                    min={0}
                    max={5}
                    autoComplete="off"
                    {...fields.average_rating}
                  />
                  <TextField autoComplete="off" disabled label="Topic" multiline={2} value={topicAdvisor} />
                  <TextField autoComplete="off" disabled label="Category" multiline={2} value={categoryAdvisor} />

                  {/* <TextField label="Paypal (coin)" type="number" min={0} autoComplete="off" {...fields.paypal} /> */}
                  {/* <TextField
                    label="Năm kinh nghiệm"
                    type="number"
                    min={0}
                    autoComplete="off"
                    {...fields.yearExperience}
                  /> */}
                  <Select
                    label="Quốc gia"
                    options={option_country}
                    onChange={changeHandler}
                    value={user_nation_selected}
                  />
                  <TextField multiline={2} label="Bio" autoComplete="off" {...fields.bio} />
                  {/* <p>Certificate</p>
                  <div className="feedback-image" key={"image_cerificate"}>
                    <img src={certificate?.certificate} crossOrigin="anonymous" alt="Certificate image" />
                  </div> */}
                </FormLayout>
                <br />
                {/* Upload Sample video  */}

                {/* <Button>Delete sample video</Button> */}
                {videoSampleList ? (
                  videoSampleList.map((el) => {
                    return (
                      <Stack spacing="loose">
                        <Stack.Item>
                          <div key={`a_b_c_${el.meta_id}`}>
                            <VideoPlayer controls={true} src={el.meta_value} width="295px" height="130px" />
                          </div>
                        </Stack.Item>
                      </Stack>
                    );
                  })
                ) : (
                  <p>No video Sample ...</p>
                )}
                <Stack vertical>
                  <DropZone
                    accept="video/mp4"
                    variableHeight
                    allowMultiple
                    errorOverlayText="File type can be image, video, zip, docx, xlsx, ppt"
                    type="file"
                    label="File should be less than 20MB"
                    onDrop={handleSubmitFile}
                    onDropAccepted={() => {
                      console.info("Dropped file");
                    }}
                  >
                    <DropZone.FileUpload />
                  </DropZone>
                  <p>{uploading ? "... Uploading. Wait a second!" : null}</p>
                </Stack>
              </Form>
            </Layout.Section>
          </Layout>
        </Card.Section>
      </Card>
      <br />
      <Layout>
        <Layout.Section oneHalf>
          <Card>
            <Card.Section title="Dịch vụ thường">
              <Page
                primaryAction={{
                  content: `Tạo dịch vụ thường`,
                  onAction: generateService,
                  disabled: hideAutoGenerate,
                }}
              >
                {generalService}
              </Page>
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section oneHalf>
          <Card>
            <Card.Section title="Dịch vụ premium">
              <Page
                primaryAction={{
                  content: `Tạo dịch vụ premium`,
                  disabled: hidePremiumService,
                  onAction: toggleUpdateActive,
                }}
              >
                {premiumService}
              </Page>
            </Card.Section>
          </Card>
          <CreatePremiumService showModal={uploadModelactive} closeModal={toggleUpdateActive} />
        </Layout.Section>
      </Layout>
    </>,
  ];

  const securityTabContent = [
    <Card>
      <Card.Section>
        <Form onSubmit={submitSecurity}>
          <TextField
            maxLength={1000}
            requiredIndicator
            label="Mật khẩu"
            autoComplete="off"
            {...fieldSecurity.user_pass}
          />
          <br />
          <Stack vertical>
            <RadioButton
              label="Tài khoản chuyên gia"
              helpText="Người dùng là chuyên gia"
              checked={optionRoleValue === true}
              id="disabled"
              name="accounts"
              value="0"
              onChange={handleRadioButtonChange}
            />
            <RadioButton
              label="Tài khoản thường"
              helpText="Người dùng là người dùng thường"
              id="enabled"
              name="accounts"
              value="1"
              checked={optionRoleValue === false}
              onChange={handleRadioButtonChange}
            />
          </Stack>
        </Form>
      </Card.Section>
    </Card>,
  ];

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá video này?"
      primaryAction={{
        content: "Xoá",
        // onAction: onModalAgree,

        // onAction: () => {
        //   onModalAgree(meta_id);
        // },
        disabled: updating,
        destructive: true,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  const tabContent = selected ? securityTabContent : generalTabContent;

  const Actual_page =
    entity || !_advisor_id ? (
      <>
        <Page
          title="Chuyên gia"
          breadcrumbs={[{ content: "Advisor list", url: "/advisors" }]}
          primaryAction={{
            content: "Lưu",
            // disabled: uploading,
            loading: updating,
            onAction: selected ? submitSecurity : submitGeneral,
          }}
        >
          <Layout>
            <Layout.Section>
              <Tabs selected={selected} onSelect={handleTabChange} tabs={tabs}>
                <Card.Section title={tabs[selected].content}></Card.Section>
              </Tabs>
              {tabContent}
            </Layout.Section>
          </Layout>
        </Page>
      </>
    ) : (
      emptyData
    );
  return (
    <>
      {_Modal}
      {uploading ? <Loading /> : null}
      {<EditService toggle_show_edit_modal={showModalEditor} service_id={currentServiceId} />}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

