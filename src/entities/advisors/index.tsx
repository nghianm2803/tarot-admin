import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import advisors_list from "./advisors.list";
import advisors_detail from "./advisors.detail";

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = advisors_list;
      break;
    case "edit":
      ActualPage = advisors_detail;
      break;
    default:
      ActualPage = Theme404;
      break;
  }
  return <>{<ActualPage />}</>;
};
