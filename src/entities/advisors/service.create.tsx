import {
  Card,
  DisplayText,
  DropZone,
  Form,
  Modal,
  Select,
  TextField,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import {
  getEntity,
  createEntity,
  getEntities as getServices,
} from "../../store/services.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import helpers from "helpers";
import QuickUploadImage from "components/oneclick-upload";
import thumbnail_img from "../../media/thumbnail.jpg";
import { getEntity as getImages } from "store/media.store.reducer";

export default function CreatePremiumService({ showModal, closeModal }) {
  const entity = useAppSelector((state) => state.services.entity);
  // const _media_id = useAppSelector((state) => state.media.entity);
  const updating = useAppSelector((state) => state.services.updating);

  const [uploading, setUploading] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const [uploadModelactive, setUploadModelactive] = useState<boolean>(false);
  const toggleUpdateActive = useCallback(() => {
    setUploadModelactive((active) => !active);
  }, [uploadModelactive]);

  /**
   * Price form
   */
  const [priceValue, setPriceValue] = useState<string>("2");
  const handlePriceChange = useCallback((value) => setPriceValue(value), []);

  /**
   * Duration form
   */
  const [durationValue, setDurationValue] = useState<string>("1");
  const [typeDurationValue, setTypeDurationValue] = useState<string>("hour");
  const handleDurationChange = useCallback(
    (value) => setDurationValue(value),
    []
  );
  const handleTypeDurationChange = useCallback(
    (value) => setTypeDurationValue(value),
    []
  );

  const optionsDuration = [
    { label: "Giây", value: "second" },
    { label: "Phút", value: "minute" },
    { label: "Giờ", value: "hour" },
    { label: "Ngày", value: "day" },
  ];

  /**
   * Upload thumbnail premium service
   */
  const [thumb, setThumb] = useState<string>("");
  const [srcThumb, setSrcThumb] = useState(thumbnail_img);
  const [internalErrorNotice, setInternalErrorNotice] = useState<string>("");

  useEffect(() => {
    setUploadModelactive(showModal);
    showUploadModal();
  }, [showModal]);

  function showUploadModal() {
    setUploading(false);
  }

  useEffect(() => {
    dispatch(getImages(entity?.service_thumbnail));
  }, [dispatch, entity?.service_thumbnail]);

  function handQuickUpdateSuccess(res: any) {
    let media_root = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;
    setSrcThumb(media_root + "" + res.media_filename);
    setThumb(`${process.env.REACT_APP_AJAX_UPLOAD_IMAGE}` + res.media_filename);
  }

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.advisors_slug || false;
  useEffect(() => {
    if (Param) {
      dispatch(getEntity(Param));
    } else {
      serviceReset();
    }
  }, []);

  const useFields = {
    service_name: useField<string>({
      value: "",
      validates: [
        notEmpty("Tên dịch vụ trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự"),
        lengthMoreThan(6, "Tên dịch vụ quá ngắn"),
      ],
    }),

    service_description: useField<string>({
      value: "",
      validates: [
        notEmpty("Mô tả dịch vụ trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Mô tả quá ngắn."),
      ],
    }),

    service_category: useField<string>({
      value: "",
      validates: [
        notEmpty("Category dịch vụ trống!"),
        (inputValue) => {
          if (!helpers.isNumber(inputValue)) {
            return "Category dịch vụ phải là số!";
          }
        },
      ],
    }),

    service_group: useField<string>({
      value: "premium_service",
      validates: [
      ],
    }),

    service_duration: useField<string>({
      value: "",
      validates: [
        notEmpty("Thời gian dịch vụ trống!"),
        (inputValue) => {
          if (!helpers.isNumber(inputValue)) {
            return "Thời gian dịch vụ phải là số!";
          }
        },
      ],
    }),

    service_price: useField<string>({
      value: "",
      validates: [
        notEmpty("Giá dịch vụ trống!"),
        lengthLessThan(6, "Giá dịch vụ không được lớn hơn 5 chữ số."),
        (inputValue) => {
          if (!helpers.isNumber(inputValue)) {
            return "Giá dịch vụ phải là số!";
          }
        },
      ],
    }),
    service_unit: useField<string>({
      value: "",
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    dirty,
    reset: serviceReset,
    submitErrors,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          // create new
          dispatch(
            createEntity({
              service_name: values.service_name,
              service_category: Number(values.service_category),
              service_description: values.service_description,
              service_duration: Number(values.service_duration),
              service_price: Number(values.service_price),
              service_group: values.service_group,
              service_thumbnail: thumb,
              service_active: 1,
              service_unit: typeDurationValue,
              createBy: Number(Param),
            })
          );
          dispatch(
            getServices({
              createBy: Param,
            })
          );
        }
        serviceReset();
        toggleUpdateActive();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log("Log bug here!!!", submitErrors);
  // }, [submitErrors]);

  return (
    <Modal
      open={uploadModelactive}
      onClose={() => closeModal()}
      title={
        uploading
          ? "Do NOT close this modal or refresh your browser!"
          : "Create premium service"
      }
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: () => {
          submit();
        },
      }}
      secondaryActions={[
        {
          content: "Đóng",
          disabled: uploading,
          onAction: toggleUpdateActive,
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card>
            <Card.Section>
              <DisplayText size="small">Thumbnail</DisplayText>
              <div id="profile_heading">
                <div className="service_thumbnail_inner">
                  <div className="service_thumbnail-avatar">
                    <QuickUploadImage
                      onSuccess={handQuickUpdateSuccess}
                      onError={handUploadError}
                    />
                    <DropZone>
                      <img
                        src={srcThumb}
                        crossOrigin="anonymous"
                        alt="Upload service thumbnail here"
                        style={{ width: "115px", height: "128px" }}
                      />
                    </DropZone>
                  </div>
                </div>
              </div>
              <br />
              <TextField
                label="Tên dịch vụ"
                autoComplete="off"
                {...fields.service_name}
              />
              <TextField
                label="Category"
                type="number"
                min={1}
                autoComplete="off"
                {...fields.service_category}
              />
              <TextField
                label="Thời gian"
                type="number"
                min="1"
                value={durationValue}
                onChange={handleDurationChange}
                autoComplete="off"
                {...fields.service_duration}
                connectedRight={
                  <Select
                    value={typeDurationValue}
                    label="Đơn vị thời gian"
                    onChange={handleTypeDurationChange}
                    labelHidden
                    options={optionsDuration}
                  />
                }
              />
              <TextField
                label="Giá"
                type="number"
                min="0"
                value={priceValue}
                onChange={handlePriceChange}
                autoComplete="off"
                {...fields.service_price}
              />
              <TextField
                label="Mô tả"
                maxLength={200}
                multiline={2}
                autoComplete="off"
                showCharacterCount
                {...fields.service_description}
              />
            </Card.Section>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );
}
