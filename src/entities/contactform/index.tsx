import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import contactform_list from "./contactform.list";
import contactform_detail from "./contactform.detail";
import contactform_new from "./contactform.new";

/**
 *   Create index file for Contactform
 */

export default function List_contactform() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = contactform_list;
      break;
    case "edit":
      ActualPage = contactform_detail;
      break;
    case "new":
      ActualPage = contactform_new;
      break;
    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
