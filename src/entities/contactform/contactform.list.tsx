import { Card, DataTable, EmptyState, Layout, Page, Select, Stack, TextStyle, Toast } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { TickSmallMinor, LockMinor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/contactform.store.reducer";
import helpers from "../../helpers";
import ContactNew from "./contactform.new";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";
import SearchFilter from "./filter";

export default function Contactform_List() {
  const entity = useAppSelector((state) => state.contactform.entity);
  const entities = useAppSelector((state) => state.contactform.entities);
  const loading = useAppSelector((state) => state.contactform.loading);
  const errorMessage = useAppSelector((state) => state.contactform.errorMessage);
  const totalItems = useAppSelector((state) => state.contactform.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "contactform_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback((value) => setQueryValue(value), []);
  /**
   * Change page number
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  const [selectedParentId, setSelectedParentId] = useState<string>("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `&contactform_name=${_value}` },
    });
  }, []);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  // const toggleNewActive = useCallback(
  //   () => setNewModelactive((active) => !active),
  //   []
  // );

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/contact" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [dispatch, history, mainQuery, useParam.search]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    [mainQuery]
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param contactform_id
   */
  const shortcutActions = (contactform_id: number) => {
    history("edit/" + contactform_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (contactform: any) => {
    let userContent = null;
    try {
      userContent = JSON.parse(contactform.contactform_content);
    } catch (e) {
      userContent = String(userContent);
    }
    const { contactform_id, contactform_name, contactform_email, contactform_status, createAt } = contactform;
    return [
      <div className="clickable" key={contactform_id + "_contact_id"} onClick={() => shortcutActions(contactform_id)}>
        {contactform_id}
      </div>,
      <div className="clickable" key={contactform_id + "_contact_name"} onClick={() => shortcutActions(contactform_id)}>
        {contactform_name}
      </div>,
      <div
        className="clickable"
        key={contactform_id + "_contact_email"}
        onClick={() => shortcutActions(contactform_id)}
      >
        {contactform_email}
      </div>,

      //Contact form content <--------------------
      <div
        className="clickable"
        key={contactform_id + "_contact_content"}
        onClick={() => shortcutActions(contactform_id)}
      >
        <div>
          <p>
            {userContent.name ? (
              <p>
                <TextStyle variation="strong">Tên:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.name)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.display_name ? (
              <p>
                <TextStyle variation="strong">Tên:&nbsp;</TextStyle>
                {userContent.display_name}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.question ? (
              <p>
                <TextStyle variation="strong">Câu hỏi:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.question)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.detail ? (
              <p>
                <TextStyle variation="strong">Chi tiết:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.detail)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.feedback ? (
              <p>
                <TextStyle variation="strong">Feedback:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.feedback)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.payment_method ? (
              <p>
                <TextStyle variation="strong">Payment method:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.payment_method.payment_method)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.coin ? (
              <p>
                <TextStyle variation="strong">Coin:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.coin)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
        <div>
          <p>
            {userContent.experience ? (
              <p>
                <TextStyle variation="strong">Kinh nghiệm:&nbsp;</TextStyle>
                {helpers.trimContentString(userContent.experience)}
              </p>
            ) : (
              ""
            )}
          </p>
        </div>
      </div>,

      <div
        className="clickable"
        key={contactform_id + "_contact_email"}
        onClick={() => shortcutActions(contactform_id)}
      >
        <div>
          <p>{userContent.phone_number ? <p>{userContent.phone_number}</p> : ""}</p>
        </div>
      </div>,
      <div
        className="clickable"
        key={contactform_id + "_contact_email"}
        onClick={() => shortcutActions(contactform_id)}
      >
        <div>
          {userContent.category ? (
            <p>
              <TextStyle variation="strong">Category </TextStyle>
              {userContent.category ? <p>{userContent.category.map((x: { name: any }) => x.name).join(", ")}</p> : ""}
              <TextStyle variation="strong">Topic</TextStyle>{" "}
              {userContent.topic ? <p>{userContent.topic.map((x: { name: any }) => x.name).join(", ")}</p> : ""}
            </p>
          ) : (
            ""
          )}
        </div>
      </div>,
      <div
        className="clickable"
        key={contactform_id + "_contact_email"}
        onClick={() => shortcutActions(contactform_id)}
      >
        <div>
          <p>{userContent.opinion ? <p>{userContent.opinion}</p> : ""}</p>
        </div>
      </div>,
      <div
        className="small-icon"
        key={contactform_id + "_contact_status"}
        onClick={() => shortcutActions(contactform_id)}
      >
        {contactform_status === "publish" ? <LockMinor /> : <TickSmallMinor />}
      </div>,
      <div
        className="clickable"
        key={contactform_id + "_contact_createAt"}
        onClick={() => shortcutActions(contactform_id)}
      >
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["numeric", "text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Contact form name",
            "Email",
            "Content",
            "Phone number",
            "Category & Topic",
            "Quan điểm tâm linh",
            "Trang thái",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          margin-left: 0.7rem;
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        fullWidth
        title="Contact"
        // primaryAction={{
        //   content: "Add new",
        //   disabled: false,
        //   onAction: toggleNewActive,
        // }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack alignment="center">
                  <Stack.Item fill>
                    <SearchFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                  </Stack.Item>
                  <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "All", value: "" },
                        { label: "Feedback", value: "Feedback" },
                        { label: "BecomeAdvisor", value: "BecomeAdvisor" },
                        { label: "Forgot Password", value: "ForgotPassword" },
                        { label: "Contact Us", value: "ContactUs" },
                        { label: "Remove User", value: "RemoveUser" },
                        { label: "Withdrawal", value: "Withdrawal" },
                      ]}
                    />
                  </Stack.Item>
                </Stack>
              </div>
              {PagesList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <ContactNew show={newModelactive} onClose={() => setNewModelactive(false)} />
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

