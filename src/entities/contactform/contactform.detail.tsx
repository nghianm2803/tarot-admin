import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  Select,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  autoSyncDataAdvisor,
} from "../../store/contactform.store.reducer";
import { useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";
import fullNameCountry from "components/countries";
import VideoPlayer from "react-video-js-player";

export default function Edit_contactform() {
  const entity = useAppSelector((state) => state.contactform.entity);
  const updating = useAppSelector((state) => state.contactform.updating);
  const loading = useAppSelector((state) => state.contactform.loading);
  const errorMessage = useAppSelector((state) => state.contactform.errorMessage);
  const updateSuccess = useAppSelector((state) => state.contactform.updateSuccess);

  const dispatch = useAppDispatch();

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  /**
   * Status
   */
  const options = [
    { label: "Hoạt động", value: "Active" },
    { label: "Không hoạt động", value: "Inactive" },
  ];
  const [user_status_selected, setUser_status_selected] = useState<any>("Active");
  const handleSelectChange = useCallback((value) => setUser_status_selected(value), []);

  /**
   * Withdrawal status
   */

  const [withdrawalStatus, setWithdrawalStatus] = useState("0");

  const optionWithdrawalStatus = [
    { label: "Đang chờ", value: "0" },
    { label: "Đang xử lý", value: "1" },
    { label: "Đã hoàn thành", value: "2" },
    { label: "Đã từ chối", value: "3" },
  ];

  const handleChangeWithdrawalStatus = useCallback((value) => setWithdrawalStatus(value), []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.contact_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else formReset();
  }, []);

  /**
   * Add data become advisor from contact form to advisor profile
   */
  const [hideSyncData, setHideSyncData] = useState<boolean>(false);

  const convertDataAdvisor = useCallback(() => {
    dispatch(autoSyncDataAdvisor(Param));
    setHideSyncData(true);
  }, []);

  // useEffect(() => {
  //   if (entity) {
  //     if (entity.is_premium === 1) {
  //       setHideSyncData(false);
  //     } else {
  //       setHideSyncData(true);
  //     }
  //   }
  // }, [entity]);

  useEffect(() => {
    if (entity) {
      setWithdrawalStatus(entity?.contactform_status.toString());
    }
  }, [entity]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Thông tin đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const {
    fields,
    submit,
    reset: formReset,
  } = useForm({
    fields: {
      contactform_name: useField<string>({
        value: entity?.contactform_name ?? "",
        validates: [],
      }),
      contactform_email: useField<string>({
        value: entity?.contactform_email ?? "",
        validates: [],
      }),
      contactform_content: useField<string>({
        value: entity?.contactform_content ?? "",
        validates: [],
      }),
      contactform_ip: useField<string>({
        value: entity?.contactform_ip ?? "",
        validates: [],
      }),
    },
    async onSubmit(values) {
      try {
        dispatch(
          updateEntity({
            contactform_id: entity.contactform_id,
            contactform_status: Number(withdrawalStatus),
          })
        );

        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Contact" breadcrumbs={[{ content: "Contactform list", url: "/contact" }]}>
      <EmptyState heading="No contact here!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? <Toast content={notification} onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  let userContent = null;
  try {
    userContent = JSON.parse(entity.contactform_content);
  } catch (e) {
    userContent = String(userContent);
  }

  /**
   * get full nation name
   * @param code string
   * @returns string
   */
  // function handleGetNationNameByCode(code: string): string {
  //   if (code) {
  //     let data = fullNameCountry().filter((item, index) => {
  //       if (item.alpha2 === code) {
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     });
  //     if (data && data[0]) {
  //       return data[0].name;
  //     }
  //   }
  //   return code;
  // }

  let imgFeedbackList = null;
  try {
    imgFeedbackList = JSON.parse(userContent.image);
  } catch (e) {
    imgFeedbackList = String(imgFeedbackList);
  }

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Contact"
          breadcrumbs={[{ content: "Contactform list", url: "/contact" }]}
          primaryAction={{
            content: "Sync data advisor",
            disabled: hideSyncData,
            onAction: () => convertDataAdvisor(),
          }}
        >
          <Form onSubmit={submit}>
            <Card>
              <Card.Section>
                <Layout>
                  <Layout.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        disabled
                        label="Tên Contact form"
                        {...fields.contactform_name}
                      />
                      <TextField autoComplete="off" disabled label="Email" {...fields.contactform_email} />
                    </FormLayout>
                  </Layout.Section>
                  {entity.contactform_name === "BecomeAdvisor" ? (
                    <Layout.Section>
                      <FormLayout>
                        <TextField autoComplete="off" disabled label="Họ và tên" value={userContent.full_name} />
                        <TextField autoComplete="off" disabled label="Số điện thoại" value={userContent.phone_number} />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Số năm kinh nghiệm"
                          value={userContent.experience}
                        />
                        {/* <TextField
                          autoComplete="off"
                          disabled
                          label="Topic"
                          multiline={2}
                          value={
                            userContent.topic ? userContent.topic.map((x: { name: any }) => x.name).join(", ") : null
                          }
                        />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Category"
                          multiline={2}
                          value={
                            userContent.category
                              ? userContent.category.map((x: { name: any }) => x.name).join(", ")
                              : null
                          }
                        /> */}
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Mô tả ngắn về bản thân"
                          multiline={2}
                          value={userContent.bio}
                        />
                      </FormLayout>
                    </Layout.Section>
                  ) : entity.contactform_name === "Feedback" ? (
                    <Layout.Section>
                      <FormLayout>
                        <TextField autoComplete="off" disabled label="Tên" value={userContent.name} />
                        <TextField autoComplete="off" disabled label="Phản hồi" value={userContent.feedback} />
                        <p>Ảnh</p>
                        {imgFeedbackList &&
                          imgFeedbackList.map((img: any, index: number) => {
                            return (
                              <div className="feedback-image" key={"image_" + index}>
                                <img src={img} crossOrigin="anonymous" alt="Feedback image" />
                              </div>
                            );
                          })}
                         {" "}
                      </FormLayout>
                    </Layout.Section>
                  ) : entity.contactform_name === "RemoveUser" ? (
                    <Layout.Section>
                      <FormLayout>
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Chi tiết"
                          multiline={2}
                          value={userContent.detail}
                        />
                      </FormLayout>
                    </Layout.Section>
                  ) : entity.contactform_name === "ContactUs" ? (
                    <Layout.Section>
                      <FormLayout>
                        <TextField autoComplete="off" disabled label="Tên hiển thị" value={userContent.display_name} />
                        <TextField autoComplete="off" disabled label="Số điện thoại" value={userContent.phone_number} />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Câu hỏi"
                          multiline={2}
                          value={userContent.question}
                        />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Chi tiết"
                          multiline={2}
                          value={userContent.detail}
                        />
                      </FormLayout>
                    </Layout.Section>
                  ) : entity.contactform_name === "Withdrawal" ? (
                    <Layout.Section>
                      <FormLayout>
                        <div>
                          {userContent.payment_method.payment_value ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Payment Name"
                                value={userContent.payment_method.payment_name}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          {userContent.payment_method.payment_value ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Email"
                                value={userContent.payment_method.payment_value}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          {userContent.payment_method.bank_name ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Tên ngân hàng"
                                value={userContent.payment_method.bank_name}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          {userContent.payment_method.bank_id ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Số tài khoản"
                                value={userContent.payment_method.bank_id}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <div>
                          {userContent.payment_method.bank_owner ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Chủ tài khoản"
                                value={userContent.payment_method.bank_owner}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Phương thức thanh toán"
                          value={userContent.payment_method.payment_method}
                        />
                        <div>
                          {userContent.payment_method.payment_address ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Địa chỉ thanh toán"
                                value={userContent.payment_method.payment_address}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>

                        <div>
                          {userContent.payment_method.payment_tax ? (
                            <div>
                              <TextField
                                autoComplete="off"
                                disabled
                                label="Thuế"
                                value={userContent.payment_method.payment_tax}
                              />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </FormLayout>
                    </Layout.Section>
                  ) : (
                    ""
                  )}
                  {entity.contactform_name === "BecomeAdvisor" ? (
                    <Layout.Section oneThird>
                      <FormLayout>
                        <TextField autoComplete="off" disabled label="Ngày sinh" value={userContent.date_of_birth} />
                        <TextField autoComplete="off" disabled label="Social link" value={userContent.social_link} />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Quan điểm tâm linh"
                          multiline={2}
                          value={userContent.opinion}
                        />
                        {/* <TextField
                          autoComplete="off"
                          disabled
                          label="Quốc gia"
                          value={handleGetNationNameByCode(userContent.user_nation)}
                        /> */}
                        <div>
                          {userContent?.video_link ? (
                            <div>
                              <p>Video giới thiệu</p>
                              <VideoPlayer controls={true} src={userContent.video_link} width="295px" height="130px" />
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                        <TextField autoComplete="off" disabled label="IP" {...fields.contactform_ip} />
                        <Select
                          label="Trạng thái"
                          options={options}
                          disabled
                          onChange={handleSelectChange}
                          value={user_status_selected}
                        />
                      </FormLayout>
                    </Layout.Section>
                  ) : entity.contactform_name === "Withdrawal" ? (
                    <Layout.Section oneThird>
                      <FormLayout>
                        <TextField autoComplete="off" disabled label="Coin" value={userContent.coin} />
                        <TextField
                          autoComplete="off"
                          disabled
                          label="Tỉ lệ hiện tại"
                          value={userContent.payment_method.current_rate}
                        />
                        <TextField autoComplete="off" disabled label="Phí hiện tại" value={userContent.current_fee} />
                        <Select
                          label="Trạng thái"
                          options={optionWithdrawalStatus}
                          onChange={handleChangeWithdrawalStatus}
                          value={withdrawalStatus}
                        />
                      </FormLayout>
                    </Layout.Section>
                  ) : (
                    ""
                  )}
                </Layout>
              </Card.Section>
            </Card>
          </Form>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: entity.contactform_name === "Withdrawal" ? false : true,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá liện hệ này?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
        disabled: updating,
        destructive: true,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

