import {
  FormLayout,
  Modal,
  TextField,
  Form,
  Card,
  Toast,
  Select,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/contactform.store.reducer";
import helpers from "helpers";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";

/**
 *   Create upload Modal for Contactform
 */

export default function ContactformUpload({ onClose, show }) {
  const updating = useAppSelector((state) => state.contactform.updating);
  const errorMessage = useAppSelector(
    (state) => state.contactform.errorMessage
  );

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  useEffect(() => {
    formReset();
  }, []);

  /**
   * Status
   */
  const options = [
    { label: "Active", value: "Active" },
    { label: "Inactive", value: "Inactive" },
  ];
  const [user_status_selected, setUser_status_selected] =
    useState<string>("Active");
  const handleSelectChange = useCallback(
    (value) => setUser_status_selected(value),
    []
  );

  const {
    fields,
    submit,
    dirty,
    reset: formReset,
  } = useForm({
    fields: {
      contactform_name: useField<string>({
        value: "",
        validates: [
          notEmpty("Name is required!"),
          lengthMoreThan(5, "Title must be more than 5 characters"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
      contactform_email: useField<string>({
        value: "",
        validates: [
          notEmpty("Email đang trống!"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
          (inputValue) => {
            if (!helpers.isEmail(inputValue)) {
              return "Your Email không hợp lệ!";
            }
          },
        ],
      }),
      contactform_numberphone: useField<string>({
        value: "",
        validates: [
          notEmpty("Phone number is required!"),
          (inputValue) => {
            if (!helpers.isPhoneNumber(inputValue)) {
              return "Your phone number is not valid!";
            }
          },
        ],
      }),
      contactform_content: useField<string>({
        value: "",
        validates: [
          notEmpty("Nội dung trống!"),
          lengthMoreThan(10, "Title must be more than 10 characters"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
    },

    async onSubmit(values) {
      try {
        // create new
        dispatch(
          createEntity({
            contactform_name: values.contactform_name,
            contactform_email: values.contactform_email,
            contactform_numberphone: values.contactform_numberphone,
            contactform_content: values.contactform_content,
          })
        );
        formReset();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ?? "Undefined error. Try again!";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Create new contact"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                requiredIndicator
                label="Name"
                {...fields.contactform_name}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Email"
                {...fields.contactform_email}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Số điện thoại"
                {...fields.contactform_numberphone}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Nội dung"
                {...fields.contactform_content}
              />
              <Select
                label="Trạng thái"
                options={options}
                onChange={handleSelectChange}
                value={user_status_selected}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
