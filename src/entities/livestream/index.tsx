import { Page } from "@shopify/polaris";
import AppList from './livestreams.list';

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
    return (
        <>
            <Page
                title="Livestream"
            >
            </Page>
            {<AppList />}
        </>
    );
};
