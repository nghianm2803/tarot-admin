import {
  Button,
  Card,
  DataTable,
  EmptyState,
  Layout,
  Modal,
  Page,
  TextContainer,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import {
  clearError,
  deleteEntity,
  getEntities,
} from "../../store/livestream.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";

export default function Livestream_List() {
  const entities = useAppSelector((state) => state.livestream.entities);
  const updating = useAppSelector((state) => state.livestream.updating);
  const loading = useAppSelector((state) => state.livestream.loading);
  const errorMessage = useAppSelector((state) => state.livestream.errorMessage);
  const totalItems = useAppSelector((state) => state.livestream.totalItems);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;
  let Param = useParam.contact_slug || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "livestream_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState<string>("");

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      navigate("/livestream" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [dispatch, mainQuery, navigate, useParam.search]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    [mainQuery]
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param livestream_id
   */
  const shortcutActions = (livestream_id: number) => {
    navigate("/livestream/edit/" + livestream_id);
  };

  const [openModal, setOpenModal] = useState(false);

  const _deleteEntityAction = useCallback(() => {
    setOpenModal(true);
  }, []);

  const onModalAgree = useCallback(() => {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }, [Param, dispatch]);

  const emptyData = (
    <EmptyState heading="No livestream here!" image={emptyIMG}>
      <p>Oh! There is no livestream here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  const renderItem = (livestreams: any) => {
    const { livestream_id, livestream_author, livestream_session, createAt } =
      livestreams;
    return [
      <div className="clickable" key={livestream_id + "_livestream_id"}>
        {livestream_id}
      </div>,
      <div className="clickable" key={livestream_id + "_livestream_author"}>
        <div>{livestream_author}</div>
      </div>,
      <div className="clickable" key={livestream_id + "_livestream_session"}>
        {livestream_session}
      </div>,
      <div className="clickable" key={livestream_id + "_livestream_createAt"}>
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
      <div
        className="button"
        key={livestream_id + "_livestream_end"}
        onClick={() => shortcutActions(livestream_id)}
      >
        <Button plain destructive onClick={_deleteEntityAction} size="slim">
          End
        </Button>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text"]}
          headings={["ID", "Author", "Session", "Thời gian tạo", "Action"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page>
        <Layout>
          <Layout.Section>
            <Card>{PagesList}</Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="End livestream session?"
      primaryAction={{
        content: "End",
        onAction: onModalAgree,
        disabled: updating,
        destructive: true,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>After end session, you can not undo this action!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
