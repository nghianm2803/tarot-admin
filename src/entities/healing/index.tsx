import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import healing_list from "./healing.list";
import healing_edit from "./healing.edit";
import healing_view from "./healing.view";

/**
 *   Create index file for Healing
 */

export default function List_healing() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = healing_list;
      break;

    case "edit":
      ActualPage = healing_edit;
      break;

    case "new":
      ActualPage = healing_edit;
      break;

    case "view":
      ActualPage = healing_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}

