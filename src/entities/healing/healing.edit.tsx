import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  Select,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import SkeletonLoading from "components/skeleton_loading";
import { clearError, getEntity, updateEntity, deleteEntity, createEntity } from "../../store/healing.store.reducer";
import { lengthLessThan, lengthMoreThan, useField, useForm } from "@shopify/react-form";

export default function Edit_healing() {
  const entity = useAppSelector((state) => state.healing.entity);
  const updating = useAppSelector((state) => state.healing.updating);
  const entities = useAppSelector((state) => state.healing.entities);
  const loading = useAppSelector((state) => state.healing.loading);
  const errorMessage = useAppSelector((state) => state.healing.errorMessage);
  const updateSuccess = useAppSelector((state) => state.healing.updateSuccess);

  const [message, setMessage] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState<string | null>(null);

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  /**
   * Healing status
   */

  const [healingStatus, setHealingStatus] = useState("");
  const optionHealingStatus = [
    { label: "Đã ẩn", value: "0" },
    { label: "Đang hiển thị", value: "1" },
  ];
  const handleChangeHealingStatus = useCallback((value) => setHealingStatus(value), []);

  /**
   * Healing answer
   */
  const [healingAnswered, setHealingAnswered] = useState("");
  const optionHealingAnswered = [
    { label: "Chưa trả lời", value: "0" },
    { label: "Đã trả lời", value: "1" },
  ];
  const handleChangeHealingAnswered = useCallback((value) => setHealingAnswered(value), []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.healing_slug || false;
  useEffect(() => {
    if (Param) {
      dispatch(getEntity(Param));
    } else reset();
  }, []);

  useEffect(() => {
    if (entity) {
      setHealingStatus(entity?.healing_status.toString());
      setHealingAnswered(entity?.healing_answered.toString());
    }
  }, [entity]);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Dịch vụ đã được cập nhật!");
    }
  }, [updateSuccess]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    healing_content: useField<string>({
      value: entity?.healing_content ?? "",
      validates: [
        lengthLessThan(1000, "No more than 1000 characters."),
        lengthMoreThan(2, "No shorter than 2 characters."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Your title is too short, or it is empty.";
          }
        },
      ],
    }),

    healing_category: useField<string>({
      value: entity?.healing_category ?? "",
      validates: [
        lengthLessThan(250, "No more than 250 characters."),
        lengthMoreThan(2, "No shorter than 2 characters."),
      ],
    }),

    healing_status: useField<string>({
      value: entity?.healing_status ?? "",
      validates: [],
    }),

    healing_answered: useField<string>({
      value: entity?.healing_answered ?? "",
      validates: [],
    }),

    takecareBy: useField<string>({
      value: entity?.takecareBy ?? "",
      validates: [],
    }),

    healing_comment: useField<string>({
      value: entity?.healing_comment ?? "",
      validates: [],
    }),

    createBy: useField<string>({
      value: entity?.createBy ?? "",
      validates: [],
    }),

    user_healing_takecareByTouser: useField<string>({
      value: entity?.user_healing_takecareByTouser ?? "",
      validates: [],
    }),
  };

  const { fields, submit, dirty, reset } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              healing_id: entity.healing_id,
              healing_content: values.healing_content,
              healing_category: values.healing_category,
              createBy: values.createBy,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Healing" breadcrumbs={[{ content: "Healing list", url: "/healing" }]}>
      <EmptyState heading="No post here!" image={emptyIMG}>
        <p>This record maybe not exist!</p>
      </EmptyState>
    </Page>
  );

  let healingContent = null;
  try {
    healingContent = JSON.parse(entity.healing_content);
  } catch (e) {
    healingContent = String(healingContent);
  }

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Healing"
          breadcrumbs={[{ content: "Healing list", url: "/healing" }]}
          primaryAction={{ content: "Save", disabled: !dirty, loading: updating, onAction: submit }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        label="Healing created by ID"
                        {...fields.createBy}
                      />
                      <TextField autoComplete="off" multiline={2} label="Nội dung" value={healingContent.content} />
                      <p>Ảnh</p>
                      {healingContent.images &&
                        healingContent.images.map((img: any, index: number) => (
                          <img
                            src={img}
                            style={{ display: "block", marginLeft: "auto", marginRight: "auto", width: "40%" }}
                            crossOrigin="anonymous"
                            alt="Healing image"
                          />
                        ))}
                      <TextField autoComplete="off" maxLength={250} label="Category" {...fields.healing_category} />
                      <Select
                        label="Trạng thái"
                        options={optionHealingStatus}
                        onChange={handleChangeHealingStatus}
                        value={healingStatus}
                        disabled
                        helpText="Chỉ hiển thị khi chưa nhận được câu trả lời trong vòng 24 giờ."
                      />
                      <Select
                        label="Trả lời"
                        options={optionHealingAnswered}
                        onChange={handleChangeHealingAnswered}
                        value={healingAnswered}
                        disabled
                      />
                      <TextField autoComplete="off" maxLength={1000} label="Trả lời bởi" {...fields.takecareBy} />
                      <TextField autoComplete="off" multiline={2} label="Bình luận" {...fields.healing_comment} />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Save",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Delete",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={null}
      title="Delete entity?"
      primaryAction={{
        content: "Delete",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Cancel",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>After delete, you can not undo this action!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

