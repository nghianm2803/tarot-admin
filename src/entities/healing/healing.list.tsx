import { Card, DataTable, EmptyState, Layout, Page, Select, Stack, Toast } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/healing.store.reducer";
import HealingFilter from "./filter";
import dateandtime from "date-and-time";
import helpers from "helpers";
import SkeletonLoading from "components/skeleton_loading";

export default function General_healing() {
  const entity = useAppSelector((state) => state.healing.entity);
  const entities = useAppSelector((state) => state.healing.entities);
  const loading = useAppSelector((state) => state.healing.loading);
  const errorMessage = useAppSelector((state) => state.healing.errorMessage);
  const totalItems = useAppSelector((state) => state.healing.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "healing_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");
  const handleFiltersQueryChange = useCallback((value) => setQueryValue(value), []);

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({ ...mainQuery, ...{ query: mainQuery.query + `&healing_status=${_value}` } });
  }, []);

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);
  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/healing" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param healing_id
   */
  const shortcutActions = (healing_id: number) => {
    history("edit/" + healing_id);
  };

  const emptyData = (
    <EmptyState heading="No post here!" image={emptyIMG}>
      <p>Oh! There is no record here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  const renderItem = (healing: any) => {
    let healingContent: any = String(healing.healing_content);
    try {
      healingContent = JSON.parse(healing.healing_content);
    } catch (e) {}

    const {
      healing_id,
      user,
      user_healing_takecareByTouser,
      healing_category,
      healing_status,
      healing_answered,
      createAt,
    } = healing;
    return [
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {healing_id}
      </div>,
      <div className="clickable" key={healing_id + "post_display_name"} onClick={() => shortcutActions(healing_id)}>
        {user.display_name}
      </div>,
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {helpers.trimContentString(healingContent?.content)}
      </div>,
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {healing_category}
      </div>,
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {healing_status === 1 ? " Đang hiển thị" : "Đã ẩn"}
      </div>,
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {healing_answered === 1 ? " Đã trả lời" : "Chưa trả lời"}
      </div>,
      <div className="clickable" key={healing_id} onClick={() => shortcutActions(healing_id)}>
        {user_healing_takecareByTouser ? user_healing_takecareByTouser.display_name : "----------"}
      </div>,
      <div className="clickable" key={healing_id + "_healing_createAt"} onClick={() => shortcutActions(healing_id)}>
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Tên người dùng",
            "Nội dung",
            "Category",
            "Trạng thái",
            "Trả lời",
            "Trả lời bởi",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Display page ${mainQuery.page} of total ${totalItems} results...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="Healing"
      // primaryAction={{
      //   content: "Create new",
      //   disabled: false,
      //   onAction: () => {
      //     history("healing/new");
      //   },
      // }}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack distribution="equalSpacing">
                <HealingFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                <Select
                  label=""
                  value={selectedParentId}
                  onChange={handleSelectedChange}
                  options={[
                    { label: "Tất cả", value: "" },
                    { label: "Đang hiển thị", value: "1" },
                    { label: "Đã ẩn", value: "0" },
                  ]}
                />
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

