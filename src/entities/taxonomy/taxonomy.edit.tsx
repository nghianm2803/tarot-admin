import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  Select,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/taxonomy.store.reducer";
import { lengthLessThan, lengthMoreThan, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_taxonomy() {
  const entity = useAppSelector((state) => state.taxonomy.entity);
  const updating = useAppSelector((state) => state.taxonomy.updating);
  const entities = useAppSelector((state) => state.taxonomy.entities);
  const loading = useAppSelector((state) => state.taxonomy.loading);
  const errorMessage = useAppSelector((state) => state.taxonomy.errorMessage);
  const updateSuccess = useAppSelector((state) => state.taxonomy.updateSuccess);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  // jamviet.com
  const [message, setMessage] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState(null);

  /**
   * Option type
   */
  const [typeTaxonomy, setTypeTaxonomy] = useState("category");
  const handleSelectChange = useCallback((value) => setTypeTaxonomy(value), []);

  const option = [
    { label: "Topic", value: "topic" },
    { label: "Tag", value: "tag" },
    { label: "Category", value: "category" },
    { label: "Menu", value: "menu" },
    { label: "Order_tag", value: "order_tag" },
    { label: "Nation", value: "nation" },
  ];

  /**
   * Language type
   */
  const [languageType, setLanguageType] = useState("en");
  const handleLanguageChange = useCallback((value) => setLanguageType(value), []);

  const languages = [
    { label: "English", value: "en" },
    { label: "Vietnamese", value: "vi" },
  ];

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.taxonomy_slug || false;

  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (entity && entity.taxonomy_type) {
      if (Param) {
        setTypeTaxonomy(entity?.taxonomy_type);
      } else {
        setTypeTaxonomy("tag");
      }
    }
  }, [entity]);

  useEffect(() => {
    if (updateSuccess) {
      history("/taxonomy/edit/" + entity?.taxonomy_id, { replace: true });
      setNotification("This taxonomy has been updated!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (Param) {
      setTypeTaxonomy(entity?.taxonomy_type);
    }
  }, [history]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    taxonomy_name: useField<string>({
      value: entity?.taxonomy_name ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(2, "No shorter than 3 characters."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    taxonomy_slug: useField<string>({
      value: entity?.taxonomy_slug ?? "",
      validates: [lengthLessThan(1000, "Nội dung quá dài")],
    }),

    lang: useField<string>({
      value: entity?.lang ?? "",
      validates: [],
    }),

    taxonomy_description: useField<string>({
      value: entity?.taxonomy_description ?? "",
      validates: [lengthLessThan(250, "Không được dài hơn 250 ký tự.")],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: Userreset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              taxonomy_name: values.taxonomy_name,
              taxonomy_type: typeTaxonomy,
              lang: languageType,
              taxonomy_description: values.taxonomy_description,
            })
          );
        } else {
          dispatch(
            updateEntity({
              taxonomy_id: entity.taxonomy_id,
              taxonomy_name: values.taxonomy_name,
              taxonomy_type: typeTaxonomy,
              lang: languageType,
              taxonomy_description: values.taxonomy_description,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Taxonomy" breadcrumbs={[{ content: "Taxonomy list", url: "/taxonomy" }]}>
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? <Toast content={notification} onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page title="Taxonomy" breadcrumbs={[{ content: "Taxonomy list", url: "/taxonomy" }]}>
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        label="Taxonomy Name"
                        requiredIndicator
                        maxLength={250}
                        {...fields.taxonomy_name}
                      />
                      <Select
                        label="Webhook Method"
                        disabled={Param ? true : false}
                        options={option}
                        requiredIndicator
                        onChange={handleSelectChange}
                        value={typeTaxonomy}
                      />
                      <Select
                        label="Language"
                        options={languages}
                        requiredIndicator
                        onChange={handleLanguageChange}
                        value={languageType}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        label="Taxonomy Description"
                        {...fields.taxonomy_description}
                      />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              // disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Delete taxonomy?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

