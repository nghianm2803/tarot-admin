import {
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Select,
  Stack,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/taxonomy.store.reducer";
import TaxonomyFilter from "./filter";
import helpers from "helpers";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";

export default function General_taxonomy() {
  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const entity = useAppSelector((state) => state.taxonomy.entity);
  const entities = useAppSelector((state) => state.taxonomy.entities);
  const loading = useAppSelector((state) => state.taxonomy.loading);
  const errorMessage = useAppSelector((state) => state.taxonomy.errorMessage);
  const totalItems = useAppSelector((state) => state.taxonomy.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "taxonomy_id,desc",
      // taxonomy_type: "category",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");

  /**
   * Filter input
   */

  const [input, setInput] = useState("");
  const handleFiltersQueryChange = useCallback((value) => setInput(value), []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, ...{ query: input } });
  }, [input]);

  /**
   * Filter selected
   */

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ taxonomy_type: `${selectedParentId}`, page: 1 },
    });
  }, [selectedParentId]);

  /**
   * Change page number
   */

  const [numberPage, setNumberPage] = useState(1);
  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/taxonomy" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param taxonomy_id
   */
  const shortcutActions = (taxonomy_id: number) => {
    history("edit/" + taxonomy_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (taxonomy: any) => {
    const {
      taxonomy_id,
      taxonomy_name,
      taxonomy_description,
      taxonomy_slug,
      taxonomy_type,
      taxonomy_parent,
      taxonomy_count,
      lang,
      taxonomy_private,
      createBy,
      createAt,
      updateAt,
      taxonomy_meta,
      taxonomy_relationship,
    } = taxonomy;
    return [
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_id"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {taxonomy_id}
      </div>,

      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_name"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {taxonomy_name}
      </div>,
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_slug"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {taxonomy_slug}
      </div>,
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_type"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {taxonomy_type}
      </div>,
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_lang"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {lang === "en" ? "English" : "Vietnamese"}
      </div>,
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_count"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        {taxonomy_count}
      </div>,
      <div
        className="clickable"
        key={taxonomy_id + "taxonomy_createAt"}
        onClick={() => shortcutActions(taxonomy_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Taxonomy Name",
            "Taxonomy Slug",
            "Taxonomy Type",
            "Language",
            "Taxonomy Count",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="Taxonomy"
      primaryAction={{
        content: "Tạo mới",
        disabled: false,
        onAction: () => {
          history("new");
        },
      }}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack distribution="equalSpacing">
                <TaxonomyFilter
                  queryValue={StringQuery?.query}
                  onChange={handleFiltersQueryChange}
                />
                <Select
                  label=""
                  value={selectedParentId}
                  onChange={handleSelectedChange}
                  options={[
                    { label: "All", value: "" },
                    { label: "Topic", value: "topic" },
                    { label: "Tag", value: "tag" },
                    { label: "Category", value: "category" },
                    { label: "Menu", value: "menu" },
                    { label: "Order_tag", value: "order_tag" },
                    { label: "Nation", value: "nation" },
                  ]}
                />
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {/* {loading ? skeleton_loading : Actual_page} */}
      {initial_loading ? <SkeletonLoading />  : Actual_page}
    </>
  );
}
