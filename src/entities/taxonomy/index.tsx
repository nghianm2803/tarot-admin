import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from '../../layout/404';

import taxonomy_list from './taxonomy.list';
import taxonomy_edit from './taxonomy.edit';
import taxonomy_view from './taxonomy.view';

/**
*   Create index file for Taxonomy
*/

export default function List_taxonomy() {
    let useParam =  {} as any;
        useParam = useParams();
    let Param = useParam.slug || 'list';

    let ActualPage: any;
    switch (Param) {
        
        case 'list':
            ActualPage = taxonomy_list;
        break;

        case 'edit':
            ActualPage = taxonomy_edit;
        break;

        case 'new':
            ActualPage = taxonomy_edit;
        break;

        case 'view':
            ActualPage = taxonomy_view;
        break;

        default:
            ActualPage =  Theme404;
        break;
    }

    return (
        <>
            {<ActualPage />}
        </>
    );
}
