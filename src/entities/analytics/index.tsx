import "../../../node_modules/react-linechart/dist/styles.css";
import { useTheme } from "@material-ui/core/styles";
import ReactApexChart from "react-apexcharts";
import { ApexOptions } from "apexcharts";
import { Stack, TextStyle } from "@shopify/polaris";

export default function TransactionChart() {
  const theme = useTheme();

  const ageChartData: ApexOptions = {
    chart: {
      id: "Dashboard age",
      foreColor: theme.palette.primary.main,
      height: 350,
      type: "bar",
      zoom: {
        enabled: false,
      },
    },
    series: [
      {
        name: "Total",
        data: [10, 41, 35, 51, 49, 62, 69, 91, 148, 12, 43, 41],
      },
    ],
    dataLabels: {
      enabled: true,
      dropShadow: {
        enabled: true,
        left: 2,
        top: 2,
        opacity: 0.5,
      },
    },
    stroke: {
      curve: "straight",
    },
    title: {
      text: "User age chart",
      align: "left",
    },
    grid: {
      row: {
        colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
        opacity: 0.5,
      },
    },
    xaxis: {
      title: { text: "User Age" },
      categories: ["16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
    },
    yaxis: {
      title: {
        text: "Total",
      },
    },
    responsive: [
      {
        breakpoint: undefined,
        options: {},
      },
    ],
  };

  const genderChartData: ApexOptions = {
    series: [4, 20, 13],
    chart: {
      width: 380,
      type: "pie",
    },
    labels: ["Male", "Female", "Other"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 300,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  const deviceChartData: ApexOptions = {
    series: [44, 55, 13, 41, 10],
    chart: {
      width: 380,
      type: "pie",
    },
    labels: ["Iphone", "Samsung", "OPPO", "Xiaomi", "Bphone"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 300,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  const loginFromChartData: ApexOptions = {
    series: [44, 55, 13],
    chart: {
      width: 380,
      type: "pie",
    },
    labels: ["Facebook", "Google", "Apple"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 300,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  const cityChartData: ApexOptions = {
    series: [
      {
        data: [1380, 1200, 1100, 400, 430, 448, 470, 540, 580, 690],
      },
    ],
    chart: {
      type: "bar",
      height: 380,
    },
    plotOptions: {
      bar: {
        barHeight: "100%",
        distributed: true,
        horizontal: true,
        dataLabels: {
          position: "bottom",
        },
      },
    },
    colors: [
      "#33b2df",
      "#546E7A",
      "#d4526e",
      "#13d8aa",
      "#A5978B",
      "#2b908f",
      "#f9a3a4",
      "#90ee7e",
      "#f48024",
      "#69d2e7",
    ],
    dataLabels: {
      enabled: true,
      textAnchor: "start",
      style: {
        colors: ["#fff"],
      },
      formatter: function (val, opt) {
        return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val;
      },
      offsetX: 0,
      dropShadow: {
        enabled: true,
      },
    },
    stroke: {
      width: 1,
      colors: ["#fff"],
    },
    xaxis: {
      categories: [
        "Hà Nội",
        "Hồ Chí Minh",
        "Hải Phòng",
        "Vũng Tàu",
        "Đà Nẵng",
        "Cần Thơ",
        "Hải Dương",
        "Nha Trang",
        "Nam Định",
        "Đồng Nai",
      ],
    },
    yaxis: {
      labels: {
        show: false,
      },
    },
    title: {
      text: "User city",
      align: "left",
      floating: true,
    },
    tooltip: {
      theme: "dark",
      x: {
        show: true,
      },
      y: {
        title: {
          formatter: function () {
            return "";
          },
        },
      },
    },
  };

  // Các câu hỏi User đó đặt trên Tab Healing
  // Tần suất đặt câu hỏi của User

  return (
    <>
      <div style={{ marginTop: "80px", marginLeft: "50px", marginRight: "50px", marginBottom: "50px" }}>
        <ReactApexChart options={ageChartData} type="bar" height={350} series={ageChartData.series} />
        <br />
        <ReactApexChart options={cityChartData} type="bar" height={380} series={cityChartData.series} />
        <Stack distribution="fillEvenly">
          <Stack.Item>
            <ReactApexChart options={deviceChartData} type="pie" series={deviceChartData.series} />
            <div style={{ marginLeft: "100px" }}>
              {" "}
              <TextStyle variation="strong">Device </TextStyle>
            </div>
          </Stack.Item>
          <Stack.Item>
            <ReactApexChart options={loginFromChartData} type="pie" series={loginFromChartData.series} />
            <div style={{ marginLeft: "100px" }}>
              {" "}
              <TextStyle variation="strong">Login</TextStyle>
            </div>
          </Stack.Item>
          <Stack.Item>
            <ReactApexChart options={genderChartData} type="pie" series={genderChartData.series} />
            <div style={{ marginLeft: "100px" }}>
              <TextStyle variation="strong">Gender </TextStyle>
            </div>
          </Stack.Item>
        </Stack>
      </div>
    </>
  );
}

