import {
  FormLayout,
  Modal,
  TextField,
  Form,
  Card,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/app_package.store.reducer";
import helpers from "helpers";
import {
  lengthLessThan,
  lengthMoreThan,
  useField,
  useForm,
} from "@shopify/react-form";

/**
 *   Create upload Modal for Notification
 */

// eslint-disable-next-line import/no-anonymous-default-export

export default function NotificationUpload({ onClose, show }) {
  const dispatch = useAppDispatch();

  const updating = useAppSelector((state) => state.app_package.updating);
  const loading = useAppSelector((state) => state.app_package.loading);
  const errorMessage = useAppSelector(
    (state) => state.app_package.errorMessage
  );
  const updateSuccess = useAppSelector(
    (state) => state.app_package.updateSuccess
  );

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  useEffect(() => {
    // if (Param) {
    //   dispatch(getEntity(Param));
    // } else {
    formReset();
    // }
  }, []);

  const {
    fields,
    submit,
    dirty,
    reset: formReset,
  } = useForm({
    fields: {
      package_name: useField<string>({
        value: "",
        validates: [
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
          lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
          (inputValue) => {
            if (inputValue.length < 6) {
              return "Tên package quá ngắn hoặc trống.";
            }
          },
        ],
      }),
      package_value: useField<string>({
        value: "",
        validates: [
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Giá trị gói phải là số!";
            }
          },
        ],
      }),
      package_coin: useField<string>({
        value: "",
        validates: [
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Package coin phải là số!";
            }
          },
        ],
      }),
      package_slug: useField<string>({
        value: "",
        validates: [
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
          lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
          (inputValue) => {
            if (inputValue.length < 6) {
              return "Package quá ngắn hoặc trống.";
            }
          },
        ],
      }),
    },

    async onSubmit(values) {
      try {
        // create new
        dispatch(
          createEntity({
            package_name: values.package_name,
            package_value: Number(values.package_value),
            package_coin: Number(values.package_coin),
            package_slug: values.package_slug,
            package_active: 1,
          })
        );
        formReset();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ?? "Undefined error. Try again!";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });
  /* End create notification form */

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo gói"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                label="Tên gói"
                requiredIndicator
                maxLength={250}
                {...fields.package_name}
              />
              <TextField
                autoComplete="off"
                maxLength={250}
                label="Package Slug"
                requiredIndicator
                {...fields.package_slug}
              />
              <TextField
                autoComplete="off"
                maxLength={250}
                requiredIndicator
                type="number"
                min="1"
                label="Giá trị gói"
                {...fields.package_value}
              />
              <TextField
                autoComplete="off"
                maxLength={250}
                requiredIndicator
                type="number"
                min="1"
                label="Package Coin"
                {...fields.package_coin}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
