import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  RadioButton,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  reset,
} from "../../store/app_package.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  useField,
  useForm,
} from "@shopify/react-form";
import helpers from "helpers";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_app_package() {
  const entity = useAppSelector((state) => state.app_package.entity);
  const updating = useAppSelector((state) => state.app_package.updating);
  const loading = useAppSelector((state) => state.app_package.loading);
  const errorMessage = useAppSelector(
    (state) => state.app_package.errorMessage
  );
  const updateSuccess = useAppSelector(
    (state) => state.app_package.updateSuccess
  );

  const dispatch = useAppDispatch();

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  /**
   * Button value
   */

  const [packageActive, setPackageActive] = useState<number>(1);

  const [optionButtonValue, setOptionButtonValue] = useState<boolean>(true);

  const handleRadioButtonChange = useCallback((_checked, newValue) => {
    setOptionButtonValue(newValue === "enabled");
  }, []);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.app_package_slug || false;

  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (entity) {
      setPackageActive(entity?.package_active);
      if (entity?.package_active === 0) {
        setOptionButtonValue(false);
      } else {
        setOptionButtonValue(true);
      }
    }
  }, [entity]);

  useEffect(() => {
    if (optionButtonValue === false) {
      setPackageActive(0);
    } else {
      setPackageActive(1);
    }
  }, [optionButtonValue]);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Package đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    package_name: useField<string>({
      value: entity?.package_name ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Tên gói quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    package_value: useField<string>({
      value: entity?.package_value + "" ?? "",
      validates: [
        (inputValue) => {
          if (!helpers.isNumber(inputValue)) {
            return "Giá trị gói phải là số!";
          }
        },
      ],
    }),
    package_coin: useField<string>({
      value: entity?.package_coin + "" ?? "",
      validates: [
        (inputValue) => {
          if (!helpers.isNumber(inputValue)) {
            return "Package coin phải là số!";
          }
        },
      ],
    }),
    package_slug: useField<string>({
      value: entity?.package_slug ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Gói quá ngắn hoặc trống.";
          }
        },
      ],
    }),
  };

  const { fields, submit, dirty } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              package_id: entity.package_id,
              package_name: values.package_name,
              package_value: Number(values.package_value),
              package_coin: Number(values.package_coin),
              package_active: packageActive,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page
      title="App Package"
      breadcrumbs={[{ content: "App_package list", url: "/app_package" }]}
    >
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="App Package"
          breadcrumbs={[{ content: "App_package list", url: "/app_package" }]}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        label="Tên gói"
                        requiredIndicator
                        maxLength={250}
                        disabled
                        {...fields.package_name}
                      />
                      <TextField
                        autoComplete="off"
                        disabled
                        maxLength={250}
                        label="Package Slug"
                        requiredIndicator
                        {...fields.package_slug}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        requiredIndicator
                        type="number"
                        min="1"
                        label="Giá trị gói"
                        disabled
                        {...fields.package_value}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        requiredIndicator
                        type="number"
                        min="1"
                        label="Package Coin"
                        disabled
                        {...fields.package_coin}
                      />
                      <Stack vertical>
                        <RadioButton
                          label="Package active off"
                          checked={optionButtonValue === false}
                          id="disabled"
                          name="accounts"
                          value="0"
                          onChange={handleRadioButtonChange}
                        />
                        <RadioButton
                          label="Package active on"
                          id="enabled"
                          name="accounts"
                          value="1"
                          checked={optionButtonValue === true}
                          onChange={handleRadioButtonChange}
                        />
                      </Stack>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            // secondaryActions={
            //   Param
            //     ? [
            //         {
            //           content: "Xoá",
            //           destructive: true,
            //           onAction: _deleteEntityAction,
            //         },
            //       ]
            //     : null
            // }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Delete this app package?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
