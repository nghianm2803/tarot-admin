import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from '../../layout/404';

import app_package_list from './app_package.list';
import app_package_edit from './app_package.edit';
import app_package_view from './app_package.view';

/**
*   Create index file for App_package
*/

export default function List_app_package() {
    let useParam =  {} as any;
        useParam = useParams();
    let Param = useParam.slug || 'list';

    let ActualPage: any;
    switch (Param) {
        
        case 'list':
            ActualPage = app_package_list;
        break;

        case 'edit':
            ActualPage = app_package_edit;
        break;

        case 'view':
            ActualPage = app_package_view;
        break;

        default:
            ActualPage =  Theme404;
        break;
    }

    return (
        <>
            {<ActualPage />}
        </>
    );
}
