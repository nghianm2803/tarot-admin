/**
 * Page
 */

import { Page, Layout, Card } from "@shopify/polaris";
import { useEffect } from "react";
import Theme404 from "../../layout/404";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import Parser from "html-react-parser";
import dateandtime from "date-and-time";
import { getEntity } from "../../store/app_package.store.reducer";
import PageMarkupLoading from "components/page_markup_loading";

/*
    react-router-dom:
    let useParam =  {} as any;
        useParam = useParams();
        let Param = useParam.any || false;
        or
    console.log(props.match.params)
    */

export default function View_app_package() {
  const data = useAppSelector((state) => state.app_package.entity);
  const loading = useAppSelector((state) => state.app_package.loading);
  const dispatch = useAppDispatch();
  /**
   * Lấy URL ra... để query. chú ý phải load nội dung mới
   * trong useEffect
   */
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.app_package_slug || false;

  useEffect(() => {
    dispatch(getEntity(Param));
  }, [Param]);

  const actual_page = data?.package_id ? (
    <Page
      title={data?.post_title}
      subtitle={`${data?.createBy}, last update at ${dateandtime.format(
        new Date(Number(data?.createAt)),
        "DD/MM/YYYY HH:mm:ss"
      )} `}
    >
      <Layout>
        <Layout.Section>
          <Card title={null} sectioned>
            {Parser(data ? data?.post_content : " ")}
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  ) : (
    <Theme404 />
  );

  const pageMarkup = loading ? <PageMarkupLoading /> : actual_page;

  return <div id="general_page">{pageMarkup}</div>;
}
