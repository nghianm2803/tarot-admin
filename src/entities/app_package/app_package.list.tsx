import {
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { TickSmallMinor, LockMinor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/app_package.store.reducer";
import helpers from "helpers";
import dateandtime from "date-and-time";
import AppPackage from "./app_package.new";
import SkeletonLoading from "components/skeleton_loading";

export default function General_app_package() {
  const entities = useAppSelector((state) => state.app_package.entities);
  const loading = useAppSelector((state) => state.app_package.loading);
  const errorMessage = useAppSelector(
    (state) => state.app_package.errorMessage
  );
  const totalItems = useAppSelector((state) => state.app_package.totalItems);
  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "package_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");

  const [input, setInput] = useState("");

  useEffect(() => {
    setMainQuery({ ...mainQuery, ...{ query: input } });
  }, [input]);

  const [selectedParentId, setSelectedParentId] = useState("");
  
  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ post_status: `${selectedParentId}`, page: 1 },
    });
  }, [selectedParentId]);

  /**
   * Change page number
   */

  const [numberPage, setNumberPage] = useState(1);
  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  /**
   * New model
   */

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(
    () => setNewModelactive((active) => !active),
    []
  );

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/app_package" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param package_id
   */
  const shortcutActions = (package_id: number) => {
    history("edit/" + package_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (app_package: any) => {
    const {
      package_id,
      package_name,
      package_value,
      package_coin,
      package_slug,
      package_active,
      createAt,
    } = app_package;
    return [
      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
        // onClick={() => getPackageDetail(app_package)}
      >
        {package_id}
      </div>,

      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        {package_name}
      </div>,
      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        <div style={{ textAlign: "center" }}>{package_value}</div>
      </div>,
      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        <div style={{ textAlign: "center" }}>{package_coin}</div>
      </div>,
      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        {package_slug}
      </div>,
      <div
        className="small-icon"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        {package_active === 1 ? <TickSmallMinor /> : <LockMinor />}
      </div>,
      <div
        className="clickable"
        key={package_id}
        onClick={() => shortcutActions(package_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "ID",
            "Tên gói",
            "Giá trị gói",
            "Package Coin",
            "Package Slug",
            "Active",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          margin-left: 0.7rem;
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        title="App Package"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              {/* <div style={{ padding: "16px", display: "flex" }}>
                <Stack distribution="equalSpacing">
                  <App_packageFilter
                    queryValue={StringQuery?.query}
                    onChange={handleFiltersQueryChange}
                  />
                  <Select
                    label=""
                    value={selectedParentId}
                    onChange={handleSelectedChange}
                    options={[
                      { label: "All", value: "" },
                      { label: "Active", value: "active" },
                      { label: "Inactive", value: "inactive" },
                    ]}
                  />
                </Stack>
              </div> */}
              {PagesList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>

      <AppPackage
        show={newModelactive}
        onClose={() => setNewModelactive(false)}
      />
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {/* {loading ? skeleton_loading : Actual_page} */}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
