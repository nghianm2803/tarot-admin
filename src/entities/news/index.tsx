import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import news_list from "./news.list";
import news_edit from "./news.edit";
// import pages_view from './pages.view';

export default function Pagidex() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = news_list;
      break;

    case "edit":
      ActualPage = news_edit;
      break;

    case "new":
      ActualPage = news_edit;
      break;

    // case 'view':
    //     ActualPage = pages_view;
    // break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
