import { Card, DataTable, EmptyState, Layout, Page, Stack, Toast, Loading, Button } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { TickSmallMinor, LockMinor, ArchiveMinor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/posts.store.reducer";
import PostsFilter from "./filter";
import helpers from "../../helpers";
import date from "date-and-time";

export default function General() {
  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const entities = useAppSelector((state) => state.posts.entities);
  const loading = useAppSelector((state) => state.posts.loading);
  const errorMessage = useAppSelector((state) => state.posts.errorMessage);
  const totalItems = useAppSelector((state) => state.posts.totalItems);
  const { user_id } = useAppSelector((state) => state.user.account);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "post_id,desc",
      post_type: "news",
    },
  });
  const [queryValue, setQueryValue] = useState("");
  // const handleFiltersQueryChange = useCallback(
  //   (value) => setQueryValue(value),
  //   []
  // );

  const handleFiltersQueryChange = useCallback(
    (_value) => {
      setMainQuery({ ...mainQuery, ...{ query: _value, page: 1 } });
    },
    [mainQuery]
  );

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/news" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const showMyFile = () => {
    setMainQuery({ ...mainQuery, ...{ createBy: user_id } });
  };

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param post_id
   */
  const shortcutActions = (post_id: number) => {
    history("/news/edit/" + post_id);
  };

  const emptyData = (
    <EmptyState heading="No news here!" image={emptyIMG}>
      <p>Oh! There is no news here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  const handleSort = useCallback(
    (index, direction) => {
      let _direction = direction === "descending" ? "desc" : "asc";
      let sort = "";
      if (index === 6) {
        sort = "updateAt," + _direction;
      } else {
        sort = "createAt," + _direction;
      }
      setMainQuery({ ...mainQuery, sort: sort });
    },
    [entities]
  );

  const renderItem = (posts: any) => {
    const { post_id, post_title, post_excerpt, post_status, lang, post_type, comment_count, createAt, updateAt, user } =
      posts;
    return [
      <div className="clickable" key={post_id + "post_id"} onClick={() => shortcutActions(post_id)}>
        {post_id}
      </div>,
      <div className="small-icon" key={post_id + "post_status"} onClick={() => shortcutActions(post_id)}>
        {post_status === "publish" ? <TickSmallMinor /> : <LockMinor />}
      </div>,
      <div className="clickable" key={post_id + "post_display_name"} onClick={() => shortcutActions(post_id)}>
        {user.display_name}
      </div>,
      <div className="clickable" key={post_id + "post_title"} onClick={() => shortcutActions(post_id)}>
        {post_title}
      </div>,
      <div className="clickable" key={post_id + "post_lang"} onClick={() => shortcutActions(post_id)}>
        {lang === "en" ? "English" : "Vietnamese"}
      </div>,
      <div className="clickable" key={post_id + "post_excerpt"} onClick={() => shortcutActions(post_id)}>
        {helpers.trimContentString(post_excerpt)}
      </div>,
      <div className="clickable" key={post_id + "post_createAt"} onClick={() => shortcutActions(post_id)}>
        <time>{createAt ? date.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={post_id + "post_updateAt"} onClick={() => shortcutActions(post_id)}>
        <time>{updateAt ? date.format(new Date(Number(updateAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          sortable={[false, false, false, false, false, false, true, true]}
          defaultSortDirection="descending"
          initialSortColumnIndex={7}
          onSort={handleSort}
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Trạng thái", "Tác giả", "Tiêu đề", "Ngôn ngữ", "Exceprt", "Thời gian tạo", "Thời gian cập nhập"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="News"
      primaryAction={{
        content: "Tạo mới",
        disabled: false,
        onAction: () => {
          history("/news/new");
        },
      }}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack distribution="equalSpacing">
                <PostsFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                <Stack.Item>
                  <Button icon={ArchiveMinor} onClick={showMyFile}>
                    My Posts
                  </Button>
                </Stack.Item>
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {loading ? <Loading /> : null}
      {Actual_page}
      {/* {initial_loading ? skeleton_loading : Actual_page} */}
    </>
  );
}

