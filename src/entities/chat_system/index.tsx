import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { Card, Page, PageActions, TextField, FormLayout, Toast } from "@shopify/polaris";
import { createChat, clearError } from "store/settings.store.reducer";

/**
 * Setting emails
 * For internal use, eg: System sent notification ...
 */
export default function SettingEmails() {
  const updating = useAppSelector((state) => state.settings.updating);
  const updateSuccess = useAppSelector((state) => state.settings.updateSuccess);
  const errorMessage = useAppSelector((state) => state.settings.errorMessage);

  const dispatch = useAppDispatch();

  // Admin ID = 32
  const advisorId = "32";

  const [userId, setUserId] = useState("");
  const handleUserIdChange = useCallback((value) => {
    setUserId(value);
  }, []);

  const [content, setContent] = useState("");
  const handleContentChange = useCallback((value) => {
    setContent(value);
  }, []);

  // Admin ID = 32
  const chatContents = [{ chat_content: content, media_data: "", create_by: "32" }];

  const handleSubmit = () => {
    dispatch(
      createChat({
        advisor_id: advisorId,
        user_id: userId,
        chat_content: JSON.stringify(chatContents),
        duration: "0",
      })
    );
    setUserId("");
    setContent("");
  };

  const [message, setMessage] = useState(null);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  useEffect(() => {
    if (updateSuccess) {
      setMessage("This message has been sent successfully!");
    }
  }, [updateSuccess]);

  return (
    <>
      <Page narrowWidth title="Chat with user">
        <FormLayout>
          <Card>
            <Card.Section>
              <TextField
                label="UserID"
                type="number"
                min={1}
                requiredIndicator
                value={userId}
                onChange={handleUserIdChange}
                multiline={true}
                autoComplete="off"
              />
              <TextField
                label="Content"
                requiredIndicator
                value={content}
                onChange={handleContentChange}
                multiline={3}
                autoComplete="off"
              />
            </Card.Section>
          </Card>
        </FormLayout>
        <PageActions
          primaryAction={{
            content: "Send",
            onAction: handleSubmit,
            disabled: updating,
            loading: updating,
          }}
        />
      </Page>
      {toastMarkup}
      {toastNotification}
    </>
  );
}

