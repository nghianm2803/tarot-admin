import {
  Button,
  EmptyState,
  Modal,
  TextContainer,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  deleteEntity,
  reset,
} from "../../store/affiliate.store.reducer";

export default function Edit_affiliate() {
  const entity = useAppSelector((state) => state.affiliate.entity);
  const updating = useAppSelector((state) => state.affiliate.updating);
  const entities = useAppSelector((state) => state.affiliate.entities);
  const loading = useAppSelector((state) => state.affiliate.loading);
  const errorMessage = useAppSelector((state) => state.affiliate.errorMessage);
  const updateSuccess = useAppSelector(
    (state) => state.affiliate.updateSuccess
  );

  const dispatch = useAppDispatch();
  // const history = useHistory();

  // jamviet.com
  const [message, setMessage] = useState(null);
  const [notification, setNotification] = useState(null);

  const [openModal, setOpenModal] = useState(false);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.affiliate_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (updateSuccess) setNotification("Cập nhật thành công!");
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
  }

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Thông tin này không tồn tại!</p>
    </EmptyState>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const [active, setActive] = useState(true);

  const handleChange = useCallback(() => setActive(!active), [active]);

  const handleScrollBottom = useCallback(() => alert("Scrolled to bottom"), []);

  const activator = <Button onClick={handleChange}>Open</Button>;

  return (
    <div style={{ height: "500px" }}>
      <Modal
        activator={activator}
        open={active}
        title="Scrollable content"
        onClose={handleChange}
        onScrolledToBottom={handleScrollBottom}
      >
        {Array.from({ length: 50 }, (_, index) => (
          <Modal.Section key={index}>
            <TextContainer>
              <p>
                Item <a href="#">#{index}</a>
              </p>
            </TextContainer>
          </Modal.Section>
        ))}
      </Modal>
    </div>
  );
}
