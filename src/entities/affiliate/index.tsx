import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import affiliate_list from "./affiliate.list";
// import affiliate_edit from "./affiliate.edit";
import affiliate_view from "./affiliate.view";

/**
 *   Create index file for Affiliate
 */

export default function List_affiliate() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = affiliate_list;
      break;

    // case "edit":
    //   ActualPage = affiliate_edit;
    //   break;

    case "view":
      ActualPage = affiliate_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
