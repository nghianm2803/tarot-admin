import { Card, DataTable, EmptyState, Layout, Page, Toast } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/affiliate.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import SkeletonLoading from "components/skeleton_loading";

export default function General_affiliate() {
  const entities = useAppSelector((state) => state.affiliate.entities);
  const loading = useAppSelector((state) => state.affiliate.loading);
  const errorMessage = useAppSelector((state) => state.affiliate.errorMessage);
  const totalItems = useAppSelector((state) => state.affiliate.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "affiliate_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);
  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/affiliate" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (affiliate: any) => {
    const { affiliate_id, user, affiliate_ip, affiliate_browser, affiliate_referrer, createAt } = affiliate;

    return [
      <div className="clickable" key={affiliate_id}>
        {affiliate_id}
      </div>,
      <div className="clickable" key={affiliate_id}>
        {user.display_name}
      </div>,
      <div className="clickable" key={affiliate_id}>
        {affiliate_ip}
      </div>,
      <div className="clickable" key={affiliate_id}>
        {affiliate_browser}
      </div>,
      <div className="clickable" key={affiliate_id}>
        <p>{helpers.trimContentString(affiliate_referrer)}</p>
      </div>,
      <div className="clickable" key={affiliate_id}>
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text"]}
          headings={[
            "Affiliate ID",
            "Username ",
            "Affiliate IP",
            "Affiliate Broswer",
            "Affiliate Referrer",
            "Start Date",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page fullWidth title="Affiliate">
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              {/* <Stack distribution="equalSpacing">
                <AffiliateFilter
                  queryValue={StringQuery?.query}
                  onChange={handleFiltersQueryChange}
                />
                <Select
                  label=""
                  value={selectedParentId}
                  onChange={handleSelectedChange}
                  options={[
                    { label: "post_type_all", value: "" },
                    { label: "post", value: "post" },
                    { label: "page", value: "page" },
                  ]}
                />
              </Stack> */}
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

