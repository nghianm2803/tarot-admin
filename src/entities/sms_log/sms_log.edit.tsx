import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import SkeletonLoading from "components/skeleton_loading";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import { clearError, getEntity, updateEntity, deleteEntity, createEntity } from "../../store/sms_log.store.reducer";
import { lengthLessThan, lengthMoreThan, useField, useForm } from "@shopify/react-form";

export default function Edit_sms_log() {
  const entity = useAppSelector((state) => state.sms_log.entity);
  const updating = useAppSelector((state) => state.sms_log.updating);
  const loading = useAppSelector((state) => state.sms_log.loading);
  const errorMessage = useAppSelector((state) => state.sms_log.errorMessage);
  const updateSuccess = useAppSelector((state) => state.sms_log.updateSuccess);

  const dispatch = useAppDispatch();

  const [message, setMessage] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState<string | null>(null);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.sms_log_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else reset();
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Dịch vụ đã được cập nhật!");
    }
  }, [updateSuccess]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    sms_timestamp: useField<string>({
      value: entity?.sms_timestamp ?? "",
      validates: [
        lengthLessThan(200, "No more than 200 characters."),
        lengthMoreThan(2, "No shorter than 2 characters."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Your title is too short, or it is empty.";
          }
        },
      ],
    }),

    sms_content: useField<string>({
      value: entity?.sms_content ?? "",
      validates: [
        lengthLessThan(250, "No more than 250 characters."),
        lengthMoreThan(2, "No shorter than 2 characters."),
      ],
    }),

    sms_sender: useField<string>({
      value: entity?.sms_sender ?? "",
      validates: [],
    }),
    sms_userId: useField<string>({
      value: entity?.sms_userId ?? "",
      validates: [],
    }),
    sms_amount: useField<string>({
      value: entity?.sms_amount ?? "",
      validates: [],
    }),

    sms_status: useField<string>({
      value: entity?.sms_status ?? "",
      validates: [],
    }),
  };

  const { fields, submit, dirty, reset } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              sms_id: entity.sms_id,
              sms_timestamp: Number(values.sms_timestamp),
              sms_content: values.sms_content,
              sms_sender: values.sms_sender,
              sms_userId: values.sms_userId,
              sms_amount: Number(values.sms_amount),
              sms_status: Number(values.sms_status),
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <EmptyState heading="No post here!" image={emptyIMG}>
      <p>This record maybe not exist!</p>
    </EmptyState>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;
  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="SMS Log"
          breadcrumbs={[{ content: "Sms_log list", url: "/sms_log" }]}
          primaryAction={{ content: "Save", disabled: !dirty, loading: updating, onAction: submit }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField autoFocus autoComplete="off" label="SMS timestamp" {...fields.sms_timestamp} />
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        label="SMS Content"
                        {...fields.sms_content}
                        multiline={2}
                      />
                      <TextField autoComplete="off" maxLength={1000} label="SMS Sender" {...fields.sms_sender} />
                      <TextField autoComplete="off" maxLength={1000} label="SMS UserId" {...fields.sms_userId} />
                      <TextField autoComplete="off" maxLength={1000} label="SMS Amount" {...fields.sms_amount} />
                      <TextField autoComplete="off" maxLength={1000} label="SMS Status" {...fields.sms_status} />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Save",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Delete",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={null}
      title="Delete entity?"
      primaryAction={{
        content: "Delete",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Cancel",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>After delete, you can not undo this action!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

