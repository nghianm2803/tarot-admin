import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import sms_log_list from "./sms_log.list";
import sms_log_edit from "./sms_log.edit";
import sms_log_view from "./sms_log.view";

/**
 *   Create index file for Sms_log
 */

export default function List_sms_log() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = sms_log_list;
      break;

    case "edit":
      ActualPage = sms_log_edit;
      break;

    case "new":
      ActualPage = sms_log_edit;
      break;

    case "view":
      ActualPage = sms_log_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}

