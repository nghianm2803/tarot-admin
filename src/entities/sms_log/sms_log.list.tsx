import { Card, DataTable, EmptyState, Layout, Page, Toast } from "@shopify/polaris";
import SkeletonLoading from "components/skeleton_loading";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/sms_log.store.reducer";
import helpers from "helpers";
import dateandtime from "date-and-time";

export default function General_sms_log() {
  const entity = useAppSelector((state) => state.sms_log.entity);
  const entities = useAppSelector((state) => state.sms_log.entities);
  const loading = useAppSelector((state) => state.sms_log.loading);
  const errorMessage = useAppSelector((state) => state.sms_log.errorMessage);
  const totalItems = useAppSelector((state) => state.sms_log.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "sms_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);
  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/sms_log" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param sms_id
   */
  const shortcutActions = (sms_id: number) => {
    history("edit/" + sms_id);
  };

  const emptyData = (
    <EmptyState heading="No post here!" image={emptyIMG}>
      <p>Oh! There is no record here! Try remove filter or add a new record!</p>
    </EmptyState>
  );

  const renderItem = (sms_log: any) => {
    const { sms_id, sms_timestamp, sms_content, sms_sender, sms_userId, sms_amount, sms_status, createAt, updateAt } =
      sms_log;
    return [
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_id}
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        <time>{sms_timestamp ? dateandtime.format(new Date(Number(sms_timestamp)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_content}
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_sender}
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_userId}
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_amount}
      </div>,
      <div className="clickable" key={sms_id} onClick={() => shortcutActions(sms_id)}>
        {sms_status}
      </div>,
      <div className="clickable" key={sms_id + "_sms_createAt"} onClick={() => shortcutActions(sms_id)}>
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={sms_id + "_sms_createAt"} onClick={() => shortcutActions(sms_id)}>
        <time>{updateAt ? dateandtime.format(new Date(Number(updateAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Timestamp", "Content", "Sender", "User Id", "Amount", "Status", "Create At", "Update At"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Display page ${mainQuery.page} of total ${totalItems} results...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="SMS Log"
      // primaryAction={{
      //   content: "Create new",
      //   disabled: false,
      //   onAction: () => {
      //     history("sms_log/new");
      //   },
      // }}
    >
      <Layout>
        <Layout.Section>
          <Card>{PagesList}</Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

