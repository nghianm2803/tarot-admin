import { useCallback, useEffect, useMemo, useState } from "react";
import {
  AppliedFilterInterface,
  FilterInterface,
  Filters,
} from "@shopify/polaris";
import debounce from "lodash.debounce";

/**
 *   Create template for Incoming_webhook
 */

export interface IIncoming_webhookFiltersProps {
  queryValue?: string;
  onChange(query: string): void;
}

export const CateWarehouseFilters = (props: IIncoming_webhookFiltersProps) => {
  const { onChange } = props;

  const [queryValue, setQueryValue] = useState<string | null>(
    props?.queryValue ?? ""
  );
  const setQueryValueCallback = useCallback(
    (_value) => setQueryValue(_value),
    []
  );

  const onChangeCallback = useMemo(
    () => debounce((_value) => onChange?.call(this, _value), 300),
    []
  );
  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  const filters = [] as FilterInterface[];
  const appliedFilters = [] as AppliedFilterInterface[];

  return (
    <Filters
      queryPlaceholder="Search by description"
      queryValue={queryValue}
      onQueryChange={setQueryValueCallback}
      onQueryClear={() => setQueryValueCallback("")}
      filters={filters}
      appliedFilters={appliedFilters}
      onClearAll={() => setQueryValueCallback("")}
    ></Filters>
  );
};

export default CateWarehouseFilters;
