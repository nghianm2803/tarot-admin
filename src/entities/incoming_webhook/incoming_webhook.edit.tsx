import {
  Button,
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
} from "../../store/incoming_webhook.store.reducer";
import { lengthLessThan, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_incoming_webhook() {
  const entity = useAppSelector((state) => state.incoming_webhook.entity);
  const updating = useAppSelector((state) => state.incoming_webhook.updating);
  const loading = useAppSelector((state) => state.incoming_webhook.loading);
  const errorMessage = useAppSelector(
    (state) => state.incoming_webhook.errorMessage
  );
  const updateSuccess = useAppSelector(
    (state) => state.incoming_webhook.updateSuccess
  );

  const dispatch = useAppDispatch();

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
    setMessage(null);
    dispatch(clearError());
  }, []);

  const { v4: uuidv4 } = require("uuid");
  const [generateToken, setGenerateToken] = useState<string>("");

  const handleChangeToken = useCallback(() => {
    setGenerateToken(uuidv4());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.incoming_webhook_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else reset_incoming();
  }, []);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Đã cập nhật incoming webhook!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  //Set generateToken = api_access_token
  useEffect(() => {
    if (entity) {
      setGenerateToken(entity.api_access_token);
    }
  }, [entity]);

  const useFields = {
    api_enable: useField<string>({
      value: entity?.api_enable + "" ?? "",
      validates: [],
    }),
    api_slug: useField<string>({
      value: entity?.api_slug ?? "",
      validates: [],
    }),
    api_description: useField<string>({
      value: entity?.api_description ?? "",
      validates: [
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Mô tả API quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    api_name: useField<string>({
      value: entity?.api_name ?? "",
      validates: [
        lengthLessThan(100, "Không được dài quá 100 ký tự."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tên API quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    api_access_token: useField<string>({
      value: entity?.api_access_token ?? "",
      validates: [],
    }),
    api_access_method: useField<string>({
      value: entity?.api_access_method ?? "",
      validates: [],
    }),
    api_role: useField<string>({
      value: entity?.api_role ?? "",
      validates: [],
    }),
    createAt: useField<string>({
      value: entity?.createAt ?? "",
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    dirty,
    reset: reset_incoming,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              api_id: entity.api_id,
              api_enable: Number(values.api_enable),
              api_description: values.api_description,
              api_name: values.api_name,
              api_access_token: generateToken,
              api_access_method: values.api_access_method,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page
      title="Incoming Webhook"
      breadcrumbs={[
        { content: "Incoming_webhook list", url: "/incoming_webhook" },
      ]}
    >
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Incoming Webhook"
          breadcrumbs={[
            { content: "Incoming_webhook list", url: "/incoming_webhook" },
          ]}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        label="API Enable"
                        disabled
                        {...fields.api_enable}
                      />
                      <TextField
                        autoComplete="off"
                        label="API Slug"
                        disabled
                        {...fields.api_slug}
                      />
                      <TextField
                        autoComplete="off"
                        label="Mô tả"
                        {...fields.api_description}
                      />
                      <TextField
                        autoComplete="off"
                        label="Tên API"
                        {...fields.api_name}
                      />
                      <Stack>
                        <Stack.Item fill>
                          <TextField
                            autoComplete="off"
                            label="API Token"
                            disabled
                            value={generateToken}
                          />
                        </Stack.Item>
                        <div className="change-token">
                          <Stack.Item>
                            <Button primary onClick={handleChangeToken}>
                              Generate
                            </Button>
                          </Stack.Item>
                        </div>
                      </Stack>
                      <TextField
                        autoComplete="off"
                        label="API Method"
                        disabled
                        {...fields.api_access_method}
                      />
                      <TextField
                        autoComplete="off"
                        label="API Role"
                        disabled
                        {...fields.api_role}
                      />
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá incoming webhook?"
      primaryAction={{
        destructive: true,
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
