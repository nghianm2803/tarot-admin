import {
  Badge,
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Select,
  Stack,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import {
  clearError,
  getEntities,
} from "../../store/incoming_webhook.store.reducer";
import helpers from "../../helpers";
import SkeletonLoading from "components/skeleton_loading";
import dateandtime from "date-and-time";
import IncomingWebhookFilter from "./filter";
import GeneralIncomingWebhookCreate from "./incoming_webhook.create";
import { TickSmallMinor, LockMinor } from "@shopify/polaris-icons";

export default function Incoming_Webhook_List() {
  const entities = useAppSelector((state) => state.incoming_webhook.entities);
  const loading = useAppSelector((state) => state.incoming_webhook.loading);
  const errorMessage = useAppSelector(
    (state) => state.incoming_webhook.errorMessage
  );
  const totalItems = useAppSelector(
    (state) => state.incoming_webhook.totalItems
  );

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "api_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(
    () => setNewModelactive((active) => !active),
    []
  );

  const [selectedParentId, setSelectedParentId] = useState<string>("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `&api_access_method=${_value}` },
    });
  }, []);

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);
  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/incoming_webhook" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param api_id
   */
  const shortcutActions = (api_id: number) => {
    history("edit/" + api_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const handleSort = (index: any, direction: string) => {
    let _direction = direction === "descending" ? "desc" : "asc";
    let sort = "";
    sort = "createAt," + _direction;
    setMainQuery({ ...mainQuery, sort: sort });
  };

  const renderItem = (incoming_webhook: any) => {
    const {
      api_id,
      api_enable,
      api_slug,
      api_description,
      api_name,
      api_access_token,
      api_access_method,
      api_role,
      lastOpen,
      createAt,
      updateAt,
    } = incoming_webhook;
    return [
      <div
        className="clickable"
        key={api_id + "_api_id"}
        onClick={() => shortcutActions(api_id)}
      >
        {api_id}
      </div>,
      <div
        className="small-icon"
        key={api_id + "_api_enable"}
        onClick={() => shortcutActions(api_id)}
      >
        <div>{api_enable === "1" ? <LockMinor /> : <TickSmallMinor />}</div>
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_slug"}
        onClick={() => shortcutActions(api_id)}
      >
        {helpers.trimContentString(api_slug)}
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_des"}
        onClick={() => shortcutActions(api_id)}
      >
        {helpers.trimContentString(api_description)}
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_name"}
        onClick={() => shortcutActions(api_id)}
      >
        {helpers.trimContentString(api_name)}
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_token"}
        onClick={() => shortcutActions(api_id)}
      >
        {helpers.trimContentString(api_access_token)}
      </div>,
      <div
        className="clickable"
        style={{ marginLeft: " 0.2rem" }}
        key={api_id + "_api_method"}
        onClick={() => shortcutActions(api_id)}
      >
        <Badge status="info">{api_access_method}</Badge>
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_role"}
        onClick={() => shortcutActions(api_id)}
      >
        {api_role === "admin" ? (
          <Badge status="success">Admin</Badge>
        ) : api_role === "advisor" ? (
          <Badge status="attention">Advisor</Badge>
        ) : (
          <Badge status="warning">User</Badge>
        )}
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_createAt"}
        onClick={() => shortcutActions(api_id)}
      >
        {createAt
          ? dateandtime.format(
              new Date(Number(createAt)),
              "DD-MM-YYYY HH:mm:ss"
            )
          : "-"}
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_updateAt"}
        onClick={() => shortcutActions(api_id)}
      >
        <div style={{ textAlign: "center" }}>
          {updateAt
            ? dateandtime.format(
                new Date(Number(updateAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </div>
      </div>,
      <div
        className="clickable"
        key={api_id + "_api_lastOpen"}
        onClick={() => shortcutActions(api_id)}
      >
        <div style={{ textAlign: "center" }}>
          {lastOpen
            ? dateandtime.format(
                new Date(Number(lastOpen)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </div>
      </div>,
    ];
  };

  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          sortable={[
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            true,
            false,
            false,
          ]}
          defaultSortDirection="descending"
          initialSortColumnIndex={9}
          onSort={handleSort}
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "ID",
            "API Enable",
            "API Slug",
            "Mô tả",
            "Tên API",
            "Access Token",
            "Access Method",
            "API Role",
            "Thời gian tạo",
            "Thời gian cập nhập",
            "Lần cuối mở",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>
          {`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          margin-left: 1.6rem;
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }`}
        </style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        fullWidth
        title="Incoming Webhook"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack distribution="equalSpacing">
                  <IncomingWebhookFilter
                    queryValue={StringQuery?.query}
                    onChange={handleFiltersQueryChange}
                  />
                  <Select
                    label=""
                    value={selectedParentId}
                    onChange={handleSelectedChange}
                    options={[
                      { label: "All", value: "" },
                      { label: "Get", value: "get" },
                      { label: "Post", value: "post" },
                      { label: "Put", value: "put" },
                      { label: "Patch", value: "patch" },
                      { label: "Delete", value: "delete" },
                      { label: "Head", value: "head" },
                    ]}
                  />
                </Stack>
              </div>
              {PagesList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <GeneralIncomingWebhookCreate
        show={newModelactive}
        onClose={() => setNewModelactive(false)}
      />
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
