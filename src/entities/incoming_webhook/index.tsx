import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import incoming_webhook_list from "./incoming_webhook.list";
import incoming_webhook_edit from "./incoming_webhook.edit";
import incoming_webhook_view from "./incoming_webhook.view";
import incoming_webhook_create from "./incoming_webhook.create";

/**
 *   Create index file for Incoming_webhook
 */

export default function List_incoming_webhook() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = incoming_webhook_list;
      break;

    case "edit":
      ActualPage = incoming_webhook_edit;
      break;

    case "new":
      ActualPage = incoming_webhook_create;
      break;

    case "view":
      ActualPage = incoming_webhook_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
