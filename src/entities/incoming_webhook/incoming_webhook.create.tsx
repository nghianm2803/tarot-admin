import {
  Button,
  Card,
  Form,
  FormLayout,
  Modal,
  Select,
  Stack,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import {
  clearError,
  createEntity,
} from "../../store/incoming_webhook.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";

/**
 *   Create upload Modal for Incoming Webhook
 */

export default function GeneralIncomingWebhookCreate({ onClose, show }) {
  const dispatch = useAppDispatch();

  const entity = useAppSelector((state) => state.incoming_webhook.entity);
  const updating = useAppSelector((state) => state.incoming_webhook.updating);
  const errorMessage = useAppSelector(
    (state) => state.incoming_webhook.errorMessage
  );

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  const { v4: uuidv4 } = require("uuid");

  const [generateToken, setGenerateToken] = useState<string>("");

  const handleChangeToken = useCallback(() => {
    setGenerateToken(uuidv4());
  }, []);

  /**
   * Api-access-method : get, post, put, patch, delete, head
   */
  const [apiAccessMethod, setApiAccessMethod] = useState<any>("get");
  const handleApitAccessMethodChange = useCallback(
    (value) => setApiAccessMethod(value),
    []
  );

  const optionsApiAccessMethod = [
    { label: "Get", value: "get" },
    { label: "Post", value: "post" },
    { label: "Put", value: "put" },
    { label: "Patch", value: "patch" },
    { label: "Delete", value: "delete" },
    { label: "Head", value: "head" },
  ];

  /**
   * API role hook
   */
  const [apiRole, setApiRole] = useState<any>("admin");
  const handleApiRoleChange = useCallback((value) => setApiRole(value), []);

  const optionsApiRole = [
    { label: "Admin", value: "admin" },
    { label: "Advisor", value: "advisor" },
    { label: "User", value: "user" },
  ];

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.incoming_webhook_slug || false;

  useEffect(() => {
    reset_incoming();
  }, []);

  //Set generateToken = api_access_token
  useEffect(() => {
    if (entity) {
      setGenerateToken(entity.api_access_token);
    }
  }, [entity]);

  const useFields = {
    api_enable: useField<string>({
      value: "",
      validates: [],
    }),
    api_slug: useField<string>({
      value: "",
      validates: [
        lengthMoreThan(15, "API slug phải có ít nhất 15 ký tự."),
        notEmpty("API slug is required."),
        lengthLessThan(100, "Không được dài quá 100 ký tự."),
      ],
    }),
    api_description: useField<string>({
      value: "",
      validates: [
        lengthMoreThan(
          20,
          "Mô tả API phải có ít nhất 20 ký tự."
        ),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Mô tả API quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    api_name: useField<string>({
      value: "",
      validates: [
        lengthMoreThan(5, "Tên API phải có ít nhất 5 ký tự"),
        lengthLessThan(100, "No more than 100 characters."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tên API quá ngắn hoặc trống.";
          }
        },
      ],
    }),
    api_access_token: useField<string>({
      value: "",
      validates: [],
    }),
    api_access_method: useField<string>({
      value: "",
      validates: [],
    }),
    api_role: useField<string>({
      value: "",
      validates: [],
    }),
  };

  const {
    fields,
    submit,
    dirty,
    reset: reset_incoming,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          dispatch(
            createEntity({
              api_enable: Number(values.api_enable),
              api_description: values.api_description,
              api_name: values.api_name,
              api_access_token: values.api_access_token,
              api_access_method: apiAccessMethod,
              api_role: apiRole,
            })
          );
        }
        reset_incoming();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo mới incoming webhook"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoComplete="off"
                label="API Slug"
                {...fields.api_slug}
              />
              <TextField
                autoComplete="off"
                label="Mô tả"
                {...fields.api_description}
              />
              <TextField
                autoComplete="off"
                label="Tên API"
                {...fields.api_name}
              />
              <Stack>
                <Stack.Item fill>
                  <TextField
                    autoComplete="off"
                    label="API Token"
                    disabled
                    value={generateToken}
                  />
                </Stack.Item>
                <div className="change-token">
                  <Stack.Item>
                    <Button primary onClick={handleChangeToken}>
                      Generate
                    </Button>
                  </Stack.Item>
                </div>
              </Stack>
              <Select
                label="Access Method"
                options={optionsApiAccessMethod}
                onChange={handleApitAccessMethodChange}
                value={apiAccessMethod}
              />
              <Select
                label="API Role"
                options={optionsApiRole}
                onChange={handleApiRoleChange}
                value={apiRole}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
