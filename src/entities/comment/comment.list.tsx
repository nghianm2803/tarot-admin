import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  IndexTable,
  Layout,
  Modal,
  Page,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
  useIndexResourceState,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "config/store";
import debounce from "lodash.debounce";
import emptyIMG from "media/empty.png";
import Pagination from "components/pagination";
import {
  clearError,
  deleteEntity,
  getEntities,
  updateEntity,
} from "store/comment.store.reducer";
import helpers from "../../helpers";
import SearchFilter from "./filter";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Comment_List() {
  const entity = useAppSelector((state) => state.comment.entity);
  const entities = useAppSelector((state) => state.comment.entities);
  const loading = useAppSelector((state) => state.comment.loading);
  const errorMessage = useAppSelector((state) => state.comment.errorMessage);
  const totalItems = useAppSelector((state) => state.comment.totalItems);
  const updating = useAppSelector((state) => state.comment.updating);
  const updateSuccess = useAppSelector((state) => state.comment.updateSuccess);

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * Comment status hook
   */
  const [statusSelected, setStatusSelected] = useState<string>("draft");
  const handleStatusChange = useCallback(
    (value) => setStatusSelected(value),
    []
  );
  const optionsStatus = [
    { label: "Draft", value: "draft" },
    { label: "Spam", value: "spam" },
    { label: "Publish", value: "publish" },
    { label: "Delete", value: "delete" },
  ];

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery = helpers.ExtractUrl(useParam.search) || false;
  let Param = useParam.comment_slug || false;

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Bình luận đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "comment_id,desc",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} onDismiss={toggleActive} />
  ) : null;

  const toastNotification = message ? (
    <Toast content={message} onDismiss={toggleActive} />
  ) : null;

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);
  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/comment" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    if (entity && entity.comment_status) {
      setStatusSelected(entity.comment_status);
    }
  }, [entity]);

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  const [showEditModel, setShowEditModel] = useState<boolean>(false);

  const closeModal = useCallback(() => {
    setShowEditModel((active) => !active);
  }, [showEditModel]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   * Delete comment
   */

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(entities, { resourceIDResolver });
  function resourceIDResolver(comment_data: any) {
    return comment_data.comment_id ?? null;
  }

  function _deleteEntityAction() {
    setOpenModal(true);
  }

  /**
   * Delete modal ...
   */
  const onModalAgree = useCallback(() => {
    dispatch(deleteEntity(selectedResources.join(",")));
    setOpenModal(false);
    // @ts-ignore
    handleSelectionChange("all", false); // <~~ This will trigger the recalculation
  }, [selectedResources]);

  useEffect(() => {
    dispatch(getEntities(mainQuery));
  }, [updateSuccess]);

  /**
   * must use callback function ...
   */
  const [commentDetail, setCommentDetail] = useState<any>(null);
  const getCommentDetail = useCallback((comment_data: any) => {
    setCommentDetail(comment_data);
    setShowEditModel(true);
  }, []);

  const { fields, dirty, submit, submitting } = useForm({
    fields: {
      comment_title: useField<string>({
        value: commentDetail?.comment_title ?? "",
        validates: [],
      }),
      comment_content: useField<string>({
        value: commentDetail?.comment_content ?? "",
        validates: [
          notEmpty("Nội dung trống!"),
          lengthMoreThan(5, "Nội dung không được ít hơn 5 ký tự"),
          lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        ],
      }),
      comment_point: useField<string>({
        value: commentDetail?.comment_point + "" ?? "",
        validates: [
          notEmpty("Điểm trống!"),
          lengthLessThan(2, "Điểm phải là số từ 0-5."),
          (inputValue) => {
            if (!helpers.isNumber(inputValue)) {
              return "Điểm không hợp lệ!";
            }
          },
        ],
      }),
      comment_status: useField<string>({
        value: commentDetail?.comment_status ?? "",
        validates: [],
      }),
    },
    async onSubmit(values) {
      try {
        dispatch(
          updateEntity({
            comment_id: commentDetail.comment_id,
            comment_title: values.comment_title,
            comment_content: values.comment_content,
            comment_point: Number(values.comment_point),
            comment_status: statusSelected,
          })
        );
        setShowEditModel(false);
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * read out commentDetail
   */
  const detailModal = (
    <Modal
      activator={null}
      open={showEditModel}
      onClose={closeModal}
      title="Chi tiết bình luận"
      primaryAction={{
        content: "Lưu",
        disabled: !dirty,
        loading: submitting,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: closeModal,
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card.Section>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                requiredIndicator
                label="Tiêu đề"
                {...fields.comment_title}
              />
              <TextField
                autoComplete="off"
                requiredIndicator
                label="Nội dung"
                {...fields.comment_content}
              />
              <TextField
                autoComplete="off"
                type="number"
                min={0}
                max={5}
                requiredIndicator
                label="Điểm"
                {...fields.comment_point}
              />
              <Select
                label="Trạng thái"
                options={optionsStatus}
                onChange={handleStatusChange}
                value={statusSelected}
              />
            </FormLayout>
          </Card.Section>
        </Form>
      </Modal.Section>
    </Modal>
  );

  /**
   *  Index table
   */
  function IndexTableComment() {
    const resourceName = {
      singular: "comment",
      plural: "comments",
    };

    const promotedBulkActions = [
      {
        content: "Xoá bình luận",
        onAction: _deleteEntityAction,
      },
    ];

    const rowMarkup = entities?.map((props, index) => {
      const {
        comment_id,
        user,
        comment_title,
        comment_content,
        object_id,
        comment_type,
        comment_status,
        comment_point,
      } = props;
      return (
        <IndexTable.Row
          id={comment_id}
          key={comment_id}
          selected={selectedResources.includes(comment_id)}
          position={index}
        >
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_id"}
              onClick={() => getCommentDetail(props)}
            >
              {user.display_name}
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_title"}
              onClick={() => getCommentDetail(props)}
            >
              <div style={{ fontWeight: "bold" }}>{comment_title}</div>
              {helpers.trimContentString(comment_content)}
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_objectID"}
              onClick={() => getCommentDetail(props)}
            >
              <div style={{ textAlign: "center" }}>{object_id}</div>
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_type"}
              onClick={() => getCommentDetail(props)}
            >
              {comment_type}
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_status"}
              onClick={() => getCommentDetail(props)}
            >
              {comment_status}
            </div>
          </IndexTable.Cell>
          <IndexTable.Cell>
            <div
              key={comment_id + "_comment_point"}
              onClick={() => getCommentDetail(props)}
            >
              <div style={{ textAlign: "center" }}>{comment_point}</div>
            </div>
          </IndexTable.Cell>
        </IndexTable.Row>
      );
    });

    return (
      <Card>
        <IndexTable
          loading={loading}
          resourceName={resourceName}
          itemCount={entities.length}
          selectedItemsCount={
            allResourcesSelected ? "All" : selectedResources.length
          }
          onSelectionChange={handleSelectionChange}
          promotedBulkActions={promotedBulkActions}
          headings={[
            { title: "Tên người dùng" },
            { title: "Tiêu đề & nội dung" },
            { title: "Object id" },
            { title: "Type" },
            { title: "Tình trạng" },
            { title: "Điểm" },
          ]}
        >
          {rowMarkup}
        </IndexTable>
      </Card>
    );
  }

  const emptyData = (
    <EmptyState heading="No comment to show here!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const PagesList =
    totalItems > 0 ? (
      <>
        <IndexTableComment />
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page title="Bình luận &amp; Đánh giá">
      {detailModal}
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack alignment="center">
                <Stack.Item fill>
                  <SearchFilter
                    queryValue={StringQuery?.query}
                    onChange={handleFiltersQueryChange}
                  />
                </Stack.Item>
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  /**
   * Delete modal
   * selectedResources <------ Toan bo ID checked o day = array
   */
  const commentArray = [selectedResources];
  commentArray.join(",");

  const _Modal = (
    <Modal
      activator={null}
      open={openModal}
      onClose={() => {
        setOpenModal(false);
      }}
      title="Xoá bình luận?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
        destructive: true,
        loading: updating,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Bạn có chắc chắn muốn xoá {selectedResources.length} bình luận?</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  );

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
