import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";
import general_service_list from "./general_service.list";
import general_service_edit from "./general_service.edit";
import general_service_view from "./general_service.view";

/**
 *   Create index file for General_service
 */
export default function List_general_service() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";
  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = general_service_list;
      break;

    case "edit":
      ActualPage = general_service_edit;
      break;

    case "new":
      ActualPage = general_service_edit;
      break;

    case "view":
      ActualPage = general_service_view;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
