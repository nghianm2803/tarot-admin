import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import { clearError, deleteEntity, getEntity, updateEntity } from "../../store/general_service.store.reducer";
import { lengthLessThan, lengthMoreThan, notEmpty, useField, useForm } from "@shopify/react-form";
import SkeletonLoading from "components/skeleton_loading";

export default function Edit_general_service() {
  const entity = useAppSelector((state) => state.general_service.entity);
  const updating = useAppSelector((state) => state.general_service.updating);
  const loading = useAppSelector((state) => state.general_service.loading);
  const errorMessage = useAppSelector((state) => state.general_service.errorMessage);
  const updateSuccess = useAppSelector((state) => state.general_service.updateSuccess);

  // jamviet.com
  const [message, setMessage] = useState<string | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [notification, setNotification] = useState<string | null>(null);

  const dispatch = useAppDispatch();

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, [dispatch]);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Dịch vụ đã được cập nhật!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }
  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  /**
   * Service type hook
   */
  const [serviceType, setServiceType] = useState<string>("chat");
  const handleServiceType = useCallback((value) => setServiceType(value), []);

  const optionsType = [
    { label: "Live Chat", value: "chat" },
    { label: "Live Voice Call", value: "call" },
    { label: "Video Reading", value: "clip" },
    { label: "Fast Video Reading", value: "qclip" },
  ];

  /**
   * Service unit hook
   */
  const [serviceUnit, setServiceUnit] = useState<string>("second");
  const handleServiceUnitChange = useCallback((value) => setServiceUnit(value), []);

  const optionsUnit = [
    { label: "Giây", value: "second" },
    { label: "Phút", value: "minute" },
    { label: "Giờ", value: "hour" },
    { label: "Ngày", value: "day" },
  ];

  /**
   * Service active hook
   */
  const [optionServiceActive, setOptionServiceActive] = useState<boolean>(false);
  const handleServiceActiveChange = useCallback((_checked, newValue) => {
    setOptionServiceActive(newValue === "on");
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.general_service_slug || false;
  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else reset();
  }, []);

  useEffect(() => {
    if (updateSuccess) setNotification("Cập nhật thành công!");
  }, [updateSuccess]);

  useEffect(() => {
    if (entity && entity.service_type) {
      setServiceType(entity.service_type);
      setOptionServiceActive(Boolean(entity.service_active));
    }
  }, [entity]);

  const useFields = {
    service_name: useField<string>({
      value: entity?.service_name ?? "",
      validates: [
        notEmpty("Tên dịch vụ trống!"),
        lengthLessThan(25, "Không được dài hơn 25 ký tự"),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    service_description: useField<string>({
      value: entity?.service_description ?? "",
      validates: [
        notEmpty("Mô tả trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(5, "Không được ngắn hơn 5 ký tự."),
      ],
    }),
    service_price: useField<string>({
      value: entity?.service_price + "" ?? "",
      validates: [notEmpty("Giá dịch vụ trống!"), lengthLessThan(5, "Không được ngắn hơn 5 ký tự.")],
    }),
    service_duration: useField<string>({
      value: entity?.service_duration + "" ?? "",
      validates: [notEmpty("Thời gian dịch vụ trống!"), lengthLessThan(50, "Không được ngắn hơn 50 ký tự.")],
    }),
    service_unit: useField<string>({
      value: entity?.service_unit ?? "",
      validates: [],
    }),
    service_type: useField<string>({
      value: entity?.service_type ?? "",
      validates: [],
    }),
    service_active: useField<string>({
      value: entity?.service_active + "" ?? "",
      validates: [],
    }),
  };

  const { fields, submit, dirty, reset } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (Param) {
          dispatch(
            updateEntity({
              service_id: entity.service_id,
              service_name: values.service_name,
              service_description: values.service_description,
              service_price: Number(values.service_price),
              service_duration: Number(values.service_duration),
              service_type: serviceType,
              service_active: Boolean(optionServiceActive),
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="General Service" breadcrumbs={[{ content: "General_service list", url: "/general_service" }]}>
      <EmptyState heading="Không có dịch vụ ở đây!" image={emptyIMG}>
        <p>Dịch vụ này có thể không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  let availableTimeContent = null;
  try {
    availableTimeContent = JSON.parse(entity.service_available_time);
  } catch (e) {
    availableTimeContent = String(availableTimeContent);
  }

  const Actual_page =
    entity || !Param ? (
      <>
        <Page title="General Service" breadcrumbs={[{ content: "General_service list", url: "/general_service" }]}>
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField autoFocus autoComplete="off" label="Tên dịch vụ" {...fields.service_name} />
                      <TextField autoComplete="off" label="Mô tả dịch vụ" {...fields.service_description} />
                      <TextField
                        type="number"
                        autoComplete="off"
                        min={0}
                        label="Giá dịch vụ (coin)"
                        {...fields.service_price}
                      />
                      <Select
                        label="Đơn vị"
                        options={optionsUnit}
                        onChange={handleServiceUnitChange}
                        value={serviceUnit}
                      />
                      {/* <TextField
                        type="number"
                        min={1}
                        autoComplete="off"
                        label="Service available time"
                        value={availableTimeContent.start_time}
                      />
                      <TextField
                        type="number"
                        min={1}
                        autoComplete="off"
                        label="Service available time"
                        value={availableTimeContent.end_time}
                      /> */}
                      <TextField
                        type="number"
                        min={1}
                        autoComplete="off"
                        label="Thời gian"
                        {...fields.service_duration}
                      />
                      <Select
                        label="Kiểu dịch vụ"
                        options={optionsType}
                        onChange={handleServiceType}
                        value={serviceType}
                      />
                      <Stack vertical>
                        <RadioButton
                          label="Service active ON"
                          id="on"
                          checked={optionServiceActive === true}
                          onChange={handleServiceActiveChange}
                        />
                        <RadioButton
                          label="Service active OFF"
                          checked={optionServiceActive === false}
                          id="off"
                          onChange={handleServiceActiveChange}
                        />
                      </Stack>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : null
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );
  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Xoá dịch vụ này?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

