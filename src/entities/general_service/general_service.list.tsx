import {
  Badge,
  Card,
  DataTable,
  EmptyState,
  Layout,
  Page,
  Select,
  Stack,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import {
  clearError,
  getEntities,
} from "../../store/general_service.store.reducer";
import helpers from "../../helpers";
import dateandtime from "date-and-time";
import GeneralServiceCreate from "./general_service.create";
import SkeletonLoading from "components/skeleton_loading";
import SearchFilter from "./filter";

export default function General_Service_List() {
  const entities = useAppSelector((state) => state.general_service.entities);
  const loading = useAppSelector((state) => state.general_service.loading);
  const errorMessage = useAppSelector(
    (state) => state.general_service.errorMessage
  );
  const totalItems = useAppSelector(
    (state) => state.general_service.totalItems
  );

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    sort: "service_id,desc",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  const [queryValue, setQueryValue] = useState<string>("");
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );

  /**
   * Change page number
   */
  const onChangePageNumber = useCallback((numPage) => {
    setMainQuery({ ...mainQuery, page: numPage });
  }, []);

  const [newModelactive, setNewModelactive] = useState<boolean>(false);
  const toggleNewActive = useCallback(
    () => setNewModelactive((active) => !active),
    []
  );

  const [selectedParentId, setSelectedParentId] = useState<string>("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
    setMainQuery({
      ...mainQuery,
      ...{ query: mainQuery.query + `&service_type=${_value}` },
    });
  }, []);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      history("/general_service" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "")
          setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param service_id
   */
  const shortcutActions = (service_id: number) => {
    history("/general_service/edit/" + service_id);
  };

  /**
   * Coin
   */

  const coin = (
    <>
      <svg
        data-v-2d000d8f=""
        // viewBox="-5 -10 35 35"
        viewBox="0 0 24 24 "
        style={{
          width: "18px",
          height: "18px",
          margin: "0px 0px -3px 2px",
          fill: "#ee9b00",
        }}
      >
        <path
          data-v-2d000d8f=""
          d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 3c-4.971 0-9 4.029-9 9s4.029 9 9 9 9-4.029 9-9-4.029-9-9-9zm1 13.947v1.053h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.385-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85v-1.067h1v1.018c.725.019 1.535.145 2.442.42l-.362 1.648c-.768-.27-1.616-.515-2.442-.465-1.489.087-1.62 1.376-.581 1.916 1.711.804 3.943 1.401 3.943 3.546.002 1.718-1.344 2.632-3 2.864z"
        ></path>
      </svg>
    </>
  );

  const emptyData = (
    <EmptyState heading="No service here!" image={emptyIMG}>
      <p>Oh! Không có dịch vụ ở đây! Thử bỏ bộ lọc và thêm dịch vụ!</p>
    </EmptyState>
  );

  const renderItem = (general_service: any) => {
    const {
      service_id,
      service_name,
      service_description,
      service_price,
      service_duration,
      service_type,
      service_active,
      service_unit,
      createAt,
      updateAt,
    } = general_service;
    return [
      <div
        className="clickable"
        key={service_id + "_service_id"}
        onClick={() => shortcutActions(service_id)}
      >
        {service_id}
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_name"}
        onClick={() => shortcutActions(service_id)}
      >
        {service_name}
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_des"}
        onClick={() => shortcutActions(service_id)}
      >
        <p>{service_description}</p>
      </div>,
      <div
        style={{ textAlign: "right" }}
        className="clickable"
        key={service_id + "_service_price"}
        onClick={() => shortcutActions(service_id)}
      >
        {service_price} {coin}
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_duration"}
        onClick={() => shortcutActions(service_id)}
      >
        <div style={{ textAlign: "center" }}>
          {service_duration}
          {service_unit === "second"
            ? " sec"
            : service_unit === "minute"
            ? " min"
            : service_unit === "hour"
            ? " hour"
            : " day"}
        </div>
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_type"}
        onClick={() => shortcutActions(service_id)}
      >
        <Badge status="info">{service_type}</Badge>
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_active"}
        onClick={() => shortcutActions(service_id)}
      >
        {service_active === 1 ? (
          <Badge status="success">Bật</Badge>
        ) : (
          <Badge status="critical">Tắt</Badge>
        )}
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_createAt"}
        onClick={() => shortcutActions(service_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
      <div
        className="clickable"
        key={service_id + "_service_updateAt"}
        onClick={() => shortcutActions(service_id)}
      >
        <time>
          {updateAt
            ? dateandtime.format(
                new Date(Number(updateAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "ID",
            "Tên dịch vụ",
            "Mô tả",
            "Giá",
            "Thời gian",
            "Type",
            "Active",
            "Thời gian tạo",
            "Thời gian cập nhật",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        title="Dịch vụ chung"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack alignment="center">
                  <Stack.Item fill>
                    <SearchFilter
                      queryValue={StringQuery?.query}
                      onChange={handleFiltersQueryChange}
                    />
                  </Stack.Item>
                  <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "All", value: "" },
                        { label: "Live Chat", value: "chat" },
                        { label: "Live Voice Call", value: "call" },
                        { label: "Video Reading", value: "clip" },
                        { label: "Fast Video Reading", value: "qclip" },
                      ]}
                    />
                  </Stack.Item>
                </Stack>
              </div>
              {PagesList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <GeneralServiceCreate
        show={newModelactive}
        onClose={() => setNewModelactive(false)}
      />
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
