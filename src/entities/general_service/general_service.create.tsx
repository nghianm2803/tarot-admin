import {
  FormLayout,
  Modal,
  TextField,
  Form,
  Card,
  Toast,
  Select,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/general_service.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";
import { useParams } from "react-router-dom";

/**
 *   Create upload Modal for General Service
 */
export default function GeneralServiceCreate({ onClose, show }) {
  const dispatch = useAppDispatch();

  const entity = useAppSelector((state) => state.general_service.entity);
  const updating = useAppSelector((state) => state.general_service.updating);
  const errorMessage = useAppSelector(
    (state) => state.general_service.errorMessage
  );

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, [dispatch]);

  /**
   * Service type hook
   */
  const [serviceType, setServiceType] = useState<string>("chat");
  const handleServiceTypeChange = useCallback(
    (value) => setServiceType(value),
    []
  );

  const optionsType = [
    { label: "Live Chat", value: "chat" },
    { label: "Live Voice Call", value: "call" },
    { label: "Video Reading", value: "clip" },
    { label: "Fast Video Reading", value: "qclip" },
  ];

  /**
   * Service unit hook
   */
  const [serviceUnit, setServiceUnit] = useState<string>("second");
  const handleServiceUnitChange = useCallback(
    (value) => setServiceUnit(value),
    []
  );

  const optionsUnit = [
    { label: "Giây", value: "second" },
    { label: "Phút", value: "minute" },
    { label: "Giờ", value: "hour" },
    { label: "Ngày", value: "day" },
  ];

  /**
   * Service active hook
   */
  const [serviceActiveSelected, setServiceActiveSelected] = useState<string>("on");
  const handleServiceActiveChange = useCallback(
    (value) => setServiceActiveSelected(value),
    []
  );

  const serviceActive = [
    { label: "Bật", value: "on" },
    { label: "Tắt", value: "off" },
  ];

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.general_service_slug || false;

  useEffect(() => {
    reset();
  }, []);

  useEffect(() => {
    if (entity && entity.service_type) {
      setServiceType(entity.service_type);
      setServiceActiveSelected(entity.service_active);
    }
  }, [entity]);

  const useFields = {
    service_name: useField<string>({
      value: "",
      validates: [
        notEmpty("Tên dịch vụ trống!"),
        lengthLessThan(25, "Không được dài hơn 25 ký tự"),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    service_description: useField<string>({
      value: "",
      validates: [
        notEmpty("Mô tả trống!"),
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(5, "Không được ngắn hơn 5 ký tự."),
      ],
    }),

    service_price: useField<string>({
      value: "",
      validates: [
        notEmpty("Giá dịch vụ trống!"),
        lengthLessThan(5, "Không được ngắn hơn 5 ký tự."),
      ],
    }),
    service_duration: useField<string>({
      value: "",
      validates: [
        notEmpty("Thời gian dịch vụ trống!"),
        lengthLessThan(50, "Không được dài hơn 50 ký tự."),
      ],
    }),
    service_available_time: useField<string>({
      value: "{\"start_time\":1660701600000,\"end_time\":1660752000000}",
      validates: [],
    }),
    service_unit: useField<string>({
      value: "",
      validates: [],
    }),
    service_type: useField<string>({
      value: "",
      validates: [],
    }),
    service_active: useField<string>({
      value: "",
      validates: [],
    }),
    service_meta: useField<string>({
      value: "",
      validates: [],
    }),
  };

  const { fields, submit, dirty, reset, submitErrors } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              service_name: values.service_name,
              service_description: values.service_description,
              service_price: Number(values.service_price),
              service_duration: Number(values.service_duration),
              service_available_time: values.service_available_time,
              service_type: serviceType,
              service_unit: serviceUnit,
              service_active: Boolean(serviceActiveSelected),
              service_meta: values.service_meta,
            })
          );
        }
        reset();
        onClose();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Log debug
   */
  // useEffect(() => {
  //   console.log('Debug here!!!', submitErrors);
  // }, [submitErrors]);

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo dịch vụ mới"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                label="Tên dịch vụ"
                {...fields.service_name}
              />
              <TextField
                autoComplete="off"
                label="Mô tả dịch vụ"
                {...fields.service_description}
              />
              <TextField
                type="number"
                min="0"
                autoComplete="off"
                label="Giá dịch vụ (coin)"
                {...fields.service_price}
              />
              <Select
                label="Đơn vị"
                options={optionsUnit}
                onChange={handleServiceUnitChange}
                value={serviceUnit}
              />
              <TextField
                type="number"
                autoComplete="off"
                min={1}
                label="Thời gian"
                {...fields.service_duration}
              />
              <Select
                label="Kiểu dịch vụ"
                options={optionsType}
                onChange={handleServiceTypeChange}
                value={serviceType}
              />
              <Select
                label="Trạng thái dịch vụ"
                options={serviceActive}
                onChange={handleServiceActiveChange}
                value={serviceActiveSelected}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
