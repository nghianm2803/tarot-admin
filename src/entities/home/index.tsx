import { Layout, Card, Page, Button } from "@shopify/polaris";

export default function Home(props: any) {
  return (
    <>
      <Page fullWidth>
        <Layout sectioned={false}>
          <Layout.Section>
            <Card title="Order details" sectioned>
              <p>View a summary of your order.</p>
            </Card>
          </Layout.Section>
          <Layout.Section secondary>
            <Card title="Admin support" sectioned>
              <Button primary url="https://admin.fuda.appuni.io/chat">
              {/* <Button primary url="https://fudaly.appuni.io/chat/"> */}
                Go to chat website
              </Button>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
      <Page title="Products" fullWidth>
        <p>
          When things are getting interesting, those things can play very powerful role for any kind of business. Good
          presentation of logo design is very important on these days because of this era is really competitive. You can
          see people do lots of business in a same criteria, suppose one person can do graphic design service business
          beside him you can see other people also doing same business so its like red ocean zone where competition rate
          is very high.
        </p>
        <p>
          When things are getting interesting, those things can play very powerful role for any kind of business. Good
          presentation of logo design is very important on these days because of this era is really competitive. You can
          see people do lots of business in a same criteria, suppose one person can do graphic design service business
          beside him you can see other people also doing same business so its like red ocean zone where competition rate
          is very high.
        </p>
        <p>
          I have a huge passion for tech, design, and marketers so I wanted to share everything I find and uncover in an
          easy to share format! 😊
        </p>
      </Page>
    </>
  );
}

