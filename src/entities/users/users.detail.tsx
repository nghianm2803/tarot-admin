import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  Tabs,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import { clearError, getEntity, updateEntity, reset } from "../../store/users.store.reducer";
import { lengthLessThan, lengthMoreThan, notEmpty, useField, useForm } from "@shopify/react-form";
import helpers from "helpers";
import countriesData from "components/countryData";
import SkeletonLoading from "components/skeleton_loading";

export default () => {
  const entity = useAppSelector((state) => state.users.entity);
  const updating = useAppSelector((state) => state.users.updating);
  const loading = useAppSelector((state) => state.users.loading);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const updateSuccess = useAppSelector((state) => state.users.updateSuccess);

  const dispatch = useAppDispatch();

  const [notification, setNotification] = useState(null);
  const [user_role_selected, setUser_role_selected] = useState("user");
  const [user_nation_selected, setUser_nation_selected] = useState("");
  const [srcImageAvatar, setSrcImageAvatar] = useState("");

  const option_country = countriesData?.countriesData;

  const changeHandler = (value) => {
    setUser_nation_selected(value);
  };

  const handleSelectChange = useCallback((value) => setUser_role_selected(value), []);

  const options = [
    { label: "User", value: "user" },
    { label: "Advisor", value: "advisor" },
    { label: "Admin", value: "admin" },
  ];
  const [optionButtonValue, setOptionButtonValue] = useState(false);
  const handleRadioButtonChange = useCallback((_checked, newValue) => {
    setOptionButtonValue(newValue === "enabled");
  }, []);

  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback((selectedTabIndex) => setSelected(selectedTabIndex), []);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.users_slug || false;
  useEffect(() => {
    if (Param) {
      dispatch(getEntity(Param));
    } else {
      dispatch(reset());
    }
  }, []);

  useEffect(() => {
    if (entity && entity?.user_avatar) {
      setSrcImageAvatar(entity?.user_avatar);
    }
  }, [entity]);

  useEffect(() => {
    if (entity && entity.user_role) {
      setUser_role_selected(entity.user_role);
      setUser_nation_selected(entity.user_nation);
      setOptionButtonValue(Boolean(entity.user_status));
    }
  }, [entity]);

  useEffect(() => {
    if (updateSuccess) {
      setNotification("Cập nhật thành công!");
    }
  }, [updateSuccess]);

  const useFields = {
    user_email: useField<string>({
      value: entity?.user_email ?? "",
      validates: [
        notEmpty("Email đang trống!"),
        (inputValue) => {
          if (!helpers.isEmail(inputValue)) {
            return "Email không hợp lệ!";
          }
        },
      ],
    }),

    display_name: useField<string>({
      value: entity?.display_name ?? "",
      validates: [lengthLessThan(250, "Không được dài hơn 250 ký tự."), lengthMoreThan(2, "Your name is too short.")],
    }),

    user_numberphone: useField<string>({
      value: entity?.user_numberphone ?? "",
      validates: [],
    }),

    user_job: useField<string>({
      value: entity?.user_job ?? "",
      validates: [lengthLessThan(50, "Nội dung quá dài"), lengthMoreThan(3, "Nội dung quá ngắn")],
    }),
    user_balance: useField<string>({
      value: entity?.user_balance ?? "",
      validates: [],
    }),

    user_address: useField<string>({
      value: entity?.user_address ?? "",
      validates: [lengthLessThan(200, "Nội dung quá dài"), lengthMoreThan(3, "Nội dung quá ngắn")],
    }),

    bio: useField<string>({
      value: entity?.bio ?? "",
      validates: [],
    }),
  };

  const useFieldsSecurity = {
    user_pass: useField<string>({
      value: "",
      validates: [
        (inputValue) => {
          if (helpers.isUTF8(inputValue)) {
            return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
          }
          if (inputValue.length > 0 && helpers.getPasswordStrength(inputValue) < 2) {
            return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
          }
        },
      ],
    }),

    retype_password: useField<string>({
      value: "",
      validates: [
        lengthMoreThan(9, "Mật khẩu quá ngắn!"),
        (inputValue) => {
          if (inputValue) {
            if (helpers.isUTF8(inputValue)) {
              return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
            }
            if (helpers.getPasswordStrength(inputValue) < 2) {
              return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
            }
          }
        },
      ],
    }),
  };

  const {
    fields,
    submit: submitGeneral,
    dirty: dirtyGeneral,
    reset: Userreset,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        dispatch(
          updateEntity({
            user_id: entity.user_id,
            user_email: values.user_email,
            user_role: user_role_selected,
            display_name: values.display_name,
            user_numberphone: values.user_numberphone,
            user_job: values.user_job,
            user_nation: user_nation_selected,
            user_address: values.user_address,
            bio: values.bio,
          })
        );
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const {
    fields: fieldSecurity,
    submit: submitSecurity,
    dirty: dirtySecurity,
  } = useForm({
    fields: useFieldsSecurity,
    async onSubmit(values) {
      try {
        if (values.user_pass && values.user_pass !== values.retype_password) {
          alert("Hai mật khẩu không khớp nhau!");
          return { status: "fail", errors: [] };
        }
        dispatch(
          updateEntity({
            user_id: entity.user_id,
            user_role: user_role_selected,
            user_pass: values.user_pass,
            user_status: Number(optionButtonValue),
          })
        );
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Users" breadcrumbs={[{ content: "User list", url: "/users" }]}>
      <EmptyState heading="No users here!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? <Toast content={notification} onDismiss={toggleActive} /> : null;

  const tabs = [
    {
      id: "general-tab",
      content: "Thông tin chung",
      panelID: "tab1-content",
    },
    {
      id: "security-tab",
      content: "Thông tin bảo mật",
      panelID: "tab2-content",
    },
  ];

  const generalTabContent = [
    <Form onSubmit={submitGeneral}>
      <FormLayout>
        <div className="profile-edit-avatar">
          <img src={srcImageAvatar} crossOrigin="anonymous" alt="User avatar" />
        </div>
        <TextField label="Email" autoFocus autoComplete="off" requiredIndicator {...fields.user_email} />
        <TextField label="Tên người dùng" requiredIndicator autoComplete="off" {...fields.display_name} />
        <TextField label="Số điện thoại" type="number" autoComplete="off" {...fields.user_numberphone} />
        <Select label="Vai trò" options={options} onChange={handleSelectChange} value={user_role_selected} />
        <TextField label="Số dư" disabled={true} autoComplete="off" {...fields.user_balance} />
        <TextField label="Công việc" maxLength={1000} autoComplete="off" {...fields.user_job} />
        <Select label="Quốc gia" options={option_country} onChange={changeHandler} value={user_nation_selected} />
        <TextField label="Địa chỉ" maxLength={1000} autoComplete="off" {...fields.user_address} />
        <TextField label="Thông tin" multiline={2} autoComplete="off" {...fields.bio} />
      </FormLayout>
    </Form>,
  ];

  const securityTabContent = [
    <Form onSubmit={submitSecurity}>
      <FormLayout>
        <TextField
          maxLength={1000}
          requiredIndicator
          label="Mật khẩu"
          autoComplete="off"
          {...fieldSecurity.user_pass}
        />
        <TextField
          autoComplete="off"
          requiredIndicator
          label="Xác nhận mật khẩu"
          helpText={<p>Gõ lại mật khẩu của bạn!</p>}
          {...fieldSecurity.retype_password}
        />
        <Stack vertical>
          <RadioButton
            label="Tài khoản bị vô hiệu hoá"
            helpText="Người dùng không thể đăng nhập"
            checked={optionButtonValue === false}
            id="disabled"
            name="accounts"
            value="0"
            onChange={handleRadioButtonChange}
          />
          <RadioButton
            label="Tài khoản được kích hoạt"
            helpText="Người dùng có thể đăng nhập"
            id="enabled"
            name="accounts"
            value="1"
            checked={optionButtonValue === true}
            onChange={handleRadioButtonChange}
          />
        </Stack>
      </FormLayout>
    </Form>,
  ];

  const tabContent = selected ? securityTabContent : generalTabContent;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Danh sách người dùng"
          breadcrumbs={[{ content: "Danh sách người dùng", url: "/users" }]}
          primaryAction={{
            content: "Lưu",
            // disabled: selected ? !dirtySecurity : !dirtyGeneral,
            loading: updating,
            onAction: selected ? submitSecurity : submitGeneral,
          }}
        >
          <Layout>
            <Layout.Section>
              {" "}
              <Tabs selected={selected} onSelect={handleTabChange} tabs={tabs}>
                <Card.Section title={tabs[selected].content}></Card.Section>
              </Tabs>
              <Card>
                <Card.Section> {tabContent}</Card.Section>
              </Card>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: selected ? submitSecurity : submitGeneral,
              // disabled: selected ? !dirtySecurity : !dirtyGeneral,
              loading: updating,
            }}
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  return (
    <>
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
};

