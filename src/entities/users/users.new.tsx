import {
  FormLayout,
  Modal,
  TextField,
  Form,
  Card,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect } from "react";
import { useAppDispatch, useAppSelector } from "config/store";
import { clearError, createEntity } from "store/users.store.reducer";
import helpers from "helpers";
import {
  lengthLessThan,
  lengthMoreThan,
  notEmpty,
  useField,
  useForm,
} from "@shopify/react-form";

/**
 *   Create upload Modal for Notification
 */

// eslint-disable-next-line import/no-anonymous-default-export

export default function NotificationUpload({ onClose, show }) {
  const dispatch = useAppDispatch();

  const updating = useAppSelector((state) => state.users.updating);
  const loading = useAppSelector((state) => state.users.loading);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const updateSuccess = useAppSelector((state) => state.users.updateSuccess);

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  useEffect(() => {
    // if (Param) {
    //   dispatch(getEntity(Param));
    // } else {
    formReset();
    // }
  }, []);

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: formReset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: {
      user_email: useField<string>({
        value: "",
        validates: [
          notEmpty("Email đang trống!"),
          (inputValue) => {
            if (!helpers.isEmail(inputValue)) {
              return "Email không hợp lệ!";
            }
          },
        ],
      }),

      display_name: useField<string>({
        value: "",
        validates: [
          lengthLessThan(250, "Không được dài hơn 250 ký tự."),
          (inputValue) => {
            if (inputValue.length < 3) {
              return "Tên hiển thị quá ngắn!";
            }
          },
        ],
      }),

      user_pass: useField<string>({
        value: "",
        validates: [
          (inputValue) => {
            if (helpers.isUTF8(inputValue)) {
              return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
            }
            if (
              inputValue.length > 0 &&
              helpers.getPasswordStrength(inputValue) < 2
            ) {
              return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
            }
          },
        ],
      }),

      retype_password: useField<string>({
        value: "",
        validates: [
          lengthMoreThan(9, "Mật khẩu quá ngắn!"),
          (inputValue) => {
            if (inputValue) {
              if (helpers.isUTF8(inputValue)) {
                return "Không nên dùng mã Unicode trong mật khẩu của bạn!";
              }
              if (helpers.getPasswordStrength(inputValue) < 2) {
                return "Mật khẩu của bạn quá yếu, hãy trộn lẫn cả số, chữ và các ký tự đặc biệt khó đoán.";
              }
            }
          },
        ],
      }),
    },

    async onSubmit(values) {
      try {
        if (values.user_pass && values.user_pass !== values.retype_password) {
          alert("Hai mật khẩu không khớp nhau!");
          return { status: "fail", errors: [] };
        }
        // create new
        dispatch(
          createEntity({
            user_email: values.user_email,
            display_name: values.display_name,
            user_pass: values.user_pass,
            user_status: 1,
            // user_login: "testttt",
          })
        );
        formReset();

        onClose();

        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ?? "Undefined error. Try again!";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });
  /* End create notification form */

  const Actual_page = (
    <Modal
      open={show}
      onClose={() => onClose()}
      title={"Tạo người dùng mới"}
      primaryAction={{
        content: "Tạo",
        disabled: !dirty,
        loading: updating,
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: "Đóng",
          onAction: () => onClose(),
        },
      ]}
    >
      <Modal.Section>
        <Form onSubmit={submit}>
          <Card sectioned>
            <FormLayout>
              <TextField
                autoFocus
                autoComplete="off"
                requiredIndicator
                label="User Email"
                {...fields.user_email}
              />

              <TextField
                label="Tên người dùng"
                requiredIndicator
                autoComplete="off"
                {...fields.display_name}
              />

              <TextField
                maxLength={1000}
                requiredIndicator
                label="Mật khẩu"
                autoComplete="off"
                {...fields.user_pass}
              />

              <TextField
                requiredIndicator
                autoComplete="off"
                label="Xác nhận mật khẩu"
                // type="password"
                helpText={<p>Gõ lại mật khẩu của bạn!</p>}
                {...fields.retype_password}
              />
            </FormLayout>
          </Card>
        </Form>
      </Modal.Section>
    </Modal>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {Actual_page}
    </>
  );
}
