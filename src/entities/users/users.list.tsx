import { Card, DataTable, EmptyState, Layout, Page, Select, Stack, Toast, Loading } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/users.store.reducer";
import UsersFilter from "./filter";
import helpers from "../../helpers";
import CountryName from "components/countries";
import UserNew from "./users.new";
import date from "date-and-time";

export default function General() {
  const entities = useAppSelector((state) => state.users.entities);
  const loading = useAppSelector((state) => state.users.loading);
  const errorMessage = useAppSelector((state) => state.users.errorMessage);
  const totalItems = useAppSelector((state) => state.users.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "user_id,desc",
      // user_role: "",
    },
  });

  const [queryValue, setQueryValue] = useState("");

  /**
   * Filter input
   */

  const [input, setInput] = useState("");
  const handleFiltersQueryChange = useCallback((value) => setInput(value), []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, ...{ query: input } });
  }, [input]);

  /**
   * Filter selected
   */

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ user_role: `${selectedParentId}`, page: 1 },
    });
  }, [selectedParentId]);

  /**
   * Change page number
   */
  const [numberPage, setNumberPage] = useState(1);  // <------------------ Reload page 1
  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  const [newModelActive, setNewModelActive] = useState(false);
  const toggleNewActive = useCallback(() => setNewModelActive((active) => !active), []);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/users" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  /**
   *
   * @param user_id
   */
  const shortcutActions = (user_id: number) => {
    history("/users/edit/" + user_id);
  };

  const emptyData = (
    <EmptyState heading="No user here!" image={emptyIMG}>
      <p>Oh! Không có người dùng ở đây! Thử bỏ bộ lọc hoặc thêm người dùng!</p>
    </EmptyState>
  );

  const handleSort = (index: any, direction: string) => {
    let _direction = direction === "descending" ? "desc" : "asc";
    let sort = "";
    sort = "createAt," + _direction;
    setMainQuery({ ...mainQuery, sort: sort });
  };

  /**
   * Get nation name
   * @param code string
   * @returns string
   */
  // function handleGetNationNameByCode(code: string): string {
  //   if (code) {
  //     let data = CountryName().filter((item, index) => {
  //       if (item.alpha2 === code) {
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     });
  //     if (data && data[0]) {
  //       return data[0].name;
  //     }
  //   }
  //   return code;
  // }

  /**
   * Coin
   */

  const coin = (
    <>
      <svg
        data-v-2d000d8f=""
        // viewBox="-5 -10 35 35"
        viewBox="0 0 24 24 "
        style={{
          width: "18px",
          height: "18px",
          margin: "0px 0px -3px 2px",
          fill: "#ee9b00",
        }}
      >
        <path
          data-v-2d000d8f=""
          d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 3c-4.971 0-9 4.029-9 9s4.029 9 9 9 9-4.029 9-9-4.029-9-9-9zm1 13.947v1.053h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.385-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85v-1.067h1v1.018c.725.019 1.535.145 2.442.42l-.362 1.648c-.768-.27-1.616-.515-2.442-.465-1.489.087-1.62 1.376-.581 1.916 1.711.804 3.943 1.401 3.943 3.546.002 1.718-1.344 2.632-3 2.864z"
        ></path>
      </svg>
    </>
  );

  const renderItem = (users: any) => {
    const {
      user_id,
      user_avatar,
      user_email,
      user_role,
      user_nation,
      display_name,
      createAt,
      lastActive,
      user_balance,
    } = users;
    return [
      <div className="clickable" key={user_id + "user_id"} onClick={() => shortcutActions(user_id)}>
        {user_id}
      </div>,
      <div
        className="clickable"
        style={{ textAlign: "center" }}
        key={user_id + "user_avatar"}
        onClick={() => shortcutActions(user_id)}
      >
        <div className="user_avatar">
          <img src={user_avatar ? user_avatar : user_id?.user_avatar} crossOrigin="anonymous" />
        </div>
      </div>,
      <div className="clickable" key={user_id + "user_display_name"} onClick={() => shortcutActions(user_id)}>
        {display_name}
      </div>,
      <div className="clickable" key={user_id + "user_email"} onClick={() => shortcutActions(user_id)}>
        {helpers.trimContentString(user_email)}
      </div>,
      <div className="clickable" key={user_id + "user_role"} onClick={() => shortcutActions(user_id)}>
        {user_role}
      </div>,
      <div className="clickable" key={user_id + "user_balance"} onClick={() => shortcutActions(user_id)}>
        {user_balance}
        {coin}
      </div>,
      // <div className="clickable" key={user_id + "user_address"} onClick={() => shortcutActions(user_id)}>
      //   {user_address}
      //   <p>{handleGetNationNameByCode(user_nation) || ""} </p>
      // </div>,
      <div className="clickable" key={user_id + "user_lastActive"} onClick={() => shortcutActions(user_id)}>
        <time>{lastActive ? date.format(new Date(Number(lastActive)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={user_id + "user_createAt"} onClick={() => shortcutActions(user_id)}>
        <time>{createAt ? date.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const UsersList =
    entities.length > 0 ? (
      <>
        <DataTable
          sortable={[false, false, false, false, false, false, false, true]}
          defaultSortDirection="descending"
          initialSortColumnIndex={6}
          onSort={handleSort}
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={[
            "ID",
            "Avatar",
            "Tên người dùng",
            "Gmail",
            "User role",
            "Coin",
            "Hoạt động lần cuối",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page
        title="Users"
        primaryAction={{
          content: "Tạo mới",
          disabled: false,
          onAction: toggleNewActive,
        }}
      >
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px", display: "flex" }}>
                <Stack distribution="equalSpacing">
                  <UsersFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                  {/* <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "All", value: "" },
                        { label: "User", value: "user" },
                        { label: "Advisor", value: "advisor" },
                        { label: "Admin", value: "admin" },
                      ]}
                    />
                  </Stack.Item> */}
                  <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "All", value: "" },
                        { label: "User ID", value: "user_id" },
                      ]}
                    />
                  </Stack.Item>
                </Stack>
              </div>
              {UsersList}
            </Card>
            <br />
            {totalItems > mainQuery.limit ? (
              <Pagination
                TotalRecord={totalItems}
                activeCurrentPage={mainQuery.page}
                pageSize={mainQuery.limit}
                onChangePage={onChangePageNumber}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
      <UserNew show={newModelActive} onClose={() => setNewModelActive(false)} />
    </>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {loading ? <Loading /> : null}
      {Actual_page}
      {/* {loading ? skeleton_loading : Actual_page} */}
    </>
  );
}

