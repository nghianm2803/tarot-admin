import { useParams } from "react-router-dom";
import Theme404 from '../../layout/404';

import users_list from './users.list';
import users_detail from './users.detail';
import users_view from './users.view';



export default  () => {
    let useParam =  {} as any;
        useParam = useParams();
    let Param = useParam.slug || 'list';

    let ActualPage: any;
    switch (Param) {
        
        case 'list':
            ActualPage = users_list;
        break;

        case 'edit':
            ActualPage = users_detail;
        break;

        case 'new':
            ActualPage = users_detail;
        break;

        case 'view':
            ActualPage = users_view;
        break;

        default:
            ActualPage =  Theme404;
        break;
    }

    return (
        <>
            {<ActualPage />}
        </>
    );
}
