import { useCallback, useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "config/store";
import {
  Button,
  Card,
  ChoiceList,
  FormLayout,
  Icon,
  Label,
  OptionList,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  TextContainer,
  TextField,
  TextStyle,
  Toast,
} from "@shopify/polaris";
import { clearError, clearMessage, getEntities, getEntity, reset, updateEntity } from "store/settings.store.reducer";
import { getEntity as getSystemInformation, cleanSession } from "store/system.store.reducer";
import { lengthLessThan, notEmpty, useField, useForm } from "@shopify/react-form";
import helpers from "../../helpers";
import { ExchangeMajor } from "@shopify/polaris-icons";

export default function General() {
  const entity = useAppSelector((state) => state.settings.entity);
  const entities = useAppSelector((state) => state.settings.entities);
  const loading = useAppSelector((state) => state.settings.loading);
  const updating = useAppSelector((state) => state.settings.updating);
  const message = useAppSelector((state) => state.settings.message);
  const system_information = useAppSelector((state) => state.system.entity);
  const systemloading = useAppSelector((state) => state.system.updating);

  const dispatch = useAppDispatch();

  /**
   * Notification
   */
  const toggleToastActive = useCallback(() => {
    dispatch(clearError());
    dispatch(clearMessage());
  }, []);

  /**
   * fileSizeUpload
   */
  const [fileSizeUpload, setFileSizeUpload] = useState("0");
  const handleFileSizeInputChange = useCallback((newValue) => setFileSizeUpload(newValue), []);

  // Highlight category VN
  const [highlightCategoryVI, setHighlightCategoryVI] = useState([]);
  const highlightArrayVI = [highlightCategoryVI];
  highlightArrayVI.join(",");

  const handleHighlightCategoryVIChange = useCallback((value) => setHighlightCategoryVI(value), []);
  const categories_vi = [
    { value: "819", label: "Kinh dịch" },
    { value: "820", label: "Tử Vi" },
    { value: "821", label: "Tarot" },
    { value: "822", label: "Oracle" },
    { value: "823", label: "Lenormand" },
    { value: "824", label: "Thần Số Học" },
    { value: "825", label: "Xem chỉ tay" },
    { value: "826", label: "Bản đồ sao" },
  ];

  // Highlight category EN
  const [highlightCategoryEN, setHighlightCategoryEN] = useState([]);
  const highlightArrayEN = [highlightCategoryEN];
  highlightArrayEN.join(",");

  const handleHighlightCategoryENChange = useCallback((value) => setHighlightCategoryEN(value), []);
  const categories_en = [
    { value: "774", label: "Mind reading" },
    { value: "411", label: "Tarot reading" },
    { value: "419", label: "Palm reading" },
    { value: "791", label: "Predict future" },
    { value: "769", label: "Tea leaves" },
    { value: "767", label: "Crystal ball" },
  ];

  /**
   * Setting language
   */
  const [language, setLanguage] = useState<string>("english");
  const handleLanguageChange = useCallback((value) => setLanguage(value), []);
  const optionsLanguage = [
    { label: "English", value: "english" },
    { label: "Vietnamese", value: "vietnamese" },
  ];

  /**
   * Conversion RateCoin <------------------------
   */
  const [coin, setCoin] = useState(100);
  const handleCoinChange = useCallback((newValue) => setCoin(newValue), []);
  const [rateCoin, setRateCoin] = useState(1);
  const handleRateCoinChange = useCallback((newValue) => setRateCoin(newValue), []);

  /**
   * Setting exchange coin 170
   */
  const [exchangeCoin, setExchangeCoin] = useState(150);
  const handleExchangeCoin = useCallback((newValue) => setExchangeCoin(newValue), []);

  /**
   * Free coin to new user 100
   */
  const [freeCoin, setFreeCoin] = useState(100);
  const handleFreeCoinChange = useCallback((newValue) => setFreeCoin(newValue), []);

  /**
   * General setting
   */
  const [choiselistselected, setChoiselistselected] = useState(["welcome_new_user"]);
  const [adminEmail, setAdminEmail] = useState("");
  const changeemailcallback = useCallback((newValue) => setAdminEmail(newValue), []);
  const handleChoiseListChange = useCallback((value) => setChoiselistselected(value), []);

  /**
   * Initial
   */
  useEffect(() => {
    dispatch(getSystemInformation());
    dispatch(getEntities());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  //useParam = useLocation();

  const useFields = {};
  useParam = useParams();

  const { submit } = useForm({
    fields: useFields,
    async onSubmit() {
      try {
        dispatch(
          updateEntity({
            maintenance_mode: maintenanceMode,
            file_size_upload: String(fileSizeUpload || 0).trim(),
            admin_email: helpers.isEmail(adminEmail) ? adminEmail : "",
            notification_mode: choiselistselected,
            highlight_category_vi: highlightCategoryVI,
            highlight_category_en: highlightCategoryEN,
            rate_coin: rateCoin,
            free_coin: freeCoin,
            exchange_rate_dong_to_coin: exchangeCoin,
          })
        );
        reset();
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  /**
   * Maintenance mode
   */
  const [maintenanceMode, setMaintenanceMode] = useState("disabled");
  const handleMaintananceModeChange = useCallback((_checked, newValue) => setMaintenanceMode(newValue), []);

  const ClearSessionCallBack = () => {
    dispatch(cleanSession());
  };

  useEffect(() => {
    if (entities && typeof entities?.maintenancemode !== "undefined") {
      setMaintenanceMode(entities?.maintenancemode);
    }

    if (entities && typeof entities?.file_size_upload !== "undefined") {
      setFileSizeUpload(entities?.file_size_upload);
    }

    if (entities && typeof entities?.highlight_category_vi !== "undefined") {
      setHighlightCategoryVI(String(entities?.highlight_category_vi || " ").split(","));
    }

    if (entities && typeof entities?.highlight_category_en !== "undefined") {
      setHighlightCategoryEN(String(entities?.highlight_category_en || " ").split(","));
    }

    if (entities && typeof entities?.language !== "undefined") {
      setLanguage(entities?.language);
    }

    if (entities && typeof entities?.rate_coin !== "undefined") {
      setRateCoin(entities?.rate_coin);
    }

    if (entities && typeof entities?.free_coin !== "undefined") {
      setFreeCoin(entities?.free_coin);
    }

    if (entities && typeof entities?.exchange_rate_dong_to_coin !== "undefined") {
      setExchangeCoin(entities?.exchange_rate_dong_to_coin);
    }

    if (entities && typeof entities?.admin_emainhnl !== "undefined") {
      setAdminEmail(entities?.admin_email);
    }

    if (entities && typeof entities?.notification_mode !== "undefined") {
      setChoiselistselected(String(entities?.notification_mode || " ").split(","));
    }
  }, [entities]);

  const general_setting_field = (
    <FormLayout>
      <ChoiceList
        allowMultiple
        title=""
        choices={[
          {
            label: "Welcome new user",
            value: "welcome_new_user",
            helpText: "Send an email to new user.",
          },
          {
            label: "Welcome new customer",
            value: "welcome_new_customer",
            helpText: "Send an email to new customer if they have an email.",
          },
        ]}
        selected={choiselistselected}
        onChange={handleChoiseListChange}
      />
      <TextField
        label="Email nhận thông tin từ hệ thống"
        value={adminEmail}
        onChange={changeemailcallback}
        placeholder="admin@you.com"
        helpText={`Là email nhận các thông báo tự động từ hệ thống. Email có thể chứa các thông tin quan trọng.`}
        autoComplete="off"
      />
    </FormLayout>
  );

  const toastMarkup = message ? <Toast content={message} onDismiss={toggleToastActive} duration={4500} /> : null;

  return (
    <>
      {toastMarkup}
      <Page
        narrowWidth
        title="Cài đặt chung"
        primaryAction={{
          content: "Lưu",
          onAction: submit,
          disabled: false,
          loading: updating,
        }}
      >
        <Card>
          <Card.Section title="Chế độ bảo trì">
            <TextContainer>
              <TextStyle variation="subdued">
                Khi chế độ bảo trì được bật, chỉ những người có vai trò admin được phép đăng nhập.
              </TextStyle>
            </TextContainer>
            <Stack vertical>
              <RadioButton
                label="Tắt chế độ bảo trì"
                helpText="Mọi tài khoản đều được đăng nhập."
                checked={maintenanceMode === "disabled"}
                id="disabled"
                name="accounts"
                onChange={handleMaintananceModeChange}
              />
              <RadioButton
                label="Bật chế độ bảo trì"
                helpText="Khi chế độ bảo trì được bật, chỉ những admin được phép đăng nhập."
                id="enabled"
                name="accounts"
                checked={maintenanceMode === "enabled"}
                onChange={handleMaintananceModeChange}
              />
            </Stack>
          </Card.Section>

          {/* Option category Vietnam */}
          <Card>
            <OptionList
              title="Highlight category Vietnam"
              onChange={handleHighlightCategoryVIChange}
              options={categories_vi}
              selected={highlightCategoryVI}
              allowMultiple
            />
          </Card>

          {/* Option category English */}
          <Card>
            <OptionList
              title="Highlight category English"
              onChange={handleHighlightCategoryENChange}
              options={categories_en}
              selected={highlightCategoryEN}
              allowMultiple
            />
          </Card>

          {/* Setting language */}
          <Card.Section title="Cài đặt ngôn ngữ">
            <Select label="Language" options={optionsLanguage} onChange={handleLanguageChange} value={language} />
          </Card.Section>

          <Card.Section title="Thông báo từ hệ thống">{general_setting_field}</Card.Section>

          {/* Rating coin */}
          <Card.Section title="Tỉ lệ chuyển đổi coin">
            <Stack distribution="fill">
              <Stack.Item>
                <TextField
                  label="Coin"
                  value={coin.toString()}
                  onChange={handleCoinChange}
                  autoComplete="off"
                  type="number"
                  min={0}
                />
              </Stack.Item>
              <Stack.Item>
                <div style={{ marginTop: "2rem" }}>
                  <Icon source={ExchangeMajor} color="base" />
                </div>
              </Stack.Item>
              <Stack.Item>
                <TextField
                  label="Tỉ lệ hiện tại USD"
                  value={((coin / 100) * rateCoin).toString()}
                  autoComplete="off"
                  type="number"
                  min={0}
                  disabled={true}
                />
              </Stack.Item>
            </Stack>
            <br />
            <div style={{ margin: "auto", width: "280px" }}>
              <TextField
                label="Rate"
                value={rateCoin.toString()}
                onChange={handleRateCoinChange}
                autoComplete="off"
                type="number"
                min={0}
                disabled={loading}
              />
              <TextContainer>
                <TextStyle variation="subdued">Coin devide by 100 then * Rate Coin = USD</TextStyle>
              </TextContainer>
            </div>
          </Card.Section>

          {/* Give free coin to new user */}
          <Card.Section title="Free coin">
            <TextField
              label="Free coin to new user"
              value={freeCoin.toString()}
              onChange={handleFreeCoinChange}
              autoComplete="off"
              type="number"
              disabled={loading}
            />
            <TextContainer>
              <TextStyle variation="subdued">Cài đặt free coin mặc định và chuyển cho người dùng mới</TextStyle>
            </TextContainer>
          </Card.Section>

          <Card.Section title="Exchange coin">
            <TextField
              label="Exchange coin "
              value={exchangeCoin.toString()}
              onChange={handleExchangeCoin}
              autoComplete="off"
              type="number"
              disabled={loading}
            />
            <TextContainer>
              <TextStyle variation="subdued">Setting exchange coin</TextStyle>
            </TextContainer>
          </Card.Section>

          <Card.Section title="Upload">
            <TextField
              label="Maximum File size in upload form. (in MB)"
              value={fileSizeUpload}
              onChange={handleFileSizeInputChange}
              autoComplete="off"
              type="number"
              disabled={loading}
            />
            <TextContainer>
              <TextStyle variation="subdued">
                Default is 20Mb, if you want to remove this limit, change it to 0
              </TextStyle>
            </TextContainer>
          </Card.Section>

          <Card.Section title="Dọn dẹp phiên làm việc">
            <br />
            <Button destructive outline loading={systemloading} onClick={ClearSessionCallBack}>
              Clear all session
            </Button>

            <br />
            <br />
            <TextContainer>
              <TextStyle variation="subdued">
                Khi dọn dẹp phiên làm việc, mọi phiên làm việc hết hạn trong cơ sở dữ liệu sẽ được xóa bỏ một cách an
                toàn.
              </TextStyle>
            </TextContainer>
          </Card.Section>

          <Card.Section title="System Information">
            <p>
              This Information is marked as{" "}
              <strong>
                <u>SENSITIVE INFORMATION</u>
              </strong>
              , DO NOT share with other people.
            </p>
            <pre
              style={{
                overflow: "scroll",
                backgroundColor: "#ccc",
                padding: "10px",
                maxHeight: "300px",
                whiteSpace: "break-spaces",
              }}
            >
              {JSON.stringify(system_information, null, 4)}
            </pre>
          </Card.Section>
        </Card>

        <PageActions
          primaryAction={{
            content: "Lưu",
            onAction: submit,
            disabled: false,
            loading: updating,
          }}
        />
      </Page>
    </>
  );
}

