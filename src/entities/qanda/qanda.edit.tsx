import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  Stack,
  TextContainer,
  TextField,
  Toast,
  Tag,
  Button,
  Autocomplete,
  Select,
  PageActions,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import emptyIMG from "../../media/empty.png";
import SkeletonLoading from "components/skeleton_loading";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/posts.store.reducer";
import { getEntities as getCategories } from "store/taxonomy.category.store.reducer";
import {
  getEntities as getTags,
  createEntity as createTags,
} from "store/taxonomy.tag.store.reducer";
import {
  getEntities as getTaxonomyRelationshiptags,
  createEntity as createTaxonomyRelationshiptags,
  deleteEntity as deleteTaxonomyRelationshiptags,
} from "../../store/taxonomy_relationship.tag.store.reducer";
import {
  getEntity as getTaxonomyRelationshipcategory,
  getEntities as getTaxonomyRelationshipcategorys,
  createEntity as createTaxonomyRelationshipcategorys,
} from "../../store/taxonomy_relationship.category.store.reducer";
import {
  lengthLessThan,
  lengthMoreThan,
  useField,
  useForm,
} from "@shopify/react-form";
import QuickUploadImage from "components/oneclick-upload";
import uploadimage from "../../media/choose-file.png";
import { TinyMCE } from "components/tinyMCE";

/**
 * Convert Tag object to String and join with comma
 * @param tags Object
 */

function convertTagObjectToString(tags: any) {
  let TagArray = [];
  for (var p in tags) {
    if (tags.hasOwnProperty(p)) {
      TagArray.push(tags[p]);
    }
  }
  const TagArrayTaxonomy = TagArray.map((option) => {
    if (option.taxonomy) {
      return option.taxonomy.taxonomy_name;
    } else {
      return option;
    }
  });
  // return null;
  return TagArrayTaxonomy.join(",");
}

/**
 * Main function
 * @returns
 */

export default function NewEdit() {
  const entity = useAppSelector((state) => state.posts.entity);
  const updating = useAppSelector((state) => state.posts.updating);
  const loading = useAppSelector((state) => state.posts.loading);
  const errorMessage = useAppSelector((state) => state.posts.errorMessage);
  const updateSuccess = useAppSelector((state) => state.posts.updateSuccess);

  const entitycategories = useAppSelector(
    (state) => state.taxonomy_category.entities
  );

  const entitietags = useAppSelector((state) => state.taxonomy_tag.entities);

  const entitytag = useAppSelector((state) => state.taxonomy_tag.entity);

  const updateSuccessTaxonomyTag = useAppSelector(
    (state) => state.taxonomy_tag.updateSuccess
  );

  const entitiestaxonomy_relationship_tag = useAppSelector(
    (state) => state.taxonomy_relationship_tag.entities
  );

  const entitiestaxonomy_relationship_category = useAppSelector(
    (state) => state.taxonomy_relationship_category.entity
  );

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const [openModal, setOpenModal] = useState(false);
  const [notification, setNotification] = useState(null);

  const [post_category_selected, setPost_category_selected] = useState(1);

  const [inputValueTag, setInputValueTag] = useState("");
  const [textContent, setTextContent] = useState("");
  const [internalErrorNotice, setInternalErrorNotice] = useState("");
  const [categoryList, setCategoryList] = useState([]);
  const [selectedOptionTags, setSelectedOptionTags] = useState([]);
  const [optionTags, setOptionTags] = useState([]);

  const handCategoryChange = useCallback((value) => {
    setPost_category_selected(value);
  }, []);

  const toggleActive = useCallback(() => {
    setNotification(errorMessage);
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.qanda_slug || false;

  useEffect(() => {
    dispatch(getCategories({ taxonomy_type: "category" }));
    dispatch(getTags({ taxonomy_type: "tag" }));
    if (Param) {
      dispatch(getEntity(Param));
    } else {
      setTextContent(" ");
      dispatch(reset());
    }
  }, []);

  useEffect(() => {
    if (entity && entity.post_content) {
      dispatch(getTaxonomyRelationshipcategory(entity?.post_id));

      dispatch(
        getTaxonomyRelationshiptags({
          object_id: entity?.post_id,
          object_type: "tag",
        })
      );

      dispatch(
        getTaxonomyRelationshipcategorys({
          object_id: entity?.post_id,
          object_type: "category",
        })
      );

      if (Param) {
        setTimeout(() => {
          setTextContent(entity.post_content);
        }, 10);
      } else {
        setTextContent(" ");
      }
    }
  }, [entity]);

  useEffect(() => {
    if (entitiestaxonomy_relationship_category) {
      if (Param) {
        setPost_category_selected(
          entitiestaxonomy_relationship_category[0]?.taxonomy_id
        );
      } else {
        setPost_category_selected(1);
      }
    }
  }, [entitiestaxonomy_relationship_category]);

  /**
   * upload image
   */

  const [srcImage, setSrcImage] = useState(uploadimage);
  const [image, setImage] = useState("");

  useEffect(() => {
    if (Param) {
      if (entity && entity?.post_thumbnail) {
        setSrcImage(entity?.post_thumbnail);
        setImage(entity?.post_thumbnail);
      } else {
        setSrcImage(uploadimage);
        setImage(uploadimage);
      }
    } else {
      setSrcImage(uploadimage);
      setImage(uploadimage);
    }
  }, [entity]);

  function handQuickUpdateSuccess(res: any) {
    let media_root = process.env.REACT_APP_AJAX_UPLOAD_IMAGE;
    setSrcImage(media_root + "" + res.media_filename);
    setImage(`${process.env.REACT_APP_AJAX_UPLOAD_IMAGE}` + res.media_filename);
  }

  useEffect(() => {
    const all_tag = [];
    for (let e of entitiestaxonomy_relationship_tag) {
      if (e.taxonomy.taxonomy_type == "tag") {
        all_tag.push(e);
      }
    }
    if (Param) {
      if (all_tag.length > 0) {
        setSelectedOptionTags(all_tag);
      } else {
        setSelectedOptionTags([]);
      }
    } else {
      setSelectedOptionTags([]);
    }
  }, [entitiestaxonomy_relationship_tag]);

  useEffect(() => {
    const optionType = [];
    for (let { taxonomy_id, taxonomy_name } of entitycategories) {
      optionType.push({
        label: String(taxonomy_name),
        value: String(taxonomy_id),
      });
    }
    setCategoryList(optionType);
  }, [entitycategories]);

  const deselectedTags = entitietags.map(({ taxonomy_slug, taxonomy_name }) => {
    return {
      label: String(taxonomy_name),
      value: String(taxonomy_name),
    };
  });

  /**
   * Create taxonomy
   */
  useEffect(() => {
    if (updateSuccess === true) {
      history("/qanda/edit/" + entity?.post_id, { replace: true });
      setNotification("This page has been updated!");
      dispatch(
        createTaxonomyRelationshipcategorys({
          taxonomy_id: post_category_selected,
          object_id: entity?.post_id,
        })
      );

      for (let tagItem of selectedOptionTags) {
        dispatch(
          createTags({
            tag: tagItem,
          })
        );
      }
    }
  }, [updateSuccess]);

  /**
   * Create taxonomy_relationship
   */

  useEffect(() => {
    if (entitytag) {
      dispatch(
        createTaxonomyRelationshiptags({
          taxonomy_id: entitytag[0]?.taxonomy_id,
          object_id: entity?.post_id,
        })
      );
    }
  }, [entitytag]);

  /**
   * Delete taxonomy_relationship
   */

  useEffect(() => {
    if (updateSuccessTaxonomyTag === true) {
      for (let tagItem of deleteTag) {
        dispatch(deleteTaxonomyRelationshiptags(tagItem));
      }
    }
  }, [updateSuccessTaxonomyTag]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }

  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
    // navigate(-1);
  }

  const updateTag = useCallback(
    (value) => {
      setInputValueTag(value);

      if (value === "") {
        setOptionTags(deselectedTags);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptions = deselectedTags.filter((option) =>
        option.label.match(filterRegex)
      );
      let endIndex = resultOptions.length - 1;
      if (resultOptions.length === 0) {
        endIndex = 0;
      }
      setOptionTags(resultOptions);
    },
    [deselectedTags]
  );

  /**
   * Remove tag
   */

  const [deleteTag, setDeleteTag] = useState([]);

  const removeTagTaxonomy = useCallback(
    (tag) => () => {
      setSelectedOptionTags((previousTags) =>
        previousTags.filter((previousTag) => previousTag !== tag)
      );

      let prepareData = deleteTag;
      prepareData = [...prepareData, ...[tag.taxonomy_relationship_id]];
      setDeleteTag(prepareData);
    },
    [selectedOptionTags]
  );

  const tagsMarkup = selectedOptionTags.map((option) => {
    let tagLabel = String(option || " ").replace("_", " ");
    return (
      <Tag
        key={`option${tagLabel}`}
        onRemove={removeTagTaxonomy(option)}
        accessibilityLabel="2"
      >
        {option.taxonomy?.taxonomy_name
          ? option.taxonomy?.taxonomy_name
          : tagLabel}
      </Tag>
    );
  });

  const createTag = () => {
    if (inputValueTag.length > 2) {
      const newTag = inputValueTag.split(",");
      setSelectedOptionTags([...selectedOptionTags, ...newTag]);
      setInputValueTag("");
    } else {
      alert("Tag quá ngắn");
    }
  };

  const tagField = (
    <Autocomplete.TextField
      autoComplete={null}
      onChange={updateTag}
      label="Tags"
      value={inputValueTag}
      placeholder="Find or create tags"
      connectedRight={<Button onClick={createTag}>Add Tag</Button>}
    />
  );

  const emptyData = (
    <Page
      title="Câu hỏi và trả lời"
      fullWidth
      breadcrumbs={[{ content: "Page list", url: "/qanda" }]}
    >
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin không tồn tại!</p>
      </EmptyState>
    </Page>
  );
  const toastMarkup = notification ? (
    <Toast content={notification} onDismiss={toggleActive} />
  ) : null;

  function handUploadError(err: any) {
    setInternalErrorNotice("Tải ảnh thất bại! Ảnh quá lớn!");
  }

  const useFields = {
    post_title: useField<string>({
      value: entity?.post_title ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
        (inputValue) => {
          if (inputValue.length < 2) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    post_excerpt: useField<string>({
      value: entity?.post_excerpt ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    post_name: useField<string>({
      value: entity?.post_name ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),

    post_status: useField<string>({
      value: entity?.post_status ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(2, "Không được ngắn hơn 2 ký tự."),
      ],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: Userreset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              post_title: values.post_title,
              post_content: textContent,
              post_excerpt: values.post_excerpt,
              post_name: values.post_name,
              post_thumbnail: image,
              post_type: "qanda",
              post_status: "publish",
            })
          );
        } else {
          dispatch(
            updateEntity({
              post_id: entity.post_id,
              post_title: values.post_title,
              post_excerpt: values.post_excerpt,
              post_name: values.post_name,
              post_content: textContent,
              post_status: values.post_status,
              post_thumbnail: image,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message =
          e?.response?.data?.title ??
          "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Câu hỏi và trả lời"
          fullWidth
          breadcrumbs={[{ content: "Page list", url: "/qanda" }]}
          primaryAction={{
            content: "Lưu",
            disabled: !dirty,
            loading: updating,
            onAction: submit,
          }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoComplete="off"
                        autoFocus
                        requiredIndicator
                        label="Tiêu đề"
                        {...fields.post_title}
                      />
                      <div>
                        <span>Nội dung</span>
                        <div className="editor" style={{ minHeight: "500px" }}>
                          <TinyMCE
                            value={`${textContent}` ? `${textContent}` : " "}
                            onChange={(data) => {
                              setTextContent(data);
                            }}
                          />
                        </div>
                      </div>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
            <Layout.Section secondary>
              <Card title="Thông tin" sectioned>
                <FormLayout>
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Slug"
                    requiredIndicator
                    {...fields.post_name}
                  />
                  <TextField
                    autoComplete="off"
                    maxLength={250}
                    label="Excerpt"
                    requiredIndicator
                    {...fields.post_excerpt}
                    multiline={4}
                  />
                  <p>Thumbnail</p>
                  <div id="profile_heading">
                    <div className="news_thumbnail_inner">
                      <div className="news_thumbnail_avatar">
                        <QuickUploadImage
                          onSuccess={handQuickUpdateSuccess}
                          onError={handUploadError}
                        />
                        <img
                          src={srcImage}
                          crossOrigin="anonymous"
                          style={{ width: "115px" }}
                        />
                      </div>
                    </div>
                  </div>
                  <Select
                    label="Category"
                    disabled={Param ? true : false}
                    options={categoryList}
                    onChange={handCategoryChange}
                    value={post_category_selected?.toString()}
                  />
                  <div>
                    <Autocomplete
                      allowMultiple
                      options={optionTags}
                      selected={selectedOptionTags}
                      textField={tagField}
                      onSelect={setSelectedOptionTags}
                      listTitle="Suggested Tags"
                    />
                    <br />
                    <TextContainer>
                      <Stack>{tagsMarkup}</Stack>
                      <Stack>{createTag}</Stack>
                    </TextContainer>
                  </div>
                </FormLayout>
              </Card>
            </Layout.Section>
          </Layout>
          <PageActions
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Xoá câu hỏi &amp; câu trả lời này?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}
