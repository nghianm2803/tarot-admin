import React from "react";
import { useParams } from "react-router-dom";
import Theme404 from "../../layout/404";

import qanda_list from "./qanda.list";
import qanda_edit from "./qanda.edit";
// import pages_view from './pages.view';

export default function Pagidex() {
  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.slug || "list";

  let ActualPage: any;
  switch (Param) {
    case "list":
      ActualPage = qanda_list;
      break;

    case "edit":
      ActualPage = qanda_edit;
      break;

    case "new":
      ActualPage = qanda_edit;
      break;

    default:
      ActualPage = Theme404;
      break;
  }

  return <>{<ActualPage />}</>;
}
