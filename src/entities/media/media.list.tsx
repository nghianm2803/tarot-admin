import {
  Badge,
  Button,
  Card,
  DataTable,
  EmptyState,
  Layout,
  Loading,
  Modal,
  Page,
  RadioButton,
  Select,
  Stack,
  Subheading,
  TextField,
  TextStyle,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import dateandtime from "date-and-time";
import {
  TickSmallMinor,
  LockMinor,
  AlertMinor,
  ArchiveMinor,
  EditMinor,
  ArrowDownMinor,
  CancelSmallMinor,
} from "@shopify/polaris-icons";
import emptyIMG from "media/empty.png";
import Pagination from "components/pagination";
import { useAppDispatch, useAppSelector } from "config/store";
import {
  clearError,
  getEntity,
  getEntities,
  partialUpdateEntity,
  deleteEntity,
} from "store/media.store.reducer";
import { getCurrentUser } from "store/user.store.reducer";
import QuickSearch from "components/quick_search";
import helpers from "../../helpers";
import VideoPlayer from "react-video-js-player";
import ReactAudioPlayer from "react-audio-player";

export default function General() {
  const entity = useAppSelector((state) => state.media.entity);
  const entities = useAppSelector((state) => state.media.entities);
  const loading = useAppSelector((state) => state.media.loading);
  const errorMessage = useAppSelector((state) => state.media.errorMessage);
  const totalItems = useAppSelector((state) => state.media.totalItems);
  const updating = useAppSelector((state) => state.media.updating);
  const { user_id, user_role } = useAppSelector((state) => state.user.account);

  const current_session_id = helpers.get_session_id();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
    dispatch(getCurrentUser());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const initialQuery = {
    query: "",
    page: 1,
    limit: 20,
    sort: "media_id,desc",
  };

  const [mainQuery, setMainQuery] = useState({
    ...StringQuery,
    ...initialQuery,
  });

  const [selectedParentId, setSelectedParentId] = useState(
    StringQuery.private_access || ""
  );
  const handleSelectedChange = useCallback(
    (_value) => {
      setSelectedParentId(_value);
      setMainQuery({ ...mainQuery, ...{ private_access: _value, page: 1 } });
    },
    [mainQuery]
  );

  /**
   * Image
   */
  const [srcImage, setSrcImage] = useState("");
  useEffect(() => {
    if (entity) {
      if (entity?.media_description === "SaveOnAmazonS3") {
        let str = entity?.media_url;
        let words = str.split("?AWSAccessKeyId");
        setSrcImage(words[0]);
      } else {
        setSrcImage(
          process.env.REACT_APP_AJAX_URL +
            `download/${entity?.media_filename}?session_id=${current_session_id}`
        );
      }
    }
  }, [entity]);

  /**
   * Sort colume
   */
  const handleSort = useCallback(
    (index, direction) => {
      let _direction = direction === "descending" ? "desc" : "asc";
      let sort = "";
      if (index === 2) {
        sort = "download_count," + _direction;
      } else {
        sort = "createAt," + _direction;
      }
      setMainQuery({ ...mainQuery, sort: sort });
    },
    [entities]
  );

  /**
   * Change page number
   * Must be mainQuery or it will reset mainQuery. BUG!
   */
  const onChangePageNumber = useCallback(
    (numPage) => {
      setMainQuery({ ...mainQuery, page: numPage });
    },
    [mainQuery]
  );

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch)
      window.history.replaceState(null, "Media", "/media" + buildURLSearch);
    // history.push( '/media' + buildURLSearch );
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const handleFiltersQueryChange = useCallback(
    (_value) => {
      setMainQuery({ ...mainQuery, ...{ query: _value, page: 1 } });
    },
    [mainQuery]
  );

  const showAll = () => {
    setMainQuery(initialQuery);
  };

  const showMyFile = () => {
    setMainQuery({ ...mainQuery, ...{ createBy: user_id } });
  };

  /**
   * Query media detail
   * @param media_id
   */

  const [showEditModel, setShowEditModel] = useState(false);
  const [showFormEdits, setShowFormEdits] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [formValue, setFormValue] = useState("");
  const [optionvalue, setOptionvalue] = useState("disabled");
  const getMediaDetail = (media_id: number) => {
    dispatch(getEntity(media_id));
    setShowEditModel(true);
    setDeleteConfirm(false);
    setShowFormEdits(false);
  };
  const toggleEditModalActive = useCallback(() => {
    if (showEditModel) {
      // close model ? Reload data
      dispatch(getEntities(mainQuery));
    }
    setShowEditModel((active) => !active);
  }, [showEditModel]);

  const deleteFile = useCallback(() => {
    dispatch(deleteEntity(entity?.media_id));
    toggleEditModalActive();
    dispatch(getEntities(mainQuery));
  }, [entity]);

  const deleteFileConfirm = useCallback(() => {
    setDeleteConfirm((deleteConfirm) => !deleteConfirm);
  }, []);

  const showFormEdit = () => {
    setShowFormEdits(true);
  };

  useEffect(() => {
    if (entity) {
      setFormValue(entity?.media_description);
      setOptionvalue(entity?.private_access === 1 ? "optional" : "disabled");
    }
  }, [entity]);

  const handChangeFormValue = useCallback(
    (newValue) => setFormValue(newValue),
    []
  );
  const handleOptionChange = useCallback(
    (_checked, newValue) => setOptionvalue(newValue),
    []
  );

  const saveForm = () => {
    dispatch(
      partialUpdateEntity({
        media_id: entity?.media_id,
        media_description: formValue,
        private_access: optionvalue === "disabled" ? 0 : 1,
      })
    );
    setShowFormEdits(false);
  };
  const cancelForm = () => {
    setShowFormEdits(false);
  };

  const [isOpen, setIsOpen] = useState(false);
  const handleToggle = () => {
    setIsOpen(!isOpen);
    console.log(isOpen);
  };

  const editForm = (
    <>
      <Subheading>Access</Subheading>
      <br />
      <Stack vertical>
        <RadioButton
          label="Private Access"
          helpText="Chỉ tác giả mới có thể xem hoặc tải"
          checked={optionvalue === "optional"}
          id="optional"
          name="accounts"
          onChange={handleOptionChange}
        />
        <RadioButton
          label="Public Access"
          helpText="Mọi người có thể xem hoặc tải"
          id="disabled"
          name="accounts"
          checked={optionvalue === "disabled"}
          onChange={handleOptionChange}
        />
      </Stack>
      <br />
      <div id="profile_heading">
        <div className="news_thumbnail_inner">
          <div className="news_thumbnail_avatar">
            {entity?.media_filetype.split("/")[0] === "image" ? (
              <img
                src={srcImage}
                crossOrigin="anonymous"
                style={{ width: "115px" }}
              />
            ) : entity?.media_filetype.split("/")[0] === "video" ? (
              <>
                <VideoPlayer
                  controls={true}
                  src={srcImage}
                  width="580px"
                  height="115px"
                />
              </>
            ) : (
              <ReactAudioPlayer
                src={srcImage}
                autoPlay
                controls
                style={{ width: "580px", height: "115px" }}
              />
            )}
          </div>
        </div>
      </div>
      <br />
      <Subheading>GHI CHÚ</Subheading>
      <TextField
        label=""
        value={formValue}
        onChange={handChangeFormValue}
        maxLength={200}
        multiline={2}
        autoComplete="off"
        showCharacterCount
      />
      <Button plain onClick={saveForm} loading={updating}>
        Lưu 
      </Button>
      &nbsp; hoặc &nbsp;
      <Button plain onClick={cancelForm} loading={updating}>
        Huỷ
      </Button>
    </>
  );

  const detailModal = (
    <Modal
      open={showEditModel}
      onClose={toggleEditModalActive}
      title="View File"
      titleHidden
      secondaryActions={[
        {
          content: "Đóng",
          onAction: toggleEditModalActive,
        },
      ]}
    >
      <Modal.Section>
        {!entity && !loading ? (
          <Subheading>Oh! Không tìm thấy dữ liệu!</Subheading>
        ) : (
          <>
            <Subheading>
              <strong>{entity?.media_filename}</strong>
            </Subheading>
            <br />
            <Stack>
              <Stack.Item>
                <Badge status="success">{entity?.media_filetype}</Badge>
              </Stack.Item>
              <Stack.Item>
                <Badge status="critical">
                  {helpers.bytesToSize(entity?.media_filesize)}
                </Badge>
              </Stack.Item>
              <Stack.Item>Thời gian tải: {entity?.download_count}</Stack.Item>
            </Stack>
            <br />
            <p>Tác giả: {entity?.user?.display_name}</p>
            <p>
              Thời gian tạo:{" "}
              <time>
                {entity?.createAt
                  ? dateandtime.format(
                      new Date(Number(entity?.createAt)),
                      "DD-MM-YYYY HH:mm:ss"
                    )
                  : "-"}
              </time>
            </p>
            <p>
              Thời gian cập nhật:{" "}
              <time>
                {entity?.updateAt
                  ? dateandtime.format(
                      new Date(Number(entity.updateAt)),
                      "DD-MM-YYYY HH:mm:ss"
                    )
                  : "-"}
              </time>
            </p>
            <br />
            {showFormEdits ? editForm : null}
            {!showFormEdits ? (
              <>
                <div>
                  <Subheading>
                    {entity?.private_access === 1
                      ? "Private Access"
                      : "Public Access"}
                  </Subheading>
                  <p>
                    {entity?.private_access === 1
                      ? "Only author can view or download file."
                      : "Everyone can view and download."}
                  </p>
                </div>
                <br />
                <div>
                  <p>
                    Note:{" "}
                    <TextStyle variation="positive">
                      {entity?.media_description || "Nothing!"}
                    </TextStyle>
                  </p>
                </div>
              </>
            ) : null}
            <br />
            <br />
            <Stack>
              <Stack.Item>
                <Button
                  download
                  plain
                  icon={ArrowDownMinor}
                  url={
                    entity?.media_description === "SaveOnAmazonS3"
                      ? entity?.media_url.split("?AWSAccessKeyId")[0]
                      : process.env.REACT_APP_AJAX_URL +
                        `download/${entity?.media_filename}?session_id=${current_session_id}`
                  }
                >
                  {" "}
                  Tải ngay
                </Button>
              </Stack.Item>
              <Stack.Item>
                <Button
                  plain
                  disabled={
                    user_id !== entity?.createBy && user_role !== "admin"
                  }
                  primary
                  icon={EditMinor}
                  onClick={showFormEdit}
                >
                  Sửa
                </Button>
              </Stack.Item>
              <Stack.Item>
                <Button
                  destructive
                  disabled={
                    user_id !== entity?.createBy && user_role !== "admin"
                  }
                  onClick={deleteFileConfirm}
                  plain
                  icon={CancelSmallMinor}
                >
                  {" "}
                  Chuyển đến thùng rác
                </Button>
              </Stack.Item>
              {deleteConfirm ? (
                <Stack.Item>
                  <Button
                    destructive
                    onClick={deleteFile}
                    plain
                    icon={AlertMinor}
                    loading={updating}
                  >
                    {" "}
                    Are you sure?
                  </Button>
                </Stack.Item>
              ) : null}
            </Stack>
          </>
        )}
      </Modal.Section>
    </Modal>
  );

  /**
   * END QUERY MEDIA DETAIL
   */

  const emptyData = (
    <EmptyState heading="No media here!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (media: any) => {
    const {
      media_id,
      media_url,
      media_filename,
      media_description,
      media_filetype,
      media_filesize,
      media_extention,
      download_count,
      private_access,
      user,
      createBy,
      createAt,
      updateAt,
    } = media;
    return [
      <div
        className="small-icon"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        {private_access ? <LockMinor /> : <TickSmallMinor />}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        <p>{helpers.trimMiddleString(media_filename)}</p>
        {media_description ? (
          <TextStyle variation="positive">
            {helpers.getTrimContent(media_description, 30)}
          </TextStyle>
        ) : null}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        {download_count}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        {media_filetype}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        {helpers.bytesToSize(media_filesize)}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        {user.display_name}
      </div>,
      <div
        className="clickable"
        key={media_id}
        onClick={() => getMediaDetail(media_id)}
      >
        <time>
          {createAt
            ? dateandtime.format(
                new Date(Number(createAt)),
                "DD-MM-YYYY HH:mm:ss"
              )
            : "-"}
        </time>
      </div>,
    ];
  };

  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          sortable={[false, false, true, false, false, false, true]}
          defaultSortDirection="descending"
          initialSortColumnIndex={6}
          onSort={handleSort}
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          headings={[
            "",
            "Tên file",
            "Tải",
            "Filetype",
            "Filesize",
            "Tác giả",
            "Thời gian tạo",
          ]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <>
      <Page>
        {detailModal}
        <Layout>
          <Layout.Section>
            <Card>
              <div style={{ padding: "16px" }}>
                <Stack alignment="center">
                  <Stack.Item fill>
                    <QuickSearch
                      queryValue={StringQuery?.query}
                      onChange={handleFiltersQueryChange}
                    />
                  </Stack.Item>
                  <Stack.Item>
                    <Select
                      label=""
                      value={selectedParentId}
                      onChange={handleSelectedChange}
                      options={[
                        { label: "Tất cả", value: "" },
                        { label: "Riêng tư", value: "1" },
                        { label: "Công khai", value: "0" },
                      ]}
                    />
                  </Stack.Item>
                  <Stack.Item>
                    <Button icon={ArchiveMinor} onClick={showMyFile}>
                      My Files
                    </Button>
                  </Stack.Item>
                </Stack>
              </div>
              {PagesList}
            </Card>
            <br />
            {totalItems > 0 ? (
              <Pagination
                TotalRecord={totalItems}
                onChangePage={onChangePageNumber}
                pageSize={Number(mainQuery.limit)}
                activeCurrentPage={Number(mainQuery.page)}
              />
            ) : null}
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );

  const toastMarkup = errorMessage ? (
    <Toast content={errorMessage} error onDismiss={toggleActive} />
  ) : null;

  return (
    <>
      {toastMarkup}
      {loading ? <Loading /> : null}
      {Actual_page}
    </>
  );
}
