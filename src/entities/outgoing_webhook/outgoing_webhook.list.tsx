import { Badge, Card, DataTable, EmptyState, Layout, Page, Select, Stack, Toast } from "@shopify/polaris";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import debounce from "lodash.debounce";
import { TickSmallMinor, LockMinor } from "@shopify/polaris-icons";
import emptyIMG from "../../media/empty.png";
import Pagination from "../../components/pagination";
import { clearError, getEntities } from "../../store/outgoing_webhook.store.reducer";
import Outgoing_webhookFilter from "./filter";
import helpers from "../../helpers";
import SkeletonLoading from "components/skeleton_loading";
import dateandtime from "date-and-time";

export default function General_outgoing_webhook() {
  const [initial_loading, setInitial_loading] = useState<boolean>(true);

  const entity = useAppSelector((state) => state.outgoing_webhook.entity);
  const entities = useAppSelector((state) => state.outgoing_webhook.entities);
  const loading = useAppSelector((state) => state.outgoing_webhook.loading);
  const errorMessage = useAppSelector((state) => state.outgoing_webhook.errorMessage);
  const totalItems = useAppSelector((state) => state.outgoing_webhook.totalItems);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  const toggleActive = useCallback(() => {
    dispatch(clearError());
  }, []);

  /**
   * If user apply filter, it will add to URL, then parse URL back to initial state
   */
  let useParam = {} as any;
  useParam = useLocation();
  let StringQuery: any = helpers.ExtractUrl(useParam.search) || false;

  const [mainQuery, setMainQuery] = useState({
    ...{
      query: "",
      page: 1,
      limit: 20,
      sort: "webhook_id,desc",
      webhook_method: "",
    },
    ...StringQuery,
  });
  const [queryValue, setQueryValue] = useState("");

  const [input, setInput] = useState("");
  const handleFiltersQueryChange = useCallback((value) => setInput(value), []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, ...{ query: input } });
  }, [input]);

  const [selectedParentId, setSelectedParentId] = useState("");
  const handleSelectedChange = useCallback((_value) => {
    setSelectedParentId(_value);
  }, []);

  useEffect(() => {
    setMainQuery({
      ...mainQuery,
      ...{ webhook_method: `${selectedParentId}`, page: 1 },
    });
  }, [selectedParentId]);

  /**
   * Change page number
   */

  const [numberPage, setNumberPage] = useState(1);

  const onChangePageNumber = useCallback((numPage) => {
    setNumberPage(numPage);
  }, []);

  useEffect(() => {
    setMainQuery({ ...mainQuery, page: numberPage });
  }, [numberPage]);

  useEffect(() => {
    let buildURLSearch = helpers.buildEndUrl(mainQuery);
    if (useParam.search !== buildURLSearch) history("/outgoing_webhook" + buildURLSearch);
    dispatch(getEntities(mainQuery));
  }, [mainQuery]);

  const onChangeCallback = useMemo(
    () =>
      debounce((_value) => {
        if (_value !== "") setMainQuery({ ...mainQuery, query: _value ? _value : "" });
      }, 500),
    []
  );

  useEffect(() => {
    onChangeCallback(queryValue);
  }, [queryValue]);

  useEffect(() => {
    if (loading === false) setInitial_loading(false);
  }, [loading]);

  /**
   *
   * @param webhook_id
   */
  const shortcutActions = (webhook_id: number) => {
    history("edit/" + webhook_id);
  };

  const emptyData = (
    <EmptyState heading="Không có thông tin!" image={emptyIMG}>
      <p>Oh! Không có thông tin ở đây! Thử bỏ bộ lọc hoặc thêm chuyên gia mới!</p>
    </EmptyState>
  );

  const renderItem = (outgoing_webhook: any) => {
    const {
      webhook_id,
      webhook_title,
      webhook_description,
      webhook_url,
      webhook_active,
      webhook_method,
      webhook_events,
      createAt,
      createBy,
      updateAt,
      lastOpen,
    } = outgoing_webhook;
    return [
      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        {webhook_id}
      </div>,
      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        {webhook_title}
      </div>,
      <div
        className="clickable"
        style={{ textAlign: "center" }}
        key={webhook_id}
        onClick={() => shortcutActions(webhook_id)}
      >
        <Badge status="info">{webhook_method}</Badge>
        {/* {webhook_method} */}
      </div>,
      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        {decodeURIComponent(webhook_url)}
      </div>,
      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        {webhook_description}
      </div>,
      <div className="small-icon" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        {/* {webhook_active} */}
        {webhook_active === 1 ? <TickSmallMinor /> : <LockMinor />}
      </div>,

      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        <time>{createAt ? dateandtime.format(new Date(Number(createAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
      <div className="clickable" key={webhook_id} onClick={() => shortcutActions(webhook_id)}>
        <time>{updateAt ? dateandtime.format(new Date(Number(updateAt)), "DD-MM-YYYY HH:mm:ss") : "-"}</time>
      </div>,
    ];
  };
  const PagesList =
    totalItems > 0 ? (
      <>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text", "text", "text", "text", "text"]}
          headings={["ID", "Webhook Title", "Method", "Webhook Url", "Description", "Active", "Thời gian tạo", "Thời gian cập nhập"]}
          rows={entities?.map(renderItem)}
          hideScrollIndicator
          footerContent={`Hiển thị trang ${mainQuery.page} trong tổng số ${totalItems} kết quả...`}
        />
        <style>{`
        .clickable {
          margin: -1.6rem;
          padding: 1.6rem;
          cursor: pointer;
        }
        .small-icon {
          font-size: 12px;
          padding: 0;
          width: 15px;
          height: auto;
        }
      `}</style>
      </>
    ) : (
      emptyData
    );

  const Actual_page = (
    <Page
      title="Outgoing Webhook"
      primaryAction={{
        content: "Tạo mới",
        disabled: false,
        onAction: () => {
          history("new");
        },
      }}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <div style={{ padding: "16px", display: "flex" }}>
              <Stack distribution="equalSpacing">
                <Outgoing_webhookFilter queryValue={StringQuery?.query} onChange={handleFiltersQueryChange} />
                <Select
                  label=""
                  value={selectedParentId}
                  onChange={handleSelectedChange}
                  options={[
                    { label: "All", value: "" },
                    { label: "Get", value: "get" },
                    { label: "Post", value: "post" },
                    { label: "Patch", value: "patch" },
                    { label: "Delete", value: "delete" },
                  ]}
                />
              </Stack>
            </div>
            {PagesList}
          </Card>
          <br />
          {totalItems > mainQuery.limit ? (
            <Pagination
              TotalRecord={totalItems}
              activeCurrentPage={mainQuery.page}
              pageSize={mainQuery.limit}
              onChangePage={onChangePageNumber}
            />
          ) : null}
        </Layout.Section>
      </Layout>
    </Page>
  );

  const toastMarkup = errorMessage ? <Toast content={errorMessage} error onDismiss={toggleActive} /> : null;

  return (
    <>
      {toastMarkup}
      {initial_loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

