import {
  Card,
  EmptyState,
  Form,
  FormLayout,
  Layout,
  Modal,
  Page,
  PageActions,
  RadioButton,
  Select,
  Stack,
  TextContainer,
  TextField,
  Toast,
} from "@shopify/polaris";
import { useCallback, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../config/store";
import SkeletonLoading from "components/skeleton_loading";
import emptyIMG from "../../media/empty.png";
import {
  clearError,
  getEntity,
  updateEntity,
  deleteEntity,
  createEntity,
  reset,
} from "../../store/outgoing_webhook.store.reducer";
import { lengthLessThan, lengthMoreThan, useField, useForm, notEmpty } from "@shopify/react-form";
import helpers from "helpers";

export default function Edit_outgoing_webhook() {
  const entity = useAppSelector((state) => state.outgoing_webhook.entity);
  const updating = useAppSelector((state) => state.outgoing_webhook.updating);
  const entities = useAppSelector((state) => state.outgoing_webhook.entities);
  const loading = useAppSelector((state) => state.outgoing_webhook.loading);
  const errorMessage = useAppSelector((state) => state.outgoing_webhook.errorMessage);
  const updateSuccess = useAppSelector((state) => state.outgoing_webhook.updateSuccess);

  const dispatch = useAppDispatch();
  const history = useNavigate();

  // jamviet.com
  const [message, setMessage] = useState(null);

  const [openModal, setOpenModal] = useState(false);

  const [notification, setNotification] = useState(null);

  /**
   * Button value
   */

  const [webhookActive, setWebhookActive] = useState(1);

  const [optionButtonValue, setOptionButtonValue] = useState(true);

  const handleRadioButtonChange = useCallback((_checked, newValue) => {
    setOptionButtonValue(newValue === "enabled");
  }, []);

  /**
   * Option method
   */

  const [webhook_method, setWebhook_method] = useState("post");

  const method = [
    { label: "get", value: "get" },
    { label: "post", value: "post" },
    { label: "patch", value: "patch" },
    { label: "delete", value: "delete" },
  ];

  const handleSelectChange = useCallback((value) => setWebhook_method(value), []);

  const toggleActive = useCallback(() => {
    setMessage(null);
    dispatch(clearError());
  }, []);

  let useParam = {} as any;
  useParam = useParams();
  let Param = useParam.outgoing_webhook_slug || false;

  useEffect(() => {
    if (Param) dispatch(getEntity(Param));
    else dispatch(reset());
  }, []);

  useEffect(() => {
    if (entity && entity?.webhook_method) {
      if (Param) {
        setWebhook_method(entity?.webhook_method);
      } else {
        setWebhook_method("post");
      }
      setWebhookActive(entity?.webhook_active);
      if (entity?.webhook_active === 0) {
        setOptionButtonValue(false);
      } else {
        setOptionButtonValue(true);
      }
    }
  }, [entity]);

  useEffect(() => {
    if (optionButtonValue === false) {
      setWebhookActive(0);
    } else {
      setWebhookActive(1);
    }
  }, [optionButtonValue]);

  useEffect(() => {
    if (updateSuccess) {
      history("/outgoing_webhook/edit/" + entity?.webhook_id, {
        replace: true,
      });

      setNotification("This webhook has been updated!");
    }
  }, [updateSuccess]);

  useEffect(() => {
    if (Param) {
      setWebhook_method(entity?.webhook_method);
    }
  }, [history]);

  useEffect(() => {
    if (errorMessage) {
      setNotification(errorMessage);
      dispatch(clearError());
    }
  }, [errorMessage]);

  function _deleteEntityAction() {
    setOpenModal(true);
  }

  function onModalAgree() {
    dispatch(deleteEntity(Param));
    setOpenModal(false);
  }

  const useFields = {
    webhook_title: useField<string>({
      value: entity?.webhook_title ?? "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Tiêu đề quá ngắn hoặc trống.";
          }
        },
      ],
    }),

    webhook_description: useField<string>({
      value: entity?.webhook_description ?? "",
      validates: [
        lengthLessThan(250, "Không được dài hơn 250 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
      ],
    }),

    webhook_url: useField<string>({
      value: entity?.webhook_url ? decodeURIComponent(entity?.webhook_url) : "",
      validates: [
        notEmpty("URL trống!"),
        (inputValue) => {
          if (!helpers.isUrl(inputValue)) {
            return "URL không hợp lệ!";
          }
        },
      ],
    }),

    webhook_events: useField<string>({
      value: entity?.outgoing_webhook_action ? entity?.outgoing_webhook_action[0]?.action_key : "",
      validates: [
        lengthLessThan(200, "Không được dài hơn 200 ký tự."),
        lengthMoreThan(6, "Không được ngắn hơn 6 ký tự."),
        (inputValue) => {
          if (inputValue.length < 6) {
            return "Sự kiến quá ngắn hoặc trống.";
          }
        },
      ],
    }),
  };

  const {
    fields,
    submit,
    submitting,
    dirty,
    reset: Userreset,
    submitErrors,
    makeClean,
  } = useForm({
    fields: useFields,
    async onSubmit(values) {
      try {
        if (!Param) {
          // create new
          dispatch(
            createEntity({
              webhook_title: values.webhook_title,
              webhook_description: values.webhook_description,
              webhook_url: values.webhook_url,
              webhook_method: webhook_method,
              webhook_events: values.webhook_events,
              webhook_active: webhookActive,
            })
          );
        } else {
          dispatch(
            updateEntity({
              webhook_id: entity.webhook_id,
              webhook_title: values.webhook_title,
              webhook_description: values.webhook_description,
              webhook_url: values.webhook_url,
              webhook_method: webhook_method,
              webhook_events: values.webhook_events,
              webhook_active: webhookActive,
            })
          );
        }
        return { status: "success" };
      } catch (e: any) {
        console.error(`Submit error`, e);
        const message = e?.response?.data?.title ?? "Lỗi không xác định, vui lòng thử lại sau.";
        const field = e?.response?.data?.errorKey ?? "base";
        return { status: "fail", errors: [{ field, message }] };
      }
    },
  });

  const emptyData = (
    <Page title="Outgoing Webhook" breadcrumbs={[{ content: "Outgoing Webhook list", url: "/outgoing_webhook" }]}>
      <EmptyState heading="Không có thông tin!" image={emptyIMG}>
        <p>Thông tin này không tồn tại!</p>
      </EmptyState>
    </Page>
  );

  const toastMarkup = notification ? <Toast content={notification} onDismiss={toggleActive} /> : null;

  const toastNotification = message ? <Toast content={message} onDismiss={toggleActive} /> : null;

  const Actual_page =
    entity || !Param ? (
      <>
        <Page
          title="Outgoing Webhook"
          breadcrumbs={[{ content: "Outgoing Webhook list", url: "/outgoing_webhook" }]}
          primaryAction={{
            content: "Lưu",
            disabled: !dirty,
            loading: updating,
            onAction: submit,
          }}
        >
          <Layout>
            <Layout.Section>
              <Form onSubmit={submit}>
                <Card>
                  <Card.Section>
                    <FormLayout>
                      <TextField
                        autoFocus
                        autoComplete="off"
                        requiredIndicator
                        label="Webhook Title"
                        {...fields.webhook_title}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={1000}
                        requiredIndicator
                        label="Webhook Url"
                        {...fields.webhook_url}
                      />
                      <Select
                        label="Webhook Method"
                        options={method}
                        requiredIndicator
                        onChange={handleSelectChange}
                        value={webhook_method}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={1000}
                        requiredIndicator
                        label="Webhook Events"
                        {...fields.webhook_events}
                      />
                      <TextField
                        autoComplete="off"
                        maxLength={250}
                        label="Webhook Description"
                        {...fields.webhook_description}
                      />
                      <Stack vertical>
                        <RadioButton
                          label="Webhook active OFF"
                          checked={optionButtonValue === false}
                          id="disabled"
                          name="accounts"
                          value="0"
                          onChange={handleRadioButtonChange}
                        />
                        <RadioButton
                          label="Webhook active ON"
                          id="enabled"
                          name="accounts"
                          value="1"
                          checked={optionButtonValue === true}
                          onChange={handleRadioButtonChange}
                        />
                      </Stack>
                    </FormLayout>
                  </Card.Section>
                </Card>
              </Form>
            </Layout.Section>
          </Layout>
          <PageActions
            primaryAction={{
              content: "Lưu",
              onAction: submit,
              disabled: !dirty,
              loading: updating,
            }}
            secondaryActions={
              Param
                ? [
                    {
                      content: "Xoá",
                      destructive: true,
                      onAction: _deleteEntityAction,
                    },
                  ]
                : []
            }
          />
        </Page>
      </>
    ) : (
      emptyData
    );

  const _Modal = openModal ? (
    <Modal
      activator={null}
      open={true}
      onClose={() => setOpenModal(false)}
      title="Xoá dữ liệu?"
      primaryAction={{
        content: "Xoá",
        onAction: onModalAgree,
      }}
      secondaryActions={[
        {
          content: "Huỷ",
          onAction: () => {
            setOpenModal(false);
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>
          <p>Sau khi xoá bạn sẽ không thể hoàn tác!</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  ) : null;

  return (
    <>
      {_Modal}
      {toastMarkup}
      {toastNotification}
      {loading ? <SkeletonLoading /> : Actual_page}
    </>
  );
}

