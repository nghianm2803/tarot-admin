/**
*   Interface/model file auto generate for Api
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IIncoming_webhook {
  api_id?: number;
  api_enable?: number;
  api_slug?: string;
  api_description?: string;
  api_name?: string;
  api_access_token?: string; // uuid v4
  api_access_method?: string;
  api_role?: string;
  createAt?: any;
  createBy?: number;
  updateAt?: any;
  lastOpen?: any;
}

/**
*   Default value for IApi
*/
export const defaultIIncoming_webhook: Readonly<IIncoming_webhook> = {
  api_id: 0,
  api_enable: 0,
  api_role: "user",
  api_description: "",
  api_access_token: "",
  api_access_method: "",
  createAt: new Date(),
  createBy: 0,

};