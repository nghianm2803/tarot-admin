/**
*   Interface/model file auto generate for App_package
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IApp_package {
	 package_id?: string; 
	 package_name?: string; 
	 package_value?: number; 
	 package_coin?: number; 
	 package_slug?: string; 
	 package_active?: number; 
	 createAt?: any; 
	 createBy?: string; 
}

/**
*   Default value for IApp_package
*/
export const defaultIApp_package: Readonly<IApp_package> = {
    package_id: "", 
	package_name: "", 
	package_value: 0, 
	package_coin: 0, 
	package_slug: "", 
	package_active: 0, 
	createAt: new Date(), 
	createBy: "", 
};