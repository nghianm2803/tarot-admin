export interface TypedChat {
  advisor_id: string;
  user_id: string;
  chat_content?: string;
  duration?: string;
}

export const defaultTypedChat: Readonly<TypedChat> = {
  advisor_id: "",
  user_id: "",
  chat_content: "",
  duration: "0",
};