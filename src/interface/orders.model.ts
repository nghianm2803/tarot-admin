/**
 *   Interface/model file auto generate for Orders
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IOrders {
  order_id?: number;
  order_customer_id?: number;
  order_seller_id?: number;
  order_service_id?: number;
  order_price?: number;
  order_status?: "draft" | "cancelled" | "refunded" | "processed" | "in_process" | "paid" | any;
  order_note?: string;
  order_pnr?: string;
  order_deadline?: any;
  order_quantity?: number;
  createAt?: any;
  createBy?: number;
  updateAt?: any;
  updateBy?: number;
}

/**
 *   Default value for IOrders
 */
export const defaultIOrders: Readonly<IOrders> = {
  order_id: 0,
  order_customer_id: 0,
  order_seller_id: 0,
  order_service_id: 0,
  order_price: 0,
  order_quantity: 1,
  order_status: "draft",
  createAt: new Date(),
  createBy: 0,
};
