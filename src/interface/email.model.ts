/**
*   Interface/model file auto generate for Email
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IEmail {
    email_id?: number; 
    email_from?: string; 
    email_to?: string; 
    email_cc?: string; 
    email_bcc?: string; 
    email_subject?: string; 
    email_content?: string; 
    email_status?: string | "pending" | "cancel" | "sent" | "queue" | "fail" | "draft";
    createAt?: any; 
    sendAt?: any; 
    createBy?: number; 
}

/**
*   Default value for IEmail
*/
export const defaultIEmail: Readonly<IEmail> = {
       email_id: 0, 
   email_from: "", 
   email_to: "", 
   email_cc: "", 
   email_bcc: "", 
   email_subject: "", 
   email_content: "", 
   email_status: "pending",
   createAt: new Date(), 
   createBy: 0, 
};