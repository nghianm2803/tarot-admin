/**
*   Interface/model file auto generate for Contactform
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IContactform {
	contactform_id?: number;
	contactform_name?: string;
	contactform_email?: string;
	contactform_numberphone?: string;
	contactform_content?: string;
	contactform_ip?: string;
	contactform_status?: number;
	createAt?: any;
}

/**
*   Default value for IContactform
*/
export const defaultIContactform: Readonly<IContactform> = {
	contactform_id: 0,
	contactform_name: "",
	contactform_email: "",
	contactform_content: "",
	contactform_ip: "",
	contactform_status: 0,
	createAt: new Date(),
};