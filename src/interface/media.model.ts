/**
*   Interface/model file auto generate for Media
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IMedia {
	 media_id?: number; 
	 media_url?: string; 
	 media_filename?: string; 
	 media_description?: string; 
	 media_filetype?: string; 
	 media_filesize?: number; 
	 media_extention?: string; 
	 private_access?: number; 
	 createBy?: number; 
	 createAt?: any; 
	 updateAt?: any; 
	 download_count?: number;
	users?: any
}

/**
*   Default value for IMedia
*/
export const defaultIMedia: Readonly<IMedia> = {
	media_id: 0, 
	download_count: 0,
	media_url: "", 
	media_filesize: 0, 
	private_access: 0, 
	createBy: 0, 
	createAt: new Date(), 

};