/**
*   Interface/model file auto generate for Setting
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface ISettings {
   setting_id?: number;
   setting_name?: string;
   setting_value?: string;
   top_advisor?: string;
   createBy?: number;
   createAt?: any;
}

/**
*   Default value for IMedia
*/
export const defaultISettings: Readonly<ISettings> = {
   setting_id: 0,
   setting_name: '',
   setting_value: '',
   createBy: 0,
   createAt: new Date(),
};