/**
 *   Interface/model file auto generate for Review
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IReview {
  review_id?: string;
  order_id?: string;
  seller_id?: string;
  review_content?: string;
  review_point?: number;
  createBy?: string;
  createAt?: any;
}

/**
 *   Default value for IReview
 */
export const defaultIReview: Readonly<IReview> = {
  review_id: "",
  order_id: "",
  seller_id: "",
  review_content: "",
  review_point: 0,
  createBy: "",
  createAt: new Date(),
};
