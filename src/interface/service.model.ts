/**
 *   Interface/model file auto generate for Service
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IService {
  service_id?: number;
  service_name?: string;
  service_description?: string;
  service_price?: number;
  service_duration?: number;
  service_category?: number;
  service_thumbnail?: string;
  service_active?: number;
  service_unit?: "second" | "minute" | "hour" | "day" | string;
  service_readonly?: number;
  service_type?: string;
  service_group?: string;
  createAt?: any;
  createBy?: number;
  updateAt?: any;
}

/**
 *   Default value for IService
 */
export const defaultIService: Readonly<IService> = {
  service_id: 0,
  service_name: "",
  service_description: "",
  service_price: 0,
  service_duration: 0,
  service_category: 0,
  service_thumbnail: "",
  createAt: new Date(),
  createBy: 0,
};
