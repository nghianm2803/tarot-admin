/**
*   Interface/model file auto generate for User_role
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IUser_role {
	role_id?: string;
	role_name?: string;
	role_content?: string;
	createAt?: any;
	updateAt?: any;
	createBy?: string;
}

/**
*   Default value for IUser_role
*/
export const defaultIUser_role: Readonly<IUser_role> = {
	role_id: "",
	role_name: "",
	role_content: "",
	createAt: new Date(),
	createBy: "",
};