/**
 *   Interface/model file auto generate for User_meta
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IUser_Meta {
  meta_id?: string;
  user_id?: string;
  meta_key?: string;
  meta_value?: string;
  createAt?: any;
  updateAt?: any;
}

/**
 *   Default value for IUser_Meta
 */
export const defaultIUser_Meta: Readonly<IUser_Meta> = {
  meta_id: "",
  user_id: "",
  meta_key: "",
  meta_value: "",
  createAt: new Date(),
  updateAt: new Date(),
};
