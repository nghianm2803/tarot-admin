/**
*   Interface/model file auto generate for Banner
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IBanner {
	 banner_id?: string; 
	 banner_content?: string; 
	 banner_title?: string; 
	 banner_active?: number; 
	 banner_media?: string; 
	 banner_URL?:string;
	 post_id?:number
	 createAt?: any; 
	 updateAt?: any; 
	 createBy?: string; 

}

/**
*   Default value for IBanner
*/
export const defaultIBanner: Readonly<IBanner> = {
    	banner_id: "", 
	banner_content: "", 
	banner_title: "", 
	banner_active: 0, 
	banner_media: "", 
	banner_URL:"",
	post_id:0,
	createAt: new Date(), 
	updateAt: new Date(), 
	createBy: "", 

};