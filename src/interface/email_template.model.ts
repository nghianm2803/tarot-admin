/**
*   Interface/model file auto generate for Email_template
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IEmail_template {
	 template_id?: number; 
	 template_title?: string; 
	 template_slug?: string; 
	 template_content?: string; 
	 template_status?: number;
	 createAt?: any; 
	 updateAt?: any; 
	 createBy?: number; 
}

/**
*   Default value for IEmail_template
*/
export const defaultIEmail_template: Readonly<IEmail_template> = {
	template_id: 0, 
	template_title: "", 
	template_slug: "", 
	template_content: "", 
	template_status: 1,
	createAt: new Date(), 
	createBy: 0, 
};