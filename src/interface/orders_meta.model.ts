/**
 *   Interface/model file auto generate for Orders_meta
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IOrders_meta {
  meta_id: number;
  order_id: number;
  meta_key: string;
  meta_value: string;
  createAt: any;
  createBy: number;
}

/**
 *   Default value for IOrders_meta
 */
export const defaultIOrders_meta: Readonly<IOrders_meta> = {
  meta_id: 0,
  order_id: 0,
  meta_key: "",
  meta_value: "",
  createAt: new Date(),
  createBy: 0,
};
