/**
*   Interface/model file auto generate for Livestream
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface ILivestream {
	livestream_id: number;
	livestream_author: number;
	livestream_session: string;
	createAt: any;
}

/**
*   Default value for ILivestream
*/
export const defaultILivestream: Readonly<ILivestream> = {
	livestream_id: 0,
	livestream_author: 0,
	livestream_session: "",
	createAt: new Date(),
};