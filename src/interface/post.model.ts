export interface IPosts {
  post_id?: number;
  createBy?: number;
  post_name?: string;
  post_title: string;
  post_excerpt?: string;
  post_content: string;
  post_status?: string;
  comment_status?: boolean;
  post_type?: string;
  post_parent?: number;
  comment_count?: number;
  post_tag?: string;
  post_category?: number;
  post_thumbnail?: any;
  lang?: string;
  createAt?: any;
  updateAt?: any;
}

export const defaultValue: Readonly<IPosts> = {
  createBy: 1,
  post_name: "",
  post_title: "Untitle",
  post_excerpt: "",
  lang: "",
  post_content: "",
  createAt: "",
};
