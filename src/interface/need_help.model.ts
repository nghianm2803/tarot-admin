/**
 *   Interface/model file auto generate for NeedHelp
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface INeedHelp {
  help_id?: number;
  help_title?: string;
  help_description?: string;
  language?: string;
  createBy?: number;
  createAt?: any;
  updateAt?: any;
}

/**
 *   Default value for INeedHelp
 */
export const defaultINeedHelp: Readonly<INeedHelp> = {
  help_id: 0,
  createBy: 0,
  createAt: new Date(),
};
