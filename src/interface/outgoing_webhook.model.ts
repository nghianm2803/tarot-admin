/**
*   Interface/model file auto generate for Outgoing_webhook
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IOutgoing_webhook {
	 webhook_id?: number; 
	 webhook_title?: string; 
	 webhook_description?: string; 
	 webhook_url?: string; 
	 webhook_active?: number; 
	 webhook_method?: string; 
	 webhook_events?: string; 
	 createAt?: any; 
	 createBy?: number; 
	 updateAt?: any; 
	 lastOpen?: any; 
}

/**
*   Default value for IOutgoing_webhook
*/
export const defaultIOutgoing_webhook: Readonly<IOutgoing_webhook> = {
    	webhook_id: 0, 
	webhook_title: "", 
	webhook_description: "", 
	webhook_url: "", 
	webhook_active: 0, 
	webhook_method: "", 
	webhook_events: "", 
	createAt: new Date(), 
	createBy: 0, 

};