/**
*   Interface/model file auto generate for Affiliate
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IAffiliate {
	 affiliate_id?: number; 
	 user_id: number; 
	 affiliate_click?: number; 
	 affiliate_ip?: string; 
	 affiliate_browser?: string; 
	 affiliate_referrer: string; 
	 createAt?: any; 

}

/**
*   Default value for IAffiliate
*/
export const defaultIAffiliate: Readonly<IAffiliate> = {
    	affiliate_id: 0, 
	user_id: 0, 
	affiliate_click: 0, 
	affiliate_browser: "", 
	affiliate_referrer: "", 
	createAt: new Date(), 

};