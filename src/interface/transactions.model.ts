/**
*   Interface/model file auto generate for Transactions
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface ITransactions {
	transaction_id?: number;
	transaction_value?: number;
	transaction_ref?: string;
	transaction_note?: string;
	transaction_condition?: "pending" | "cancel" | "done" | any;
	transaction_current_balance?: string;
	transaction_new_balance?: string;
	transaction_method?: "minus" | "plus" | string;
	object_id?: number;
	createAt?: any;
	updateAt?: any;
}

/**
*   Default value for ITransactions
*/
export const defaultITransactions: Readonly<ITransactions> = {
	transaction_id: 0,
	transaction_value: 0,
	transaction_condition: "",
	createAt: new Date(),
};