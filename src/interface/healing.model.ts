/**
 *   Interface/model file auto generate for Healing
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface IHealing {
  healing_id?: string;
  healing_content?: string;
  healing_category?: string;
  healing_status?: number;
  createAt?: string;
  createBy?: string;
}

/**
 *   Default value for IHealing
 */
export const defaultIHealing: Readonly<IHealing> = {
  healing_id: "",
  healing_content: "",
  healing_category: "",
  healing_status: 0,
  createAt: "",
  createBy: "",
};
