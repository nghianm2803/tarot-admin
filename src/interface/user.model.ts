export interface IUser {
  user_id?: number;
  user_login?: string;
  user_email?: string;
  display_name?: string;
  user_pass?: string;
  bio?: string;
  user_role?: string;
  user_status?: number;
  user_nation?: string;
  user_job?: string;
  user_department?: string;
  user_address?: string;
  user_birthday?: string;
  user_numberphone?: string;
  user_avatar?: string;
  user_balance?: number;
  average_rating?: any;
  is_premium?: number;
  createBy?: number;
  createAt?: any;
  updateAt?: any;
  lastActive?: any;
}

export const defaultValue: Readonly<IUser> = {
  user_status: 0
};

export interface ILogin {
  user_email: string;
  password: string;
  remember?: boolean;
  device_type?: string;
  device_uuid?: any;
  device_signature?: any;
  display_name?: string;
  user_address?: string;
}

export interface IRegister {
  user_email: string;
  password: string;
  display_name: string;
  user_referrer?: string;
}