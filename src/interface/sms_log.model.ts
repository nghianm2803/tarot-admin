/**
 *   Interface/model file auto generate for Sms_log
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface ISms_log {
  sms_id?: string;
  sms_timestamp?: number;
  sms_content?: string;
  sms_sender?: string;
  sms_userId?: string;
  sms_amount?: number;
  sms_status?: number;
  updateAt?: string;
  createAt?: string;
}

/**
 *   Default value for ISms_log
 */
export const defaultISms_log: Readonly<ISms_log> = {
  sms_id: "",
  sms_timestamp: 0,
  sms_content: "",
  sms_sender: "",
  sms_userId: "",
  sms_amount: 0,
  sms_status: 0,
  updateAt: "",
  createAt: "",
};
