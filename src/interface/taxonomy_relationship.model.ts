/**
*   Interface/model file auto generate for Taxonomy_relationship
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface ITaxonomy_relationship {
	 taxonomy_relationship_id?: number; 
	 taxonomy_id?: number; 
	 object_id?: number; 
	 object_type?:string
	 object_order?: number; 
	 taxonomy?: string; 

}

/**
*   Default value for ITaxonomy_relationship
*/
export const defaultITaxonomy_relationship: Readonly<ITaxonomy_relationship> = {
    	taxonomy_relationship_id: 0, 
	taxonomy_id: 0, 
	object_id: 0, 
	object_type:"",
	object_order: 0, 
	taxonomy: "", 

};