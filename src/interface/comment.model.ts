/**
*   Interface/model file auto generate for Comment
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IComment {
	comment_id?: any;
	comment_title?: string;
	comment_content?: string;
	object_id?: number;
	comment_type?: string;
	comment_parent?: number;
	comment_status?: string;
	comment_point?: number;
	users?: string;
	createBy?: number;
	createAt?: any;
	updateAt?: any;
}

/**
*   Default value for IComment
*/
export const defaultIComment: Readonly<IComment> = {
	comment_id: 0,
	comment_content: "",
	object_id: 0,
	comment_type: "",
	comment_status: "",
	comment_point: 0,
};