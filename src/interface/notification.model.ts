// export interface DATA_NOTIFICATION {
// 	"type_action" : "deeplink" | "screen" | "none" | "link";
// 	"param" : string | null | undefined;
// }

/**
*   Interface/model file auto generate for Notification
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface INotification {
	notification_id?: number;
	notification_title: string;
	notification_content: string;
	notification_schedule?: any;
	notification_status?: number;
	notification_channel?: string; // Send to a group. Ex: Advisors only.
	notification_user?: bigint | string | number | undefined; // Not select any channel. Push user_id to send a notification to a specific user by id.
	// notification_data?: DATA_NOTIFICATION | null; // Storage data key:value. Ex: {deptrai: "xinh_gai"}
	notification_data?: string; // Storage data key:value. Ex: {deptrai: "xinh_gai"}
	createAt?: bigint;
	createBy?: bigint;
}

/**
*   Default value for INotification
*/
export const defaultINotification: Readonly<INotification> = {
	notification_id: 0,
	notification_title: "",
	notification_content: "",
	notification_channel: "/topics/all",
	notification_status: 0,
	notification_data: "",
};