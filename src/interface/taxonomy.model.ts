/**
 *   Interface/model file auto generate for Taxonomy
 *   Interface for back-end
 *   Model for front-end.
 *   They are the same!
 */
export interface ITaxonomy {
  taxonomy_id?: number;
  taxonomy_name?: string;
  taxonomy_description?: string;
  taxonomy_slug?: string;
  taxonomy_type?: string;
  order_tag?: string;
  tag?: string;
  taxonomy_parent?: number;
  taxonomy_count?: number;
  taxonomy_private?: number;
  createBy?: number;
  createAt?: any;
  updateAt?: any;
  taxonomy_meta?: string;
  taxonomy_relationship?: string;
  lang?: string;
}

/**
 *   Default value for ITaxonomy
 */
export const defaultITaxonomy: Readonly<ITaxonomy> = {
  taxonomy_id: 0,
  taxonomy_name: "",
  taxonomy_slug: "",
  taxonomy_type: "",
  lang: "",
  order_tag: "",
  tag: "",
  taxonomy_parent: 0,
  taxonomy_count: 0,
  taxonomy_private: 0,
  createBy: 0,
  createAt: new Date(),
  updateAt: new Date(),
  taxonomy_meta: "",
  taxonomy_relationship: "",
};
