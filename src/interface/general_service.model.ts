/**
*   Interface/model file auto generate for General_service
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IGeneral_service {
	service_id?: number;
	service_name?: string;
	service_description?: string;
	service_parent?: number;
	service_price?: number;
	service_duration?: number;
	service_type?: string;
	service_active?: boolean | number;
	service_unit?: "second" | "minute" | "hour" | "day" | string;
	service_meta?: string;
	service_available_time?:string;
	createAt?: any;
	createBy?: number;
	updateAt?: any;

}

/**
*   Default value for IGeneral_service
*/
export const defaultIGeneral_service: Readonly<IGeneral_service> = {
	service_id: 0,
	service_name: "",
	service_description: "",
	service_parent: 0,
	service_price: 0,
	service_meta: "",
	service_duration: 0,
	service_type: "",
	service_available_time:"",
	service_active: 0,
	createAt: new Date(),
	createBy: 0,

};