/**
*   Interface/model file auto generate for IHoroscope
*   Interface for back-end
*   Model for front-end.
*   They are the same!
*/
export interface IHoroscope {
	horoscope_id?: number; 
	horoscope_zodiac?: string; 
	horoscope_type?: string; 
	horoscope_date?: any; 
	horoscope_week?: number; 
	horoscope_month?: number; 
	horoscope_year?: number; 
	horoscope_value?: string; 
	slug?: string; 
	language?:string
	createAt?: any;
	createBy?: number;
	updateAt?: any;
	seo_title?:string;
	seo_description?:string;
	seo_keyword?:string;
	meta_index?:string;
	meta_image?:string;
	meta_json?:string;
}

/**
*   Default value for IHoroscope
*/
export const defaultIOutgoing_webhook: Readonly<IHoroscope> = {
	horoscope_id: 0, 
	horoscope_zodiac: "", 
	horoscope_type: "", 
	horoscope_date: "", 
	horoscope_week: 1, 
	horoscope_month: 1, 
	horoscope_year: 2000, 
	horoscope_value: "", 
	slug: "", 
	language: "", 
	createAt: new Date(), 
	createBy: 0, 
	updateAt:new Date(), 
	seo_title:"",
	seo_description:"",
	seo_keyword:"",
	meta_index:"",
	meta_image:"",
	meta_json:"",

};