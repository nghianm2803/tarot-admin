import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";

const firebaseApp = initializeApp(firebaseConfig);
const messaging = getMessaging(firebaseApp);

const firebaseConfig = {
  apiKey: "AIzaSyBij1Rhk-yZfPawCUxMtgseuCYHMPsmgtI",
  authDomain: "taki-tarot-mobile-app.firebaseapp.com",
  projectId: "taki-tarot-mobile-app",
  storageBucket: "taki-tarot-mobile-app.appspot.com",
  messagingSenderId: "521286800",
  appId: "1:521286800:web:8375a1a34daa396098d415",
  measurementId: "G-TNTZEP071N",
};

initializeApp(firebaseConfig);

export const getToken = (setTokenFound) => {
  return getToken(messaging, { vapidKey: "GENERATED_MESSAGING_KEY" })
    .then((currentToken) => {
      if (currentToken) {
        console.log("current token for client: ", currentToken);
        setTokenFound(true);
        // Track the token -> client mapping, by sending to backend server
        // show on the UI that permission is secured
      } else {
        console.log("No registration token available. Request permission to generate one.");
        setTokenFound(false);
        // shows on the UI that permission is required
      }
    })
    .catch((err) => {
      console.log("An error occurred while retrieving token. ", err);
      // catch error while creating client token
    });
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    onMessage(messaging, (payload) => {
      resolve(payload);
    });
  });
