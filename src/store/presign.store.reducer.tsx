import { axiosGetInstanceBackend } from "../helpers/axios-config";

class presignClass {
  createNewPresign = async (fileName: string, fileType: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/media/presign?file_name=avatar/${fileName}&file_type=${fileType}`;
    return await axiosGetInstanceBackend( dataGet, config);
  }
}

export const presignService =  new presignClass();
