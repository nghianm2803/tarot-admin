import axios from "axios";
import {
  createAsyncThunk,
  isFulfilled,
  isPending,
  isRejected,
} from "@reduxjs/toolkit";
import helpers from "../helpers";
// import { cleanEntity, buildEndUrl } from "../helpers";
import {
  createEntitySlice,
  serializeAxiosError,
} from "../config/reducer.utils";
import { ITaxonomy_relationship } from "../interface/taxonomy_relationship.model";

/**
 *   Reducer used for front-end, with taxonomy_relationship.model.ts
 *   Interface.ts can be use in both front-end and back-end! But prefer using taxonomy_relationship.model.ts
 */

const initialState = {
  loading: false,
  errorMessage: [] as any,
  entities: [],
  entity: null,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

const apiUrl = "taxonomy_relationship";

// Actions

export const getEntities = createAsyncThunk(
  "taxonomy_relationship_topic/fetch_entity_list",
  async (object: any) => {
    const requestUrl = `${apiUrl}/${object.object_type}/${
      object.object_id
    }?cacheBuster=${new Date().getTime()}`;
    return axios.get<any>(requestUrl);
  }
);

export const getEntity = createAsyncThunk(
  "taxonomy_relationship_topic/fetch_entity",
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/topic/${id}?cacheBuster=${new Date().getTime()}`;
    return axios.get<any>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const createEntity = createAsyncThunk(
  "taxonomy_relationship_topic/create_entity",
  async (entity: ITaxonomy_relationship, thunkAPI) => {
    const result = await axios.post<ITaxonomy_relationship>(
      `${apiUrl}/topic/?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    // thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const updateEntity = createAsyncThunk(
  "taxonomy_relationship_topic/update_entity",
  async (entity: ITaxonomy_relationship, thunkAPI) => {
    const result = await axios.patch<ITaxonomy_relationship>(
      `${apiUrl}/${
        entity.taxonomy_relationship_id
      }?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    // thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const partialUpdateEntity = createAsyncThunk(
  "taxonomy_relationship_topic/partial_update_entity",
  async (entity: ITaxonomy_relationship, thunkAPI) => {
    const result = await axios.patch<ITaxonomy_relationship>(
      `${apiUrl}/${
        entity.taxonomy_relationship_id
      }?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    // thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const deleteEntity = createAsyncThunk(
  "taxonomy_relationship_topic/delete_entity",
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/${id}`;
    return axios.delete<any>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

// slice

export const Reducer_Taxonomy_relationship_topic = createEntitySlice({
  name: "taxonomy_relationship",
  initialState,
  reducers: {
    clearError: (state) => {
      state.errorMessage = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getEntity.fulfilled, (state, action) => {
        state.loading = false;
        state.entity = action.payload.data;
      })
      .addCase(getEntity.rejected, (state, action) => {
        state.loading = false;
        state.entity = null;
      })
      .addCase(createEntity.rejected, (state, action) => {
        state.loading = false;
        state.entity = null;
        state.errorMessage = action.payload;
      })

      .addMatcher(isFulfilled(getEntities), (state, action) => {
        return {
          ...state,
          loading: false,
          entities: action.payload.data,
          totalItems: parseInt(action.payload.headers["x-total-count"], 10),
        };
      })
      .addMatcher(
        isRejected(
          createEntity,
          updateEntity,
          partialUpdateEntity,
          deleteEntity
        ),
        (state, action) => {
          state.loading = false;
          state.updating = false;
          state.updateSuccess = false;
          state.errorMessage = action.payload;
        }
      )
      .addMatcher(
        isFulfilled(createEntity, updateEntity, partialUpdateEntity),
        (state, action) => {
          state.updating = false;
          state.loading = false;
          state.updateSuccess = true;
          state.entity = action.payload.data;
        }
      )
      .addMatcher(isFulfilled(deleteEntity), (state, action) => {
        state.updating = false;
        state.loading = false;
        state.updateSuccess = true;
        state.entity = null;
      })
      .addMatcher(isPending(deleteEntity), (state) => {
        state.updating = true;
        state.loading = true;
        state.updateSuccess = false;
        state.entity = null;
      })
      .addMatcher(isPending(getEntities, getEntity), (state) => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      })
      .addMatcher(
        isPending(createEntity, updateEntity, partialUpdateEntity),
        (state) => {
          state.errorMessage = null;
          state.updateSuccess = false;
          state.updating = true;
        }
      );
  },
});

export const { clearError } = Reducer_Taxonomy_relationship_topic.actions;

// Reducer
export default Reducer_Taxonomy_relationship_topic.reducer;
