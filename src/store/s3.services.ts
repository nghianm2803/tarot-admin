import { axiosPutInstanceBackend } from "../helpers/axios-config";

class s3ServiceClass {
  createUserMeta = async (urlPut: string, contentType: string, dataPut: any) => {
    // const config = {
    //   headers: {
    //     "Content-Type": contentType
    //   },
    // };

    return await fetch(urlPut, {
      method: 'PUT',
      body: dataPut,
      headers: {
        "Content-Type": contentType
      },
    })
       .then((response) => {return response})
       .then((data) => {
          // console.log(data);
          return data;
          // Handle data
       })
       .catch((err) => {
          console.log(err.message);
          return null;
       });

    //return await axiosPutInstanceBackend(urlPut, dataPut, config);
  }
}

export const s3Service = new s3ServiceClass();
