import user from "./user.store.reducer";
import toast from "./toast.store.reducer";
import posts from "./posts.store.reducer";
import users from "./users.store.reducer";
import user_meta from "./user_meta.store.reducer";
import customer from "./customer.store.reduce";
import advisor from "./advisor.store.reduce";
import media from "./media.store.reducer";
import settings from "./settings.store.reducer";
import system from "./system.store.reducer";
import email from "./email.store.reducer";
import email_template from "./email_template.store.reducer";
import notification from "./notification.store.reducer";
import affiliate from "./affiliate.store.reducer";
import contactform from "./contactform.store.reducer";
import livestream from "./livestream.store.reducer";
import comment from "./comment.store.reducer";
import taxonomy from "./taxonomy.store.reducer";
import taxonomy_order from "./taxonomy.order.store.reducer";
import taxonomy_tag from "./taxonomy.tag.store.reducer";
import taxonomy_topic from "./taxonomy.topic.store.reducer";
import taxonomy_category from "./taxonomy.category.store.reducer";
import taxonomy_relationship from "./taxonomy_relationship.store.reducer";
import taxonomy_relationship_order from "./taxonomy_relationship.order.store.reducer";
import taxonomy_relationship_tag from "./taxonomy_relationship.tag.store.reducer";
import taxonomy_relationship_category from "./taxonomy_relationship.category.store.reducer";
import taxonomy_relationship_topic from "./taxonomy_relationship.topic.store.reducer";
import services from "./services.store.reducer";
import transactions from "./transactions.store.reducer";
import general_service from "./general_service.store.reducer";
import orders from "./orders.store.reducer";
import orders_meta from "./orders_meta.store.reducer";
import incoming_webhook from "./incoming_webhook.store.reducer";
import app_package from "./app_package.store.reducer";
import user_role from "./user_role.store.reducer";
import outgoing_webhook from "./outgoing_webhook.store.reducer";
import banner from "./banner.store.reducer";
import review from "./review.store.reducer";
import horoscope from "./horoscope.store.reduce";
import need_help from "./need_help.store.reducer";
import healing from "./healing.store.reducer";
import sms_log from "./sms_log.store.reducer";
import chat from "./chat.store.reducer";

const rootReducer = {
  email,
  email_template,
  user,
  users,
  user_meta,
  customer,
  advisor,
  media,
  settings,
  system,
  posts,
  toast,
  notification,
  affiliate,
  contactform,
  livestream,
  comment,
  taxonomy,
  taxonomy_order,
  taxonomy_tag,
  taxonomy_topic,
  taxonomy_category,
  taxonomy_relationship,
  taxonomy_relationship_order,
  taxonomy_relationship_tag,
  taxonomy_relationship_topic,
  taxonomy_relationship_category,
  services,
  transactions,
  general_service,
  orders,
  orders_meta,
  incoming_webhook,
  outgoing_webhook,
  user_role,
  app_package,
  banner,
  review,
  horoscope,
  need_help,
  healing,
  sms_log,
  chat,
};

export default rootReducer;

