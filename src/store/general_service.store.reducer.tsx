import axios from 'axios';
import { createAsyncThunk, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import helpers from 'helpers';
import { IQueryParams, createEntitySlice, EntityState, serializeAxiosError } from '../config/reducer.utils';
import { IGeneral_service, defaultIGeneral_service } from '../interface/general_service.model';
import { AppThunk } from '../config/store';


/**
*   Reducer used for front-end, with general_service.model.ts
*   Interface.ts can be use in both front-end and back-end! But prefer using general_service.model.ts
*/

const initialState = {
  loading: false,
  errorMessage: [] as any,
  entities: [],
  entity: null,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
  generate_Success: false,
};

const apiUrl = 'general_service';

// Actions

export const getEntities = createAsyncThunk('general_service/fetch_entity_list', async (object: any) => {
  console.log(object, "GET ENTITIES")
  const EndURL = helpers.buildEndUrl(object);
  const requestUrl = `${apiUrl}${EndURL}&cacheBuster=${new Date().getTime()}`;
  return axios.get<any>(requestUrl);
});

export const autoGenerateService = createAsyncThunk('general_service/autoGenerateService',
  async (user_id: number, thunkAPI) => {
    const requestUrl = `users/${user_id}/generate_service?cacheBuster=${new Date().getTime()}`;
    thunkAPI.dispatch(getEntities({}));
    return await axios.patch<any>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const getEntity = createAsyncThunk(
  'general_service/fetch_entity',
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/${id}?cacheBuster=${new Date().getTime()}`;
    return axios.get<any>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const createEntity = createAsyncThunk(
  'general_service/create_entity',
  async (entity: IGeneral_service, thunkAPI) => {
    const result = await axios.post<IGeneral_service>(`${apiUrl}?cacheBuster=${new Date().getTime()}`, helpers.cleanEntity(entity));
    thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const updateEntity = createAsyncThunk(
  'general_service/update_entity',
  async (entity: IGeneral_service, thunkAPI) => {
    const result = await axios.patch<IGeneral_service>(`${apiUrl}/${entity.service_id}?cacheBuster=${new Date().getTime()}`, helpers.cleanEntity(entity));
    // thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const partialUpdateEntity = createAsyncThunk(
  'general_service/partial_update_entity',
  async (entity: IGeneral_service, thunkAPI) => {
    const result = await axios.patch<IGeneral_service>(`${apiUrl}/${entity.service_id}?cacheBuster=${new Date().getTime()}`, helpers.cleanEntity(entity));
    // thunkAPI.dispatch(getEntities({}));
    return result;
  },
  { serializeError: serializeAxiosError }
);


export const deleteEntity = createAsyncThunk(
  'general_service/delete_entity',
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/${id}`;
    return axios.delete<any>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

// slice

export const Reducer_General_service = createEntitySlice({
  name: 'general_service',
  initialState,
  reducers: {
    clearError: (state) => {
      state.errorMessage = null;
    }
  },
  extraReducers(builder) {
    builder
      .addCase(getEntity.fulfilled, (state, action) => {
        state.loading = false;
        state.entity = action.payload.data;
      })
      .addCase(getEntity.rejected, (state, action) => {
        state.loading = false;
        state.entity = null;
      })
      .addCase(createEntity.rejected, (state, action) => {
        state.loading = false;
        state.entity = null;
        state.errorMessage = action.payload
      })
      .addCase(autoGenerateService.rejected, (state, action) => {
        state.loading = false;
        state.entities = null;
        state.errorMessage = action.payload;
        state.generate_Success = false;
        state.updateSuccess = false;
      })
      .addCase(autoGenerateService.fulfilled, (state, action) => {
        console.log("FULLFILLED")
        state.loading = false;
        state.generate_Success = true;
        state.updateSuccess = true;
      })
      .addMatcher(isFulfilled(getEntities), (state, action) => {
        return {
          ...state,
          loading: false,
          entities: action.payload.data,
          totalItems: parseInt(action.payload.headers['x-total-count'], 10),
        };
      })
      .addMatcher(isRejected(createEntity, updateEntity, partialUpdateEntity, deleteEntity, autoGenerateService), (state, action) => {
        state.loading = false;
        state.updating = false;
        state.updateSuccess = false;
        state.errorMessage = action.payload;
      })
      .addMatcher(isFulfilled(createEntity, updateEntity, partialUpdateEntity, autoGenerateService), (state, action) => {
        state.updating = false;
        state.loading = false;
        state.updateSuccess = true;
        state.entity = action.payload.data;
      })
      .addMatcher(isFulfilled(deleteEntity), (state, action) => {
        state.updating = false;
        state.loading = false;
        state.updateSuccess = true;
        state.entity = null;
      })
      .addMatcher(isPending(deleteEntity), state => {
        state.updating = true;
        state.loading = true;
        state.updateSuccess = false;
        state.entity = null;
      })
      .addMatcher(isPending(getEntities, getEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      })
      .addMatcher(isPending(createEntity, updateEntity, partialUpdateEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.updating = true;
      });
  },
});

export const { clearError } = Reducer_General_service.actions;

// Reducer
export default Reducer_General_service.reducer;
