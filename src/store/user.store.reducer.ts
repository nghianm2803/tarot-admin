import axios, { AxiosResponse } from "axios";
import { createAsyncThunk, isFulfilled, isPending, isRejected, createSlice } from "@reduxjs/toolkit";
import { IUser, ILogin, IRegister, defaultValue } from "../interface/user.model";
import { IQueryParams, createEntitySlice, EntityState, serializeAxiosError } from "../config/reducer.utils";

import { AppThunk } from "../config/store";
import helpers from "helpers";

export const initialState = {
  loading: false,
  isAuthenticated: false,
  isAuthenticating: true,
  account: {} as any,
  session: false as any,
  errorMessage: null as unknown as string, // Errors returned from server side
};

const apiUrl = "user";

/**
 * JamDev: CheckLogin and getCurrentUser
 */
export const getCurrentUser = createAsyncThunk("user/getcurrentuser", async () => {
  const requestUrl = `${apiUrl}?cacheBuster=${new Date().getTime()}`;
  return await axios.get<any>(requestUrl);
});

/**
 * JamDev: Logout
 */
export const logout = createAsyncThunk("logout", async () => {
  const requestUrl = `logout?cacheBuster=${new Date().getTime()}`;
  localStorage.removeItem("session");
  localStorage.removeItem("user");
  return axios.get<any>(requestUrl);
});

/**
 * JamDev: clearAuthentication
 */
export const clearAuthentication = (): AppThunk => (dispatch, getState) => {
  dispatch(logout());
};

/**
 * JamDev: Login
 */

export const login = createAsyncThunk(
  "login",
  async (entity: ILogin, thunkAPI) => {
    const result = await axios.post<ILogin>(`login?cacheBuster=${new Date().getTime()}`, helpers.cleanEntity(entity));
    thunkAPI.dispatch(getCurrentUser());
    return result;
  },
  { serializeError: serializeAxiosError }
);

/**
 * Create session before login.
 */
//  export const createSession = createAsyncThunk('user/create_session', async () => {
//     return await axios.post<any>(`${apiUrl}/create_session?cacheBuster=${new Date().getTime()}`);
//   },
//   { serializeError: serializeAxiosError }
// );

/**
 * JamDev: Update profile
 */

export const updateProfile = createAsyncThunk(
  "user/update_profile",
  async (entity: IUser, thunkAPI) => {
    const result = await axios.patch<IUser>(
      `${apiUrl}/${entity.user_id}?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    thunkAPI.dispatch(getCurrentUser());
    return result;
  },
  { serializeError: serializeAxiosError }
);

/**
 * JamDev: register
 */

export const register = createAsyncThunk(
  "register",
  async (entity: IRegister) => {
    const result = await axios.put<IRegister>(
      `register?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    return result;
  },
  { serializeError: serializeAxiosError }
);

/**
 * JamDev: RecoverPassword
 */
export const recoverPassword = createAsyncThunk(
  "recoverPassword",
  async (entity: any) => {
    const result = await axios.post<any>(
      `recoverpassword?cacheBuster=${new Date().getTime()}`,
      helpers.cleanEntity(entity)
    );
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const USER = createEntitySlice({
  name: "user",
  initialState: initialState as any,
  reducers: {
    clearError: (state) => {
      state.errorMessage = null;
    },
  },

  extraReducers(builder) {
    builder
      .addMatcher(isFulfilled(getCurrentUser, login), (state, action) => {
        /**
         * Login success
         */
        return {
          ...state,
          loading: false,
          isAuthenticated: true,
          isAuthenticating: false,
          account: action.payload.data,
          // errorMessage: null
        };
      })

      .addMatcher(isFulfilled(register), (state, action) => {
        /**
         * Register OKAY!
         */
        return {
          ...state,
          loading: false,
          isAuthenticated: false,
          account: action.payload.data,
          errorMessage: null,
        };
      })

      // .addMatcher( isFulfilled(createSession), (state, action) => {
      //   /**
      //    * Register OKAY!
      //    */
      //   return {
      //     ...state,
      //     loading: false,
      //     isAuthenticated: false,
      //     session: action.payload.data,
      //     errorMessage: null
      //   };
      // })

      .addMatcher(isFulfilled(updateProfile), (state, action) => {
        /**
         * Update profile okay
         */
        return {
          ...state,
          loading: false,
          errorMessage: "Update successfully!",
        };
      })
      .addMatcher(isRejected(login, register, getCurrentUser, recoverPassword), (state, action) => {
        /**
         * Login fail, register fail
         */
        // console.info( action.error.response.data.message );
        return {
          ...state,
          loading: false,
          isAuthenticated: false,
          isAuthenticating: false,
          account: {},
          errorMessage: action.error.message,
        };
      })
      .addMatcher(isFulfilled(logout), (state, action) => {
        /**
         * Log out ...
         */
        return {
          ...state,
          loading: false,
          isAuthenticated: false,
          account: {},
        };
      })
      .addMatcher(isFulfilled(recoverPassword), (state, action) => {
        /**
         * RecoverPassword
         */
        return {
          ...state,
          loading: false,
          isAuthenticated: false,
          account: {},
        };
      })
      .addMatcher(isPending(register, getCurrentUser, updateProfile, login), (state) => {
        /**
         * Loading ...
         */
        state.errorMessage = null;
        state.isAuthenticating = true;
        //state.account = {};
        //state.isAuthenticated = false;
        state.loading = true;
      });
  },
});
export const { clearError } = USER.actions;
// Reducer
export default USER.reducer;